package ru.m210projects.Duke3D.desktop;

import com.badlogic.gdx.backends.LwjglLauncherUtil;
import com.badlogic.gdx.backends.lwjgl3.audio.OpenALAudio;
import ru.m210projects.Build.Architecture.common.audio.AudioDriver;
import ru.m210projects.Build.Architecture.common.audio.BuildAudio;
import ru.m210projects.Build.settings.GameConfig;
import ru.m210projects.Duke3D.Config;
import ru.m210projects.Duke3D.Main;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Locale;

public class DesktopLauncher {

    public static final String appname = "DukeGDX";

    public static void main(final String[] arg) throws IOException {
        //Run configurations: "D:\Games\Duke3D"
        GameConfig cfg = new Config(Paths.get(arg[0], (appname + ".ini").toLowerCase(Locale.ROOT)));
        cfg.load();
        cfg.setGamePath(cfg.getCfgPath().getParent());
        cfg.registerAudioDriver(AudioDriver.DUMMY_AUDIO, new BuildAudio.DummyAudio(cfg));
        cfg.registerAudioDriver(AudioDriver.OPENAL_AUDIO, new OpenALAudio(cfg));
        cfg.addMidiDevices(LwjglLauncherUtil.getMidiDevices());
        LwjglLauncherUtil.launch(new Main(cfg, appname, "?.??", 0, false, false), null);
    }
}
