// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D;

import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Duke3D.Types.*;

import java.util.concurrent.atomic.AtomicInteger;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Sounds.NUM_SOUNDS;

public class Globals {

    public static final int BACKBUTTON = 9216;
    public static final int MOUSECURSOR = 9217;
    public static final int ALTHUDLEFT = 9218;
    public static final int ALTHUDRIGHT = 9219;

    public static final int HUDARMOR = 9220;
    public static final int HUDKEYS = 9221;
    public static final int BYTEVERSION15 = 116;
    public static final int JFBYTEVERSION = 127;
    public static final int GDXBYTEVERSION = 147;
    public static final int BYTEVERSION = GDXBYTEVERSION;
    public static final int nMaxMaps = 11;
    public static final int nMaxEpisodes = 5;
    public static final int nMaxSkills = 5;
    public static final int TICRATE = 120;
    public static final int TICSPERFRAME = (TICRATE / 26);

    public static final int TILE_ANIM = MAXTILES - 3;
    public static final int TILE_VIEWSCR = MAXTILES - 4;
    public static final int MAX_WEAPONS = 13;
    // Defines weapon, not to be used with the 'shoot' keyword.;
    public static final int KNEE_WEAPON = 0;
    public static final int PISTOL_WEAPON = 1;
    public static final int SHOTGUN_WEAPON = 2;
    public static final int CHAINGUN_WEAPON = 3;
    public static final int RPG_WEAPON = 4;
    public static final int HANDBOMB_WEAPON = 5;
    public static final int SHRINKER_WEAPON = 6;
    public static final int DEVISTATOR_WEAPON = 7;
    public static final int TRIPBOMB_WEAPON = 8;
    public static final int FREEZE_WEAPON = 9;
    public static final int HANDREMOTE_WEAPON = 10;
    public static final int GROW_WEAPON = 11;
    public static final int FLAMETHROWER_WEAPON = 12; //Twentieth Anniversary World Tour
    public static final int MAXANIMWALLS = 512;
    public static final int NUMOFFIRSTTIMEACTIVE = 256;
    public static final int MAXCYCLERS = 256;
    public static final int RECSYNCBUFSIZ = 2520; //2520 is the (LCM of 1-8)*3
    public static final int FOURSLEIGHT = 1 << 8;
    public static final int PHEIGHT = (38 << 8);
    public static final int MAXSLEEPDIST = 16384;
    public static final int SLEEPTIME = 24 * 64;
    public static final int AUTO_AIM_ANGLE = 48;

    public static final int MODE_EOL = 1;
    public static final int MODE_END = 2;

    // Hit definitions
    public static final int kHitTypeMask = HIT_TYPE_MASK;
    public static final int kHitIndexMask = HIT_INDEX_MASK;
    public static final int kHitSector = HIT_SECTOR;
    public static final int kHitWall = HIT_WALL;
    public static final int kHitSprite = HIT_SPRITE;
    public static final int kAngleMask = 0x7FF;
    public static final short[] weaponsandammosprites = {
            RPGSPRITE,
            CHAINGUNSPRITE,
            DEVISTATORAMMO,
            RPGAMMO,
            RPGAMMO,
            JETPACK,
            SHIELD,
            FIRSTAID,
            STEROIDS,
            RPGAMMO,
            RPGAMMO,
            RPGSPRITE,
            RPGAMMO,
            FREEZESPRITE,
            FREEZEAMMO
    };
    public static Entry boardfilename;
    public static boolean mFakeMultiplayer;
    public static int nFakePlayers;
    public static GameInfo defGame;
    public static GameInfo currentGame;
    public static final NetInfo pNetInfo = new NetInfo();
    public static int musicvolume, musiclevel;
    public static int VIEWSCR_Lock = 199;
    public static final AtomicInteger fz = new AtomicInteger();
    public static final AtomicInteger cz = new AtomicInteger();
    public static int gVisibility;
    public static int uGameFlags = 0;
    public static boolean MODE_TYPE;
    public static final PlayerOrig[] po = new PlayerOrig[MAXPLAYERS];
    public static final PlayerStruct[] ps = new PlayerStruct[MAXPLAYERS];
    public static final Weaponhit[] hittype = new Weaponhit[MAXSPRITES];
    public static final UserDefs ud = new UserDefs();
    public static short global_random;
    public static int neartagsector, neartagwall, neartagsprite;
    public static int numframes, neartaghitdist, lockclock;
    public static final short[] spriteq = new short[1024];
    public static short spriteqloc;
//    public static short moustat;
    public static final Animwalltype[] animwall = new Animwalltype[MAXANIMWALLS];
    public static short numanimwalls;
    public static final int[] msx = new int[2048];
    public static final int[] msy = new int[2048];
    public static final short[][] cyclers = new short[MAXCYCLERS][6];
    public static short numcyclers;
    public static final char[] buf = new char[80];
    public static short camsprite;
    public static final short[] mirrorwall = new short[64];
    public static final short[] mirrorsector = new short[64];
    public static short mirrorcnt;
//    public static int checksume;
    public static final int[] soundsiz = new int[NUM_SOUNDS];
//    public static short title_zoom;
    public static final Sample[] Sound = new Sample[NUM_SOUNDS];
    public static short numplayersprites, earthquaketime;
    public static boolean loadfromgrouponly;
    public static int fricxv, fricyv;
    public static final Input[] sync = new Input[MAXPLAYERS];
    //Multiplayer syncing variables
    public static short screenpeek;
    //Game recording variables
//    public static int groupfile;
    public static char display_mirror; //, typebuflen;
    public static final byte[] tempbuf = new byte[2048];
    //GLOBAL.C - replace the end "my's" with this
    public static final short[][] frags = new short[MAXPLAYERS][MAXPLAYERS];
    public static byte multipos, multiwhat, multiflag;

    public static char everyothertime;
    public static final char gamequit = 0;
    public static short numclouds;
    public static final short[] clouds = new short[128];
    public static final short[] cloudx = new short[128];
    public static final short[] cloudy = new short[128];
    public static int cloudtotalclock = 0;//, totalmemory = 0;
    public static int startofdynamicinterpolations = 0;
}
