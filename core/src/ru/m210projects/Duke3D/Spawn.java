package ru.m210projects.Duke3D;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Build.Pragmas.ksgn;
import static ru.m210projects.Duke3D.Actors.*;
import static ru.m210projects.Duke3D.Gamedef.getglobalz;
import static ru.m210projects.Duke3D.Gamedef.makeitfall;
import static ru.m210projects.Duke3D.Gameutils.FindDistance2D;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Premap.*;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.SoundDefs.SUBWAY;
import static ru.m210projects.Duke3D.Sounds.check_fta_sounds;

public class Spawn {

    public static int tempwallptr;

    public static void lotsofglass(final int i, int wallnum, int n) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        Wall wal = boardService.getWall(wallnum);
        if (wal == null) {
            for (int j = n - 1; j >= 0; j--) {
                int a = (sp.getAng() - 256 + (engine.krand() & 511) + 1024);
                int vz = 1024 - (engine.krand() & 1023);
                EGS(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ(), GLASSPIECES + (j % 3), -32, 36, 36, a,
                        32 + (engine.krand() & 63), vz, i, 5);
            }
            return;
        }

        int j = n + 1;

        int x1 = wal.getX();
        int y1 = wal.getY();
        int xv = wal.getWall2().getX() - x1;
        int yv = wal.getWall2().getY() - y1;

        x1 -= ksgn(yv);
        y1 += ksgn(xv);

        xv /= j;
        yv /= j;

        int sect = -1;
        for (j = n; j > 0; j--) {
            x1 += xv;
            y1 += yv;

            sect = engine.updatesector(x1, y1, sect);
            Sector sec = boardService.getSector(sect);
            if (sec != null) {
                int z = sec.getFloorz() - (engine.krand() & (klabs(sec.getCeilingz() - sec.getFloorz())));
                if (z < -(32 << 8) || z > (32 << 8)) {
                    z = sp.getZ() - (32 << 8) + (engine.krand() & ((64 << 8) - 1));
                }
                int a = (sp.getAng() - 1024);
                int vz = -(engine.krand() & 1023);
                EGS(sp.getSectnum(), x1, y1, z, GLASSPIECES + (j % 3), -32, 36, 36, a, 32 + (engine.krand() & 63), vz, i, 5);
            }
        }
    }

    public static void spriteglass(final int i, int n) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        for (int j = n; j > 0; j--) {
            int a = engine.krand() & 2047;
            int z = sp.getZ() - ((engine.krand() & 16) << 8);
            int vz = -512 - (engine.krand() & 2047);
            int ve = 32 + (engine.krand() & 63);
            int k = EGS(sp.getSectnum(), sp.getX(), sp.getY(), z, GLASSPIECES + (j % 3), engine.krand() & 15, 36, 36,
                    a, ve, vz, i, 5);
            Sprite s = boardService.getSprite(k);
            if (s != null) {
                s.setPal(sp.getPal());
            }
        }
    }

    public static void ceilingglass(final int i, int sectnum, int n) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        Sector sec = boardService.getSector(sectnum);
        if (sec == null) {
            return;
        }

        int startwall = sec.getWallptr();
        int endwall = (startwall + sec.getWallnum());

        for (int s = startwall; s < (endwall - 1); s++) {
            Wall w1 = boardService.getWall(s);
            Wall w2 = boardService.getWall(s + 1);
            if (w1 == null || w2 == null) {
                continue;
            }

            int x1 = w1.getX();
            int y1 = w1.getY();

            int xv = (w2.getX() - x1) / (n + 1);
            int yv = (w2.getY() - y1) / (n + 1);

            for (int j = n; j > 0; j--) {
                x1 += xv;
                y1 += yv;
                int a = (engine.krand() & 2047);
                int z = sec.getCeilingz() + ((engine.krand() & 15) << 8);
                EGS(sectnum, x1, y1, z, GLASSPIECES + (j % 3), -32, 36, 36, a, (engine.krand() & 31), 0, i, 5);
            }
        }
    }

    public static void lotsofcolourglass(final int i, int wallnum, int n) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        Wall wal = boardService.getWall(wallnum);
        if (wal == null) {
            for (int j = n - 1; j >= 0; j--) {
                int a = (engine.krand() & 2047);
                int vz = 1024 - (engine.krand() & 2047);
                int ve = 32 + (engine.krand() & 63);
                int k = EGS(sp.getSectnum(), sp.getX(), sp.getY(), sp.getZ() - (engine.krand() & (63 << 8)),
                        GLASSPIECES + (j % 3), -32, 36, 36, a, ve, vz, i, 5);

                Sprite s = boardService.getSprite(k);
                if (s != null) {
                    s.setPal((short) (engine.krand() & 15));
                }
            }
            return;
        }

        int j = n + 1;
        int x1 = wal.getX();
        int y1 = wal.getY();
        int xv = (wal.getWall2().getX() - wal.getX()) / j;
        int yv = (wal.getWall2().getY() - wal.getY()) / j;

        int sect = -1;
        for (j = n; j > 0; j--) {
            x1 += xv;
            y1 += yv;

            sect = engine.updatesector(x1, y1, sect);
            Sector sec = boardService.getSector(sect);
            if (sec == null) {
                continue;
            }

            int z = sec.getFloorz() - (engine.krand() & (klabs(sec.getCeilingz() - sec.getFloorz())));
            if (z < -(32 << 8) || z > (32 << 8)) {
                z = sp.getZ() - (32 << 8) + (engine.krand() & ((64 << 8) - 1));
            }
            int a = (sp.getAng() - 1024);
            int zv = -(engine.krand() & 2047);
            int k = EGS(sp.getSectnum(), x1, y1, z, GLASSPIECES + (j % 3), -32, 36, 36, a, 32 + (engine.krand() & 63), zv,
                    i, 5);

            Sprite s = boardService.getSprite(k);
            if (s != null) {
                s.setPal((short) (engine.krand() & 7));
            }
        }
    }

    public static int EGS(int whatsect, int s_x, int s_y, int s_z, int s_pn, int s_s, int s_xr, int s_yr, int s_a,
                          int s_ve, int s_zv, int s_ow, int s_ss) {
        if (boardService.getSector(whatsect) == null) {
            throw new WarningException("Wrong sector! " + whatsect);
        }

        int i = engine.insertsprite(whatsect, s_ss);
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            throw new WarningException("Too many sprites spawned.");
        }

        s.setX(s_x);
        s.setY(s_y);
        s.setZ(s_z);
        s.setCstat(0);
        s.setPicnum(s_pn);

        s.setShade(s_s);
        s.setXrepeat(s_xr);
        s.setYrepeat(s_yr);
        s.setPal(0);

        s.setAng(s_a);
        s.setXvel(s_ve);
        s.setZvel(s_zv);
        s.setOwner(s_ow);
        s.setXoffset(0);
        s.setYoffset(0);
        s.setYvel(0);
        s.setClipdist(0);
        s.setPal(0);
        s.setLotag(0);

        Sprite spo = boardService.getSprite(s_ow);
        if (spo != null) {
            hittype[i].picnum = spo.getPicnum();
        }

        hittype[i].lastvx = 0;
        hittype[i].lastvy = 0;

        hittype[i].timetosleep = 0;
        hittype[i].actorstayput = -1;
        hittype[i].extra = -1;
        hittype[i].owner = s_ow;
        hittype[i].cgg = 0;
        hittype[i].movflag = 0;
        hittype[i].tempang = 0;
        hittype[i].dispicnum = 0;
        if (s_ow != -1) {
            hittype[i].floorz = hittype[s_ow].floorz;
            hittype[i].ceilingz = hittype[s_ow].ceilingz;
        }

        hittype[i].temp_data[0] = hittype[i].temp_data[2] = hittype[i].temp_data[3] = hittype[i].temp_data[5] = 0;
        if (s_pn < MAXTILES && currentGame.getCON().actorscrptr[s_pn] != 0) {
            s.setExtra((short) currentGame.getCON().script[currentGame.getCON().actorscrptr[s_pn]]);
            hittype[i].temp_data[4] = currentGame.getCON().script[currentGame.getCON().actorscrptr[s_pn] + 1];
            hittype[i].temp_data[1] = currentGame.getCON().script[currentGame.getCON().actorscrptr[s_pn] + 2];
            s.setHitag((short) currentGame.getCON().script[currentGame.getCON().actorscrptr[s_pn] + 3]);
        } else {
            hittype[i].temp_data[1] = hittype[i].temp_data[4] = 0;
            s.setExtra(0);
            s.setHitag(0);
        }

        if (show2dsector.getBit(s.getSectnum())) {
            show2dsprite.setBit(i);
        } else {
            show2dsprite.clearBit(i);
        }

        game.pInt.clearspriteinterpolate(i);
        game.pInt.setsprinterpolate(i, s);

        return (i);
    }

    public static int spawn(final int j, int pn) {
        int clostest = 0;
        int x, y, d;
        Sprite spawner = boardService.getSprite(j);

        final int i;
        if (spawner != null) {
            i = EGS(spawner.getSectnum(), spawner.getX(), spawner.getY(), spawner.getZ(), pn, 0, 0, 0, 0, 0, 0, j, 0);
            hittype[i].picnum = spawner.getPicnum();
        }
        else
        {
            i = pn;
            Sprite s = boardService.getSprite(i);
            if (s == null) {
                return -1;
            }

            hittype[i].picnum = s.getPicnum();
            hittype[i].timetosleep = 0;
            hittype[i].extra = -1;

            hittype[i].owner = i;
            s.setOwner(i);
            Sector sec = boardService.getSector(s.getSectnum());

            hittype[i].cgg = 0;
            hittype[i].movflag = 0;
            hittype[i].tempang = 0;
            hittype[i].dispicnum = 0;
            if (sec != null) {
                hittype[i].floorz = sec.getFloorz();
                hittype[i].ceilingz = sec.getCeilingz();
            }

            hittype[i].lastvx = 0;
            hittype[i].lastvy = 0;
            hittype[i].actorstayput = -1;

            hittype[i].temp_data[0] = hittype[i].temp_data[1] = hittype[i].temp_data[2] = hittype[i].temp_data[3] = hittype[i].temp_data[4] = hittype[i].temp_data[5] = 0;

            if (s.getPicnum() != SPEAKER && s.getPicnum() != LETTER && s.getPicnum() != DUCK
                    && s.getPicnum() != TARGET && s.getPicnum() != TRIPBOMB && s.getPicnum() != VIEWSCREEN
                    && s.getPicnum() != VIEWSCREEN2 && (s.getCstat() & 48) != 0) {
                if (!(s.getPicnum() >= CRACK1 && s.getPicnum() <= CRACK4)) {
                    game.pInt.setsprinterpolate(i, s);
                    if (s.getShade() == 127) {
                        return i;
                    }
                    if (wallswitchcheck(i) && (s.getCstat() & 16) != 0) {
                        if (s.getPicnum() != ACCESSSWITCH && s.getPicnum() != ACCESSSWITCH2
                                && s.getPal() != 0) {
                            if (ud.multimode < 2 || ud.coop == 1) {
                                s.setXrepeat(0);
                                s.setYrepeat(0);
                                s.setCstat(0);
                                s.setLotag(0);
                                s.setHitag(0);
                                return i;
                            }
                        }
                        s.setCstat(s.getCstat() | 257);
                        if (s.getPal() != 0 && s.getPicnum() != ACCESSSWITCH && s.getPicnum() != ACCESSSWITCH2) {
                            s.setPal(0);
                        }
                        return i;
                    }

                    if (s.getHitag() != 0) {
                        engine.changespritestat(i, (short) 12);
                        s.setCstat(s.getCstat() | 257);
                        s.setExtra((short) currentGame.getCON().impact_damage);
                        return i;
                    }
                }
            }

            final int actorscrptr = currentGame.getCON().actorscrptr[s.getPicnum()];

            if ((s.getCstat() & 1) != 0) {
                s.setCstat(s.getCstat() | 256);
            }

            if (actorscrptr != 0) {
                s.setExtra((short) currentGame.getCON().script[actorscrptr]);
                hittype[i].temp_data[4] = currentGame.getCON().script[actorscrptr + 1];
                hittype[i].temp_data[1] = currentGame.getCON().script[actorscrptr + 2];
                if (currentGame.getCON().script[actorscrptr + 3] != 0 && s.getHitag() == 0) {
                    s.setHitag((short) currentGame.getCON().script[actorscrptr + 3]);
                }
            } else {
                hittype[i].temp_data[1] = hittype[i].temp_data[4] = 0;
            }
        }

        game.pInt.clearspriteinterpolate(i);

        final Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return -1;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return -1;
        }

        final int sect = sp.getSectnum();

        if (currentGame.getCON().type == 20) { // Twentieth Anniversary World Tour
            switch (sp.getPicnum()) {
                case BOSS5STAYPUT:
                    hittype[i].actorstayput = sp.getSectnum();
                case FIREFLY:
                case BOSS5:
                    if (sp.getPicnum() == BOSS5 || sp.getPicnum() == BOSS5STAYPUT) {
                        if (spawner != null && spawner.getPicnum() == RESPAWN) {
                            sp.setPal(spawner.getPal());
                        }

                        if (sp.getPal() != 0) {
                            sp.setClipdist(80);
                            sp.setXrepeat(40);
                            sp.setYrepeat(40);
                        } else {
                            sp.setXrepeat(80);
                            sp.setYrepeat(80);
                            sp.setClipdist(164);
                        }
                    } else {
                        sp.setXrepeat(40);
                        sp.setYrepeat(40);
                        sp.setClipdist(80);
                    }

                    if (spawner != null) {
                        sp.setLotag(0);
                    }

                    if ((sp.getLotag() > ud.player_skill) || ud.monsters_off) {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        return i;
                    } else {
                        makeitfall(currentGame.getCON(), i);

                        sp.setCstat(sp.getCstat() | 257);
                        ps[connecthead].max_actors_killed++;

                        if (spawner != null) {
                            hittype[i].timetosleep = 0;
                            check_fta_sounds(i);
                            engine.changespritestat(i, (short) 1);
                        } else {
                            engine.changespritestat(i, (short) 2);
                        }
                    }
                    game.pInt.setsprinterpolate(i, sp);
                    return i;
                case FIREFLYFLYINGEFFECT:
                    sp.setOwner(j);
                    engine.changespritestat(i, (short) 5);
                    sp.setXrepeat(16);
                    sp.setYrepeat(16);
                    game.pInt.setsprinterpolate(i, sp);
                    return i;
                case LAVAPOOLBUBBLE:
                    game.pInt.setsprinterpolate(i, sp);
                    if (spawner != null && spawner.getXrepeat() < 30) {
                        return i;
                    }
                    sp.setOwner(j);
                    engine.changespritestat(i, (short) 5);
                    sp.setX(sp.getX() + engine.krand() % 512 - 256);
                    sp.setY(sp.getY() + engine.krand() % 512 - 256);
                    sp.setXrepeat(16);
                    sp.setYrepeat(16);
                    return i;
                case WHISPYSMOKE:
                    engine.changespritestat(i, (short) 5);
                    sp.setX(sp.getX() + engine.krand() % 256 - 128);
                    sp.setY(sp.getY() + engine.krand() % 256 - 128);
                    sp.setXrepeat(20);
                    sp.setYrepeat(20);
                    game.pInt.setsprinterpolate(i, sp);
                    return i;
                case 5846: // SERIOUSSAM
                    engine.changespritestat(i, (short) 2);
                    sp.setCstat(257);
                    sp.setExtra(150);
                    game.pInt.setsprinterpolate(i, sp);
                    return i;
            }
        }

        switch (sp.getPicnum()) {
            default:
                if (currentGame.getCON().actorscrptr[sp.getPicnum()] != 0) {
                    if (j == -1 && sp.getLotag() > ud.player_skill) {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        break;
                    }

                    // Init the size
                    if (sp.getXrepeat() == 0 || sp.getYrepeat() == 0) {
                        sp.setXrepeat(1);
                        sp.setYrepeat(1);
                    }

                    if ((currentGame.getCON().actortype[sp.getPicnum()] & 3) != 0) {
                        if (ud.monsters_off) {
                            sp.setXrepeat(0);
                            sp.setYrepeat(0);
                            engine.changespritestat(i, (short) 5);
                            break;
                        }

                        makeitfall(currentGame.getCON(), i);

                        if ((currentGame.getCON().actortype[sp.getPicnum()] & 2) != 0) {
                            hittype[i].actorstayput = sp.getSectnum();
                        }

                        ps[connecthead].max_actors_killed++;
                        sp.setClipdist(80);
                        if (spawner != null) {
                            if (spawner.getPicnum() == RESPAWN) {
                                sp.setPal(spawner.getPal());
                                hittype[i].tempang = spawner.getPal();
                            }
                            engine.changespritestat(i, (short) 1);
                        } else {
                            engine.changespritestat(i, (short) 2);
                        }
                    } else {
                        sp.setClipdist(40);
                        sp.setOwner(i);
                        engine.changespritestat(i, (short) 1);
                    }

                    hittype[i].timetosleep = 0;

                    if (spawner != null) {
                        sp.setAng(spawner.getAng());
                    }
                }
                break;
            case FOF:
                sp.setXrepeat(0);
                sp.setYrepeat(0);
                engine.changespritestat(i, (short) 5);
                break;
            case WATERSPLASH2:
                if (spawner != null) {
                    engine.setsprite(i, spawner.getX(), spawner.getY(), spawner.getZ());
                    int size = (8 + (engine.krand() & 7));
                    sp.setXrepeat(size);
                    sp.setYrepeat(size);
                } else {
                    int size = 16 + (engine.krand() & 15);
                    sp.setXrepeat(size);
                    sp.setYrepeat(size);
                }

                sp.setShade(-16);
                sp.setCstat(sp.getCstat() | 128);

                if (spawner != null) {
                    Sector ssec = boardService.getSector(spawner.getSectnum());
                    if (ssec != null) {
                        if (ssec.getLotag() == 2) {
                            sp.setZ(engine.getceilzofslope(sp.getSectnum(), sp.getX(), sp.getY()) + (16 << 8));
                            sp.setCstat(sp.getCstat() | 8);
                        } else if (ssec.getLotag() == 1) {
                            sp.setZ(engine.getflorzofslope(sp.getSectnum(), sp.getX(), sp.getY()));
                        }
                    }
                }

                if (sec.getFloorpicnum() == FLOORSLIME || sec.getCeilingpicnum() == FLOORSLIME) {
                    sp.setPal(7);
                }
            case NEON1:
            case NEON2:
            case NEON3:
            case NEON4:
            case NEON5:
            case NEON6:
            case DOMELITE:
                if (sp.getPicnum() != WATERSPLASH2) {
                    sp.setCstat(sp.getCstat() | 257);
                }
            case NUKEBUTTON:
                if (sp.getPicnum() == DOMELITE) {
                    sp.setCstat(sp.getCstat() | 257);
                }
            case JIBS1:
            case JIBS2:
            case JIBS3:
            case JIBS4:
            case JIBS5:
            case JIBS6:
            case HEADJIB1:
            case ARMJIB1:
            case LEGJIB1:
            case LIZMANHEAD1:
            case LIZMANARM1:
            case LIZMANLEG1:
            case DUKETORSO:
            case DUKEGUN:
            case DUKELEG:
                engine.changespritestat(i, (short) 5);
                break;
            case TONGUE:
                if (spawner != null) {
                    sp.setAng(spawner.getAng());
                }
                sp.setZ(sp.getZ() - (38 << 8));
                sp.setZvel((short) (256 - (engine.krand() & 511)));
                sp.setXvel((short) (64 - (engine.krand() & 127)));
                engine.changespritestat(i, (short) 4);
                break;
            case NATURALLIGHTNING:
                sp.setCstat(sp.getCstat() & ~257);
                sp.setCstat(sp.getCstat() | 32768);
                break;
            case TRANSPORTERSTAR:
            case TRANSPORTERBEAM:
                if (spawner == null) {
                    break;
                }

                if (sp.getPicnum() == TRANSPORTERBEAM) {
                    sp.setXrepeat(31);
                    sp.setYrepeat(1);
                    Sector ssec = boardService.getSector(spawner.getSectnum());
                    if (ssec != null) {
                        sp.setZ(ssec.getFloorz() - (40 << 8));
                    }
                } else {
                    if (spawner.getStatnum() == 4) {
                        sp.setXrepeat(8);
                        sp.setYrepeat(8);
                    } else {
                        sp.setXrepeat(48);
                        sp.setYrepeat(64);
                        if (spawner.getStatnum() == 10 || badguy(spawner)) {
                            sp.setZ(sp.getZ() - (32 << 8));
                        }
                    }
                }

                sp.setShade(-127);
                sp.setCstat(128 | 2);
                sp.setAng(spawner.getAng());

                sp.setXvel(128);
                engine.changespritestat(i, (short) 5);
                ssp(i, CLIPMASK0);
                engine.setsprite(i, sp.getX(), sp.getY(), sp.getZ());
                break;

            case FRAMEEFFECT1:
                if (spawner != null) {
                    sp.setXrepeat(spawner.getXrepeat());
                    sp.setYrepeat(spawner.getYrepeat());
                    hittype[i].temp_data[1] = spawner.getPicnum();
                } else {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                }

                engine.changespritestat(i, (short) 5);

                break;

            case LASERLINE:
                sp.setYrepeat(6);
                sp.setXrepeat(32);

                if (currentGame.getCON().lasermode == 1) {
                    sp.setCstat(16 + 2);
                } else if (currentGame.getCON().lasermode == 0 || currentGame.getCON().lasermode == 2) {
                    sp.setCstat(16);
                } else {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                }

                if (spawner != null) {
                    sp.setAng((short) (hittype[j].temp_data[5] + 512));
                }
                engine.changespritestat(i, (short) 5);
                break;

            case FORCESPHERE:
                if (j == -1) {
                    sp.setCstat((short) 32768);
                    engine.changespritestat(i, (short) 2);
                } else {
                    sp.setXrepeat(1);
                    sp.setYrepeat(1);
                    engine.changespritestat(i, (short) 5);
                }
                break;

            case BLOOD:
                sp.setXrepeat(16);
                sp.setYrepeat(16);
                sp.setZ(sp.getZ() - (26 << 8));
                if (spawner != null && spawner.getPal() == 6) {
                    sp.setPal(6);
                }
                engine.changespritestat(i, (short) 5);
                break;
            case BLOODPOOL:
            case PUKE:
            case LAVAPOOL: {
                if (sp.getPicnum() == LAVAPOOL && currentGame.getCON().type != 20) // Twentieth Anniversary World Tour
                {
                    return i;
                }

                int s1 = engine.updatesector(sp.getX() + 108, sp.getY() + 108, sp.getSectnum());
                Sector sec1 = boardService.getSector(s1);
                if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                    s1 = engine.updatesector(sp.getX() - 108, sp.getY() - 108, s1);
                    sec1 = boardService.getSector(s1);
                    if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                        s1 = engine.updatesector(sp.getX() + 108, sp.getY() - 108, s1);
                        sec1 = boardService.getSector(s1);
                        if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                            s1 = engine.updatesector(sp.getX() - 108, sp.getY() + 108, s1);
                            sec1 = boardService.getSector(s1);
                            if (sec1 != null && sec1.getFloorz() != sec.getFloorz()) {
                                sp.setXrepeat(0);
                                sp.setYrepeat(0);
                                engine.changespritestat(i, (short) 5);
                                break;
                            }
                        } else {
                            sp.setXrepeat(0);
                            engine.changespritestat(i, (short) 5);
                            break;
                        }
                    } else {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        break;
                    }
                } else {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }


                if (sec.getLotag() == 1) {
                    engine.changespritestat(i, (short) 5);
                    break;
                }

                if (spawner != null && sp.getPicnum() != PUKE) {
                    if (spawner.getPal() == 1) {
                        sp.setPal(1);
                    } else if (spawner.getPal() != 6 && spawner.getPicnum() != NUKEBARREL && spawner.getPicnum() != TIRE) {
                        if (spawner.getPicnum() == FECES) {
                            sp.setPal(7); // Brown
                        } else {
                            sp.setPal(2); // Red
                        }
                    } else {
                        sp.setPal(0); // green
                    }

                    if (spawner.getPicnum() == TIRE) {
                        sp.setShade(127);
                    }
                }
                sp.setCstat(sp.getCstat() | 32);
                if (sp.getPicnum() == LAVAPOOL) { // Twentieth Anniversary World Tour
                    int fz = engine.getflorzofslope(sp.getSectnum(), sp.getX(), sp.getY());
                    if (fz != sp.getZ()) {
                        sp.setZ(fz);
                    }
                    sp.setZ(sp.getZ() - 200);
                }
            }
            case FECES:
                if (spawner != null) {
                    sp.setXrepeat(1);
                    sp.setYrepeat(1);
                }
                engine.changespritestat(i, (short) 5);
                break;

            case BLOODSPLAT1:
            case BLOODSPLAT2:
            case BLOODSPLAT3:
            case BLOODSPLAT4:
                sp.setCstat(sp.getCstat() | 16);
                sp.setXrepeat((short) (7 + (engine.krand() & 7)));
                sp.setYrepeat((short) (7 + (engine.krand() & 7)));
                sp.setZ(sp.getZ() - (16 << 8));
                if (spawner != null && spawner.getPal() == 6) {
                    sp.setPal(6);
                }
                insertspriteq(i);
                engine.changespritestat(i, (short) 5);
                break;

            case TRIPBOMB:
                if (sp.getLotag() > ud.player_skill) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }

                sp.setXrepeat(4);
                sp.setYrepeat(5);

                sp.setOwner(i);
                sp.setHitag(i);

                sp.setXvel(16);
                ssp(i, CLIPMASK0);
                hittype[i].temp_data[0] = 17;
                hittype[i].temp_data[2] = 0;
                hittype[i].temp_data[5] = sp.getAng();

            case SPACEMARINE:
                if (sp.getPicnum() == SPACEMARINE) {
                    sp.setExtra(20);
                    sp.setCstat(sp.getCstat() | 257);
                }
                engine.changespritestat(i, (short) 2);
                break;

            case HYDRENT:
            case PANNEL1:
            case PANNEL2:
            case SATELITE:
            case FUELPOD:
            case SOLARPANNEL:
            case ANTENNA:
            case GRATE1:
            case CHAIR1:
            case CHAIR2:
            case CHAIR3:
            case BOTTLE1:
            case BOTTLE2:
            case BOTTLE3:
            case BOTTLE4:
            case BOTTLE5:
            case BOTTLE6:
            case BOTTLE7:
            case BOTTLE8:
            case BOTTLE10:
            case BOTTLE11:
            case BOTTLE12:
            case BOTTLE13:
            case BOTTLE14:
            case BOTTLE15:
            case BOTTLE16:
            case BOTTLE17:
            case BOTTLE18:
            case BOTTLE19:
            case OCEANSPRITE1:
            case OCEANSPRITE2:
            case OCEANSPRITE3:
            case OCEANSPRITE5:
            case MONK:
            case INDY:
            case LUKE:
            case JURYGUY:
            case SCALE:
            case VACUUM:
            case FANSPRITE:
            case CACTUS:
            case CACTUSBROKE:
            case HANGLIGHT:
            case FETUS:
            case FETUSBROKE:
            case CAMERALIGHT:
            case MOVIECAMERA:
            case IVUNIT:
            case POT1:
            case POT2:
            case POT3:
            case TRIPODCAMERA:
            case SUSHIPLATE1:
            case SUSHIPLATE2:
            case SUSHIPLATE3:
            case SUSHIPLATE4:
            case SUSHIPLATE5:
            case WAITTOBESEATED:
            case VASE:
            case PIPE1:
            case PIPE2:
            case PIPE3:
            case PIPE4:
            case PIPE5:
            case PIPE6:
                sp.setClipdist(32);
                sp.setCstat(sp.getCstat() | 257);
            case OCEANSPRITE4:
                engine.changespritestat(i, (short) 0);
                break;
            case FEMMAG1:
            case FEMMAG2:
                sp.setCstat(sp.getCstat() & ~257);
                engine.changespritestat(i, (short) 0);
                break;
            case DUKETAG:
            case SIGN1:
            case SIGN2:
                if (ud.multimode < 2 && sp.getPal() != 0) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                } else {
                    sp.setPal(0);
                }
                break;
            case MASKWALL1:
            case MASKWALL2:
            case MASKWALL3:
            case MASKWALL4:
            case MASKWALL5:
            case MASKWALL6:
            case MASKWALL7:
            case MASKWALL8:
            case MASKWALL9:
            case MASKWALL10:
            case MASKWALL11:
            case MASKWALL12:
            case MASKWALL13:
            case MASKWALL14:
            case MASKWALL15: {
                int cs = sp.getCstat() & 60;
                sp.setCstat((short) (cs | 1));
                engine.changespritestat(i, (short) 0);
                break;
            }
            case FOOTPRINTS:
            case FOOTPRINTS2:
            case FOOTPRINTS3:
            case FOOTPRINTS4: {
                if (spawner != null) {
                    int s1 = engine.updatesector(sp.getX() + 84, sp.getY() + 84, sp.getSectnum());
                    Sector sec1 = boardService.getSector(s1);
                    if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                        s1 = engine.updatesector(sp.getX() - 84, sp.getY() - 84, s1);
                        sec1 = boardService.getSector(s1);
                        if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                            s1 = engine.updatesector(sp.getX() + 84, sp.getY() - 84, s1);
                            sec1 = boardService.getSector(s1);
                            if (sec1 != null && sec1.getFloorz() == sec.getFloorz()) {
                                s1 = engine.updatesector(sp.getX() - 84, sp.getY() + 84, s1);
                                sec1 = boardService.getSector(s1);
                                if (sec1 != null && sec1.getFloorz() != sec.getFloorz()) {
                                    sp.setXrepeat(0);
                                    sp.setYrepeat(0);
                                    engine.changespritestat(i, (short) 5);
                                    break;
                                }
                            } else {
                                sp.setXrepeat(0);
                                sp.setYrepeat(0);
                                break;
                            }
                        } else {
                            sp.setXrepeat(0);
                            sp.setYrepeat(0);
                            break;
                        }
                    } else {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        break;
                    }

                    sp.setCstat((short) (32 + ((ps[spawner.getYvel()].footprintcount & 1) << 2)));
                    sp.setAng(spawner.getAng());
                }

                sp.setZ(sec.getFloorz());
                if (sec.getLotag() != 1 && sec.getLotag() != 2) {
                    sp.setXrepeat(32);
                    sp.setYrepeat(32);
                }

                insertspriteq(i);
                engine.changespritestat(i, (short) 5);
                break;
            }
            case FEM1:
            case FEM2:
            case FEM3:
            case FEM4:
            case FEM5:
            case FEM6:
            case FEM7:
            case FEM8:
            case FEM9:
            case FEM10:
            case PODFEM1:
            case NAKED1:
            case STATUE:
            case TOUGHGAL:
                sp.setYvel(sp.getHitag());
                sp.setHitag(-1);
                if (sp.getPicnum() == PODFEM1) {
                    sp.setExtra(sp.getExtra() << 1);
                }
            case BLOODYPOLE:

            case QUEBALL:
            case STRIPEBALL:

                if (sp.getPicnum() == QUEBALL || sp.getPicnum() == STRIPEBALL) {
                    sp.setCstat(256);
                    sp.setClipdist(8);
                } else {
                    sp.setCstat(sp.getCstat() | 257);
                    sp.setClipdist(32);
                }

                engine.changespritestat(i, (short) 2);
                break;

            case DUKELYINGDEAD:
                if (spawner != null && spawner.getPicnum() == APLAYER) {
                    sp.setXrepeat(spawner.getXrepeat());
                    sp.setYrepeat(spawner.getYrepeat());
                    sp.setShade(spawner.getShade());
                    sp.setPal(ps[spawner.getYvel()].palookup);
                }
            case DUKECAR:
            case HELECOPT:
//	                if(sp.picnum == HELECOPT || sp.picnum == DUKECAR) sp.xvel = 1024;
                sp.setCstat(0);
                sp.setExtra(1);
                sp.setXvel(292);
                sp.setZvel(360);
            case RESPAWNMARKERRED:
            case BLIMP:

                if (sp.getPicnum() == RESPAWNMARKERRED) {
                    sp.setXrepeat(24);
                    sp.setYrepeat(24);
                    if (spawner != null) {
                        sp.setZ(hittype[j].floorz); // -(1<<4);
                    }
                } else {
                    sp.setCstat(sp.getCstat() | 257);
                    sp.setClipdist(128);
                }
            case MIKE:
                if (sp.getPicnum() == MIKE) {
                    sp.setYvel(sp.getHitag());
                }
            case WEATHERWARN:
                engine.changespritestat(i, (short) 1);
                break;

            case SPOTLITE:
                hittype[i].temp_data[0] = sp.getX();
                hittype[i].temp_data[1] = sp.getY();
                break;
            case BULLETHOLE:
                sp.setXrepeat(3);
                sp.setYrepeat(3);
                sp.setCstat((short) (16 + (engine.krand() & 12)));
                insertspriteq(i);
            case MONEY:
            case MAIL:
            case PAPER:
                if (sp.getPicnum() == MONEY || sp.getPicnum() == MAIL || sp.getPicnum() == PAPER) {
                    hittype[i].temp_data[0] = engine.krand() & 2047;
                    sp.setCstat((short) (engine.krand() & 12));
                    sp.setXrepeat(8);
                    sp.setYrepeat(8);
                    sp.setAng((short) (engine.krand() & 2047));
                }
                engine.changespritestat(i, (short) 5);
                break;

            case VIEWSCREEN:
            case VIEWSCREEN2:
                sp.setOwner(i);
                sp.setLotag(1);
                sp.setExtra(1);
                engine.changespritestat(i, (short) 6);
                break;

            case SHELL: // From the player
            case SHOTGUNSHELL:
                if (spawner != null) {
                    short snum, a;

                    if (spawner.getPicnum() == APLAYER) {
                        snum = spawner.getYvel();
                        a = (short) (ps[snum].ang - (engine.krand() & 63) + 8); // Fine tune

                        hittype[i].temp_data[0] = engine.krand() & 1;
                        if (sp.getPicnum() == SHOTGUNSHELL) {
                            sp.setZ((6 << 8) + ps[snum].pyoff + ps[snum].posz
                                    - ((ps[snum].horizoff + (int) ps[snum].horiz - 100) << 4));
                        } else {
                            sp.setZ((3 << 8) + ps[snum].pyoff + ps[snum].posz
                                    - ((ps[snum].horizoff + (int) ps[snum].horiz - 100) << 4));
                        }
                        sp.setZvel((short) -(engine.krand() & 255));
                    } else {
                        a = sp.getAng();
                        sp.setZ(spawner.getZ() - PHEIGHT + (3 << 8));
                    }

                    sp.setX(spawner.getX() + (EngineUtils.sin((a + 512) & 2047) >> 7));
                    sp.setY(spawner.getY() + (EngineUtils.sin(a & 2047) >> 7));

                    sp.setShade(-8);

                    sp.setAng((short) (a - 512));
                    sp.setXvel(20);

                    sp.setXrepeat(4);
                    sp.setYrepeat(4);

                    engine.changespritestat(i, (short) 5);
                }
                break;

            case RESPAWN:
                sp.setExtra(66 - 13);
            case MUSICANDSFX:
                if (ud.multimode < 2 && sp.getPal() == 1) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }
                sp.setCstat((short) 32768);
                engine.changespritestat(i, (short) 11);
                break;

            case EXPLOSION2:
            case EXPLOSION2BOT:
            case BURNING:
            case BURNING2:
            case SMALLSMOKE:
            case SHRINKEREXPLOSION:
            case COOLEXPLOSION1:
            case ONFIRE:
                // Twentieth Anniversary World Tour
                if (sp.getPicnum() == ONFIRE && currentGame.getCON().type != 20) {
                    break;
                }

                if (spawner != null) {
                    sp.setAng(spawner.getAng());
                    sp.setShade(-64);
                    sp.setCstat((short) (128 | (engine.krand() & 4)));
                }

                if (sp.getPicnum() == EXPLOSION2 || sp.getPicnum() == EXPLOSION2BOT) {
                    sp.setXrepeat(48);
                    sp.setYrepeat(48);
                    sp.setShade(-127);
                    sp.setCstat(sp.getCstat() | 128);
                } else if (sp.getPicnum() == SHRINKEREXPLOSION) {
                    sp.setXrepeat(32);
                    sp.setYrepeat(32);
                } else if (sp.getPicnum() == SMALLSMOKE || sp.getPicnum() == ONFIRE) {
                    // 64 "money"
                    sp.setXrepeat(24);
                    sp.setYrepeat(24);
                } else if (sp.getPicnum() == BURNING || sp.getPicnum() == BURNING2) {
                    sp.setXrepeat(4);
                    sp.setYrepeat(4);
                }

                if (spawner != null) {
                    x = engine.getflorzofslope(sp.getSectnum(), sp.getX(), sp.getY());
                    if (sp.getZ() > x - (12 << 8)) {
                        sp.setZ(x - (12 << 8));
                    }
                }

                if (sp.getPicnum() == ONFIRE) {
                    sp.setX(sp.getX() + engine.krand() % 256 - 128);
                    sp.setY(sp.getY() + engine.krand() % 256 - 128);
                    sp.setZ(sp.getZ() - engine.krand() % 10240);
                    sp.setCstat(sp.getCstat() | 0x80);
                }

                engine.changespritestat(i, (short) 5);

                break;

            case PLAYERONWATER:
                if (spawner != null) {
                    sp.setXrepeat(spawner.getXrepeat());
                    sp.setYrepeat(spawner.getYrepeat());
                    sp.setZvel(128);
                    if (sec.getLotag() != 2) {
                        sp.setCstat(sp.getCstat() | 32768);
                    }
                }
                engine.changespritestat(i, (short) 13);
                break;

            case APLAYER: {
                sp.setXrepeat(0);
                sp.setYrepeat(0);
                int coop = ud.coop;
                if (coop == 2) {
                    coop = 0;
                }

                if (ud.multimode < 2 || coop != sp.getLotag()) {
                    engine.changespritestat(i, (short) 5);
                } else {
                    engine.changespritestat(i, (short) 10);
                }
                break;
            }
            case WATERBUBBLE:
                if (spawner != null && spawner.getPicnum() == APLAYER) {
                    sp.setZ(sp.getZ() - (16 << 8));
                }
                if (sp.getPicnum() == WATERBUBBLE) {
                    if (spawner != null) {
                        sp.setAng(spawner.getAng());
                    }
                    sp.setXrepeat(4);
                    sp.setYrepeat(4);
                } else {
                    sp.setXrepeat(32);
                    sp.setYrepeat(32);
                }

                engine.changespritestat(i, (short) 5);
                break;

            case CRANE:

                sp.setCstat(sp.getCstat() | (64 | 257));

                sp.setPicnum(sp.getPicnum() + 2);
                sp.setZ(sec.getCeilingz() + (48 << 8));
                hittype[i].temp_data[4] = tempwallptr;

                msx[tempwallptr] = sp.getX();
                msy[tempwallptr] = sp.getY();
                msx[tempwallptr + 2] = sp.getZ();

                ListNode<Sprite> node = boardService.getStatNode(0);
                while (node != null) {
                    Sprite spr = node.get();
                    if (spr.getPicnum() == CRANEPOLE && sp.getHitag() == (spr.getHitag())) {
                        msy[tempwallptr + 2] = node.getIndex();

                        hittype[i].temp_data[1] = spr.getSectnum();

                        spr.setXrepeat(48);
                        spr.setYrepeat(128);

                        msx[tempwallptr + 1] = spr.getX();
                        msy[tempwallptr + 1] = spr.getY();

                        spr.setX(sp.getX());
                        spr.setY(sp.getY());
                        spr.setZ(sp.getZ());
                        spr.setShade(sp.getShade());

                        engine.setsprite(node.getIndex(), spr.getX(), spr.getY(), spr.getZ());
                        break;
                    }
                    node = node.getNext();
                }

                tempwallptr += 3;
                sp.setOwner(-1);
                sp.setExtra(8);
                engine.changespritestat(i, (short) 6);
                break;

            case WATERDRIP:
                if (spawner != null && (spawner.getStatnum() == 10 || spawner.getStatnum() == 1)) {
                    sp.setShade(32);
                    if (spawner.getPal() != 1) {
                        sp.setPal(2);
                        sp.setZ(sp.getZ() - (18 << 8));
                    } else {
                        sp.setZ(sp.getZ() - (13 << 8));
                    }
                    sp.setAng(EngineUtils.getAngle(ps[connecthead].posx - sp.getX(), ps[connecthead].posy - sp.getY()));
                    sp.setXvel((short) (48 - (engine.krand() & 31)));
                    ssp(i, CLIPMASK0);
                } else if (j == -1) {
                    sp.setZ(sp.getZ() + (4 << 8));
                    hittype[i].temp_data[0] = sp.getZ();
                    hittype[i].temp_data[1] = engine.krand() & 127;
                }
            case TRASH:

                if (sp.getPicnum() != WATERDRIP) {
                    sp.setAng((short) (engine.krand() & 2047));
                }

            case WATERDRIPSPLASH:

                sp.setXrepeat(24);
                sp.setYrepeat(24);

                engine.changespritestat(i, (short) 6);
                break;

            case PLUG:
                sp.setLotag(9999);
                engine.changespritestat(i, (short) 6);
                break;
            case TOUCHPLATE:
                hittype[i].temp_data[2] = sec.getFloorz();
                if (sec.getLotag() != 1 && sec.getLotag() != 2) {
                    sec.setFloorz(sp.getZ());
                }

                if (currentGame.getCON().type != 20) {
                    if (sp.getPal() != 0 && ud.multimode > 1) {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        break;
                    }
                } else { // Twentieth Anniversary World Tour addition
                    if ((sp.getPal() == 1 && ud.multimode > 1) // Single-game Only
                            || (sp.getPal() == 2 && (ud.multimode == 1 || ud.multimode > 1 && ud.coop != 1)) // Co-op Only
                            || (sp.getPal() == 3 && (ud.multimode == 1 || ud.multimode > 1 && ud.coop == 1))) // Dukematch Only
                    {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        break;
                    }
                }
            case WATERBUBBLEMAKER:
                sp.setCstat(sp.getCstat() | 32768);
                engine.changespritestat(i, (short) 6);
                break;
            case BOLT1:
            case BOLT1 + 1:
            case BOLT1 + 2:
            case BOLT1 + 3:
            case SIDEBOLT1:
            case SIDEBOLT1 + 1:
            case SIDEBOLT1 + 2:
            case SIDEBOLT1 + 3:
                hittype[i].temp_data[0] = sp.getXrepeat();
                hittype[i].temp_data[1] = sp.getYrepeat();
            case MASTERSWITCH:
                if (sp.getPicnum() == MASTERSWITCH) {
                    sp.setCstat(sp.getCstat() | 32768);
                }
                sp.setYvel(0);
                engine.changespritestat(i, (short) 6);
                break;
            case TARGET:
            case DUCK:
            case LETTER:
                sp.setExtra(1);
                sp.setCstat(sp.getCstat() | 257);
                engine.changespritestat(i, (short) 1);
                break;
            case OCTABRAINSTAYPUT:
            case LIZTROOPSTAYPUT:
            case PIGCOPSTAYPUT:
            case LIZMANSTAYPUT:
            case BOSS1STAYPUT:
            case BOSS2STAYPUT:
            case BOSS3STAYPUT:
            case BOSS4STAYPUT:
            case PIGCOPDIVE:
            case COMMANDERSTAYPUT:
                hittype[i].actorstayput = sp.getSectnum();
            case BOSS1:
            case BOSS2:
            case BOSS3:
            case BOSS4:
            case ROTATEGUN:
            case GREENSLIME:
                if (sp.getPicnum() == GREENSLIME) {
                    sp.setExtra(1);
                }
            case DRONE:
            case LIZTROOPONTOILET:
            case LIZTROOPJUSTSIT:
            case LIZTROOPSHOOT:
            case LIZTROOPJETPACK:
            case LIZTROOPDUCKING:
            case LIZTROOPRUNNING:
            case LIZTROOP:
            case OCTABRAIN:
            case COMMANDER:
            case PIGCOP:
            case LIZMAN:
            case LIZMANSPITTING:
            case LIZMANFEEDING:
            case LIZMANJUMP:
            case ORGANTIC:
            case RAT:
            case SHARK:

                if (sp.getPal() == 0) {
                    switch (sp.getPicnum()) {
                        case LIZTROOPONTOILET:
                        case LIZTROOPSHOOT:
                        case LIZTROOPJETPACK:
                        case LIZTROOPDUCKING:
                        case LIZTROOPRUNNING:
                        case LIZTROOPSTAYPUT:
                        case LIZTROOPJUSTSIT:
                        case LIZTROOP:
                            sp.setPal(22);
                            break;
                    }
                }

                if (bossguy(sp.getPicnum())) {
                    if (spawner != null && spawner.getPicnum() == RESPAWN) {
                        sp.setPal(spawner.getPal());
                    }
                    if (sp.getPal() != 0) {
                        sp.setClipdist(80);
                        sp.setXrepeat(40);
                        sp.setYrepeat(40);
                    } else {
                        sp.setXrepeat(80);
                        sp.setYrepeat(80);
                        sp.setClipdist(164);
                    }
                } else {
                    if (sp.getPicnum() != SHARK) {
                        sp.setXrepeat(40);
                        sp.setYrepeat(40);
                        sp.setClipdist(80);
                    } else {
                        sp.setXrepeat(60);
                        sp.setYrepeat(60);
                        sp.setClipdist(40);
                    }
                }

                if (spawner != null) {
                    sp.setLotag(0);
                }

                if ((sp.getLotag() > ud.player_skill) || ud.monsters_off) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                } else {
                    makeitfall(currentGame.getCON(), i);

                    if (sp.getPicnum() == RAT) {
                        sp.setAng((short) (engine.krand() & 2047));
                        sp.setXrepeat(48);
                        sp.setYrepeat(48);
                        sp.setCstat(0);
                    } else {
                        sp.setCstat(sp.getCstat() | 257);

                        if (sp.getPicnum() != SHARK) {
                            ps[connecthead].max_actors_killed++;
                        }
                    }

                    if (sp.getPicnum() == ORGANTIC) {
                        sp.setCstat(sp.getCstat() | 128);
                    }

                    if (spawner != null) {
                        hittype[i].timetosleep = 0;
                        check_fta_sounds(i);
                        engine.changespritestat(i, (short) 1);
                    } else {
                        engine.changespritestat(i, (short) 2);
                    }
                }

                if (sp.getPicnum() == ROTATEGUN) {
                    sp.setZvel(0);
                }

                break;

            case LOCATORS:
                sp.setCstat(sp.getCstat() | 32768);
                engine.changespritestat(i, (short) 7);
                break;

            case ACTIVATORLOCKED:
            case ACTIVATOR:
                sp.setCstat((short) 32768);
                if (sp.getPicnum() == ACTIVATORLOCKED) {
                    sec.setLotag(sec.getLotag() | 16384);
                }
                engine.changespritestat(i, (short) 8);
                break;

            case DOORSHOCK:
                sp.setCstat(sp.getCstat() | 1 + 256);
                sp.setShade(-12);
                engine.changespritestat(i, (short) 6);
                break;

            case OOZ:
            case OOZ2: {
                sp.setShade(-12);

                if (spawner != null) {
                    if (spawner.getPicnum() == NUKEBARREL) {
                        sp.setPal(8);
                    }
                    insertspriteq(i);
                }

                engine.changespritestat(i, (short) 1);

                getglobalz(i);

                int z = (hittype[i].floorz - hittype[i].ceilingz) >> 9;

                sp.setYrepeat((short) z);
                sp.setXrepeat((short) (25 - (z >> 1)));
                sp.setCstat(sp.getCstat() | (engine.krand() & 4));

                break;
            }
            case HEAVYHBOMB:
                if (spawner != null) {
                    sp.setOwner((short) j);
                } else {
                    sp.setOwner(i);
                }
                sp.setXrepeat(9);
                sp.setYrepeat(9);
                sp.setYvel(4);
            case REACTOR2:
            case REACTOR:
            case RECON:
                if (sp.getPicnum() == RECON) {
                    if (sp.getLotag() > ud.player_skill) {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        game.pInt.setsprinterpolate(i, sp);
                        return i;
                    }
                    ps[connecthead].max_actors_killed++;
                    hittype[i].temp_data[5] = 0;
                    if (ud.monsters_off) {
                        sp.setXrepeat(0);
                        sp.setYrepeat(0);
                        engine.changespritestat(i, (short) 5);
                        break;
                    }
                    sp.setExtra(130);
                }

                if (sp.getPicnum() == REACTOR || sp.getPicnum() == REACTOR2) {
                    sp.setExtra((short) currentGame.getCON().impact_damage);
                }

                sp.setCstat(sp.getCstat() | 257); // Make it hitable

                if (ud.multimode < 2 && sp.getPal() != 0) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }
                sp.setPal(0);
                sp.setShade(-17);

                engine.changespritestat(i, (short) 2);
                break;

            case FLAMETHROWERSPRITE:
            case FLAMETHROWERAMMO: // Twentieth Anniversary World Tour
                if (currentGame.getCON().type != 20) {
                    break;
                }

            case ATOMICHEALTH:
            case STEROIDS:
            case HEATSENSOR:
            case SHIELD:
            case AIRTANK:
            case TRIPBOMBSPRITE:
            case JETPACK:
            case HOLODUKE:

            case FIRSTGUNSPRITE:
            case CHAINGUNSPRITE:
            case SHOTGUNSPRITE:
            case RPGSPRITE:
            case SHRINKERSPRITE:
            case FREEZESPRITE:
            case DEVISTATORSPRITE:

            case SHOTGUNAMMO:
            case FREEZEAMMO:
            case HBOMBAMMO:
            case CRYSTALAMMO:
            case GROWAMMO:
            case BATTERYAMMO:
            case DEVISTATORAMMO:
            case RPGAMMO:
            case BOOTS:
            case AMMO:
            case AMMOLOTS:
            case COLA:
            case FIRSTAID:
            case SIXPAK:

                if (spawner != null) {
                    sp.setLotag(0);
                    sp.setZ(sp.getZ() - (32 << 8));
                    sp.setZvel(-1024);
                    ssp(i, CLIPMASK0);
                    sp.setCstat((short) (engine.krand() & 4));
                } else {
                    sp.setOwner(i);
                    sp.setCstat(0);
                }

                if ((ud.multimode < 2 && sp.getPal() != 0) || (sp.getLotag() > ud.player_skill)) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }

                sp.setPal(0);

            case ACCESSCARD:

                if (sp.getPicnum() == ATOMICHEALTH) {
                    sp.setCstat(sp.getCstat() | 128);
                }

                if (ud.multimode > 1 && ud.coop != 1 && sp.getPicnum() == ACCESSCARD) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                } else {
                    if (sp.getPicnum() == AMMO) {
                        sp.setXrepeat(16);
                        sp.setYrepeat(16);
                    } else {
                        sp.setXrepeat(32);
                        sp.setYrepeat(32);
                    }
                }

                sp.setShade(-17);

                if (spawner != null) {
                    engine.changespritestat(i, (short) 1);
                } else {
                    engine.changespritestat(i, (short) 2);
                    makeitfall(currentGame.getCON(), i);
                }
                break;

            case WATERFOUNTAIN:
                sp.setLotag(1);

            case TREE1:
            case TREE2:
            case TIRE:
            case CONE:
            case BOX:
                sp.setCstat(257); // Make it hitable
                sp.setExtra(1);
                engine.changespritestat(i, (short) 6);
                break;

            case FLOORFLAME:
                sp.setShade(-127);
                engine.changespritestat(i, (short) 6);
                break;

            case BOUNCEMINE:
                sp.setOwner(i);
                sp.setCstat(sp.getCstat() | 1 + 256); // Make it hitable
                sp.setXrepeat(24);
                sp.setYrepeat(24);
                sp.setShade(-127);
                sp.setExtra((short) (currentGame.getCON().impact_damage << 2));
                engine.changespritestat(i, (short) 2);
                break;

            case CAMERA1:
            case CAMERA1 + 1:
            case CAMERA1 + 2:
            case CAMERA1 + 3:
            case CAMERA1 + 4:
            case CAMERAPOLE:
                sp.setExtra(1);

                if (currentGame.getCON().camerashitable != 0) {
                    sp.setCstat(257);
                } else {
                    sp.setCstat(0);
                }

            case GENERICPOLE:

                if (ud.multimode < 2 && sp.getPal() != 0) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                } else {
                    sp.setPal(0);
                }
                if (sp.getPicnum() == CAMERAPOLE || sp.getPicnum() == GENERICPOLE) {
                    break;
                }
                sp.setPicnum(CAMERA1);
                engine.changespritestat(i, (short) 1);
                break;
            case STEAM:
                if (spawner != null) {
                    sp.setAng(spawner.getAng());
                    sp.setCstat(16 + 128 + 2);
                    sp.setXrepeat(1);
                    sp.setYrepeat(1);
                    sp.setXvel(-8);
                    ssp(i, CLIPMASK0);
                }
            case CEILINGSTEAM:
                engine.changespritestat(i, (short) 6);
                break;

            case SECTOREFFECTOR:
                sp.setYvel(sec.getExtra());
                sp.setCstat(sp.getCstat() | 32768);
                sp.setXrepeat(0);
                sp.setYrepeat(0);

                switch (sp.getLotag()) {
                    case 28:
                        hittype[i].temp_data[5] = 65;// Delay for lightning
                        break;
                    case 7: // Transporters!!!!
                    case 23:// XPTR END
                        if (sp.getLotag() != 23) {
                            int owner;
                            for (owner = 0; owner < boardService.getSpriteCount(); owner++) {
                                Sprite spo = boardService.getSprite(owner);
                                if (spo != null && (spo.getStatnum() < MAXSTATUS && spo.getPicnum() == SECTOREFFECTOR
                                        && (spo.getLotag() == 7 || spo.getLotag() == 23) && i != owner
                                        && spo.getHitag() == sp.getHitag())) {
                                    sp.setOwner((short) owner);
                                    break;
                                }
                            }
                        } else {
                            sp.setOwner(i);
                        }

                        hittype[i].temp_data[4] = (sec.getFloorz() == sp.getZ()) ? 1 : 0;
                        sp.setCstat(0);
                        engine.changespritestat(i, (short) 9);
                        game.pInt.setsprinterpolate(i, sp);
                        return i;
                    case 1:
                        sp.setOwner(-1);
                        hittype[i].temp_data[0] = 1;
                        break;
                    case 18:

                        if (sp.getAng() == 512) {
                            hittype[i].temp_data[1] = sec.getCeilingz();
                            if (sp.getPal() != 0) {
                                sec.setCeilingz(sp.getZ());
                            }
                        } else {
                            hittype[i].temp_data[1] = sec.getFloorz();
                            if (sp.getPal() != 0) {
                                sec.setFloorz(sp.getZ());
                            }
                        }

                        sp.setHitag(sp.getHitag() << 2);
                        break;

                    case 19:
                        sp.setOwner(-1);
                        break;
                    case 25: // Pistons
                        hittype[i].temp_data[3] = sec.getCeilingz();
                        hittype[i].temp_data[4] = 1;
                        sec.setCeilingz(sp.getZ());
                        game.pInt.setceilinterpolate(sect, sec); // ceilinz
                        break;
                    case 35:
                        sec.setCeilingz(sp.getZ());
                        break;
                    case 27:
                        if (gDemoScreen.isDemoRecording()) { // camera
                            sp.setXrepeat(64);
                            sp.setYrepeat(64);
                            sp.setCstat(sp.getCstat() & 32767);
                        }
                        break;
                    case 12:

                        hittype[i].temp_data[1] = sec.getFloorshade();
                        hittype[i].temp_data[2] = sec.getCeilingshade();
                        break;

                    case 13:

                        hittype[i].temp_data[0] = sec.getCeilingz();
                        hittype[i].temp_data[1] = sec.getFloorz();

                        if (klabs(hittype[i].temp_data[0] - sp.getZ()) < klabs(hittype[i].temp_data[1] - sp.getZ())) {
                            sp.setOwner(1);
                        } else {
                            sp.setOwner(0);
                        }

                        if (sp.getAng() == 512) {
                            if (sp.getOwner() != 0) {
                                sec.setCeilingz(sp.getZ());
                            } else {
                                sec.setFloorz(sp.getZ());
                            }
                        } else {
                            int z = sp.getZ();
                            sec.setFloorz(z);
                            sec.setCeilingz(z);
                        }

                        if ((sec.getCeilingstat() & 1) != 0) {
                            sec.setCeilingstat(sec.getCeilingstat() ^ 1);
                            hittype[i].temp_data[3] = 1;

                            if (sp.getOwner() == 0 && sp.getAng() == 512) {
                                sec.setCeilingstat(sec.getCeilingstat() ^ 1);
                                hittype[i].temp_data[3] = 0;
                            }

                            sec.setCeilingshade(sec.getFloorshade());

                            if (sp.getAng() == 512) {
                                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    Sector nextsec = boardService.getSector(wal.getNextsector());
                                    if (nextsec != null) {
                                        if ((nextsec.getCeilingstat() & 1) == 0) {
                                            sec.setCeilingpicnum(nextsec.getCeilingpicnum());
                                            sec.setCeilingshade(nextsec.getCeilingshade());
                                            break; // Leave earily
                                        }
                                    }
                                }
                            }
                        }

                        break;

                    case 17:

                        hittype[i].temp_data[2] = sec.getFloorz(); // Stopping loc

                        Sector s1 = boardService.getSector(engine.nextsectorneighborz(sect, sec.getFloorz(), -1, -1));
                        if (s1 != null) {
                            hittype[i].temp_data[3] = s1.getCeilingz();
                        }

                        Sector s2 = boardService.getSector(engine.nextsectorneighborz(sect, sec.getCeilingz(), 1, 1));
                        if (s2 != null) {
                            hittype[i].temp_data[4] = s2.getFloorz();
                        }

                        game.pInt.setceilinterpolate(sect, sec);
                        game.pInt.setfloorinterpolate(sect, sec);

                        break;

                    case 24:
                        sp.setYvel(sp.getYvel() << 1);
                    case 36:
                        break;

                    case 20: {
                        // find the two most clostest wall x's and y's
                        long q = 0x7fffffff;

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            x = wal.getX();
                            y = wal.getY();

                            d = FindDistance2D(sp.getX() - x, sp.getY() - y);
                            if (d < q) {
                                q = d;
                                clostest = wn.getIndex();
                            }
                        }

                        hittype[i].temp_data[1] = clostest;

                        q = 0x7fffffff;

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            int s = wn.getIndex();
                            x = wal.getX();
                            y = wal.getY();
                            d = FindDistance2D(sp.getX() - x, sp.getY() - y);

                            if (d < q && s != hittype[i].temp_data[1]) {
                                q = d;
                                clostest = s;
                            }
                        }

                        hittype[i].temp_data[2] = clostest;
                    }

                    break;

                    case 3:

                        hittype[i].temp_data[3] = sec.getFloorshade();

                        sec.setFloorshade(sp.getShade());
                        sec.setCeilingshade(sp.getShade());

                        sp.setOwner((short) (sec.getCeilingpal() << 8));
                        sp.setOwner(sp.getOwner() | sec.getFloorpal());

                        // fix all the walls;

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if ((wal.getHitag() & 1) == 0) {
                                wal.setShade(sp.getShade());
                            }

                            Wall w2 = boardService.getWall(wal.getNextwall());
                            if ((wal.getCstat() & 2) != 0 && w2 != null) {
                                w2.setShade(sp.getShade());
                            }
                        }
                        break;

                    case 31:
                        hittype[i].temp_data[1] = sec.getFloorz();
                        if (sp.getAng() != 1536) {
                            sec.setFloorz(sp.getZ());
                        }

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getHitag() == 0) {
                                wal.setHitag(9999);
                            }
                        }

                        game.pInt.setfloorinterpolate(sect, sec); // floorz

                        break;
                    case 32:
                        hittype[i].temp_data[1] = sec.getCeilingz();
                        hittype[i].temp_data[2] = sp.getHitag();
                        if (sp.getAng() != 1536) {
                            sec.setCeilingz(sp.getZ());
                        }

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getHitag() == 0) {
                                wal.setHitag(9999);
                            }
                        }

                        game.pInt.setceilinterpolate(sect, sec); // ceiling

                        break;

                    case 4: // Flashing lights

                        hittype[i].temp_data[2] = sec.getFloorshade();

                        sp.setOwner((short) (sec.getCeilingpal() << 8));
                        sp.setOwner(sp.getOwner() | sec.getFloorpal());

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getShade() > hittype[i].temp_data[3]) {
                                hittype[i].temp_data[3] = wal.getShade();
                            }
                        }

                        break;

                    case 9:
                        if (sec.getLotag() != 0 && klabs(sec.getCeilingz() - sp.getZ()) > 1024) {
                            sec.setLotag(sec.getLotag() | 32768); // If its open
                        }
                    case 8:
                        // First, get the ceiling-floor shade

                        hittype[i].temp_data[0] = sec.getFloorshade();
                        hittype[i].temp_data[1] = sec.getCeilingshade();

                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getShade() > hittype[i].temp_data[2]) {
                                hittype[i].temp_data[2] = wal.getShade();
                            }
                        }

                        hittype[i].temp_data[3] = 1; // Take Out;

                        break;

                    case 11:// Pivitor rotater
                        if (sp.getAng() > 1024) {
                            hittype[i].temp_data[3] = 2;
                        } else {
                            hittype[i].temp_data[3] = -2;
                        }
                    case 0:
                    case 2:// Earthquakemakers
                    case 5:// Boss Creature
                    case 6:// Subway
                    case 14:// Caboos
                    case 15:// Subwaytype sliding door
                    case 16:// That rotating blocker reactor thing
                    case 26:// ESCELATOR
                    case 30:// No rotational subways

                        if (sp.getLotag() == 0) {
                            if (sec.getLotag() == 30) {
                                if (sp.getPal() != 0) {
                                    sp.setClipdist(1);
                                } else {
                                    sp.setClipdist(0);
                                }
                                hittype[i].temp_data[3] = sec.getFloorz();
                                sec.setHitag(i);
                            }

                            int owner;
                            for (owner = 0; owner < boardService.getSpriteCount(); owner++) {
                                Sprite spo = boardService.getSprite(owner);
                                if (spo != null && spo.getStatnum() < MAXSTATUS) {
                                    if (spo.getPicnum() == SECTOREFFECTOR && spo.getLotag() == 1
                                            && spo.getHitag() == sp.getHitag()) {
                                        if (sp.getAng() == 512) {
                                            sp.setX(spo.getX());
                                            sp.setY(spo.getY());
                                        }
                                        break;
                                    }
                                }
                            }

                            if (owner == boardService.getSpriteCount()) {
                                Console.out.println("Found lonely Sector Effector (lotag 0) at (" + sp.getX() + "," + sp.getY() + ")",
                                        OsdColor.RED);
                                break;
                            }
                            sp.setOwner(owner);
                        }

                        hittype[i].temp_data[1] = tempwallptr;
                        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            msx[tempwallptr] = wal.getX() - sp.getX();
                            msy[tempwallptr] = wal.getY() - sp.getY();
                            tempwallptr++;
                            if (tempwallptr > 2047) {
                                Console.out.println("Too many moving sectors at (" + wal.getX() + "," + wal.getY() + ")",
                                        OsdColor.RED);
                                break;
                            }
                        }
                        if (sp.getLotag() == 30 || sp.getLotag() == 6 || sp.getLotag() == 14 || sp.getLotag() == 5) {

                            if (sec.getHitag() == -1) {
                                sp.setExtra(0);
                            } else {
                                sp.setExtra(1);
                            }

                            sec.setHitag(i);

                            int found = 0;
                            int s = -1;
                            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                s = wn.getIndex();

                                Sector ns = boardService.getSector(wal.getNextsector());
                                if (ns != null && ns.getHitag() == 0
                                        && ns.getLotag() < 3) {
                                    s = wal.getNextsector();
                                    found = 1;
                                    break;
                                }
                            }

                            if (found == 0) {
                                Console.out.println("Subway found no zero'd sectors with locators\nat (" + sp.getX() + "," + sp.getY() + ")",
                                        OsdColor.RED);
                                break;
                            }

                            sp.setOwner(-1);
                            hittype[i].temp_data[0] = s;

                            if (sp.getLotag() != 30) {
                                hittype[i].temp_data[3] = sp.getHitag();
                            }
                        } else if (sp.getLotag() == 16) {
                            hittype[i].temp_data[3] = sec.getCeilingz();
                        } else if (sp.getLotag() == 26) {
                            hittype[i].temp_data[3] = sp.getX();
                            hittype[i].temp_data[4] = sp.getY();
                            if (sp.getShade() == sec.getFloorshade()) // UP
                            {
                                sp.setZvel(-256);
                            } else {
                                sp.setZvel(256);
                            }

                            sp.setShade(0);
                        } else if (sp.getLotag() == 2) {
                            hittype[i].temp_data[5] = sec.getFloorheinum();
                            sec.setFloorheinum(0);
                        }
                }

                switch (sp.getLotag()) {
                    case 6:
                    case 14:
                        int si = callsound(sect, i);
                        if (si == -1) {
                            si = SUBWAY;
                        }
                        hittype[i].lastvx = si;
                    case 30:
                        if (numplayers > 1 || mFakeMultiplayer) {
                            break;
                        }
                    case 0:
                    case 1:
                    case 5:
                    case 11:
                    case 15:
                    case 16:
                    case 26:
                        setsectinterpolate(i);
                        break;
                }

                switch (sp.getLotag()) {
                    case 40:
                    case 41:
                        engine.changespritestat(i, (short) 15);
                        if (rorcnt < 16) {
                            if (sp.getLotag() == 41) {
                                rortype[rorcnt] = 1; // ceiling
                            }
                            if (sp.getLotag() == 40) {
                                rortype[rorcnt] = 2; // floor
                            }
                            rorsector[rorcnt++] = sp.getSectnum();
                        }
                        break;
                    default:
                        engine.changespritestat(i, (short) 3);
                        break;
                }

                break;

            case SEENINE:
            case OOZFILTER:

                sp.setShade(-16);
                if (sp.getXrepeat() <= 8) {
                    sp.setCstat((short) 32768);
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                } else {
                    sp.setCstat(1 + 256);
                }
                sp.setExtra((short) (currentGame.getCON().impact_damage << 2));
                sp.setOwner(i);

                engine.changespritestat(i, (short) 6);
                break;

            case CRACK1:
            case CRACK2:
            case CRACK3:
            case CRACK4:
            case FIREEXT:
                if (sp.getPicnum() == FIREEXT) {
                    sp.setCstat(257);
                    sp.setExtra((short) (currentGame.getCON().impact_damage << 2));
                } else {
                    sp.setCstat(sp.getCstat() | 17);
                    sp.setExtra(1);
                }

                if (ud.multimode < 2 && sp.getPal() != 0) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                    break;
                }

                sp.setPal(0);
                sp.setOwner(i);
                engine.changespritestat(i, (short) 6);
                sp.setXvel(8);
                ssp(i, CLIPMASK0);
                break;

            case TOILET:
            case STALL:
                sp.setLotag(1);
                sp.setCstat(sp.getCstat() | 257);
                sp.setClipdist(8);
                sp.setOwner(i);
                break;
            case CANWITHSOMETHING:
            case CANWITHSOMETHING2:
            case CANWITHSOMETHING3:
            case CANWITHSOMETHING4:
            case RUBBERCAN:
                sp.setExtra(0);
            case EXPLODINGBARREL:
            case HORSEONSIDE:
            case FIREBARREL:
            case NUKEBARREL:
            case FIREVASE:
            case NUKEBARRELDENTED:
            case NUKEBARRELLEAKED:
            case WOODENHORSE:

                if (spawner != null) {
                    sp.setXrepeat(32);
                    sp.setYrepeat(32);
                }
                sp.setClipdist(72);
                makeitfall(currentGame.getCON(), i);
                if (spawner != null) {
                    sp.setOwner((short) j);
                } else {
                    sp.setOwner(i);
                }
            case EGG:
                if (ud.monsters_off && sp.getPicnum() == EGG) {
                    sp.setXrepeat(0);
                    sp.setYrepeat(0);
                    engine.changespritestat(i, (short) 5);
                } else {
                    if (sp.getPicnum() == EGG) {
                        sp.setClipdist(24);
                        ps[connecthead].max_actors_killed++;
                    }
                    sp.setCstat((short) (257 | (engine.krand() & 4)));
                    engine.changespritestat(i, (short) 2);
                }
                break;
            case TOILETWATER:
                sp.setShade(-16);
                engine.changespritestat(i, (short) 6);
                break;
        }

        game.pInt.setsprinterpolate(i, sp);
        return i;
    }

    public static void lotsofmoney(Sprite s, int n) {
        for (int i = n; i > 0; i--) {
            int ang = engine.krand() & 2047;
            int sz = s.getZ() - (engine.krand() % (47 << 8));
            int j = EGS(s.getSectnum(), s.getX(), s.getY(), sz, MONEY, -32, 8, 8, ang, 0, 0, 0, (short) 5);
            Sprite sp = boardService.getSprite(j);
            if (sp != null) {
                sp.setCstat((engine.krand() & 12));
            }
        }
    }

    public static void lotsofmail(Sprite s, int n) {
        for (int i = n; i > 0; i--) {
            int ang = engine.krand() & 2047;
            int sz = s.getZ() - (engine.krand() % (47 << 8));
            int j = EGS(s.getSectnum(), s.getX(), s.getY(), sz, MAIL, -32, 8, 8, ang, 0, 0, 0, (short) 5);
            Sprite sp = boardService.getSprite(j);
            if (sp != null) {
                sp.setCstat((engine.krand() & 12));
            }
        }
    }

    public static void lotsofpaper(Sprite s, int n) {
        for (int i = n; i > 0; i--) {
            int ang = engine.krand() & 2047;
            int sz = s.getZ() - (engine.krand() % (47 << 8));
            int j = EGS(s.getSectnum(), s.getX(), s.getY(), sz, PAPER, -32, 8, 8, ang, 0, 0, 0, (short) 5);
            Sprite sp = boardService.getSprite(j);
            if (sp != null) {
                sp.setCstat((engine.krand() & 12));
            }
        }
    }

    public static void guts(Sprite s, int gtype, int n, int p) {
        if (boardService.getSector(s.getSectnum()) == null) {
            return;
        }

        char sx, sy;
        if (badguy(s) && s.getXrepeat() < 16) {
            sx = sy = 8;
        } else {
            sx = sy = 32;
        }

        int gutz = s.getZ() - (8 << 8);
        int floorz = engine.getflorzofslope(s.getSectnum(), s.getX(), s.getY());

        if (gutz > (floorz - (8 << 8))) {
            gutz = floorz - (8 << 8);
        }

        if (s.getPicnum() == COMMANDER) {
            gutz -= (24 << 8);
        }

        int pal = 0;
        if (badguy(s) && s.getPal() == 6) {
            pal = 6;
        }

        for (int j = 0; j < n; j++) {
            int a = engine.krand() & 2047;

            int zv = -512 - (engine.krand() & 2047);
            int ve = 48 + (engine.krand() & 31);

            int esz = gutz - (engine.krand() & 8191);
            int esy = s.getY() + (engine.krand() & 255) - 128;
            int esx = s.getX() + (engine.krand() & 255) - 128;
            int i = EGS(s.getSectnum(), esx, esy, esz, gtype, -32, sx, sy, a, ve, zv, ps[p].i, (short) 5);

            Sprite sp = boardService.getSprite(i);
            if (sp != null) {
                if (sp.getPicnum() == JIBS2) {
                    sp.setXrepeat(sp.getXrepeat() >> 2);
                    sp.setYrepeat(sp.getYrepeat() >> 2);
                }
                if (pal == 6) {
                    sp.setPal(6);
                }
            }
        }
    }

    public static void gutsdir(Sprite s, int gtype, int n, int p) {
        char sx, sy;
        if (badguy(s) && s.getXrepeat() < 16) {
            sx = sy = 8;
        } else {
            sx = sy = 32;
        }

        int gutz = s.getZ() - (8 << 8);
        int floorz = engine.getflorzofslope(s.getSectnum(), s.getX(), s.getY());

        if (gutz > (floorz - (8 << 8))) {
            gutz = floorz - (8 << 8);
        }

        if (s.getPicnum() == COMMANDER) {
            gutz -= (24 << 8);
        }

        for (int j = 0; j < n; j++) {
            int a = engine.krand() & 2047;
            int zv = -512 - (engine.krand() & 2047);
            int ve = 256 + (engine.krand() & 127);
            EGS(s.getSectnum(), s.getX(), s.getY(), gutz, gtype, -32, sx, sy, a, ve, zv, ps[p].i, 5);
        }
    }

    public static boolean bossguy(int pn) {
        if (pn == BOSS4STAYPUT || pn == BOSS1 || pn == BOSS1STAYPUT || pn == BOSS2 || pn == BOSS2STAYPUT || pn == BOSS3
                || pn == BOSS3STAYPUT || pn == BOSS4) {
            return true;
        }

        // Twentieth Anniversary World Tour BOSS5
        return currentGame.getCON().type == 20 && (pn == 5310 || pn == 5311);
    }
}
