//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import java.util.Arrays;

import static java.lang.Math.*;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Duke3D.Actors.badguy;
import static ru.m210projects.Duke3D.Factory.DukeGameProcessor.MAXHORIZ;
import static ru.m210projects.Duke3D.Gamedef.furthestangle;
import static ru.m210projects.Duke3D.Gamedef.getincangle;
import static ru.m210projects.Duke3D.Gameutils.*;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Premap.LeaveMap;
import static ru.m210projects.Duke3D.Screen.changepalette;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;
import static ru.m210projects.Duke3D.Spawn.guts;
import static ru.m210projects.Duke3D.Spawn.spawn;
import static ru.m210projects.Duke3D.View.FTA;
import static ru.m210projects.Duke3D.Weapons.*;

public class Player {

    public static final byte[] waterpal = new byte[768], slimepal = new byte[768], titlepal = new byte[768],
            drealms = new byte[768], endingpal = new byte[768];

    public static final int[][] fdmatrix = {
            // KNEE PIST SHOT CHAIN RPG PIPE SHRI DEVI WALL FREE HAND EXPA
            {128, -1, -1, -1, 128, -1, -1, -1, 128, -1, 128, -1, -1}, // KNEE
            {1024, 1024, 1024, 1024, 2560, 128, 2560, 2560, 1024, 2560, 2560, 2560, -1}, // PIST
            {512, 512, 512, 512, 2560, 128, 2560, 2560, 1024, 2560, 2560, 2560, -1}, // SHOT
            {512, 512, 512, 512, 2560, 128, 2560, 2560, 1024, 2560, 2560, 2560, -1}, // CHAIN
            {2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, -1}, // RPG
            {512, 512, 512, 512, 2048, 512, 2560, 2560, 512, 2560, 2560, 2560, -1}, // PIPE
            {128, 128, 128, 128, 2560, 128, 2560, 2560, 128, 128, 128, 128, -1}, // SHRI
            {1536, 1536, 1536, 1536, 2560, 1536, 1536, 1536, 1536, 1536, 1536, 1536, -1}, // DEVI
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, // WALL
            {128, 128, 128, 128, 2560, 128, 2560, 2560, 128, 128, 128, 128, -1}, // FREE
            {2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, 2560, -1}, // HAND
            {128, 128, 128, 128, 2560, 128, 2560, 2560, 128, 128, 128, 128, -1}, // EXPA
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},};
    public static final int[] goalx = new int[MAXPLAYERS];
    public static final int[] goaly = new int[MAXPLAYERS];
    public static final int[] goalz = new int[MAXPLAYERS];
    public static final int[] goalsect = new int[MAXPLAYERS];
    public static final int[] goalwall = new int[MAXPLAYERS];
    public static final int[] goalsprite = new int[MAXPLAYERS];
    public static final int[] goalplayer = new int[MAXPLAYERS];
    public static final int[] clipmovecount = new int[MAXPLAYERS];
    public static final short[] searchsect = new short[MAXSECTORS];
    public static final short[] searchparent = new short[MAXSECTORS];
    public static final byte[] dashow2dsector = new byte[(MAXSECTORS + 7) >> 3];

    public static void InitPlayers() {
        ps[myconnectindex].reset();

        ps[myconnectindex].palette = engine.getPaletteManager().getBasePalette();
        ps[myconnectindex].aim_mode = cfg.isgMouseAim() ? 1 : 0;
        ps[myconnectindex].auto_aim = cfg.gAutoAim ? 1 : 0;

        for (int i = 1; i < MAXPLAYERS; i++) {
            ps[i].copy(ps[myconnectindex]);
        }

        game.net.getnames();
    }

    public static void checkavailinven(PlayerStruct p) {
        if (p.firstaid_amount > 0) {
            p.inven_icon = 1;
        } else if (p.steroids_amount > 0) {
            p.inven_icon = 2;
        } else if (p.holoduke_amount > 0) {
            p.inven_icon = 3;
        } else if (p.jetpack_amount > 0) {
            p.inven_icon = 4;
        } else if (p.heat_amount > 0) {
            p.inven_icon = 5;
        } else if (p.scuba_amount > 0) {
            p.inven_icon = 6;
        } else if (p.boot_amount > 0) {
            p.inven_icon = 7;
        } else {
            p.inven_icon = 0;
        }
    }

    public static void checkavailweapon(PlayerStruct p) {
        int weap;
        if (p.wantweaponfire >= 0) {
            weap = p.wantweaponfire;
            p.wantweaponfire = -1;

            if (weap == p.curr_weapon || weap >= MAX_WEAPONS) {
                return;
            } else if (p.gotweapon[weap] && p.ammo_amount[weap] > 0) {
                addweapon(p, weap);
                return;
            }
        }

        weap = p.curr_weapon;
        if (p.gotweapon[weap] && p.ammo_amount[weap] > 0) {
            return;
        }

        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        int snum = psp.getYvel(), i;
        for (i = 0; i < 10; i++) {
            weap = ud.wchoice[snum][i];

            if (weap == 0) {
                weap = 9;
            } else {
                weap--;
            }

            if (weap == 0 || (p.gotweapon[weap] && p.ammo_amount[weap] > 0)) {
                break;
            }
        }

        if (i == 10) {
            weap = 0;
        }

        // Found the weapon

        p.last_weapon = p.curr_weapon;
        p.random_club_frame = 0;
        p.curr_weapon = (short) weap;
        p.kickback_pic = 0;
        if (p.holster_weapon == 1) {
            p.holster_weapon = 0;
            p.weapon_pos = 10;
        } else {
            p.weapon_pos = -1;
        }
    }

    public static boolean inventory(Sprite s) {
        switch (s.getPicnum()) {
            case FIRSTAID:
            case STEROIDS:
            case HEATSENSOR:
            case BOOTS:
            case JETPACK:
            case HOLODUKE:
            case AIRTANK:
                return true;
        }
        return false;
    }

    public static void setpal(PlayerStruct p) {
        if (p.heat_on != 0) {
            p.palette = slimepal;
        } else {
            Sector psec = boardService.getSector(p.cursectnum);
            if (psec == null) {
                return;
            }

            switch (psec.getCeilingpicnum()) {
                case FLOORSLIME:
                case FLOORSLIME + 1:
                case FLOORSLIME + 2:
                    p.palette = slimepal;
                    break;
                default:
                    if (psec.getLotag() == 2) {
                        p.palette = waterpal;
                    } else {
                        p.palette = engine.getPaletteManager().getBasePalette();
                    }
                    break;
            }
        }

        changepalette = 1;
    }

    public static void incur_damage(PlayerStruct p) {
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        psp.setExtra(psp.getExtra() - (p.extra_extra8 >> 8));

        int damage = psp.getExtra() - p.last_extra;

        if (damage < 0) {
            p.extra_extra8 = 0;

            if (p.shield_amount > 0) {
                int shield_damage = damage * (20 + (engine.krand() % 30)) / 100;
                damage -= shield_damage;

                p.shield_amount += (short) shield_damage;

                if (p.shield_amount < 0) {
                    damage += p.shield_amount;
                    p.shield_amount = 0;
                }
            }

            psp.setExtra((p.last_extra + damage));
        }
    }

    public static void quickkill(PlayerStruct p) {
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        p.pals[0] = 48;
        p.pals[1] = 48;
        p.pals[2] = 48;
        p.pals_time = 48;

        psp.setExtra(0);
        psp.setCstat(psp.getCstat() | 32768);
        if (!ud.god) {
            guts(psp, JIBS6, 8, myconnectindex);
        }
    }

    public static void forceplayerangle(PlayerStruct p) {
        int n = 128 - (engine.krand() & 255);

        p.horiz += 64;
        p.return_to_center = 9;
        p.look_ang = (short) (n >> 1);
        p.rotscrnang = (short) (n >> 1);
    }

    public static int hits(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return 0;
        }

        int zoff = 0;
        if (spr.getPicnum() == APLAYER) {
            zoff = (40 << 8);
        }

        engine.hitscan(spr.getX(), spr.getY(), spr.getZ() - zoff, spr.getSectnum(), EngineUtils.sin((spr.getAng() + 512) & 2047),
                EngineUtils.sin(spr.getAng() & 2047), 0, pHitInfo, CLIPMASK1);

        return (FindDistance2D(pHitInfo.hitx - spr.getX(), pHitInfo.hity - spr.getY()));
    }

    public static int hitasprite(int i)
    {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return ((1 << 30));
        }

        int zoff = 0;
        if (badguy(sp)) {
            zoff = (42 << 8);
        } else if (sp.getPicnum() == APLAYER) {
            zoff = (39 << 8);
        }

        engine.hitscan(sp.getX(), sp.getY(), sp.getZ() - zoff, sp.getSectnum(),
                EngineUtils.cos((sp.getAng()) & 2047), EngineUtils.sin(sp.getAng() & 2047), 0, pHitInfo, CLIPMASK1);

        Wall hw = boardService.getWall(pHitInfo.hitwall);
        if (hw != null && (hw.getCstat() & 16) != 0 && badguy(sp)) {
            return ((1 << 30));
        }

        return (FindDistance2D(pHitInfo.hitx - sp.getX(), pHitInfo.hity - sp.getY()));
    }

    public static int hitawall(PlayerStruct p)
    {
        if (IsOriginalDemo()) {
            engine.hitscan(p.posx, p.posy, p.posz, p.cursectnum, EngineUtils.sin(((int) p.ang + 512) & 2047),
                    EngineUtils.sin((int) p.ang & 2047), 0, pHitInfo, CLIPMASK0);

        } else {
            engine.hitscan(p.posx, p.posy, p.posz, p.cursectnum, (int) BCosAngle(BClampAngle(p.ang)),
                    (int) BSinAngle(BClampAngle(p.ang)), 0, pHitInfo, CLIPMASK0);
        }

        return (FindDistance2D(pHitInfo.hitx - p.posx, pHitInfo.hity - p.posy));
    }

    public static boolean doincrements(PlayerStruct p) {
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return false;
        }

        int snum = psp.getYvel();

        p.player_par++;

        if (p.invdisptime > 0) {
            p.invdisptime--;
        }

        if (p.tipincs > 0) {
            p.tipincs--;
        }

        if (p.last_pissed_time > 0) {
            p.last_pissed_time--;

            if (p.last_pissed_time == (26 * 219)) {
                spritesound(FLUSH_TOILET, p.i);
                if (snum == screenpeek || ud.coop == 1) {
                    spritesound(DUKE_PISSRELIEF, p.i);
                }
            }

            if (p.last_pissed_time == (26 * 218)) {
                p.holster_weapon = 0;
                p.weapon_pos = 10;
            }
        }

        if (p.crack_time > 0) {
            p.crack_time--;
            if (p.crack_time == 0) {
                p.knuckle_incs = 1;
                p.crack_time = 777;
            }
        }

        if (p.steroids_amount > 0 && p.steroids_amount < 400) {
            p.steroids_amount--;
            if (p.steroids_amount == 0) {
                checkavailinven(p);
            }
            if ((p.steroids_amount & 7) == 0) {
                if (snum == screenpeek || ud.coop == 1) {
                    spritesound(DUKE_HARTBEAT, p.i);
                }
            }
        }

        if (p.heat_on != 0 && p.heat_amount > 0) {
            p.heat_amount--;
            if (p.heat_amount == 0) {
                p.heat_on = 0;
                checkavailinven(p);
                spritesound(NITEVISION_ONOFF, p.i);
                setpal(p);
            }
        }

        if (p.holoduke_on >= 0) {
            p.holoduke_amount--;
            if (p.holoduke_amount <= 0) {
                spritesound(TELEPORTER, p.i);
                p.holoduke_on = -1;
                checkavailinven(p);
            }
        }

        if (p.jetpack_on != 0 && p.jetpack_amount > 0) {
            p.jetpack_amount--;
            if (p.jetpack_amount <= 0) {
                p.jetpack_on = 0;
                checkavailinven(p);
                spritesound(DUKE_JETPACK_OFF, p.i);
                stopsound(DUKE_JETPACK_IDLE, p.i);
                stopsound(DUKE_JETPACK_ON, p.i);
            }
        }

        if (p.quick_kick > 0 && psp.getPal() != 1) {
            p.quick_kick--;
            if (p.quick_kick == 8) {
                shoot(p.i, KNEE);
            }
        }

        if (p.access_incs != 0 && psp.getPal() != 1) {
            p.access_incs++;
            if (psp.getExtra() <= 0) {
                p.access_incs = 12;
            }
            if (p.access_incs == 12) {
                Sprite asp = boardService.getSprite(p.access_spritenum);
                if (asp != null) {
                    checkhitswitch(snum, p.access_spritenum, 1);

                    switch (asp.getPal()) {
                        case 0:
                            p.got_access &= (short) (0xffff - 0x1);
                            break;
                        case 21:
                            p.got_access &= (short) (0xffff - 0x2);
                            break;
                        case 23:
                            p.got_access &= (short) (0xffff - 0x4);
                            break;
                    }
                    p.access_spritenum = -1;
                } else {
                    Wall awal = boardService.getWall(p.access_wallnum);
                    if (awal != null) {
                        checkhitswitch(snum, p.access_wallnum, 0);
                        switch (awal.getPal()) {
                            case 0:
                                p.got_access &= (short) (0xffff - 0x1);
                                break;
                            case 21:
                                p.got_access &= (short) (0xffff - 0x2);
                                break;
                            case 23:
                                p.got_access &= (short) (0xffff - 0x4);
                                break;
                        }
                    }
                }
            }

            if (p.access_incs > 20) {
                p.access_incs = 0;
                p.weapon_pos = 10;
                p.kickback_pic = 0;
            }
        }

        Sector psec = boardService.getSector(p.cursectnum);
        if (p.scuba_on == 0 && psec != null && psec.getLotag() == 2) {
            if (p.scuba_amount > 0) {
                p.scuba_on = 1;
                p.inven_icon = 6;
                FTA(76, p);
            } else {
                if (p.airleft > 0) {
                    p.airleft--;
                } else {
                    p.extra_extra8 += 32;
                    if (p.last_extra < (currentGame.getCON().max_player_health >> 1) && (p.last_extra & 3) == 0) {
                        spritesound(DUKE_LONGTERM_PAIN, p.i);
                    }
                }
            }
        } else if (p.scuba_amount > 0 && p.scuba_on != 0) {
            p.scuba_amount--;
            if (p.scuba_amount == 0) {
                p.scuba_on = 0;
                checkavailinven(p);
            }
        }

        if (p.knuckle_incs != 0) {
            p.knuckle_incs++;
            if (p.knuckle_incs == 10) {
                if (engine.getTotalClock() > 1024) {
                    if (snum == screenpeek || ud.coop == 1) {
                        if ((engine.rand() & 1) != 0) { // spritesound
                            spritesound(DUKE_CRACK, p.i);
                        } else {
                            spritesound(DUKE_CRACK2, p.i);
                        }
                    }
                }
                spritesound(DUKE_CRACK_FIRST, p.i);
            } else if (p.knuckle_incs == 22 || (sync[snum].bits & (1 << 2)) != 0) {
                p.knuckle_incs = 0;
            }

            return true;
        }
        return false;
    }

    public static void processinput(final int snum) {
        final PlayerStruct p = ps[snum];
        int pi = p.i;
        Sprite psp = boardService.getSprite(pi);
        if (psp == null) {
            return;
        }

        int sb_snum = 0;
        if (p.cheat_phase <= 0) {
            sb_snum = sync[snum].bits;
        }

        int psect = p.cursectnum;
        if (psect == -1) {
            if (psp.getExtra() > 0 && !ud.clipping) {
                quickkill(p);
                spritesound(SQUISHED, pi);
            }
            psect = 0;
        }

        Sector psec = boardService.getSector(psect);
        if (!boardService.isValidSector(psp.getSectnum()) || psec == null) {
            return;
        }

        int psectlotag = psec.getLotag();
        p.spritebridge = 0;

        boolean shrunk = (psp.getYrepeat() < 32);
        engine.getzrange(p.posx, p.posy, p.posz, psect, 163, CLIPMASK0);
        int cz = zr_ceilz;
        final int hz = zr_ceilhit;
        int fz = zr_florz;
        final int lz = zr_florhit;
        // int j =

        p.truefz = engine.getflorzofslope(psect, p.posx, p.posy);
        p.truecz = engine.getceilzofslope(psect, p.posx, p.posy);

        int truefdist = klabs(p.posz - p.truefz);
        if ((lz & kHitTypeMask) == kHitSector && psectlotag == 1 && truefdist > PHEIGHT + (16 << 8)) {
            psectlotag = 0;
        }

        hittype[pi].floorz = fz;
        hittype[pi].ceilingz = cz;

        p.ohoriz = p.horiz;
        p.ohorizoff = p.horizoff;

        if (p.aim_mode == 0 && p.on_ground && psectlotag != 2 && (psec.getFloorstat() & 2) != 0) {
            int x, y;
            if (IsOriginalDemo()) {
                x = p.posx + (EngineUtils.sin(((int) p.ang + 512) & 2047) >> 5);
                y = p.posy + (EngineUtils.sin((int) p.ang & 2047) >> 5);
            } else {
                x = (int) (p.posx + (BCosAngle(BClampAngle(p.ang)) / 32.0f));
                y = (int) (p.posy + (BSinAngle(BClampAngle(p.ang)) / 32.0f));
            }
            int tempsect = engine.updatesector(x, y, psect);
            if (tempsect >= 0) {
                int k = engine.getflorzofslope(psect, x, y);
                if (psect == tempsect) {
                    p.horizoff += (short) mulscale(p.truefz - k, 160, 16);
                } else if (klabs(engine.getflorzofslope(tempsect, x, y) - k) <= (4 << 8)) {
                    p.horizoff += (short) mulscale(p.truefz - k, 160, 16);
                }
            }
        }

        if (p.horizoff > 0) {
            p.horizoff -= (short) ((p.horizoff >> 3) + 1);
        } else if (p.horizoff < 0) {
            p.horizoff += (short) (((-p.horizoff) >> 3) + 1);
        }

        if ((hz & kHitTypeMask) == kHitSprite) {
            Sprite pHitSprite = boardService.getSprite(hz & kHitIndexMask);
            if (pHitSprite != null && pHitSprite.getStatnum() == 1 && pHitSprite.getExtra() >= 0) {
                cz = p.truecz;
            }
        }

        if ((lz & kHitTypeMask) == kHitSprite) {
            Sprite pHitSprite = boardService.getSprite(lz & kHitIndexMask);

            if (pHitSprite != null && (pHitSprite.getCstat() & 33) == 33) {
                psectlotag = 0;
                p.footprintcount = 0;
                p.spritebridge = 1;
            } else if (badguy(pHitSprite) && pHitSprite.getXrepeat() > 24 && klabs(psp.getZ() - pHitSprite.getZ()) < (84 << 8)) {
                int j = EngineUtils.getAngle(pHitSprite.getX() - p.posx, pHitSprite.getY() - p.posy);
                p.posxv -= EngineUtils.cos(j & 2047) << 4;
                p.posyv -= EngineUtils.sin(j & 2047) << 4;
            }
        }

        if (psp.getExtra() > 0) {
            incur_damage(p);
        } else {
            psp.setExtra(0);
            p.shield_amount = 0;
        }

        p.last_extra = psp.getExtra();

        if (p.loogcnt > 0) {
            p.loogcnt--;
        } else {
            p.loogcnt = 0;
        }

        if (p.fist_incs != 0) {
            p.fist_incs++;
            if (p.fist_incs == 28) {
                gDemoScreen.onStopRecord();
                sound(PIPEBOMB_EXPLODE);
                p.pals[0] = 64;
                p.pals[1] = 64;
                p.pals[2] = 64;
                p.pals_time = 48;
            }

            if (p.fist_incs > 42) {
                if (p.buttonpalette != 0 && ud.from_bonus == 0) {
                    ud.from_bonus = ud.level_number + 1;
                    if (ud.secretlevel > 0 && ud.secretlevel < 12) {
                        ud.level_number = ud.secretlevel - 1;
                    }
                } else {
                    if (ud.from_bonus != 0) {
                        ud.level_number = ud.from_bonus;
                        ud.from_bonus = 0;
                    } else {
                        ud.level_number++;
                    }
                }

                LeaveMap();
                p.fist_incs = 0;

                return;
            }
        }

        if (p.timebeforeexit > 1 && p.last_extra > 0) {
            p.timebeforeexit--;
            if (p.timebeforeexit == 26 * 5) {
                Sounds.stopAllSounds();
                clearsoundlocks();
                if (p.customexitsound >= 0) {
                    sound(p.customexitsound);
                    FTA(102, p);
                }
            } else if (p.timebeforeexit == 1) {
                LeaveMap();
                if (ud.from_bonus != 0) {
                    ud.level_number = ud.from_bonus;
                    ud.from_bonus = 0;
                } else {
                    ud.level_number++;
                }
                return;
            }
        }

        if (p.pals_time > 0) {
            p.pals_time--;
        }

        if (psp.getExtra() <= 0) {
            if (p.dead_flag == 0) {
                if (psp.getPal() != 1) {
                    p.pals[0] = 63;
                    p.pals[1] = 0;
                    p.pals[2] = 0;
                    p.pals_time = 63;
                    p.posz -= (16 << 8);
                    psp.setZ(psp.getZ() - (16 << 8));
                }

                if (ud.multimode < 2) {
                    gDemoScreen.onStopRecord();
                }

                if (psp.getPal() != 1) {
                    p.dead_flag = (short) ((512 - ((engine.krand() & 1) << 10) + (engine.krand() & 255) - 512) & 2047);
                }

                p.jetpack_on = 0;
                p.holoduke_on = -1;

                stopsound(DUKE_JETPACK_IDLE, p.i);
                if (p.scream_voice != null) {
                    p.scream_voice.stop();
                    p.scream_voice = null;
                }

                if (psp.getPal() != 1 && (psp.getCstat() & 32768) == 0) {
                    psp.setCstat(0);
                }

                if (ud.multimode > 1 && (psp.getPal() != 1 || (psp.getCstat() & 32768) != 0)) {
                    if (p.frag_ps != snum && p.frag_ps < ps.length) {
                        ps[p.frag_ps].frag++;
                        frags[p.frag_ps][snum]++;
                        if (snum == screenpeek) {
                            if (ud.user_name[p.frag_ps] != null && !ud.user_name[p.frag_ps].isEmpty()) {
                                buildString(currentGame.getCON().fta_quotes[115], 0, "KILLED BY ",
                                        ud.user_name[p.frag_ps]);
                            } else {
                                buildString(currentGame.getCON().fta_quotes[115], 0, "KILLED BY PLAYER ",
                                        1 + p.frag_ps);
                            }
                            FTA(115, p);
                        } else {
                            if (ud.user_name[snum] != null && !ud.user_name[snum].isEmpty()) {
                                buildString(currentGame.getCON().fta_quotes[116], 0, "KILLED ", ud.user_name[snum]);
                            } else {
                                buildString(currentGame.getCON().fta_quotes[116], 0, "KILLED PLAYER ", 1 + snum);
                            }
                            FTA(116, ps[p.frag_ps]);
                        }
                    } else {
                        p.fraggedself++;
                    }

                    p.frag_ps = (short) snum;
                }
            }

            if (psectlotag == 2) {
                if (p.on_warping_sector == 0) {
                    if (klabs(p.posz - fz) > (PHEIGHT >> 1)) {
                        p.posz += 348;
                    }
                } else {
                    psp.setZ(psp.getZ() - 512);
                    psp.setZvel(-348);
                }

                engine.clipmove(p.posx, p.posy, p.posz, p.cursectnum, 0, 0, 164, (4 << 8), (4 << 8), CLIPMASK0);

                if (clipmove_sectnum != -1) {
                    p.posx = clipmove_x;
                    p.posy = clipmove_y;
                    p.posz = clipmove_z;
                    p.cursectnum = clipmove_sectnum;
                }
            }

            p.oposx = p.posx;
            p.oposy = p.posy;
            p.oposz = p.posz;
            p.oang = p.ang;
            p.opyoff = p.pyoff;

            p.horiz = 100;
            p.horizoff = 0;

            int sect = engine.updatesector(p.posx, p.posy, p.cursectnum);
            if (sect != -1) {
                p.cursectnum = sect;
            }

            engine.pushmove(p.posx, p.posy, p.posz, p.cursectnum, 128, (4 << 8), (20 << 8), CLIPMASK0);

            if (pushmove_sectnum != -1) {
                p.posx = pushmove_x;
                p.posy = pushmove_y;
                p.posz = pushmove_z;
                p.cursectnum = pushmove_sectnum;
            }

            if (fz > cz + (16 << 8) && psp.getPal() != 1) {
                p.rotscrnang = (short) ((p.dead_flag + ((fz + p.posz) >> 7)) & 2047);
            }

            p.on_warping_sector = 0;
            return;
        }

        if (p.transporter_hold > 0) {
            p.transporter_hold--;
            if (p.transporter_hold == 0 && p.on_warping_sector != 0) {
                p.transporter_hold = 2;
            }
        }
        if (p.transporter_hold < 0) {
            p.transporter_hold++;
        }

        boolean SHOOTINCODE = false;
        if (p.newowner >= 0) {
            hittype[p.i].tempang = (int) p.ang;
            Sprite spo = boardService.getSprite(p.newowner);
            if (spo != null) {
                p.posx = spo.getX();
                p.posy = spo.getY();
                p.posz = spo.getZ();
                p.ang = spo.getAng();
            }

            psp.setXvel(0);
            p.posxv = p.posyv = 0;
            p.look_ang = 0;
            p.rotscrnang = 0;

            doincrements(p);

            if (p.curr_weapon == HANDREMOTE_WEAPON) {
                SHOOTINCODE = true;
            } else {
                return;
            }
        }

        if (!SHOOTINCODE) {
            int doubvel = TICSPERFRAME;

            if (p.rotscrnang > 0) {
                p.rotscrnang -= (short) ((p.rotscrnang >> 1) + 1);
            } else if (p.rotscrnang < 0) {
                p.rotscrnang += (short) (((-p.rotscrnang) >> 1) + 1);
            }

            p.look_ang -= (p.look_ang >> 2);

            if ((sb_snum & (1 << 6)) != 0) {
                p.look_ang -= 152;
                p.rotscrnang += 24;
            }

            if ((sb_snum & (1 << 7)) != 0) {
                p.look_ang += 152;
                p.rotscrnang -= 24;
            }

            if (p.on_crane < 0) {
                if (psp.getXvel() < 32 || !p.on_ground || p.bobcounter == 1024) {
                    if ((p.weapon_sway & 2047) > (1024 + 96)) {
                        p.weapon_sway -= 96;
                    } else if ((p.weapon_sway & 2047) < (1024 - 96)) {
                        p.weapon_sway += 96;
                    } else {
                        p.weapon_sway = 1024;
                    }
                } else {
                    p.weapon_sway = p.bobcounter;
                }

                psp.setXvel((short) EngineUtils.sqrt(
                        (p.posx - p.bobposx) * (p.posx - p.bobposx) + (p.posy - p.bobposy) * (p.posy - p.bobposy)));
                if (p.on_ground) {
                    p.bobcounter += psp.getXvel() >> 1;
                }

                Sector psec2 = boardService.getSector(p.cursectnum);
                if (!ud.clipping && (psec2 == null || psec2.getFloorpicnum() == MIRROR)) {
                    p.posx = p.oposx;
                    p.posy = p.oposy;
                } else {
                    p.oposx = p.posx;
                    p.oposy = p.posy;
                }

                p.bobposx = p.posx;
                p.bobposy = p.posy;

                p.oposz = p.posz;
                p.opyoff = p.pyoff;
                p.oang = p.ang;

                if (p.one_eighty_count < 0) {
                    p.one_eighty_count += 128;
                    p.ang += 128;
                }

                // Shrinking code

                int zoffs = 40;

                if (psectlotag == 2) {
                    p.jumping_counter = 0;

                    p.pycount += 32;
                    p.pycount &= 2047;
                    p.pyoff = EngineUtils.sin(p.pycount) >> 7;

                    if (Sound[DUKE_UNDERWATER].getSoundOwnerCount() == 0) {
                        spritesound(DUKE_UNDERWATER, pi);
                    }

                    if ((sb_snum & 1) != 0) {
                        if (p.poszv > 0) {
                            p.poszv = 0;
                        }
                        p.poszv -= 348;
                        if (p.poszv < -(256 * 6)) {
                            p.poszv = -(256 * 6);
                        }
                    } else if ((sb_snum & (1 << 1)) != 0) {
                        if (p.poszv < 0) {
                            p.poszv = 0;
                        }
                        p.poszv += 348;
                        if (p.poszv > (256 * 6)) {
                            p.poszv = (256 * 6);
                        }
                    } else {
                        if (p.poszv < 0) {
                            p.poszv += 256;
                            if (p.poszv > 0) {
                                p.poszv = 0;
                            }
                        }
                        if (p.poszv > 0) {
                            p.poszv -= 256;
                            if (p.poszv < 0) {
                                p.poszv = 0;
                            }
                        }
                    }

                    if (p.poszv > 2048) {
                        p.poszv >>= 1;
                    }

                    p.posz += p.poszv;

                    if (p.posz > (fz - (15 << 8))) {
                        p.posz += ((fz - (15 << 8)) - p.posz) >> 1;
                    }

                    if (p.posz < (cz + (4 << 8))) {
                        p.posz = cz + (4 << 8);
                        p.poszv = 0;
                    }

                    if (p.scuba_on != 0 && (engine.krand() & 255) < 8) {
                        Sprite bubbl = boardService.getSprite(spawn(pi, WATERBUBBLE));
                        if (bubbl != null) {
                            if (IsOriginalDemo()) {
                                bubbl.setX(bubbl.getX() + (EngineUtils.cos(((int) p.ang + 64 - (global_random & 128)) & 2047) >> 6));
                                bubbl.setY(bubbl.getY() + (EngineUtils.sin(((int) p.ang + 64 - (global_random & 128)) & 2047) >> 6));
                            } else {
                                bubbl.setX((int) (bubbl.getX() + BCosAngle(BClampAngle(p.ang + 64 - (global_random & 128)) / 64.0f)));
                                bubbl.setY((int) (bubbl.getY() + BSinAngle(BClampAngle(p.ang + 64 - (global_random & 128)) / 64.0f)));
                            }
                            bubbl.setXrepeat(3);
                            bubbl.setYrepeat(2);
                            bubbl.setZ(p.posz + (8 << 8));
                        }
                    }
                } else
                    if (p.jetpack_on != 0) {
                    p.on_ground = false;
                    p.jumping_counter = 0;
                    p.hard_landing = 0;
                    p.falling_counter = 0;

                    p.pycount += 32;
                    p.pycount &= 2047;
                    p.pyoff = EngineUtils.sin(p.pycount) >> 7;

                    if (p.jetpack_on < 11) {
                        p.jetpack_on++;
                        p.posz -= (p.jetpack_on << 7); // Goin up
                    } else if (p.jetpack_on == 11 && Sound[DUKE_JETPACK_IDLE].getSoundOwnerCount() < 1) {
                        spritesound(DUKE_JETPACK_IDLE, pi);
                    }

                    int j, k;
                    if (shrunk) {
                        j = 512;
                    } else {
                        j = 2048;
                    }

                    if ((sb_snum & 1) != 0) // A (soar high)
                    {
                        p.posz -= j;
                        p.crack_time = 777;
                    }

                    if ((sb_snum & (1 << 1)) != 0) // Z (soar low)
                    {
                        p.posz += j;
                        p.crack_time = 777;
                    }

                    if (!shrunk && psectlotag == 0) {
                        k = 32;
                    } else {
                        k = 16;
                    }

                    if (p.scuba_on == 1) {
                        p.scuba_on = 0;
                    }

                    if (p.posz > (fz - (k << 8))) {
                        p.posz += ((fz - (k << 8)) - p.posz) >> 1;
                    }
                    if (p.posz < (hittype[pi].ceilingz + (18 << 8))) {
                        p.posz = hittype[pi].ceilingz + (18 << 8);
                    }

                } else {
                    if (p.airleft != 15 * 26) {
                        p.airleft = 15 * 26; // Aprox twenty seconds.
                    }

                    if (p.scuba_on == 1) {
                        p.scuba_on = 0;
                    }

                    if (psectlotag == 1 && p.spritebridge == 0) {
                        if (!shrunk) {
                            zoffs = 34;
                            p.pycount += 32;
                            p.pycount &= 2047;
                            p.pyoff = EngineUtils.sin(p.pycount) >> 6;
                        } else {
                            zoffs = 12;
                        }

                        if (!shrunk && truefdist <= PHEIGHT) {
                            if (p.on_ground) {
                                if (p.dummyplayersprite == -1) {
                                    p.dummyplayersprite = (short) spawn(pi, PLAYERONWATER);
                                }

                                p.footprintcount = 6;

                                if (psec2 != null && psec2.getFloorpicnum() == FLOORSLIME) {
                                    p.footprintpal = 8;
                                } else {
                                    p.footprintpal = 0;
                                }
                                p.footprintshade = 0;
                            }
                        }
                    } else {
                        if (p.footprintcount > 0 && p.on_ground) {
                            if (psec2 != null && (psec2.getFloorstat() & 2) != 2) {
                                ListNode<Sprite> node = boardService.getSectNode(psect);
                                for (; node != null; node = node.getNext()) {
                                    Sprite sj = node.get();
                                    if (sj.getPicnum() == FOOTPRINTS || sj.getPicnum() == FOOTPRINTS2
                                            || sj.getPicnum() == FOOTPRINTS3 || sj.getPicnum() == FOOTPRINTS4) {
                                        if (klabs(sj.getX() - p.posx) < 384) {
                                            if (klabs(sj.getY() - p.posy) < 384) {
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (node == null) {
                                    p.footprintcount--;
                                    if (psec2.getLotag() == 0 && psec2.getHitag() == 0) {
                                        int foot;
                                        switch (engine.krand() & 3) {
                                            case 0:
                                                foot = spawn(pi, FOOTPRINTS);
                                                break;
                                            case 1:
                                                foot = spawn(pi, FOOTPRINTS2);
                                                break;
                                            case 2:
                                                foot = spawn(pi, FOOTPRINTS3);
                                                break;
                                            default:
                                                foot = spawn(pi, FOOTPRINTS4);
                                                break;
                                        }

                                        Sprite sj = boardService.getSprite(foot);
                                        if (sj != null) {
                                            sj.setPal(p.footprintpal);
                                            sj.setShade((byte) p.footprintshade);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (p.posz < (fz - (zoffs << 8))) // falling
                    {
                        if ((sb_snum & 3) == 0 && p.on_ground && (psec.getFloorstat() & 2) != 0
                                && p.posz >= (fz - (zoffs << 8) - (16 << 8))) {
                            p.posz = fz - (zoffs << 8);
                        } else {
                            p.on_ground = false;
                            p.poszv += (currentGame.getCON().gc + 80); // (TICSPERFRAME<<6);
                            if (p.poszv >= (4096 + 2048)) {
                                p.poszv = (4096 + 2048);
                            }
                            if (p.poszv > 2400 && p.falling_counter < 255) {
                                p.falling_counter++;
                                if (p.falling_counter == 38) {
                                    p.scream_voice = spritesound(DUKE_SCREAM, pi);
                                }
                            }

                            if ((p.posz + p.poszv) >= (fz - (zoffs << 8))) // hit the ground
                            {
                                if (psec2 != null && psec2.getLotag() != 1) {
                                    if (p.falling_counter > 62) {
                                        quickkill(p);
                                    } else if (p.falling_counter > 9) {
                                        psp.setExtra(psp.getExtra() - (p.falling_counter - (engine.krand() & 3)));
                                        if (psp.getExtra() <= 0) {
                                            spritesound(SQUISHED, pi);
                                        } else {
                                            spritesound(DUKE_LAND, pi);
                                            spritesound(DUKE_LAND_HURT, pi);
                                        }

                                        p.pals[0] = 16;
                                        p.pals[1] = 0;
                                        p.pals[2] = 0;
                                        p.pals_time = 32;
                                    } else if (p.poszv > 2048) {
                                        spritesound(DUKE_LAND, pi);
                                    }
                                }
                            }
                        }
                    } else {
                        p.falling_counter = 0;
                        if (p.scream_voice != null) {
                            p.scream_voice.stop();
                            p.scream_voice = null;
                        }

                        if (psectlotag != 1 && !p.on_ground && p.poszv > 6144 >> 1) {
                            p.hard_landing = (short) (p.poszv >> 10);
                        }

                        p.on_ground = true;

                        if (zoffs == 40) {
                            // Smooth on the ground
                            int k = ((fz - (zoffs << 8)) - p.posz) >> 1;
                            if (klabs(k) < 256) {
                                k = 0;
                            }
                            p.posz += k;
                            p.poszv -= 768;
                            if (p.poszv < 0) {
                                p.poszv = 0;
                            }
                        } else if (p.jumping_counter == 0) {
                            p.posz += ((fz - (zoffs << 7)) - p.posz) >> 1; // Smooth on the water
                            if (p.on_warping_sector == 0 && p.posz > fz - (16 << 8)) {
                                p.posz = fz - (16 << 8);
                                p.poszv >>= 1;
                            }
                        }

                        p.on_warping_sector = 0;

                        if ((sb_snum & 2) != 0) {
                            p.posz += (2048 + 768);
                            p.crack_time = 777;
                        }

                        if ((sb_snum & 1) == 0 && p.jumping_toggle == 1) {
                            p.jumping_toggle = 0;
                        } else if ((sb_snum & 1) != 0 && p.jumping_toggle == 0) {
                            if (p.jumping_counter == 0) {
                                if ((fz - cz) > (56 << 8)) {
                                    p.jumping_counter = 1;
                                    p.jumping_toggle = 1;
                                }
                            }
                        }

                        if (p.jumping_counter != 0 && (sb_snum & 1) == 0) {
                            p.jumping_toggle = 0;
                        }
                    }

                    if (p.jumping_counter != 0) {
                        p.crouch_toggle = 0;
                        if ((sb_snum & 1) == 0 && p.jumping_toggle == 1) {
                            p.jumping_toggle = 0;
                        }

                        if (p.jumping_counter < (1024 + 256)) {
                            if (psectlotag == 1 && p.jumping_counter > 768) {
                                p.jumping_counter = 0;
                                p.poszv = -512;
                            } else {
                                p.poszv -= (EngineUtils.sin((2048 - 128 + p.jumping_counter) & 2047)) / 12;
                                p.jumping_counter += 180;
                                p.on_ground = false;
                            }
                        } else {
                            p.jumping_counter = 0;
                            p.poszv = 0;
                        }
                    }

                    p.posz += p.poszv;

                    if (p.posz < (cz + (4 << 8))) {
                        p.jumping_counter = 0;
                        if (p.poszv < 0) {
                            p.posxv = p.posyv = 0;
                        }
                        p.poszv = 128;
                        p.posz = cz + (4 << 8);
                    }
                }

                // Do the quick lefts and rights
                if (p.fist_incs != 0 || p.transporter_hold > 2 || p.hard_landing != 0 || p.access_incs > 0
                        || p.knee_incs > 0
                        || (p.curr_weapon == TRIPBOMB_WEAPON && p.kickback_pic > 1 && p.kickback_pic < 4)) {
                    doubvel = 0;
                    p.posxv = 0;
                    p.posyv = 0;
                } else if (sync[snum].avel != 0) // ENGINE calculates angvel for you
                {
                    if (IsOriginalDemo()) {
                        int tempang = ((int) sync[snum].avel) << 1;
                        if (psectlotag == 2) {
                            p.angvel = (tempang - (tempang >> 3)) * ksgn(doubvel);
                        } else {
                            p.angvel = tempang * ksgn(doubvel);
                        }

                        p.ang += p.angvel;
                        p.ang = (int) p.ang & 2047;
                    } else {
                        float tempang = (sync[snum].avel * 2f);
                        if (psectlotag == 2) {
                            p.angvel = ((tempang - (tempang / 8.0f)) * sgn(doubvel));
                        } else {
                            p.angvel = (tempang * sgn(doubvel));
                        }

                        p.ang += p.angvel;
                        p.ang = BClampAngle(p.ang);
                    }

                    p.crack_time = 777;
                }

                Sector pssec = boardService.getSector(psp.getSectnum());
                if (p.spritebridge == 0 && pssec != null) {
                    int floorpicnum = pssec.getFloorpicnum();

                    if (floorpicnum == PURPLELAVA || pssec.getCeilingpicnum() == PURPLELAVA) {
                        if (p.boot_amount > 0) {
                            p.boot_amount--;
                            p.inven_icon = 7;
                            if (p.boot_amount <= 0) {
                                checkavailinven(p);
                            }
                        } else {
                            if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                spritesound(DUKE_LONGTERM_PAIN, pi);
                            }
                            p.pals[0] = 0;
                            p.pals[1] = 8;
                            p.pals[2] = 0;
                            p.pals_time = 32;
                            psp.setExtra(psp.getExtra() - 1);
                        }
                    }

                    int k = 0;
                    if (p.on_ground && truefdist <= PHEIGHT + (16 << 8)) {
                        switch (floorpicnum) {
                            case HURTRAIL:
                                if (rnd(32)) {
                                    if (p.boot_amount > 0) {
                                        k = 1;
                                    } else {
                                        if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                            spritesound(DUKE_LONGTERM_PAIN, pi);
                                        }
                                        p.pals[0] = 64;
                                        p.pals[1] = 64;
                                        p.pals[2] = 64;
                                        p.pals_time = 32;
                                        psp.setExtra(psp.getExtra() - (1 + (engine.krand() & 3)));
                                        if (Sound[SHORT_CIRCUIT].getSoundOwnerCount() < 1) {
                                            spritesound(SHORT_CIRCUIT, pi);
                                        }
                                    }
                                }
                                break;
                            case FLOORSLIME:
                                if (rnd(16)) {
                                    if (p.boot_amount > 0) {
                                        k = 1;
                                    } else {
                                        if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                            spritesound(DUKE_LONGTERM_PAIN, pi);
                                        }
                                        p.pals[0] = 0;
                                        p.pals[1] = 8;
                                        p.pals[2] = 0;
                                        p.pals_time = 32;
                                        psp.setExtra(psp.getExtra() - (1 + (engine.krand() & 3)));
                                    }
                                }
                                break;
                            case FLOORPLASMA:
                                if (rnd(32)) {
                                    if (p.boot_amount > 0) {
                                        k = 1;
                                    } else {
                                        if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                            spritesound(DUKE_LONGTERM_PAIN, pi);
                                        }
                                        p.pals[0] = 8;
                                        p.pals[1] = 0;
                                        p.pals[2] = 0;
                                        p.pals_time = 32;
                                        psp.setExtra(psp.getExtra() - (1 + (engine.krand() & 3)));
                                    }
                                }
                                break;
                        }
                    }

                    if (k != 0) {
                        FTA(75, p);
                        p.boot_amount -= 2;
                        if (p.boot_amount <= 0) {
                            checkavailinven(p);
                        }
                    }
                }

                if (p.posxv != 0 || p.posyv != 0 || sync[snum].fvel != 0 || sync[snum].svel != 0) {
                    p.crack_time = 777;

                    int k = EngineUtils.sin(p.bobcounter & 2047) >> 12;

                    if (truefdist < PHEIGHT + (8 << 8)) {
                        if (k == 1 || k == 3) {
                            if (p.spritebridge == 0 && p.walking_snd_toggle == 0 && p.on_ground) {
                                switch (psectlotag) {
                                    case 0: {
                                        int pic = 0;
                                        if ((lz & kHitTypeMask) == kHitSprite) {
                                            Sprite hsp = boardService.getSprite(lz & kHitIndexMask);
                                            if (hsp != null) {
                                                pic = hsp.getPicnum();
                                            }
                                        } else {
                                            pic = psec.getFloorpicnum();
                                        }

                                        switch (pic) {
                                            case PANNEL1:
                                            case PANNEL2:
                                                spritesound(DUKE_WALKINDUCTS, pi);
                                                p.walking_snd_toggle = 1;
                                                break;
                                        }
                                        break;
                                    }
                                    case 1:
                                        if ((engine.krand() & 1) == 0) {
                                            spritesound(DUKE_ONWATER, pi);
                                        }
                                        p.walking_snd_toggle = 1;
                                        break;
                                }
                            }
                        } else if (p.walking_snd_toggle > 0) {
                            p.walking_snd_toggle--;
                        }
                    }

                    if (p.jetpack_on == 0 && p.steroids_amount > 0 && p.steroids_amount < 400) {
                        doubvel <<= 1;
                    }

                    p.posxv += ((sync[snum].fvel * doubvel) << 6);
                    p.posyv += ((sync[snum].svel * doubvel) << 6);

                    if ((p.curr_weapon == KNEE_WEAPON && p.kickback_pic > 10 && p.on_ground)
                            || (p.on_ground && (sb_snum & 2) != 0)) {
                        p.posxv = mulscale(p.posxv, currentGame.getCON().dukefriction - 0x2000, 16);
                        p.posyv = mulscale(p.posyv, currentGame.getCON().dukefriction - 0x2000, 16);
                    } else {
                        if (psectlotag == 2) {
                            p.posxv = mulscale(p.posxv, currentGame.getCON().dukefriction - 0x1400, 16);
                            p.posyv = mulscale(p.posyv, currentGame.getCON().dukefriction - 0x1400, 16);
                        } else {
                            p.posxv = mulscale(p.posxv, currentGame.getCON().dukefriction, 16);
                            p.posyv = mulscale(p.posyv, currentGame.getCON().dukefriction, 16);
                        }
                    }

                    if (abs(p.posxv) < 2048 && abs(p.posyv) < 2048) {
                        p.posxv = p.posyv = 0;
                    }

                    if (shrunk) {
                        p.posxv = mulscale(p.posxv, currentGame.getCON().dukefriction
                                        - (currentGame.getCON().dukefriction >> 1) + (currentGame.getCON().dukefriction >> 2),
                                16);
                        p.posyv = mulscale(p.posyv, currentGame.getCON().dukefriction
                                        - (currentGame.getCON().dukefriction >> 1) + (currentGame.getCON().dukefriction >> 2),
                                16);
                    }
                }
            }

            int i = (20 << 8);
            if (psectlotag == 1 || p.spritebridge == 1) {
                i = (4 << 8);
            }

            int moveHit;
            if (ud.clipping) {
                moveHit = 0;
                p.posx += p.posxv >> 14;
                p.posy += p.posyv >> 14;
                int sect = engine.updatesector(p.posx, p.posy, p.cursectnum);
                if (sect != -1) {
                    p.cursectnum = sect;
                }
                engine.changespritesect(pi, p.cursectnum);
            } else {

                moveHit = engine.clipmove(p.posx, p.posy, p.posz, p.cursectnum, p.posxv, p.posyv, 164, (4 << 8), i,
                        CLIPMASK0);

                if (clipmove_sectnum != -1) {
                    p.posx = clipmove_x;
                    p.posy = clipmove_y;
                    p.posz = clipmove_z;
                    p.cursectnum = clipmove_sectnum;
                }
            }

            if (p.jetpack_on == 0 && psectlotag != 2 && psectlotag != 1 && shrunk) {
                p.posz += 32 << 8;
            }

            if (moveHit != 0) {
                checkplayerhurt(p, moveHit);
            }

            if (p.jetpack_on == 0) {
                if (psp.getXvel() > 16) {
                    if (psectlotag != 1 && psectlotag != 2 && p.on_ground) {
                        p.pycount += 52;
                        p.pycount &= 2047;
                        p.pyoff = klabs(psp.getXvel() * EngineUtils.sin(p.pycount)) / 1596;
                    }
                } else if (psectlotag != 2 && psectlotag != 1) {
                    p.pyoff = 0;
                }
            }

            // RBG***
            engine.setsprite(pi, p.posx, p.posy, p.posz + PHEIGHT);

            if (psectlotag < 3) {
                psect = psp.getSectnum();
                psec = boardService.getSector(psect);
                if (!ud.clipping && psec != null && psec.getLotag() == 31) {
                    Sprite pspr = boardService.getSprite(psec.getHitag());
                    if (pspr != null && pspr.getXvel() != 0 && hittype[psec.getHitag()].temp_data[0] == 0) {
                        quickkill(p);
                        return;
                    }
                }
            }

            Sector psec2 = boardService.getSector(p.cursectnum);
            if (psec2 != null && truefdist < PHEIGHT && p.on_ground && psectlotag != 1 && !shrunk && psec2.getLotag() == 1) {
                if (Sound[DUKE_ONWATER].getSoundOwnerCount() == 0) {
                    spritesound(DUKE_ONWATER, pi);
                }
            }

            if (p.cursectnum != psp.getSectnum() && psec2 != null) {
                engine.changespritesect(pi, p.cursectnum);
            }

            int pushMove = 0;
            if (!ud.clipping) {
                pushMove = (engine.pushmove(p.posx, p.posy, p.posz, p.cursectnum, 164, (4 << 8), (4 << 8), CLIPMASK0) < 0
                        && furthestangle(pi, 8) < 512) ? 1 : 0;

                if (pushmove_sectnum != -1) {
                    p.posx = pushmove_x;
                    p.posy = pushmove_y;
                    p.posz = pushmove_z;
                    p.cursectnum = pushmove_sectnum;
                }
            }

            if (!ud.clipping) {
                if (klabs(hittype[pi].floorz - hittype[pi].ceilingz) < (48 << 8) || pushMove != 0) {
                    Sector pssec = boardService.getSector(psp.getSectnum());
                    if (pssec != null && (pssec.getLotag() & 0x8000) == 0
                            && (isanunderoperator(pssec.getLotag()) || isanearoperator(pssec.getLotag()))) {
                        activatebysector(psp.getSectnum(), pi);
                    }
                    if (pushMove != 0) {
                        quickkill(p);
                        return;
                    }
                } else if (klabs(fz - cz) < (32 << 8) && psec != null && isanunderoperator(psec.getLotag())) {
                    activatebysector(psect, pi);
                }
            }

            if ((sb_snum & (1 << 18)) != 0 || p.hard_landing != 0) {
                p.return_to_center = 9;
            }

            if ((sb_snum & (1 << 13)) != 0) {
                p.return_to_center = 9;
                if ((sb_snum & (1 << 5)) != 0) {
                    p.horiz += 12;
                }
                p.horiz += 12;
            } else if ((sb_snum & (1 << 14)) != 0) {
                p.return_to_center = 9;
                if ((sb_snum & (1 << 5)) != 0) {
                    p.horiz -= 12;
                }
                p.horiz -= 12;
            } else if ((sb_snum & (1 << 3)) != 0) {
                if ((sb_snum & (1 << 5)) != 0) {
                    p.horiz += 6;
                }
                p.horiz += 6;
            } else if ((sb_snum & (1 << 4)) != 0) {
                if ((sb_snum & (1 << 5)) != 0) {
                    p.horiz -= 6;
                }
                p.horiz -= 6;
            }
            if (p.return_to_center > 0) {
                if ((sb_snum & (1 << 13)) == 0 && (sb_snum & (1 << 14)) == 0) {
                    p.return_to_center--;
                    if (IsOriginalDemo()) {
                        int dh = (int) p.horiz / 3;
                        p.horiz += (33 - dh);
                    } else {
                        p.horiz += 33 - (p.horiz / 3);
                    }
                }
            }

            if (p.hard_landing > 0) {
                p.hard_landing--;
                p.horiz -= (p.hard_landing << 4);
            }

            if (p.aim_mode != 0) {
                if (IsOriginalDemo()) {
                    p.horiz += (int) sync[snum].horz >> 1;
                } else {
                    p.horiz += sync[snum].horz / 2;
                }
            } else {
                if (p.horiz > 95 && p.horiz < 105) {
                    p.horiz = 100;
                }
                if (p.horizoff > -5 && p.horizoff < 5) {
                    p.horizoff = 0;
                }
            }

            if (p.horiz > 299) {
                p.horiz = 299;
            } else if (p.horiz < -99) {
                p.horiz = -99;
            }

            // Shooting code/changes

            if (p.show_empty_weapon > 0) {
                p.show_empty_weapon--;
                if (p.show_empty_weapon == 0) {
                    if (p.last_full_weapon == GROW_WEAPON) {
                        p.subweapon |= (1 << GROW_WEAPON);
                    } else if (p.last_full_weapon == SHRINKER_WEAPON) {
                        p.subweapon &= ~(1 << GROW_WEAPON);
                    }
                    addweapon(p, p.last_full_weapon);
                    return;
                }
            }

            if (p.knee_incs > 0) {
                p.knee_incs++;
                p.horiz -= 48;
                p.return_to_center = 9;
                if (p.knee_incs > 15) {
                    p.knee_incs = 0;
                    p.holster_weapon = 0;
                    if (p.weapon_pos < 0) {
                        p.weapon_pos = (short) -p.weapon_pos;
                    }
                    Sprite squ = boardService.getSprite(p.actorsqu);
                    if (squ != null && dist(psp, squ) < 1400) {
                        guts(squ, JIBS6, 7, myconnectindex);
                        spawn(p.actorsqu, BLOODPOOL);
                        spritesound(SQUISHED, p.actorsqu);
                        switch (squ.getPicnum()) {
                            case FEM1:
                            case FEM2:
                            case FEM3:
                            case FEM4:
                            case FEM5:
                            case FEM6:
                            case FEM7:
                            case FEM8:
                            case FEM9:
                            case FEM10:
                            case PODFEM1:
                            case NAKED1:
                            case STATUE:
                                if (squ.getYvel() != 0) {
                                    operaterespawns(squ.getYvel());
                                }
                                break;
                        }

                        if (squ.getPicnum() == APLAYER) {
                            quickkill(ps[squ.getYvel()]);
                            ps[squ.getYvel()].frag_ps = (short) snum;
                        } else if (badguy(squ)) {
                            engine.deletesprite(p.actorsqu);
                            ps[connecthead].actors_killed++;
                        } else {
                            engine.deletesprite(p.actorsqu);
                        }
                    }
                    p.actorsqu = -1;
                } else {
                    Sprite squ = boardService.getSprite(p.actorsqu);
                    if (squ != null) {
                        p.ang += getincangle(p.ang,
                                EngineUtils.getAngle(squ.getX() - p.posx, squ.getY() - p.posy)) / 4.0f;
                    }
                }
            }

            if (doincrements(p)) {
                return;
            }

            if (p.weapon_pos != 0) {
                if (p.weapon_pos == -9) {
                    if (p.last_weapon >= 0) {
                        p.weapon_pos = 10;
                        p.last_weapon = -1;
                    } else if (p.holster_weapon == 0) {
                        p.weapon_pos = 10;
                    }
                } else {
                    p.weapon_pos--;
                }
            }
        }

        if (!shrunk) {
            weaponprocess(snum);
        }
    }

    // UPDATE THIS FILE OVER THE OLD GETSPRITESCORE/COMPUTERGETINPUT FUNCTIONS
    public static int getspritescore(int snum, int dapicnum) {
        switch (dapicnum) {
            case FIRSTGUNSPRITE:
                return (5);
            case CHAINGUNSPRITE:
            case TRIPBOMBSPRITE:
                return (50);
            case RPGSPRITE:
                return (200);
            case FREEZESPRITE:
                return (25);
            case SHRINKERSPRITE:
                return (80);
            case HEAVYHBOMB:
                return (60);
            case SHOTGUNSPRITE:
            case DEVISTATORSPRITE:
                return (120);

            case FREEZEAMMO:
                if (ps[snum].ammo_amount[FREEZE_WEAPON] < currentGame.getCON().max_ammo_amount[FREEZE_WEAPON]) {
                    return (10);
                } else {
                    return (0);
                }
                // Twentieth Anniversary World Tour
            case FLAMETHROWERAMMO:
                if (ps[snum].ammo_amount[FLAMETHROWER_WEAPON] < currentGame.getCON().max_ammo_amount[FLAMETHROWER_WEAPON]) {
                    return (10);
                } else {
                    return (0);
                }
            case AMMO:
                if (ps[snum].ammo_amount[SHOTGUN_WEAPON] < currentGame.getCON().max_ammo_amount[SHOTGUN_WEAPON]) {
                    return (10);
                } else {
                    return (0);
                }
            case BATTERYAMMO:
                if (ps[snum].ammo_amount[CHAINGUN_WEAPON] < currentGame.getCON().max_ammo_amount[CHAINGUN_WEAPON]) {
                    return (20);
                } else {
                    return (0);
                }
            case DEVISTATORAMMO:
                if (ps[snum].ammo_amount[DEVISTATOR_WEAPON] < currentGame.getCON().max_ammo_amount[DEVISTATOR_WEAPON]) {
                    return (25);
                } else {
                    return (0);
                }
            case RPGAMMO:
                if (ps[snum].ammo_amount[RPG_WEAPON] < currentGame.getCON().max_ammo_amount[RPG_WEAPON]) {
                    return (50);
                } else {
                    return (0);
                }
            case CRYSTALAMMO:
                if (ps[snum].ammo_amount[SHRINKER_WEAPON] < currentGame.getCON().max_ammo_amount[SHRINKER_WEAPON]) {
                    return (10);
                } else {
                    return (0);
                }
            case HBOMBAMMO:
                if (ps[snum].ammo_amount[HANDBOMB_WEAPON] < currentGame.getCON().max_ammo_amount[HANDBOMB_WEAPON]) {
                    return (30);
                } else {
                    return (0);
                }
            case SHOTGUNAMMO:
                if (ps[snum].ammo_amount[SHOTGUN_WEAPON] < currentGame.getCON().max_ammo_amount[SHOTGUN_WEAPON]) {
                    return (25);
                } else {
                    return (0);
                }

            case COLA: {
                Sprite psp = boardService.getSprite(ps[snum].i);
                if (psp != null && psp.getExtra() < 100) {
                    return (10);
                }
                return (0);
            }
            case SIXPAK: {
                Sprite psp = boardService.getSprite(ps[snum].i);
                if (psp != null && psp.getExtra() < 100) {
                    return (30);
                }
                return (0);
            }
            case FIRSTAID:
                if (ps[snum].firstaid_amount < 100) {
                    return (100);
                } else {
                    return (0);
                }
            case SHIELD:
                if (ps[snum].shield_amount < 100) {
                    return (50);
                } else {
                    return (0);
                }
            case STEROIDS:
                if (ps[snum].steroids_amount < 400) {
                    return (30);
                } else {
                    return (0);
                }
            case AIRTANK:
                if (ps[snum].scuba_amount < 6400) {
                    return (30);
                } else {
                    return (0);
                }
            case JETPACK:
                if (ps[snum].jetpack_amount < 1600) {
                    return (100);
                } else {
                    return (0);
                }
            case HEATSENSOR:
                if (ps[snum].heat_amount < 1200) {
                    return (10);
                } else {
                    return (0);
                }
            case ACCESSCARD:
                return (1);
            case BOOTS:
                if (ps[snum].boot_amount < 200) {
                    return (50);
                } else {
                    return (0);
                }
            case ATOMICHEALTH: {
                Sprite psp = boardService.getSprite(ps[snum].i);
                if (psp != null && psp.getExtra() < currentGame.getCON().max_player_health) {
                    return (50);
                }
                return (0);
            }
            case HOLODUKE:
                if (ps[snum].holoduke_amount < 2400) {
                    return (30);
                } else {
                    return (0);
                }
        }
        return (0);
    }

    public static void computergetinput(int snum, Input syn) {
        int k, l, x1, y1, z1, x2, y2, z2, x3, y3, z3, dx, dy;
        int dist, daang, zang, fightdist, damyang;
        int damysect, dasect;

        PlayerStruct p = ps[snum];
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        syn.fvel = 0;
        syn.svel = 0;
        syn.avel = 0;
        syn.horz = 0;
        syn.bits = 0;

        x1 = psp.getX();
        y1 = psp.getY();
        z1 = psp.getZ();
        damyang = psp.getAng();
        damysect = psp.getSectnum();
        if ((numplayers >= 2) && (snum == myconnectindex)) {
            x1 = game.net.predict.x;
            y1 = game.net.predict.y;
            z1 = game.net.predict.z + PHEIGHT;
            damyang = (short) game.net.predict.ang;
            damysect = game.net.predict.sectnum;
        }

        final Sprite goalSprite = boardService.getSprite(ps[goalplayer[snum]].i);
        if (goalSprite != null && (numframes & 7) == 0) {
            x2 = goalSprite.getX();
            y2 = goalSprite.getY();
            z2 = goalSprite.getZ();

            if (!engine.cansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                    goalSprite.getSectnum())) {
                goalplayer[snum] = snum;
            }
        }

        if ((goalplayer[snum] == snum) || (ps[goalplayer[snum]].dead_flag != 0)) {
            int j = 0x7fffffff;
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (i != snum) {
                    Sprite playerSprite = boardService.getSprite(ps[i].i);
                    if (playerSprite == null) {
                        continue;
                    }

                    dist = EngineUtils.sqrt((playerSprite.getX() - x1) * (playerSprite.getX() - x1)
                            + (playerSprite.getY() - y1) * (playerSprite.getY() - y1));

                    x2 = playerSprite.getX();
                    y2 = playerSprite.getY();
                    z2 = playerSprite.getZ();
                    if (!engine.cansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                            playerSprite.getSectnum())) {
                        dist <<= 1;
                    }

                    if (dist < j) {
                        j = dist;
                        goalplayer[snum] = i;
                    }
                }
            }
        }

        if (goalSprite == null) {
            return;
        }

        x2 = goalSprite.getX();
        y2 = goalSprite.getY();
        z2 = goalSprite.getZ();

        if (p.dead_flag != 0) {
            syn.bits |= (1 << 29);
        }
        if ((p.firstaid_amount > 0) && (p.last_extra < 100)) {
            syn.bits |= (1 << 16);
        }

        for (ListNode<Sprite> node = boardService.getStatNode(4); node != null; node = node.getNext()) {
            Sprite spr = node.get();
            switch (spr.getPicnum()) {
                case TONGUE:
                case FREEZEBLAST:
                    k = 4;
                    break;
                case SHRINKSPARK:
                case RPG:
                    k = 16;
                    break;
                default:
                    k = 0;
                    break;
            }
            if (k != 0) {
                x3 = spr.getX();
                y3 = spr.getY();
                z3 = spr.getZ();
                for (l = 0; l <= 8; l++) {
                    if (tmulscale(x3 - x1, x3 - x1, y3 - y1, y3 - y1, (z3 - z1) >> 4, (z3 - z1) >> 4, 11) < 3072) {
                        dx = EngineUtils.cos(spr.getAng() & 2047);
                        dy = EngineUtils.sin(spr.getAng() & 2047);
                        int i = k * 512;
                        if ((x1 - x3) * dy > (y1 - y3) * dx) {
                            i *= -1;
                        }
                        syn.fvel -= (short) mulscale(dy, i, 17);
                        syn.svel += (short) mulscale(dx, i, 17);
                    }
                    if (l < 7) {
                        x3 += (mulscale(spr.getXvel(), EngineUtils.cos((spr.getAng()) & 2047), 14) << 2);
                        y3 += (mulscale(spr.getXvel(), EngineUtils.sin(spr.getAng() & 2047), 14) << 2);
                        z3 += (spr.getZvel() << 2);
                    } else {
                        engine.hitscan(spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum(),
                                mulscale(spr.getXvel(), EngineUtils.cos((spr.getAng()) & 2047), 14),
                                mulscale(spr.getXvel(), EngineUtils.sin(spr.getAng() & 2047), 14), spr.getZvel(), pHitInfo,
                                CLIPMASK1);

                        x3 = pHitInfo.hitx;
                        y3 = pHitInfo.hity;
                        z3 = pHitInfo.hitz;
                    }
                }
            }
        }

        if ((ps[goalplayer[snum]].dead_flag == 0)
                && ((engine.cansee(x1, y1, z1, damysect, x2, y2, z2, goalSprite.getSectnum()))
                || (engine.cansee(x1, y1, z1 - (24 << 8), damysect, x2, y2, z2 - (24 << 8),
                goalSprite.getSectnum()))
                || (engine.cansee(x1, y1, z1 - (48 << 8), damysect, x2, y2, z2 - (48 << 8),
                goalSprite.getSectnum())))) {
            syn.bits |= (1 << 2);

            if ((p.curr_weapon == HANDBOMB_WEAPON) && ((engine.krand() & 7)) == 0) {
                syn.bits &= ~(1 << 2);
            }

            if (p.curr_weapon == TRIPBOMB_WEAPON) {
                syn.bits |= ((engine.krand() % MAX_WEAPONS) << 8);
            }

            if (p.curr_weapon == RPG_WEAPON) {
                engine.hitscan(x1, y1, z1 - PHEIGHT, damysect, EngineUtils.cos((damyang) & 2047),
                        EngineUtils.sin(damyang & 2047), (int) (100 - p.horiz - p.horizoff) * 32, pHitInfo, CLIPMASK1);

                x3 = pHitInfo.hitx;
                y3 = pHitInfo.hity;

                if ((x3 - x1) * (x3 - x1) + (y3 - y1) * (y3 - y1) < 2560 * 2560) {
                    syn.bits &= ~(1 << 2);
                }
            }

            fightdist = fdmatrix[p.curr_weapon][ps[goalplayer[snum]].curr_weapon];
            if (fightdist < 128) {
                fightdist = 128;
            }
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist == 0) {
                dist = 1;
            }
            daang = EngineUtils.getAngle(x2 + (ps[goalplayer[snum]].posxv >> 14) - x1,
                    y2 + (ps[goalplayer[snum]].posyv >> 14) - y1);
            zang = 100 - ((z2 - z1) * 8) / dist;
            fightdist = max(fightdist, (klabs(z2 - z1) >> 4));

            if (goalSprite.getYrepeat() < 32) {
                fightdist = 0;
                syn.bits &= ~(1 << 2);
            }
            if (goalSprite.getPal() == 1) {
                fightdist = 0;
                syn.bits &= ~(1 << 2);
            }

            if (dist < 256) {
                syn.bits |= (1 << 22);
            }

            x3 = x2 + ((x1 - x2) * fightdist / dist);
            y3 = y2 + ((y1 - y2) * fightdist / dist);
            syn.fvel += (short) ((x3 - x1) * 2047 / dist);
            syn.svel += (short) ((y3 - y1) * 2047 / dist);

            // Strafe attack
            if (fightdist != 0) {
                int j = engine.getTotalClock() + snum * 13468;
                int i = EngineUtils.sin((j << 6) & 2047);
                i += EngineUtils.sin(((j + 4245) << 5) & 2047);
                i += EngineUtils.sin(((j + 6745) << 4) & 2047);
                i += EngineUtils.sin(((j + 15685) << 3) & 2047);
                dx = EngineUtils.cos((goalSprite.getAng()) & 2047);
                dy = EngineUtils.sin(goalSprite.getAng() & 2047);
                if ((x1 - x2) * dy > (y1 - y2) * dx) {
                    i += 8192;
                } else {
                    i -= 8192;
                }
                syn.fvel += (short) ((EngineUtils.cos((daang + 512) & 2047) * i) >> 17);
                syn.svel += (short) ((EngineUtils.sin((daang + 512) & 2047) * i) >> 17);
            }

            syn.avel = (short) min(max((((daang + 1024 - damyang) & 2047) - 1024) >> 1, -127), 127);
            syn.horz = min(max((zang - p.horiz) / 2.0f, -MAXHORIZ), MAXHORIZ);
            syn.bits |= (1 << 23);
            return;
        }

        goalsect[snum] = -1;
        if (goalsect[snum] < 0) {
            goalwall[snum] = -1;
            final int startsect = psp.getSectnum();
            int endsect = goalSprite.getSectnum();
            Sector startsec = boardService.getSector(startsect);
            if (startsec == null) {
                return;
            }

            Arrays.fill(dashow2dsector, (byte) 0);

            searchsect[0] = (short) startsect;
            searchparent[0] = -1;
            dashow2dsector[startsect >> 3] |= (byte) (1 << (startsect & 7));
            for (int splc = 0, send = 1; splc < send; splc++) {
                Sector searchsec = boardService.getSector(searchsect[splc]);
                if (searchsec == null) {
                    continue;
                }

                for (ListNode<Wall> wn = searchsec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();

                    int j = wal.getNextsector();
                    Sector nsec = boardService.getSector(j);
                    if (nsec == null) {
                        continue;
                    }

                    dx = ((wal.getWall2().getX() + wal.getX()) >> 1);
                    dy = ((wal.getWall2().getY() + wal.getY()) >> 1);
                    if ((engine.getceilzofslope(j, dx, dy) > engine.getflorzofslope(j, dx, dy)
                            - (28 << 8)) && ((nsec.getLotag() < 15) || (nsec.getLotag() > 22))) {
                        continue;
                    }
                    if (engine.getflorzofslope(j, dx, dy) < engine.getflorzofslope(searchsect[splc], dx, dy)
                            - (72 << 8)) {
                        continue;
                    }

                    if ((dashow2dsector[j >> 3] & (1 << (j & 7))) == 0) {
                        dashow2dsector[j >> 3] |= (byte) (1 << (j & 7));
                        searchsect[send] = (short) j;
                        searchparent[send] = (short) splc;
                        send++;
                        if (j == endsect) {
                            Arrays.fill(dashow2dsector, (byte) 0);
                            for (k = send - 1; k >= 0; k = searchparent[k]) {
                                dashow2dsector[searchsect[k] >> 3] |= (byte) (1 << (searchsect[k] & 7));
                            }

                            for (k = send - 1; k >= 0; k = searchparent[k]) {
                                if (searchparent[k] == 0) {
                                    break;
                                }
                            }

                            goalsect[snum] = searchsect[k];
                            Sector goalsec = boardService.getSector(goalsect[snum]);
                            if (goalsec == null) {
                                continue;
                            }

                            int startwall = goalsec.getWallptr();
                            int endwall = startwall + goalsec.getWallnum();

                            x3 = y3 = 0;
                            for (ListNode<Wall> wn2 = goalsec.getWallNode(); wn2 != null; wn2 = wn2.getNext()) {
                                Wall wal2 = wn2.get();

                                x3 += wal2.getX();
                                y3 += wal2.getY();
                            }
                            x3 /= (endwall - startwall);
                            y3 /= (endwall - startwall);

                            l = 0;
                            k = startsec.getWallptr();
                            for (ListNode<Wall> wn2 = startsec.getWallNode(); wn2 != null; wn2 = wn2.getNext()) {
                                Wall wal2 = wn2.get();

                                if (wal2.getNextsector() != goalsect[snum]) {
                                    continue;
                                }

                                dx = wal2.getWall2().getX() - wal2.getX();
                                dy = wal2.getWall2().getY() - wal2.getY();

                                // if (dx*(y1-wal2.y) <= dy*(x1-wal2.x))
                                // if (dx*(y2-wal2.y) >= dy*(x2-wal2.x))
                                if ((x3 - x1) * (wal2.getY() - y1) <= (y3 - y1) * (wal2.getX() - x1)) {
                                    if ((x3 - x1) * ( wal2.getWall2().getY() - y1) >= (y3 - y1)
                                            * ( wal2.getWall2().getX() - x1)) {
                                        k = wn2.getIndex();
                                        break;
                                    }
                                }

                                dist = EngineUtils.sqrt(dx * dx + dy * dy);
                                if (dist > l) {
                                    l = dist;
                                    k = wn2.getIndex();
                                }
                            }

                            Wall kwal = boardService.getWall(k);
                            if (kwal == null) {
                                break;
                            }

                            goalwall[snum] = k;
                            daang = ((kwal.getWallAngle() + 1536) & 2047);
                            goalx[snum] = ((kwal.getX() + kwal.getWall2().getX()) >> 1) + (EngineUtils.cos(daang & 2047) >> 8);
                            goaly[snum] = ((kwal.getY() + kwal.getWall2().getY()) >> 1) + (EngineUtils.sin(daang & 2047) >> 8);
                            goalz[snum] = goalsec.getFloorz() - (32 << 8);
                            break;
                        }
                    }
                }

                for (ListNode<Sprite> node = boardService.getSectNode(searchsect[splc]); node != null; node = node.getNext()) {
                    Sprite sp = node.get();
                    if (sp.getLotag() == 7) {
                        Sprite spo = boardService.getSprite(sp.getOwner());
                        if (spo == null) {
                            continue;
                        }

                        int j = spo.getSectnum();
                        if ((dashow2dsector[j >> 3] & (1 << (j & 7))) == 0) {
                            dashow2dsector[j >> 3] |= (byte) (1 << (j & 7));
                            searchsect[send] = (short) j;
                            searchparent[send] = (short) splc;
                            send++;

                            if (j == endsect) {
                                Arrays.fill(dashow2dsector, (byte) 0);
                                for (k = send - 1; k >= 0; k = searchparent[k]) {
                                    dashow2dsector[searchsect[k] >> 3] |= (byte) (1 << (searchsect[k] & 7));
                                }

                                for (k = send - 1; k >= 0; k = searchparent[k]) {
                                    if (searchparent[k] == 0) {
                                        break;
                                    }
                                }

                                goalsect[snum] = searchsect[k];

                                l = 0;
                                k = startsec.getWallptr();
                                for (ListNode<Wall> wn = startsec.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();

                                    dx = wal.getWall2().getX() - wal.getX();
                                    dy = wal.getWall2().getY() - wal.getY();
                                    dist = EngineUtils.sqrt(dx * dx + dy * dy);
                                    if ((wal.getNextsector() == goalsect[snum]) && (dist > l)) {
                                        l = dist;
                                        k = wn.getIndex();
                                    }
                                }

                                Wall kwal = boardService.getWall(k);
                                if (kwal == null) {
                                    break;
                                }

                                goalwall[snum] = k;
                                daang = ((kwal.getWallAngle() + 1536) & 2047);
                                goalx[snum] = ((kwal.getX() + kwal.getWall2().getX()) >> 1)
                                        + (EngineUtils.sin((daang + 512) & 2047) >> 8);
                                goaly[snum] = ((kwal.getY() + kwal.getWall2().getY()) >> 1)
                                        + (EngineUtils.sin(daang & 2047) >> 8);
                                Sector goalsec = boardService.getSector(goalsect[snum]);
                                if (goalsec != null) { // Why walls getting from startsector but goalz from goalsector?
                                    goalz[snum] = goalsec.getFloorz() - (32 << 8);
                                }
                                break;
                            }
                        }
                    }
                }
                if (goalwall[snum] >= 0) {
                    break;
                }
            }
        }

        if ((goalsect[snum] < 0) || (goalwall[snum] < 0)) {
            if (goalsprite[snum] < 0) {
                for (k = 0; k < 4; k++) {
                    int i = (engine.krand() % boardService.getSectorCount());
                    for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                        int j = node.getIndex();
                        Sprite sp = node.get();
                        if ((sp.getXrepeat() <= 0) || (sp.getYrepeat() <= 0)) {
                            continue;
                        }
                        if (getspritescore(snum, sp.getPicnum()) <= 0) {
                            continue;
                        }
                        if (engine.cansee(x1, y1, z1 - (32 << 8), damysect, sp.getX(), sp.getY(),
                                sp.getZ() - (4 << 8), (short) i)) {
                            goalx[snum] = sp.getX();
                            goaly[snum] = sp.getY();
                            goalz[snum] = sp.getZ();
                            goalsprite[snum] = j;
                            break;
                        }
                    }
                }
            }
            x2 = goalx[snum];
            y2 = goaly[snum];
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist == 0) {
                return;
            }
            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            syn.fvel += (short) ((x2 - x1) * 2047 / dist);
            syn.svel += (short) ((y2 - y1) * 2047 / dist);
            syn.avel = (byte) min(max((((daang + 1024 - damyang) & 2047) - 1024) >> 3, -127), 127);
        } else {
            goalsprite[snum] = -1;
        }

        x3 = p.posx;
        y3 = p.posy;
        z3 = p.posz;
        dasect = p.cursectnum;
        int moveHit = engine.clipmove(x3, y3, z3, dasect, p.posxv, p.posyv, 164, 4 << 8, 4 << 8, CLIPMASK0);

        if (moveHit == 0) {
            x3 = p.posx;
            y3 = p.posy;
            z3 = p.posz + (24 << 8);
            dasect = p.cursectnum;
            moveHit = engine.clipmove(x3, y3, z3, dasect, p.posxv, p.posyv, 164, 4 << 8, 4 << 8, CLIPMASK0);
        }
        if (moveHit != 0) {
            clipmovecount[snum]++;

            int j = 0;
            if ((moveHit & kHitTypeMask) == kHitWall) {
                Wall hwal = boardService.getWall(moveHit & kHitIndexMask);
                if (hwal != null && hwal.getNextsector() >= 0) {
                    if (engine.getflorzofslope(hwal.getNextsector(), p.posx, p.posy) <= p.posz + (24 << 8)) {
                        j |= 1;
                    }
                    if (engine.getceilzofslope(hwal.getNextsector(), p.posx, p.posy) >= p.posz - (24 << 8)) {
                        j |= 2;
                    }
                }
            }

            if ((moveHit & kHitTypeMask) == kHitSprite) {
                j = 1;
            }
            if ((j & 1) != 0) {
                if (clipmovecount[snum] == 4) {
                    syn.bits |= (1);
                }
            }
            if ((j & 2) != 0) {
                syn.bits |= (1 << 1);
            }

            // Strafe attack
            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            if ((moveHit & kHitTypeMask) == kHitWall) {

                Wall hwal = boardService.getWall(moveHit & kHitIndexMask);
                if (hwal != null) {
                    daang = hwal.getWallAngle();
                }
            }
            j = engine.getTotalClock() + snum * 13468;
            int i = EngineUtils.sin((j << 6) & 2047);
            i += EngineUtils.sin(((j + 4245) << 5) & 2047);
            i += EngineUtils.sin(((j + 6745) << 4) & 2047);
            i += EngineUtils.sin(((j + 15685) << 3) & 2047);
            syn.fvel += (short) ((EngineUtils.cos((daang + 512) & 2047) * i) >> 17);
            syn.svel += (short) ((EngineUtils.sin((daang + 512) & 2047) * i) >> 17);

            if ((clipmovecount[snum] & 31) == 2) {
                syn.bits |= (1 << 29);
            }
            if ((clipmovecount[snum] & 31) == 17) {
                syn.bits |= (1 << 22);
            }
            if (clipmovecount[snum] > 32) {
                goalsect[snum] = -1;
                goalwall[snum] = -1;
                clipmovecount[snum] = 0;
            }

            goalsprite[snum] = -1;
        } else {
            clipmovecount[snum] = 0;
        }

        if ((goalsect[snum] >= 0) && (goalwall[snum] >= 0)) {
            x2 = goalx[snum];
            y2 = goaly[snum];
            dist = EngineUtils.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
            if (dist == 0) {
                return;
            }
            daang = EngineUtils.getAngle(x2 - x1, y2 - y1);
            Wall goalwal = boardService.getWall(goalwall[snum]);
            if (goalwal != null && (dist < 4096)) {
                daang = ((goalwal.getWallAngle() + 1536) & 2047);
            }

            syn.fvel += (short) ((x2 - x1) * 2047 / dist);
            syn.svel += (short) ((y2 - y1) * 2047 / dist);
            syn.avel = (byte) min(max((((daang + 1024 - damyang) & 2047) - 1024) >> 3, -127), 127);
        }
    }
}
