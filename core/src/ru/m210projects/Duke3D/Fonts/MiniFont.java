// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Fonts;

import ru.m210projects.Build.Types.font.CharInfo;
import ru.m210projects.Build.Types.font.Font;

import static ru.m210projects.Duke3D.Main.engine;
import static ru.m210projects.Duke3D.Names.MINIFONT;

public class MiniFont extends Font {

    public MiniFont() {
        this.size = engine.getTile(MINIFONT).getHeight() + 2;

        this.addCharInfo(' ', new CharInfo(this, -1, 1.0f, 5));
        int nTile = MINIFONT;

        for (int i = 0; i < 64; i++) {
            char symbol = (char) (i + '!');
            if (engine.getTile(nTile + i).getWidth() != 0) {
                this.addChar(symbol, nTile + i);
            }
        }
        for (int i = 64; i < 90; i++) {
            char symbol = (char) (i + '!');
            if (engine.getTile(nTile + i - 32).getWidth() != 0) {
                this.addChar(symbol, nTile + i - 32);
            }
        }
        for (int i = 90; i < 95; i++) {
            char symbol = (char) (i + '!');
            if (engine.getTile(nTile + i).getWidth() != 0) {
                this.addChar(symbol, nTile + i);
            }
        }
    }

    protected void addChar(char ch, int nTile) {
        this.addCharInfo(ch, new CharInfo(this, nTile, 1.0f, 4, engine.getTile(nTile).getWidth(), 0, 0) {
            @Override
            public int getHeight() {
                return engine.getTile(nTile).getHeight();
            }
        });
    }
}
