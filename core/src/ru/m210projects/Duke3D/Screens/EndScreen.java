// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Build.EngineUtils;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.InputListener;
import ru.m210projects.Duke3D.Main;

import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Duke3D.Globals.ud;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.VICTORY1;
import static ru.m210projects.Duke3D.Player.endingpal;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;

public class EndScreen extends ScreenAdapter implements InputListener {
    private final Main app;
    private final int[] bossmove = {0, 120, VICTORY1 + 3, 86, 59, 220, 260, VICTORY1 + 4, 86, 59, 260, 290, VICTORY1 + 5, 86, 59, 290, 320, VICTORY1 + 6, 86, 59, 320, 350, VICTORY1 + 7, 86, 59, 350, 380, VICTORY1 + 8, 86, 59};
    private final int[] breathe = {0, 30, VICTORY1 + 1, 176, 59, 30, 60, VICTORY1 + 2, 176, 59, 60, 90, VICTORY1 + 1, 176, 59, 90, 120, 0, 176, 59};
    protected int gCutsClock;
    private int bonuscnt = 0;

    public EndScreen(Main app) {
        this.app = app;
    }

    @Override
    public void show() {
        if (ud.volume_number == 0) {
            engine.setbrightness(ud.brightness >> 2, endingpal);
        } else {
            engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
        }
        bonuscnt = 0;
        gCutsClock = 0;
        engine.getTimer().reset();
        game.getProcessor().resetPollingStates();
    }

    @Override
    public void render(float delta) {
        Renderer renderer = game.getRenderer();
        renderer.clearview(0);

        if (game.getProcessor().isKeyPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        switch (ud.volume_number) {
            case 0:
                if (bonuscnt < 4) {
                    renderer.rotatesprite(0, 50 << 16, 65536, 0, VICTORY1, 0, 0, 2 + 8 + 16 + 64 + 128);

                    // boss
                    if (engine.getTotalClock() > 390 && engine.getTotalClock() < 780) {
                        for (int t = 0; t < bossmove.length; t += 5) {
                            if (bossmove[t + 2] != 0 && (engine.getTotalClock() % 390) > bossmove[t] && (engine.getTotalClock() % 390) <= bossmove[t + 1]) {
                                if (t == 10 && bonuscnt == 1) {
                                    sound(SHOTGUN_FIRE);
                                    sound(SQUISHED);
                                    bonuscnt++;
                                }
                                renderer.rotatesprite(bossmove[t + 3] << 16, bossmove[t + 4] << 16, 65536, 0, bossmove[t + 2], 0, 0, 2 + 8 + 16 + 64 + 128);
                            }
                        }
                    }

                    // Breathe
                    if (engine.getTotalClock() < 450 || engine.getTotalClock() >= 750) {
                        if (engine.getTotalClock() >= 750) {
                            renderer.rotatesprite(86 << 16, 59 << 16, 65536, 0, VICTORY1 + 8, 0, 0, 2 + 8 + 16 + 64 + 128);
                            if (engine.getTotalClock() >= 750 && bonuscnt == 2) {
                                sound(DUKETALKTOBOSS);
                                bonuscnt++;
                            }
                        }

                        for (int t = 0; t < breathe.length; t += 5) {
                            if (breathe[t + 2] != 0 && (engine.getTotalClock() % 120) > breathe[t] && (engine.getTotalClock() % 120) <= breathe[t + 1]) {
                                if (t == 5 && bonuscnt == 0) {
                                    sound(BOSSTALKTODUKE);
                                    bonuscnt++;
                                }
                                renderer.rotatesprite(breathe[t + 3] << 16, breathe[t + 4] << 16, 65536, 0, breathe[t + 2], 0, 0, 2 + 8 + 16 + 64 + 128);
                            }
                        }
                    }
                } else {
                    renderer.rotatesprite(0, 10 << 16, 63000, 0, 3292, 0, 0, 2 + 8 + 16 + 64);
                }
                break;
            case 1:
                renderer.rotatesprite(0, 0, 65536, 0, 3293, 0, 0, 2 + 8 + 16 + 64);
                break;
            case 3:
                app.getFont(2).drawTextScaled(renderer, 160, 60, "THANKS TO ALL OUR", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                app.getFont(2).drawTextScaled(renderer, 160, 60 + 16, "FANS FOR GIVING", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                app.getFont(2).drawTextScaled(renderer, 160, 60 + 16 + 16, "US BIG HEADS.", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

                app.getFont(2).drawTextScaled(renderer, 160, 70 + 16 + 16 + 16, "LOOK FOR A DUKE NUKEM 3D", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                app.getFont(2).drawTextScaled(renderer, 160, 70 + 16 + 16 + 16 + 16, "SEQUEL SOON.", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
                break;
            case 4:
                renderer.rotatesprite(0, 0, 65536, 0, 5367, 0, 0, 2 + 8 + 16 + 64);
                break;
        }

        if (engine.getTotalClock() - gCutsClock < 200) // 2 sec
        {
            DrawEscText(game.getFont(0));
        }

        engine.nextpage(delta);
    }

    protected void DrawEscText(Font font) {
        Renderer renderer = game.getRenderer();
        int shade = 16 + mulscale(16, EngineUtils.sin((20 * engine.getTotalClock()) & 2047), 16);
        font.drawTextScaled(renderer, 160, 5, "Press ESC to skip", 1.0f, shade, 251, TextAlign.Center, Transparent.None, ConvertType.Normal, true);
    }

    public void episode1() {
        Gdx.app.postRunnable(() -> {
            ud.volume_number = 0;
            game.changeScreen(gEndScreen);
        });
    }

    public void episode2() {
        Gdx.app.postRunnable(() -> {
            ud.volume_number = 1;
            sndStopMusic();
            gAnmScreen.init("cineov2.anm", 1);
            gAnmScreen.setCallback(() -> game.changeScreen(gEndScreen));
            game.changeScreen(gAnmScreen.escSkipping(true));
        });
    }

    public void episode3() {
        Gdx.app.postRunnable(() -> {
            ud.volume_number = 2;
            sndStopMusic();
            gAnmScreen.init("cineov3.anm", 2);
            gAnmScreen.setCallback(() -> {
                gAnmScreen.init("radlogo.anm", 3);
                gAnmScreen.setCallback(() -> game.changeScreen(gStatisticScreen));
                gAnmScreen.escSkipping(true);
            });
            game.changeScreen(gAnmScreen.escSkipping(true));
        });
    }

    public void episode4() {
        Gdx.app.postRunnable(() -> {
            ud.volume_number = 3;
            sndStopMusic();
            gAnmScreen.init("vol4e1.anm", 8);
            gAnmScreen.setCallback(() -> {
                sound(ENDSEQVOL3SND4);
                game.changeScreen(gEndScreen);
            });
            game.changeScreen(gAnmScreen.escSkipping(true));
        });
    }

    public void episode5() {
        Gdx.app.postRunnable(() -> {
            ud.volume_number = 4;
            sndStopMusic();
            sound(E5L7_DUKE_QUIT_YOU);
            game.changeScreen(gEndScreen);
        });
    }


    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        if (GameKeys.Menu_Toggle.equals(gameKey)) {
            onSkip();
            return true;
        }
        return false;
    }

    public void onSkip() {

        switch (ud.volume_number) {
            case 0:
                if (bonuscnt < 4) {
                    bonuscnt = 4;
                    engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
                    StopAllSounds();
                } else {
                    engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
                    sound(PIPEBOMB_EXPLODE);
                    game.changeScreen(gStatisticScreen);
                }
                break;
            case 1:
            case 4:
                engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
                sound(PIPEBOMB_EXPLODE);
                game.changeScreen(gStatisticScreen);
                break;
            case 3:
                gAnmScreen.setCallback(() -> game.changeScreen(gStatisticScreen));
                gAnmScreen.init("duketeam.anm", 4);
                game.changeScreen(gAnmScreen.escSkipping(false));
                break;
        }
    }

    public void anyKeyPressed() {
        game.getProcessor().prepareNext();
        gCutsClock = engine.getTotalClock();
    }

    @Override
    public InputListener getInputListener() {
        return this;
    }

    @Override
    public void processInput(GameProcessor processor) {
        processor.prepareNext();
    }

}
