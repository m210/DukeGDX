// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.LoadingAdapter;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Duke3D.Main;

import static ru.m210projects.Duke3D.Globals.ud;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.LOADSCREEN;
import static ru.m210projects.Duke3D.Sounds.StopAllSounds;

public class LoadingScreen extends LoadingAdapter {

    private final Main app;

    public LoadingScreen(Main game) {
        super(game);
        this.app = game;
    }

    @Override
    public void draw(String title, float delta) {
//		engine.clearview(129);
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(320 << 15, 200 << 15, 65536, 0, LOADSCREEN, 0, 0, 2 + 8 + 64);

        if (title == null) {
            app.getFont(2).drawTextScaled(renderer, 160, 90 + 16 + 8, "Please wait", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        } else {
            if (mUserFlag == UserFlag.UserMap) {
                app.getFont(2).drawTextScaled(renderer, 160, 90, "Entering usermap", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            } else {
                app.getFont(2).drawTextScaled(renderer, 160, 90, "Entering ", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            }
            app.getFont(2).drawTextScaled(renderer, 160, 90 + 16 + 8, title, 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }
    }

    @Override
    public void show() {
        super.show();
        StopAllSounds();
        engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
    }

}
