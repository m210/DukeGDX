// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Screens;

import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.ScreenAdapters.LogoScreen;
import ru.m210projects.Build.Render.Renderer;


import static ru.m210projects.Duke3D.Globals.currentGame;
import static ru.m210projects.Duke3D.Globals.ud;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.titlepal;
import static ru.m210projects.Duke3D.SoundDefs.FLY_BY;
import static ru.m210projects.Duke3D.SoundDefs.PIPEBOMB_EXPLODE;
import static ru.m210projects.Duke3D.Sounds.sound;

public class LogoBScreen extends LogoScreen {

    private int soundanm;

    public LogoBScreen(BuildGame game) {
        super(game, 3.0f);
        this.setTile(BETASCREEN);
    }

    @Override
    public void show() {
        super.show();
        soundanm = 0;
        engine.getTimer().reset();
        engine.setbrightness(ud.brightness >> 2, titlepal);
    }

    @Override
    public void draw(float delta) {
        super.draw(delta);

        Renderer renderer = game.getRenderer();
        if (engine.getTotalClock() < (250)) {
            gTicks = 0;
        }

        if (engine.getTotalClock() > 120 && engine.getTotalClock() < (120 + 60)) {
            if (soundanm == 0) {
                soundanm = 1;
                sound(PIPEBOMB_EXPLODE);
            }
            renderer.rotatesprite(160 << 16, 104 << 16, (engine.getTotalClock() - 120) << 10, 0, DUKENUKEM, 0, 0, 2 + 8);
        } else if (engine.getTotalClock() >= (120 + 60)) {
            renderer.rotatesprite(160 << 16, (104) << 16, 60 << 10, 0, DUKENUKEM, 0, 0, 2 + 8);
        }

        if (engine.getTotalClock() > 220 && engine.getTotalClock() < (220 + 30)) {
            if (soundanm == 1) {
                soundanm = 2;
                sound(PIPEBOMB_EXPLODE);
            }

            renderer.rotatesprite(160 << 16, (104) << 16, 60 << 10, 0, DUKENUKEM, 0, 0, 2 + 8);
            renderer.rotatesprite(160 << 16, (129) << 16, (engine.getTotalClock() - 220) << 11, 0, THREEDEE, 0, 0, 2 + 8);
        } else if (engine.getTotalClock() >= (220 + 30)) {
            renderer.rotatesprite(160 << 16, (129) << 16, 30 << 11, 0, THREEDEE, 0, 0, 2 + 8);
        }

        if (currentGame.getCON().PLUTOPAK) {    // JBF 20030804
            if (engine.getTotalClock() >= 280 && engine.getTotalClock() < 395) {
                renderer.rotatesprite(160 << 16, (151) << 16, (410 - engine.getTotalClock()) << 12, 0, PLUTOPAKSPRITE + 1, 0, 0, 2 + 8);
                if (soundanm == 2) {
                    soundanm = 3;
                    sound(FLY_BY);
                }
            } else if (engine.getTotalClock() >= 395) {
                if (soundanm == 3) {
                    soundanm = 4;
                    sound(PIPEBOMB_EXPLODE);
                }
                renderer.rotatesprite(160 << 16, (151) << 16, 30 << 11, 0, PLUTOPAKSPRITE + 1, 0, 0, 2 + 8);
            }
        }
    }

}
