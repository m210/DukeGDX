// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.input.GameKey;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.input.InputListener;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Duke3D.Types.MapInfo;

import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;

public class StatisticScreen extends ScreenAdapter implements InputListener {

    protected final char[] bonusbuf = new char[128];
    protected int bonuscnt = 0;
    protected final Main app;
    private boolean disconnected;

    public StatisticScreen(Main app) {
        this.app = app;
    }

    @Override
    public void show() {
        engine.setbrightness(ud.brightness >> 2, engine.getPaletteManager().getBasePalette());
        engine.getTimer().reset();

        StopAllSounds();
        sndStopMusic();
        clearsoundlocks();

        bonuscnt = 0;
        game.getProcessor().resetPollingStates();
        if (!cfg.isMuteMusic()) {
            sound(BONUSMUSIC);
        }
    }

    @Override
    public void render(float delta) {
        game.getRenderer().clearview(0);
        if (numplayers > 1) {
            game.pNet.GetPackets();
        }

        dobonus(false);

        engine.nextpage(delta);
    }

    public void dobonus(boolean disconnect) {
        int t, gfx_offset;
        int i, y, xfragtotal, yfragtotal;

        this.disconnected = disconnect;
        if (game.getProcessor().isKeyJustPressed(Input.Keys.ANY_KEY)) {
            anyKeyPressed();
        }

        Renderer renderer = game.getRenderer();
        if (ud.multimode > 1 && ud.coop != 1 || disconnect) {
            renderer.rotatesprite(0, 0, 65536, 0, MENUSCREEN, 16, 0, 2 + 8 + 16 + 64);
            renderer.rotatesprite(160 << 16, 34 << 16, 65536, 0, INGAMEDUKETHREEDEE, 0, 0, 10);
            if (currentGame.getCON().type != 13) // JBF 20030804
            {
                renderer.rotatesprite((260) << 16, 36 << 16, 65536, 0, PLUTOPAKSPRITE + 2, 0, 0, 2 + 8);
            }

            app.getFont(1).drawTextScaled(renderer, 160, 58 + 2, "MULTIPLAYER TOTALS", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            buildString(bonusbuf, 0, currentGame.episodes[ud.volume_number].getMapTitle(ud.level_number));
            app.getFont(1).drawTextScaled(renderer, 160, 58 + 10, bonusbuf, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            t = 0;
            app.getFont(0).drawTextScaled(renderer, 23, 80, "   NAME                                           KILLS", 1.0f, 0, 0,
                    TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            for (i = 0; i < numplayers; i++) {
                Bitoa(i + 1, bonusbuf);
                app.getFont(0).drawTextScaled(renderer, 92 + (i * 23), 80, bonusbuf, 1.0f, 0, 3, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }

            for (i = 0; i < numplayers; i++) {
                xfragtotal = 0;
                Bitoa(i + 1, bonusbuf);

                app.getFont(0).drawTextScaled(renderer, 30, 90 + t, bonusbuf, 1.0f, 0, 3, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                app.getFont(0).drawTextScaled(renderer, 38, 90 + t, ud.user_name[i], 1.0f, 0, ps[i].palookup, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                for (y = 0; y < numplayers; y++) {
                    if (i == y) {
                        Bitoa(ps[y].fraggedself, bonusbuf);
                        app.getFont(0).drawTextScaled(renderer, 92 + (y * 23), 90 + t, bonusbuf, 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                        xfragtotal -= ps[y].fraggedself;
                    } else {
                        Bitoa(frags[i][y], bonusbuf);
                        app.getFont(0).drawTextScaled(renderer, 92 + (y * 23), 90 + t, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                        xfragtotal += frags[i][y];
                    }

                    if (myconnectindex == connecthead) {
//	                    sprintf(tempbuf,"stats %ld killed %ld %d\n",i+1,y+1,frags[i][y]);
//	                    sendscore(tempbuf);
                    }
                }

                Bitoa(xfragtotal, bonusbuf);
                app.getFont(0).drawTextScaled(renderer, 101 + (8 * 23), 90 + t, bonusbuf, 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                t += 7;
            }

            for (y = 0; y < numplayers; y++) {
                yfragtotal = 0;
                for (i = 0; i < numplayers; i++) {
                    if (i == y) {
                        yfragtotal += ps[i].fraggedself;
                    }
                    yfragtotal += frags[i][y];
                }
                Bitoa(yfragtotal, bonusbuf);
                app.getFont(0).drawTextScaled(renderer, 92 + (y * 23), 96 + (8 * 7), bonusbuf, 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }

            app.getFont(0).drawTextScaled(renderer, 45, 96 + (8 * 7), "DEATHS", 1.0f, 0, 8, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            app.getFont(1).drawTextScaled(renderer, 160, 165, "PRESS ANY KEY TO CONTINUE", 1.0f, 0, 2, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        } else {
            MapInfo gMapInfo = currentGame.episodes[ud.volume_number].getMapInfo(ud.last_level - 1);
            if (ud.volume_number == 1) {
                gfx_offset = 5;
            } else {
                gfx_offset = 0;
            }

            renderer.rotatesprite(160 << 16, 100 << 16, 65536, 0, BONUSSCREEN + gfx_offset, 0, 0, 2 + 8 + 64);

            if (engine.getTotalClock() >= (1000000000) && engine.getTotalClock() < (1000000320)) {
                switch ((engine.getTotalClock() >> 4) % 15) {
                    case 0:
                        if (bonuscnt == 6) {
                            bonuscnt++;
                            sound(SHOTGUN_COCK);
                            switch (engine.rand() & 3) {
                                case 0:
                                    sound(BONUS_SPEECH1);
                                    break;
                                case 1:
                                    sound(BONUS_SPEECH2);
                                    break;
                                case 2:
                                    sound(BONUS_SPEECH3);
                                    break;
                                case 3:
                                    sound(BONUS_SPEECH4);
                                    break;
                            }
                        }
                    case 1:
                    case 4:
                    case 5:
                        renderer.rotatesprite(199 << 16, 31 << 16, 65536, 0, BONUSSCREEN + 3 + gfx_offset, 0, 0,
                                2 + 8 + 16 + 64 + 128);
                        break;
                    case 2:
                    case 3:
                        renderer.rotatesprite(199 << 16, 31 << 16, 65536, 0, BONUSSCREEN + 4 + gfx_offset, 0, 0,
                                2 + 8 + 16 + 64 + 128);
                        break;
                }
            } else if (engine.getTotalClock() > (10240 + 120)) {
                onSkip();
                return;
            } else {
                switch ((engine.getTotalClock() >> 5) & 3) {
                    case 1:
                    case 3:
                        renderer.rotatesprite(199 << 16, 31 << 16, 65536, 0, BONUSSCREEN + 1 + gfx_offset, 0, 0,
                                2 + 8 + 16 + 64 + 128);
                        break;
                    case 2:
                        renderer.rotatesprite(199 << 16, 31 << 16, 65536, 0, BONUSSCREEN + 2 + gfx_offset, 0, 0,
                                2 + 8 + 16 + 64 + 128);
                        break;
                }
            }

            String lastmapname = "null";
            if (ud.volume_number == 0 && ud.last_level == 8 && boardfilename != null && boardfilename.exists()) {
                lastmapname = boardfilename.getName();
            } else if (gMapInfo != null) {
                lastmapname = gMapInfo.getTitle();
            }

            app.getFont(2).drawTextScaled(renderer, 160, 10 - 6, lastmapname, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            app.getFont(2).drawTextScaled(renderer, 160, 26 - 6, "COMPLETED", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            app.getFont(1).drawTextScaled(renderer, 160, 192, "PRESS ANY KEY TO CONTINUE", 1.0f, 16, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);

            if (engine.getTotalClock() > (60 * 3)) {
                app.getFont(1).drawTextScaled(renderer, 10, 59 + 9, "Your Time:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                app.getFont(1).drawTextScaled(renderer, 10, 69 + 9, "Par time:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                app.getFont(1).drawTextScaled(renderer, 10, 78 + 9, "3D Realms' Time:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                if (bonuscnt == 0) {
                    bonuscnt++;
                }

                if (engine.getTotalClock() > (60 * 4)) {
                    if (bonuscnt == 1) {
                        bonuscnt++;
                        sound(PIPEBOMB_EXPLODE);
                    }

                    int num = Bitoa(ps[myconnectindex].player_par / (26 * 60), bonusbuf, 2);
                    buildString(bonusbuf, num, " : ", (ps[myconnectindex].player_par / 26) % 60, 2);
                    app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 71, 60 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                    if (gMapInfo != null) {
                        num = Bitoa(gMapInfo.partime / (26 * 60), bonusbuf, 2);
                        buildString(bonusbuf, num, " : ", (gMapInfo.partime / 26) % 60, 2);
                        app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 71, 69 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                        num = Bitoa(gMapInfo.designertime / (26 * 60), bonusbuf, 2);
                        buildString(bonusbuf, num, " : ", (gMapInfo.designertime / 26) % 60, 2);
                        app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 71, 78 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                    }
                }
            }
            if (engine.getTotalClock() > (60 * 6)) {
                app.getFont(1).drawTextScaled(renderer, 10, 94 + 9, "Enemies Killed:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                app.getFont(1).drawTextScaled(renderer, 10, 99 + 4 + 9, "Enemies Left:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                if (bonuscnt == 2) {
                    bonuscnt++;
                    sound(FLY_BY);
                }

                if (engine.getTotalClock() > (60 * 7)) {
                    if (bonuscnt == 3) {
                        bonuscnt++;
                        sound(PIPEBOMB_EXPLODE);
                    }

                    Bitoa(ps[connecthead].actors_killed, bonusbuf);
                    app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 70, 93 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                    if (ud.player_skill > 3) {
                        buildString(bonusbuf, 0, "N/A");
                        app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 70, 99 + 4 + 9, "N/A", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                    } else {
                        Bitoa(Math.max((ps[connecthead].max_actors_killed - ps[connecthead].actors_killed), 0), bonusbuf);
                        app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 70, 99 + 4 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                    }
                }
            }
            if (engine.getTotalClock() > (60 * 9)) {
                app.getFont(1).drawTextScaled(renderer, 10, 120 + 9, "Secrets Found:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                app.getFont(1).drawTextScaled(renderer, 10, 130 + 9, "Secrets Missed:", 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                if (bonuscnt == 4) {
                    bonuscnt++;
                }

                if (engine.getTotalClock() > (60 * 10)) {
                    if (bonuscnt == 5) {
                        bonuscnt++;
                        sound(PIPEBOMB_EXPLODE);
                    }
                    Bitoa(ps[connecthead].secret_rooms, bonusbuf);
                    app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 70, 120 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);

                    Bitoa(ps[connecthead].max_secret_rooms - ps[connecthead].secret_rooms, bonusbuf);
                    app.getFont(1).drawTextScaled(renderer, (320 >> 2) + 70, 130 + 9, bonusbuf, 1.0f, 0, 0, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                }
            }

            if (engine.getTotalClock() > 10240 && engine.getTotalClock() < 10240 + 10240) {
                engine.getTimer().setTotalClock(1024);
            }
        }
    }

    @Override
    public boolean gameKeyDown(GameKey gameKey) {
        return true;
    }

    public void anyKeyPressed() {
        game.getProcessor().prepareNext();
        if (ud.multimode > 1 && ud.coop != 1 || disconnected) {
            if (engine.getTotalClock() > (60 * 2)) {
                onSkip();
            }
        } else {
            if (engine.getTotalClock() > (60 * 2)) {// JBF 20030809
                if (engine.getTotalClock() < (60 * 13)) {
                    engine.getTimer().setTotalClock(60 * 13);
                } else if (engine.getTotalClock() < (1000000000)) {
                    engine.getTimer().setTotalClock(1000000000);
                }
            }
        }
    }

    public void onSkip() {
        if ((uGameFlags & MODE_END) != 0 || mUserFlag == UserFlag.UserMap) {
            game.show();
        } else {
            gGameScreen.enterlevel(gGameScreen.getTitle(), ud.volume_number, ud.level_number);
        }
    }

    @Override
    public InputListener getInputListener() {
        return this;
    }

    @Override
    public void processInput(GameProcessor processor) {
        processor.prepareNext();
    }

}
