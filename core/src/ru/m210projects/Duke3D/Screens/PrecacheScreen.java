package ru.m210projects.Duke3D.Screens;

import ru.m210projects.Build.Pattern.ScreenAdapters.DefaultPrecacheScreen;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.listeners.PrecacheListener;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Duke3D.Main;

import static ru.m210projects.Duke3D.Actors.badguy;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Premap.getsound;
import static ru.m210projects.Duke3D.Sounds.NUM_SOUNDS;

public class PrecacheScreen extends DefaultPrecacheScreen {

    public PrecacheScreen(Runnable toLoad, PrecacheListener listener) {
        super(Main.game, toLoad, listener);

        addQueue("Preload sounds...", this::precachenecessarysounds);

        addQueue("Preload floor and ceiling tiles...", () -> {
            Sector[] sectors = boardService.getBoard().getSectors();
            for (Sector sec : sectors) {
                addTile(sec.getFloorpicnum());
                addTile(sec.getCeilingpicnum());
            }
            doprecache(0);

            for (Sector sec : sectors) {
                if ((sec.getCeilingstat() & 1) != 0) {
                    if (sec.getCeilingpicnum() == LA) {
                        for (int j = 0; j < 5; j++) {
                            addTile(sec.getCeilingpicnum() + j);
                        }
                    } else {
                        addTile(sec.getCeilingpicnum());
                    }
                }
            }

            doprecache(1);
        });

        addQueue("Preload wall tiles...", () -> {
            for (Wall wall : boardService.getBoard().getWalls()) {
                addTile(wall.getPicnum());
                switch (wall.getPicnum()) {
                    case WATERTILE2:
                        for (int j = 0; j < 3; j++) {
                            addTile(wall.getPicnum() + j);
                        }
                        break;
                    case TECHLIGHT2:
                    case TECHLIGHT4:
                        addTile(wall.getPicnum());
                        break;
                    case SCREENBREAK6:
                    case SCREENBREAK7:
                    case SCREENBREAK8:
                        for (int k = SCREENBREAK6; k < SCREENBREAK9; k++) {
                            addTile(k);
                        }
                        break;
                }
                if (wall.getOverpicnum() >= 0) {
                    addTile(wall.getOverpicnum());
                    if (wall.getOverpicnum() == W_FORCEFIELD) {
                        for (int j = 0; j < 3; j++) {
                            addTile(W_FORCEFIELD + j);
                        }
                    }
                }
            }
            doprecache(0);
        });

        addQueue("Preload sprite tiles...", () -> {
            cachegoodsprites();
            for (int i = 0; i < boardService.getSectorCount(); i++) {
                ListNode<Sprite> j = boardService.getSectNode(i);
                while (j != null) {
                    Sprite spr = j.get();
                    if (spr.getXrepeat() != 0 && spr.getYrepeat() != 0 && (spr.getCstat() & 32768) == 0) {
                        cachespritenum(spr);
                    }
                    j = j.getNext();
                }
            }

            doprecache(1);
        });
    }

    @Override
    public void draw(String title, int i) {
//		engine.clearview(129);
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite(320 << 15, 200 << 15, 65536, 0, LOADSCREEN, 0, 0, 2 + 8 + 64);

        if (mUserFlag != UserFlag.UserMap || boardfilename == null) {
            game.getFont(2).drawTextScaled(renderer, 160, 90, "Entering ", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            if (currentGame.episodes[ud.volume_number] != null) {
                game.getFont(2).drawTextScaled(renderer, 160, 90 + 16 + 8, currentGame.episodes[ud.volume_number].getMapTitle(ud.level_number), 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            }
        } else {
            game.getFont(2).drawTextScaled(renderer, 160, 90, "Entering usermap", 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            game.getFont(2).drawTextScaled(renderer, 160, 90 + 16 + 8, boardfilename.getName(), 1.0f, -128, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        game.getFont(1).drawTextScaled(renderer, 160, 90 + 48 + 8, title, 1.0f, -128, 10, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
    }

    private void precachenecessarysounds() {
        for (int i = 0; i < NUM_SOUNDS; i++) {
            if (Sound[i].ptr == null) {
                getsound(i);
            }
        }
    }

    private void cachespritenum(Sprite sp) {
        char maxc;
        short j;

        if (ud.monsters_off && badguy(sp)) {
            return;
        }

        maxc = 1;

        switch (sp.getPicnum()) {
            case HYDRENT:
                addTile(BROKEFIREHYDRENT);
                for (j = TOILETWATER; j < (TOILETWATER + 4); j++) {
                    addTile(j);
                }
                break;
            case TOILET:
                addTile(TOILETBROKE);
                for (j = TOILETWATER; j < (TOILETWATER + 4); j++) {
                    addTile(j);
                }
                break;
            case STALL:
                addTile(STALLBROKE);
                for (j = TOILETWATER; j < (TOILETWATER + 4); j++) {
                    addTile(j);
                }
                break;
            case RUBBERCAN:
                maxc = 2;
                break;
            case TOILETWATER:
                maxc = 4;
                break;
            case FEMPIC1:
                maxc = 44;
                break;
            case LIZTROOP:
            case LIZTROOPRUNNING:
            case LIZTROOPSHOOT:
            case LIZTROOPJETPACK:
            case LIZTROOPONTOILET:
            case LIZTROOPDUCKING:
                for (j = LIZTROOP; j < (LIZTROOP + 72); j++) {
                    addTile(j);
                }
                for (j = HEADJIB1; j < LEGJIB1 + 3; j++) {
                    addTile(j);
                }
                maxc = 0;
                break;
            case WOODENHORSE:
                maxc = 5;
                for (j = HORSEONSIDE; j < (HORSEONSIDE + 4); j++) {
                    addTile(j);
                }
                break;
            case NEWBEAST:
            case NEWBEASTSTAYPUT:
                maxc = 90;
                break;
            case BOSS1:
            case BOSS2:
            case BOSS3:
            case SHARK:
                maxc = 30;
                break;
            case OCTABRAIN:
            case OCTABRAINSTAYPUT:
            case COMMANDER:
            case COMMANDERSTAYPUT:
                maxc = 38;
                break;
            case RECON:
                maxc = 13;
                break;
            case PIGCOP:
            case PIGCOPDIVE:
                maxc = 61;
                break;
            case LIZMAN:
            case LIZMANSPITTING:
            case LIZMANFEEDING:
            case LIZMANJUMP:
                for (j = LIZMANHEAD1; j < LIZMANLEG1 + 3; j++) {
                    addTile(j);
                }
                maxc = 80;
                break;
            case APLAYER:
                maxc = 0;
                if (ud.multimode > 1) {
                    maxc = 5;
                    for (j = 1420; j < 1420 + 106; j++) {
                        addTile(j);
                    }
                }
                break;
            case ATOMICHEALTH:
                maxc = 14;
                break;
            case DRONE:
                maxc = 10;
                break;
            case EXPLODINGBARREL:
            case SEENINE:
            case OOZFILTER:
                maxc = 3;
                break;
            case NUKEBARREL:
            case CAMERA1:
                maxc = 5;
                break;
        }

        for (j = sp.getPicnum(); j < (sp.getPicnum() + maxc); j++) {
            addTile(j);
        }
    }

    private void cachegoodsprites() {
        short i;

        if (ud.screen_size >= 2) {
            addTile(BOTTOMSTATUSBAR);
            if (ud.multimode > 1) {
                addTile(FRAGBAR);
                for (i = MINIFONT; i < MINIFONT + 63; i++) {
                    addTile(i);
                }
            }
        }

        addTile(VIEWSCREEN);

        for (i = STARTALPHANUM; i < ENDALPHANUM + 1; i++) {
            addTile(i);
        }

        for (i = FOOTPRINTS; i < FOOTPRINTS + 3; i++) {
            addTile(i);
        }

        for (i = BIGALPHANUM; i < BIGALPHANUM + 82; i++) {
            addTile(i);
        }

        for (i = BURNING; i < BURNING + 14; i++) {
            addTile(i);
        }

        for (i = BURNING2; i < BURNING2 + 14; i++) {
            addTile(i);
        }

        for (i = CRACKKNUCKLES; i < CRACKKNUCKLES + 4; i++) {
            addTile(i);
        }

        for (i = FIRSTGUN; i < FIRSTGUN + 3; i++) {
            addTile(i);
        }

        for (i = EXPLOSION2; i < EXPLOSION2 + 21; i++) {
            addTile(i);
        }

        addTile(BULLETHOLE);

        for (i = FIRSTGUNRELOAD; i < FIRSTGUNRELOAD + 8; i++) {
            addTile(i);
        }

        addTile(FOOTPRINTS);

        for (i = JIBS1; i < (JIBS5 + 5); i++) {
            addTile(i);
        }

        for (i = SCRAP1; i < (SCRAP1 + 19); i++) {
            addTile(i);
        }

        for (i = SMALLSMOKE; i < (SMALLSMOKE + 4); i++) {
            addTile(i);
        }
    }

}
