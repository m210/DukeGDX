//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Duke3D.Globals.MAX_WEAPONS;
import static ru.m210projects.Duke3D.Globals.ud;

public class PlayerInfo {
    public int aimmode;
    public int autoaim;

    public final int[] ammo_amount = new int[MAX_WEAPONS];
    public final boolean[] gotweapon = new boolean[MAX_WEAPONS];

    public short shield_amount;
    public short curr_weapon;
    public int inven_icon;

    public short firstaid_amount;
    public short steroids_amount;
    public short holoduke_amount;
    public short jetpack_amount;
    public short heat_amount;
    public short scuba_amount;
    public short boot_amount;
    public short last_extra;

    public void set(PlayerStruct p) {
        aimmode = p.aim_mode;
        autoaim = p.auto_aim;

        if (ud.multimode > 1 && ud.coop == 1 && ud.last_level >= 0) {
            for (int j = 0; j < MAX_WEAPONS; j++) {
                ammo_amount[j] = p.ammo_amount[j];
                gotweapon[j] = p.gotweapon[j];
            }
            shield_amount = p.shield_amount;
            curr_weapon = p.curr_weapon;
            inven_icon = p.inven_icon;

            firstaid_amount = p.firstaid_amount;
            steroids_amount = p.steroids_amount;
            holoduke_amount = p.holoduke_amount;
            jetpack_amount = p.jetpack_amount;
            heat_amount = p.heat_amount;
            scuba_amount = p.scuba_amount;
            boot_amount = p.boot_amount;
            last_extra = (short) p.last_extra;
        }
    }

    public void restore(PlayerStruct p) {
        p.aim_mode = aimmode;
        p.auto_aim = autoaim;

        if (ud.multimode > 1 && ud.coop == 1 && ud.last_level >= 0) {
            for (int j = 0; j < MAX_WEAPONS; j++) {
                p.ammo_amount[j] = ammo_amount[j];
                p.gotweapon[j] = gotweapon[j];
            }
            p.shield_amount = shield_amount;
            p.curr_weapon = curr_weapon;
            p.inven_icon = inven_icon;

            p.firstaid_amount = firstaid_amount;
            p.steroids_amount = steroids_amount;
            p.holoduke_amount = holoduke_amount;
            p.jetpack_amount = jetpack_amount;
            p.heat_amount = heat_amount;
            p.scuba_amount = scuba_amount;
            p.boot_amount = boot_amount;
            p.last_extra = last_extra;
        }
    }
}
