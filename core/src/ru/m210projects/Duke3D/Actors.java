//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import com.badlogic.gdx.Input;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.AssertException;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Duke3D.Screens.DemoScreen;
import ru.m210projects.Duke3D.Types.GameType;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Duke3D.Animate.getanimationgoal;
import static ru.m210projects.Duke3D.Gamedef.*;
import static ru.m210projects.Duke3D.Gameutils.*;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.LoadSave.lastload;
import static ru.m210projects.Duke3D.LoadSave.loadgame;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.*;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;
import static ru.m210projects.Duke3D.Spawn.*;
import static ru.m210projects.Duke3D.Types.ANIMATION.CEILZ;
import static ru.m210projects.Duke3D.Types.UserDefs.DEMOSTAT_NULL;
import static ru.m210projects.Duke3D.View.FTA;
import static ru.m210projects.Duke3D.Weapons.*;

public class Actors {

    public static final int[] tempshort = new int[MAXSECTORS];
    public static final byte[] statlist = {0, 1, 6, 10, 12, 2, 5};

    public static int actor_tog = 0;
    public static int otherp;

    public static boolean badguy(Sprite s) {
        if (s == null) {
            return false;
        }

        switch (s.getPicnum()) {
            case SHARK:
            case RECON:
            case DRONE:
            case LIZTROOPONTOILET:
            case LIZTROOPJUSTSIT:
            case LIZTROOPSTAYPUT:
            case LIZTROOPSHOOT:
            case LIZTROOPJETPACK:
            case LIZTROOPDUCKING:
            case LIZTROOPRUNNING:
            case LIZTROOP:
            case OCTABRAIN:
            case COMMANDER:
            case COMMANDERSTAYPUT:
            case PIGCOP:
            case EGG:
            case PIGCOPSTAYPUT:
            case PIGCOPDIVE:
            case LIZMAN:
            case LIZMANSPITTING:
            case LIZMANFEEDING:
            case LIZMANJUMP:
            case ORGANTIC:
            case BOSS1:
            case BOSS2:
            case BOSS3:
            case BOSS4:
            case GREENSLIME:
            case GREENSLIME + 1:
            case GREENSLIME + 2:
            case GREENSLIME + 3:
            case GREENSLIME + 4:
            case GREENSLIME + 5:
            case GREENSLIME + 6:
            case GREENSLIME + 7:
            case RAT:
            case ROTATEGUN:
                return true;
            case FIREFLY: // Twentieth Anniversary World Tour
            case BOSS5:
            case BOSS5STAYPUT:
                if (currentGame.getCON().type == 20) {
                    return true;
                }
                break;
        }

        return currentGame.getCON().actortype[s.getPicnum()] != 0;
    }

    public static boolean badguypic(int pn) {
        switch (pn) {
            case SHARK:
            case RECON:
            case DRONE:
            case LIZTROOPONTOILET:
            case LIZTROOPJUSTSIT:
            case LIZTROOPSTAYPUT:
            case LIZTROOPSHOOT:
            case LIZTROOPJETPACK:
            case LIZTROOPDUCKING:
            case LIZTROOPRUNNING:
            case LIZTROOP:
            case OCTABRAIN:
            case COMMANDER:
            case COMMANDERSTAYPUT:
            case PIGCOP:
            case EGG:
            case PIGCOPSTAYPUT:
            case PIGCOPDIVE:
            case LIZMAN:
            case LIZMANSPITTING:
            case LIZMANFEEDING:
            case LIZMANJUMP:
            case ORGANTIC:
            case BOSS1:
            case BOSS2:
            case BOSS3:
            case BOSS4:
            case GREENSLIME:
            case GREENSLIME + 1:
            case GREENSLIME + 2:
            case GREENSLIME + 3:
            case GREENSLIME + 4:
            case GREENSLIME + 5:
            case GREENSLIME + 6:
            case GREENSLIME + 7:
            case RAT:
            case ROTATEGUN:
                return true;
            case FIREFLY: // Twentieth Anniversary World Tour
            case BOSS5:
            case BOSS5STAYPUT:
                if (currentGame.getCON().type == 20) {
                    return true;
                }
                break;
        }

        return pn < MAXTILES && currentGame.getCON().actortype[pn] != 0;
    }

    public static int LocateTheLocator(int n, int sn) {
        ListNode<Sprite> node = boardService.getStatNode(7);
        while (node != null) {
            Sprite sp = node.get();
            if ((sn == -1 || sn == sp.getSectnum()) && n == sp.getLotag()) {
                return node.getIndex();
            }
            node = node.getNext();
        }
        return -1;
    }

    public static boolean ifsquished(final int i, int p) {
        final Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return false;
        }

        boolean squishme = false;
        if (sp.getPicnum() == APLAYER && ud.clipping) {
            return false;
        }

        Sector sc = boardService.getSector(sp.getSectnum());
        if (sc == null) {
            return false;
        }

        int floorceildist = sc.getFloorz() - sc.getCeilingz();
        if (sc.getLotag() != 23) {
            if (sp.getPal() == 1) {
                squishme = floorceildist < (32 << 8) && (sc.getLotag() & 32768) == 0;
            } else {
                squishme = floorceildist < (12 << 8);
            }
        }

        if (squishme) {
            FTA(10, ps[p]);

            if (badguy(sp)) {
                sp.setXvel(0);
            }

            if (sp.getPal() == 1) {
                hittype[i].picnum = SHOTSPARK1;
                hittype[i].extra = 1;
                return false;
            }

            return true;
        }
        return false;
    }

    public static void hitradius(final int i, int r, int hp1, int hp2, int hp3, int hp4) {
        final Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        if (s.getPicnum() != RPG || s.getXrepeat() >= 11) {
            if (s.getPicnum() != SHRINKSPARK) {
                tempshort[0] = s.getSectnum();
                int sectcnt = 0;
                int sectend = 1;
                int sect = 0;

                do {
                    final int dasect = tempshort[sectcnt++];
                    Sector sec = boardService.getSector(dasect);
                    if (sec == null || sec.getWallNode() == null) {
                        continue;
                    }

                    if (((sec.getCeilingz() - s.getZ()) >> 8) < r) {
                        Wall walptr = sec.getWallNode().get();
                        int d = klabs(walptr.getX() - s.getX()) + klabs(walptr.getY() - s.getY());
                        if (d < r) {
                            checkhitceiling(dasect);
                        } else {
                            Wall w3 = walptr.getWall2().getWall2();
                            d = klabs(w3.getX() - s.getX()) + klabs(w3.getY() - s.getY());
                            if (d < r) {
                                checkhitceiling(dasect);
                            }
                        }
                    }

                    int startwall = sec.getWallptr();
                    int endwall = startwall + sec.getWallnum();
                    for (int x = startwall; x < endwall; x++) {
                        Wall wal = boardService.getWall(x);
                        if (wal != null && (klabs(wal.getX() - s.getX()) + klabs(wal.getY() - s.getY())) < r) {
                            int nextsect = wal.getNextsector();
                            if (nextsect >= 0) {
                                int ndasect;
                                for (ndasect = (sectend - 1); ndasect >= 0; ndasect--) {
                                    if (tempshort[ndasect] == nextsect) {
                                        break;
                                    }
                                }
                                if (ndasect < 0) {
                                    tempshort[sectend++] = nextsect;
                                }
                            }

                            int x1 = (((wal.getX() + wal.getWall2().getX()) >> 1) + s.getX()) >> 1;
                            int y1 = (((wal.getY() + wal.getWall2().getY()) >> 1) + s.getY()) >> 1;
                            sect = engine.updatesector(x1, y1, sect);
                            if (sect >= 0 && engine.cansee(x1, y1, s.getZ(), sect, s.getX(), s.getY(), s.getZ(), s.getSectnum())) {
                                checkhitwall(i, x, wal.getX(), wal.getY(), s.getZ(), s.getPicnum());
                            }
                        }
                    }
                } while (sectcnt < sectend);
            }
        }

        int q = -(16 << 8) + (engine.krand() & ((32 << 8) - 1));

        for (int x = 0; x < 7; x++) {
            ListNode<Sprite> node = boardService.getStatNode(statlist[x]);
            ListNode<Sprite> nextj;
            for (; node != null; node = nextj) {
                nextj = node.getNext();
                Sprite sj = node.get();
                final int j = node.getIndex();
                Sprite sowner = boardService.getSprite(s.getOwner());

                if (sowner != null && sowner.getPicnum() == APLAYER && sj.getPicnum() == APLAYER && ud.coop != 0 && ud.ffire == 0 && s.getOwner() != j) {
                    continue;
                }

                if (sowner != null && (s.getPicnum() == FLAMETHROWERFLAME && ((sowner.getPicnum() == FIREFLY && sj.getPicnum() == FIREFLY) || (sowner.getPicnum() == BOSS5 && sj.getPicnum() == BOSS5)))) {
                    continue;
                }

                if (x == 0 || x >= 5 || AFLAMABLE(sj.getPicnum())) {

                    if (s.getPicnum() != SHRINKSPARK || (sj.getCstat() & 257) != 0) {
                        if (dist(s, sj) < r) {
                            if (badguy(sj) && !engine.cansee(sj.getX(), sj.getY(), sj.getZ() + q, sj.getSectnum(), s.getX(), s.getY(), s.getZ() + q, s.getSectnum())) {
                                continue;
                            }
                            checkhitsprite(j, i);
                        }
                    }
                } else if (sj.getExtra() >= 0 && sj != s && (sj.getPicnum() == TRIPBOMB || badguy(sj) || sj.getPicnum() == QUEBALL || sj.getPicnum() == STRIPEBALL || (sj.getCstat() & 257) != 0 || sj.getPicnum() == DUKELYINGDEAD)) {
                    if (s.getPicnum() == SHRINKSPARK && sj.getPicnum() != SHARK && (j == s.getOwner() || sj.getXrepeat() < 24)) {
                        continue;
                    }
                    if (s.getPicnum() == MORTER && j == s.getOwner()) {
                        continue;
                    }

                    if (sj.getPicnum() == APLAYER) {
                        sj.setZ(sj.getZ() - PHEIGHT);
                    }
                    int d = dist(s, sj);
                    if (sj.getPicnum() == APLAYER) {
                        sj.setZ(sj.getZ() + PHEIGHT);
                    }

                    if (d < r && engine.cansee(sj.getX(), sj.getY(), sj.getZ() - (8 << 8), sj.getSectnum(), s.getX(), s.getY(), s.getZ() - (12 << 8), s.getSectnum())) {
                        hittype[j].ang = EngineUtils.getAngle(sj.getX() - s.getX(), sj.getY() - s.getY());

                        if (s.getPicnum() == RPG && sj.getExtra() > 0) {
                            hittype[j].picnum = RPG;
                        } else {
                            if (s.getPicnum() == SHRINKSPARK || s.getPicnum() == FLAMETHROWERFLAME) {
                                hittype[j].picnum = s.getPicnum();
                            } else if (s.getPicnum() != FIREBALL || (sowner != null && sowner.getPicnum() != APLAYER)) {
                                if (s.getPicnum() == LAVAPOOL) {
                                    hittype[j].picnum = FLAMETHROWERFLAME;
                                } else {
                                    hittype[j].picnum = RADIUSEXPLOSION;
                                }
                            } else {
                                hittype[j].picnum = FLAMETHROWERFLAME;
                            }
                        }

                        if (s.getPicnum() != SHRINKSPARK && s.getPicnum() != LAVAPOOL) {
                            if (d < r / 3) {
                                if (hp4 == hp3) {
                                    hp4++;
                                }
                                hittype[j].extra = hp3 + (engine.krand() % (hp4 - hp3));
                            } else if (d < 2 * r / 3) {
                                if (hp3 == hp2) {
                                    hp3++;
                                }
                                hittype[j].extra = hp2 + (engine.krand() % (hp3 - hp2));
                            } else {
                                if (hp2 == hp1) {
                                    hp2++;
                                }
                                hittype[j].extra = hp1 + (engine.krand() % (hp2 - hp1));
                            }

                            if (sj.getPicnum() != TANK && sj.getPicnum() != ROTATEGUN && sj.getPicnum() != RECON && sj.getPicnum() != BOSS1 && sj.getPicnum() != BOSS2 && sj.getPicnum() != BOSS3 && sj.getPicnum() != BOSS4) {
                                if (sj.getXvel() < 0) {
                                    sj.setXvel(0);
                                }
                                sj.setXvel(sj.getXvel() + (s.getExtra() << 2));
                            }

                            if (sj.getPicnum() == PODFEM1 || sj.getPicnum() == FEM1 || sj.getPicnum() == FEM2 || sj.getPicnum() == FEM3 || sj.getPicnum() == FEM4 || sj.getPicnum() == FEM5 || sj.getPicnum() == FEM6 || sj.getPicnum() == FEM7 || sj.getPicnum() == FEM8 || sj.getPicnum() == FEM9 || sj.getPicnum() == FEM10 || sj.getPicnum() == STATUE || sj.getPicnum() == STATUEFLASH || sj.getPicnum() == SPACEMARINE || sj.getPicnum() == QUEBALL || sj.getPicnum() == STRIPEBALL) {
                                checkhitsprite(j, i);
                            }
                        } else if (s.getExtra() == 0) {
                            hittype[j].extra = 0;
                        }

                        if (sj.getPicnum() != RADIUSEXPLOSION && sowner != null && sowner.getStatnum() < MAXSTATUS) {
                            if (sj.getPicnum() == APLAYER) {
                                int p = sj.getYvel();

                                if (hittype[j].picnum == FLAMETHROWERFLAME && sowner.getPicnum() == APLAYER) {
                                    ps[p].numloogs = -1 - s.getYvel();
//									sub_A2F7E0(p, s, j, s.yvel);
                                }

                                if (ps[p].newowner >= 0) {
                                    ps[p].newowner = -1;
                                    ps[p].posx = ps[p].oposx;
                                    ps[p].posy = ps[p].oposy;
                                    ps[p].posz = ps[p].oposz;
                                    ps[p].ang = ps[p].oang;

                                    ps[p].cursectnum = engine.updatesector(ps[p].posx, ps[p].posy, ps[p].cursectnum);
                                    setpal(ps[p]);
                                    System.err.println("a8?");
                                    ListNode<Sprite> k = boardService.getStatNode(1);
                                    while (k != null) {
                                        Sprite sp = k.get();
                                        if (sp.getPicnum() == CAMERA1) {
                                            sp.setYvel(0);
                                        }
                                        k = k.getNext();
                                    }
                                }
                            }
                            hittype[j].owner = s.getOwner();
                        }
                    }
                }
            }
        }
    }

    public static int movesprite(int spritenum, int xchange, int ychange, int zchange, int cliptype) {
        Sprite spr = boardService.getSprite(spritenum);
        if (spr == null) {
            return 0;
        }

        int moveHit;
        boolean bg = badguy(spr);

        game.pInt.setsprinterpolate(spritenum, spr);

        if (spr.getStatnum() == 5 || (bg && spr.getXrepeat() < 4)) {
            spr.setX(spr.getX() + ((xchange * TICSPERFRAME) >> 2));
            spr.setY(spr.getY() + ((ychange * TICSPERFRAME) >> 2));
            spr.setZ(spr.getZ() + ((zchange * TICSPERFRAME) >> 2));
            if (bg) {
                engine.setsprite(spritenum, spr.getX(), spr.getY(), spr.getZ());
            }
            return 0;
        }

        int dasectnum = spr.getSectnum();
        int daz = spr.getZ();
        int h = ((engine.getTile(spr.getPicnum()).getHeight() * spr.getYrepeat()) << 1);
        daz -= h;

        if (bg) {
            int oldx = spr.getX();
            int oldy = spr.getY();

            if (spr.getXrepeat() > 60) {
                moveHit = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, (((long) xchange * TICSPERFRAME) << 11), (((long) ychange * TICSPERFRAME) << 11), 1024, (4 << 8), (4 << 8), cliptype);
            } else {
                int cd = 192;
                if (spr.getPicnum() == LIZMAN) {
                    cd = 292;
                } else if ((currentGame.getCON().actortype[spr.getPicnum()] & 3) != 0) {
                    cd = spr.getClipdist() << 2;
                }

                moveHit = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, (((long) xchange * TICSPERFRAME) << 11), (((long) ychange * TICSPERFRAME) << 11), cd, (4 << 8), (4 << 8), cliptype);
            }
            spr.setX(clipmove_x);
            spr.setY(clipmove_y);
            dasectnum = clipmove_sectnum;

            Sector sec = boardService.getSector(dasectnum);
            if (sec == null || (((hittype[spritenum].actorstayput >= 0
                    && hittype[spritenum].actorstayput != dasectnum)
                    || ((spr.getPicnum() == BOSS2) && spr.getPal() == 0 && sec.getLotag() != 3)
                    || ((spr.getPicnum() == BOSS1 || spr.getPicnum() == BOSS2) && sec.getLotag() == 1)
                    || (sec.getLotag() == 1 && (spr.getPicnum() == LIZMAN
                    || (spr.getPicnum() == LIZTROOP && spr.getZvel() == 0)))))) {

                if (dasectnum < 0) {
                    dasectnum = 0;
                    sec = boardService.getSector(dasectnum);
                }

                spr.setX(oldx);
                spr.setY(oldy);
                if (sec != null && sec.getLotag() == 1 && spr.getPicnum() == LIZMAN) {
                    spr.setAng((short) (engine.krand() & 2047));
                } else if ((hittype[spritenum].temp_data[0] & 3) == 1 && spr.getPicnum() != COMMANDER) {
                    spr.setAng((short) (engine.krand() & 2047));
                }
                engine.setsprite(spritenum, oldx, oldy, spr.getZ());

                return (kHitSector + dasectnum);
            }

            int nHitType = (moveHit & kHitTypeMask);
            if ((nHitType == kHitWall || nHitType == kHitSprite) && (hittype[spritenum].cgg == 0)) {
                spr.setAng(spr.getAng() + 768);
            }
        } else {
            if (spr.getStatnum() == 4) {
                moveHit = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, (((long) xchange * TICSPERFRAME) << 11), (((long) ychange * TICSPERFRAME) << 11), 8, (4 << 8), (4 << 8), cliptype);
            } else {
                moveHit = engine.clipmove(spr.getX(), spr.getY(), daz, dasectnum, (((long) xchange * TICSPERFRAME) << 11), (((long) ychange * TICSPERFRAME) << 11), spr.getClipdist() << 2, (4 << 8), (4 << 8), cliptype);
            }
            spr.setX(clipmove_x);
            spr.setY(clipmove_y);
            dasectnum = clipmove_sectnum;
        }

        if (dasectnum >= 0) {
            if ((dasectnum != spr.getSectnum())) {
                engine.changespritesect(spritenum, dasectnum);
            }
        }
        daz = spr.getZ() + ((zchange * TICSPERFRAME) >> 3);
        if ((daz > hittype[spritenum].ceilingz) && (daz <= hittype[spritenum].floorz)) {
            spr.setZ(daz);
        } else if (moveHit == 0) {
            return (kHitSector + dasectnum);
        }

        return (moveHit);
    }

    /**
     * Move sprite
     */
    public static boolean ssp(int i, int cliptype) { // The set sprite function
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return false;
        }

        int movetype = movesprite(i, (s.getXvel() * (EngineUtils.cos((s.getAng()) & 2047))) >> 14, (s.getXvel() * (EngineUtils.sin(s.getAng() & 2047))) >> 14, s.getZvel(), cliptype);
        return (movetype == 0);
    }

    public static void insertspriteq(int i) {
        if (currentGame.getCON().spriteqamount > 0) {
            Sprite qspr = boardService.getSprite(spriteq[spriteqloc]);
            if (qspr != null) {
                qspr.setXrepeat(0);
            }
            spriteq[spriteqloc] = (short) i;
            spriteqloc = (short) ((spriteqloc + 1) % currentGame.getCON().spriteqamount);
        } else {
            Sprite spr = boardService.getSprite(i);
            if (spr != null) {
                spr.setXrepeat(0);
                spr.setYrepeat(0);
            }
        }
    }

    public static void ms(final int i) {
        // T1,T2 and T3 are used for all the sector moving stuff!!!
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        s.setX(s.getX() + ((s.getXvel() * (EngineUtils.cos((s.getAng()) & 2047))) >> 14));
        s.setY(s.getY() + ((s.getXvel() * (EngineUtils.sin(s.getAng() & 2047))) >> 14));

        int j = hittype[i].temp_data[1];
        int k = hittype[i].temp_data[2];

        Sector sec = boardService.getSector(s.getSectnum());
        if (sec != null) {
            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Point p = EngineUtils.rotatepoint(0, 0, msx[j], msy[j], k & 2047);
                engine.dragpoint(wn.getIndex(), s.getX() + p.getX(), s.getY() + p.getY());
                j++;
            }
        }
    }

    public static void movefta() {
        ListNode<Sprite> ni = boardService.getStatNode(2);
        while (ni != null) {
            ListNode<Sprite> nexti = ni.getNext();
            final Sprite s = ni.get();
            final int i = ni.getIndex();
            int p = findplayer(s);
            int psect = s.getSectnum();
            int ssect = psect;

            Sprite psp = boardService.getSprite(ps[p].i);

            if (psp != null && psp.getExtra() > 0) {
                if (player_dist < 30000) {
                    hittype[i].timetosleep++;
                    if (hittype[i].timetosleep >= (player_dist >> 8)) {
                        boolean cansee;
                        if (badguy(s)) {
                            int px = ps[p].oposx + 64 - (engine.krand() & 127);
                            int py = ps[p].oposy + 64 - (engine.krand() & 127);
                            psect = engine.updatesector(px, py, psect);
                            if (psect == -1) {
                                ni = nexti;
                                continue;
                            }
                            int sx = s.getX() + 64 - (engine.krand() & 127);
                            int sy = s.getY() + 64 - (engine.krand() & 127);
                            ssect = engine.updatesector(px, py, ssect);
                            if (ssect == -1) {
                                ni = nexti;
                                continue;
                            }

                            int pz = ps[p].oposz - (engine.krand() % (32 << 8));
                            int sz = s.getZ() - (engine.krand() % (52 << 8));
                            cansee = engine.cansee(sx, sy, sz, s.getSectnum(), px, py, pz, ps[p].cursectnum);
                        } else {
                            int pz = ps[p].oposz - ((engine.krand() & 31) << 8);
                            int sz = s.getZ() - ((engine.krand() & 31) << 8);
                            cansee = engine.cansee(s.getX(), s.getY(), sz, s.getSectnum(), ps[p].oposx, ps[p].oposy, pz, ps[p].cursectnum);
                        }

                        if (cansee) {
                            switch (s.getPicnum()) {
                                case RUBBERCAN:
                                case EXPLODINGBARREL:
                                case WOODENHORSE:
                                case HORSEONSIDE:
                                case CANWITHSOMETHING:
                                case CANWITHSOMETHING2:
                                case CANWITHSOMETHING3:
                                case CANWITHSOMETHING4:
                                case FIREBARREL:
                                case FIREVASE:
                                case NUKEBARREL:
                                case NUKEBARRELDENTED:
                                case NUKEBARRELLEAKED:
                                case TRIPBOMB:
                                    Sector sec = boardService.getSector(s.getSectnum());
                                    if (sec != null) {
                                        if ((sec.getCeilingstat() & 1) != 0) {
                                            s.setShade(sec.getCeilingshade());
                                        } else {
                                            s.setShade(sec.getFloorshade());
                                        }
                                    }

                                    hittype[i].timetosleep = 0;
                                    engine.changespritestat(i, (short) 6);
                                    break;
                                default:
                                    hittype[i].timetosleep = 0;
                                    check_fta_sounds(i);
                                    engine.changespritestat(i, (short) 1);
                                    break;
                            }
                        } else {
                            hittype[i].timetosleep = 0;
                        }
                    }
                }
                if (badguy(s)) {
                    Sector sec = boardService.getSector(s.getSectnum());
                    if (sec != null) {
                        if ((sec.getCeilingstat() & 1) != 0) {
                            s.setShade(sec.getCeilingshade());
                        } else {
                            s.setShade(sec.getFloorshade());
                        }
                    }
                }
            }
            ni = nexti;
        }
    }

    public static int ifhitsectors(short sectnum) {
        ListNode<Sprite> ni = boardService.getStatNode(5);
        while (ni != null) {
            Sprite sp = ni.get();
            if (sp.getPicnum() == EXPLOSION2 && sectnum == sp.getSectnum()) {
                return ni.getIndex();
            }
            ni = ni.getNext();
        }
        return -1;
    }

    public static int ifhitbyweapon(final int sn) {
        Sprite npc = boardService.getSprite(sn);
        if (npc == null) {
            return -1;
        }

        if (hittype[sn].extra >= 0) {
            if (npc.getExtra() >= 0) {
                if (npc.getPicnum() == APLAYER) {
                    if (ud.god && hittype[sn].picnum != SHRINKSPARK) {
                        return -1;
                    }

                    int p = npc.getYvel();
                    final int j = hittype[sn].owner;
                    Sprite jspr = boardService.getSprite(j);

                    if (jspr != null && jspr.getPicnum() == APLAYER && ud.coop == 1 && ud.ffire == 0) {
                        return -1;
                    }

                    npc.setExtra(npc.getExtra() - hittype[sn].extra);

                    if (jspr != null) {
                        if (npc.getExtra() <= 0 && hittype[sn].picnum != FREEZEBLAST) {
                            npc.setExtra(0);

                            ps[p].wackedbyactor = (short) j;

                            if (jspr.getPicnum() == APLAYER && p != jspr.getYvel()) {
                                ps[p].frag_ps = jspr.getYvel();
                            }

                            hittype[sn].owner = ps[p].i;
                        }
                    }

                    switch (hittype[sn].picnum) {
                        case RADIUSEXPLOSION:
                        case RPG:
                        case HYDRENT:
                        case HEAVYHBOMB:
                        case SEENINE:
                        case OOZFILTER:
                        case EXPLODINGBARREL:
                            ps[p].posxv += hittype[sn].extra * (EngineUtils.cos(hittype[sn].ang & 2047)) << 2;
                            ps[p].posyv += hittype[sn].extra * (EngineUtils.sin(hittype[sn].ang & 2047)) << 2;
                            break;
                        default:
                            ps[p].posxv += hittype[sn].extra * (EngineUtils.cos(hittype[sn].ang & 2047)) << 1;
                            ps[p].posyv += hittype[sn].extra * (EngineUtils.sin(hittype[sn].ang & 2047)) << 1;
                            break;
                    }
                } else {
                    if (hittype[sn].extra == 0) {
                        if (hittype[sn].picnum == SHRINKSPARK && npc.getXrepeat() < 24) {
                            return -1;
                        }
                    }

                    if (hittype[sn].picnum == FIREFLY && npc.getXrepeat() < 48) {
                        if (hittype[sn].picnum != RADIUSEXPLOSION && hittype[sn].picnum != RPG) {
                            return -1;
                        }
                    }

                    npc.setExtra(npc.getExtra() - hittype[sn].extra);
                    Sprite npcOwner = boardService.getSprite(npc.getOwner());
                    if (npc.getPicnum() != RECON && npcOwner != null && npcOwner.getStatnum() < MAXSTATUS) {
                        npc.setOwner((short) hittype[sn].owner);
                    }
                }

                hittype[sn].extra = -1;
                return hittype[sn].picnum;
            }
        }

        if (ud.multimode < 2 || hittype[sn].picnum != FLAMETHROWERFLAME || hittype[sn].extra >= 0 || npc.getExtra() > 0 || npc.getPicnum() != APLAYER || ps[npc.getYvel()].numloogs > 0 || hittype[sn].owner < 0) {
            hittype[sn].extra = -1;
            return -1;
        } else {
            Sprite jspr = boardService.getSprite(hittype[sn].owner);
            if (jspr != null) {
                int p = npc.getYvel();
                npc.setExtra(0);

                ps[p].wackedbyactor = (short) hittype[sn].owner;
                if (jspr.getPicnum() == APLAYER && p != hittype[sn].owner) {
                    ps[p].frag_ps = (short) hittype[sn].owner;
                }

                hittype[sn].owner = ps[p].i;
                hittype[sn].extra = -1;

                return FLAMETHROWERFLAME;
            }
        }
        return -1;
    }

    public static void movecyclers() {
        for (int q = numcyclers - 1; q >= 0; q--) {
            Sector sec = boardService.getSector(cyclers[q][0]);
            if (sec == null) {
                continue;
            }

            int t = cyclers[q][3];
            int j = (t + (EngineUtils.sin(cyclers[q][1] & 2047) >> 10));
            byte cshade = (byte) cyclers[q][2];

            if (j < cshade) {
                j = cshade;
            } else if (j > t) {
                j = t;
            }

            cyclers[q][1] += sec.getExtra();
            if (cyclers[q][5] != 0) {
                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    if (wal.getHitag() != 1) {
                        wal.setShade(j);
                        Wall nwal = boardService.getWall(wal.getNextwall());
                        if ((wal.getCstat() & 2) != 0 && nwal != null) {
                            nwal.setShade(j);
                        }
                    }
                }
                sec.setCeilingshade(j);
                sec.setFloorshade(j);
            }
        }
    }

    public static void movedummyplayers() {
        ListNode<Sprite> ni = boardService.getStatNode(13);
        while (ni != null) {
            ListNode<Sprite> nexti = ni.getNext();
            final int i = ni.getIndex();
            Sprite s = ni.get();
            Sprite sonwer = boardService.getSprite(s.getOwner());
            if (sonwer == null) {
                ni = nexti;
                continue;
            }

            int p = sonwer.getYvel();
            Sprite psp = boardService.getSprite(ps[p].i);
            Sector psec = boardService.getSector(ps[p].cursectnum);

            if (ps[p].on_crane >= 0 || (psec != null && psec.getLotag() != 1) || (psp != null && psp.getExtra() <= 0)) {
                ps[p].dummyplayersprite = -1;
                engine.deletesprite(i);
                ni = nexti;
                continue;
            } else {
                Sector sec = boardService.getSector(s.getSectnum());
                if (sec != null) {
                    if (ps[p].on_ground && ps[p].on_warping_sector == 1 && psec != null && psec.getLotag() == 1) {
                        s.setCstat(257);
                        s.setZ(sec.getCeilingz() + (27 << 8));
                        s.setAng((short) ps[p].ang);
                        if (hittype[i].temp_data[0] == 8) {
                            hittype[i].temp_data[0] = 0;
                        } else {
                            hittype[i].temp_data[0]++;
                        }
                    } else {
                        if (sec.getLotag() != 2) {
                            s.setZ(sec.getFloorz());
                        }
                        s.setCstat(32768);
                    }
                }
            }

            s.setX(s.getX() + (ps[p].posx - ps[p].oposx));
            s.setY(s.getY() + (ps[p].posy - ps[p].oposy));
            engine.setsprite(i, s.getX(), s.getY(), s.getZ());

            ni = nexti;
        }
    }

    public static void moveplayers() { // Players
        ListNode<Sprite> ni = boardService.getStatNode(10);
        while (ni != null) {
            ListNode<Sprite> nexti = ni.getNext();
            Sprite s = ni.get();
            final int i = ni.getIndex();

            PlayerStruct p = ps[s.getYvel()];
            if (s.getOwner() >= 0) {
                if (p.newowner >= 0) {// Looking thru the camera
                    game.pInt.setsprinterpolate(i, s);
                    s.setX(p.oposx);
                    s.setY(p.oposy);
                    s.setZ(p.oposz + PHEIGHT);
                    s.setAng((short) p.oang);
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                } else {
                    int otherx = 0;
                    if (ud.multimode > 1) {
                        otherp = findotherplayer(s.getYvel());
                        otherx = player_dist;
                    } else {
                        otherp = s.getYvel();
                    }

                    execute(currentGame.getCON(), i, s.getYvel(), otherx);

                    if (ud.multimode > 1) {
                        Sprite psp = boardService.getSprite(ps[otherp].i);
                        if (psp != null && psp.getExtra() > 0) {
                            if (s.getYrepeat() > 32 && psp.getYrepeat() < 32) {
                                if (otherx < 1400 && p.knee_incs == 0) {
                                    p.knee_incs = 1;
                                    p.weapon_pos = -1;
                                    p.actorsqu = ps[otherp].i;
                                }
                            }
                        }
                    }

                    if (ud.god) {
                        s.setExtra((short) currentGame.getCON().max_player_health);
                        s.setCstat(257);
                        p.jetpack_amount = 1599;
                    }

                    if (s.getExtra() > 0) {
                        hittype[i].owner = i;

                        if (!ud.god) {
                            if (ceilingspace(s.getSectnum()) || floorspace(s.getSectnum())) {
                                quickkill(p);
                            }
                        }
                    } else {
                        p.posx = s.getX();
                        p.posy = s.getY();
                        p.posz = s.getZ() - (20 << 8);
                        p.newowner = -1;

                        Sprite wackedSpr = boardService.getSprite(p.wackedbyactor);
                        if (wackedSpr != null && wackedSpr.getStatnum() < MAXSTATUS) {
                            p.ang += getincangle(p.ang, EngineUtils.getAngle(wackedSpr.getX() - p.posx, wackedSpr.getY() - p.posy)) / 2.0f;
                            p.ang = BClampAngle(p.ang);
                        }

                        if (ud.multimode < 2 && lastload != null && lastload.exists() && !DemoScreen.isDemoPlaying()) {
                            if (game.getProcessor().isKeyJustPressed(Input.Keys.ENTER)) {
                                game.changeScreen(gLoadingScreen.setTitle(lastload.getName()));
                                gLoadingScreen.init(() -> {
                                    if (!loadgame(lastload)) {
                                        game.GameMessage("Can't load game!");
                                    }
                                });
                                return;
                            }
                        }
                    }
                    s.setAng((short) p.ang);
                }
            } else {
                if (p.holoduke_on == -1) {
                    engine.deletesprite(i);
                    ni = nexti;
                    continue;
                }

                game.pInt.setsprinterpolate(i, s);

                s.setCstat(0);

                if (s.getXrepeat() < 42) {
                    s.setXrepeat(s.getXrepeat() + 4);
                    s.setCstat(s.getCstat() | 2);
                } else {
                    s.setXrepeat(42);
                }
                if (s.getYrepeat() < 36) {
                    s.setYrepeat(s.getYrepeat() + 4);
                } else {
                    s.setYrepeat(36);
                    Sector sec = boardService.getSector(s.getSectnum());
                    if (sec != null) {
                        if (sec.getLotag() != 2) {
                            makeitfall(currentGame.getCON(), i);
                        }

                        if (s.getZvel() == 0 && sec.getLotag() == 1) {
                            s.setZ(s.getZ() + (32 << 8));
                        }
                    }
                }

                if (s.getExtra() < 8) {
                    s.setXvel(128);
                    s.setAng((short) p.ang);
                    s.setExtra(s.getExtra() + 1);
                    ssp(i, CLIPMASK0);
                } else {
                    s.setAng((short) (2047 - p.ang));
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                }
            }

            Sector sec = boardService.getSector(s.getSectnum());
            if (sec != null) {
                if ((sec.getCeilingstat() & 1) != 0) {
                    s.setShade(s.getShade() + ((sec.getCeilingshade() - s.getShade()) >> 1));
                } else {
                    s.setShade(s.getShade() + ((sec.getFloorshade() - s.getShade()) >> 1));
                }
            }

            ni = nexti;
        }
    }

    public static void movefx() {
        ListNode<Sprite> ni = boardService.getStatNode(11);
        while (ni != null) {
            ListNode<Sprite> nexti = ni.getNext();
            final Sprite s = ni.get();
            final int i = ni.getIndex();

            switch (s.getPicnum()) {
                case RESPAWN:
                    if (s.getExtra() == 66) {
                        spawn(i, s.getHitag());
                        engine.deletesprite(i);
                        ni = nexti;
                        continue;
                    } else if (s.getExtra() > (66 - 13)) {
                        s.setExtra(s.getExtra() + 1);
                    }
                    break;
                case MUSICANDSFX:
                    int ht = s.getHitag();
                    if (hittype[i].temp_data[1] != (cfg.isNoSound() ? 0 : 1)) {
                        hittype[i].temp_data[1] = cfg.isNoSound() ? 0 : 1;
                        hittype[i].temp_data[0] = 0;
                    }

                    Sprite psp = boardService.getSprite(ps[screenpeek].i);
                    if (psp == null) {
                        break;
                    }

                    Sector sec = boardService.getSector(s.getSectnum());
                    if (s.getLotag() >= 1000 && s.getLotag() < 2000) {
                        int x = ldist(psp, s);
                        if (x < ht && hittype[i].temp_data[0] == 0) {
                            Sounds.setReverb(true, (s.getLotag() - 1000) / 255f);
                            hittype[i].temp_data[0] = 1;
                        }
                        if (x >= ht && hittype[i].temp_data[0] == 1) {
                            Sounds.setReverb(false, 0);
                            hittype[i].temp_data[0] = 0;
                        }
                    } else if (sec != null && s.getLotag() < 999 && sec.getLotag() < 9
                            && cfg.AmbienceToggle && sec.getFloorz() != sec.getCeilingz()) {
                        if (s.getLotag() < NUM_SOUNDS && (currentGame.getCON().soundm[s.getLotag()] & 2) != 0) {
                            int x = dist(psp, s);
                            if (x < ht && hittype[i].temp_data[0] == 0 && Sounds.isAvailable(currentGame.getCON().soundpr[s.getLotag()] - 1)) {
                                if (numenvsnds == cfg.getMaxvoices()) {
                                    ListNode<Sprite> nj = boardService.getStatNode(11);
                                    while (nj != null) {
                                        int index = nj.getIndex();
                                        Sprite sp = nj.get();
                                        if (s.getPicnum() == MUSICANDSFX && index != i && sp.getLotag() < 999 && hittype[index].temp_data[0] == 1
                                                && dist(sp, psp) > x) {
                                            stopenvsound(sp.getLotag(), index);
                                            break;
                                        }
                                        nj = nj.getNext();
                                    }
                                    if (nj == null) {
                                        ni = nexti;
                                        continue;
                                    }
                                }
                                spritesound(s.getLotag(), i);
                                hittype[i].temp_data[0] = 1;
                            }
                            if (x >= ht && hittype[i].temp_data[0] == 1) {
                                hittype[i].temp_data[0] = 0;
                                stopenvsound(s.getLotag(), i);
                            }
                        }
                        if (s.getLotag() < NUM_SOUNDS && (currentGame.getCON().soundm[s.getLotag()] & 16) != 0) {
                            if (hittype[i].temp_data[4] > 0) {
                                hittype[i].temp_data[4]--;
                            } else {
                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    if (p == myconnectindex && ps[p].cursectnum == s.getSectnum()) {
                                        int j = s.getLotag() + ((global_random & 0xFFFF) % (s.getHitag() + 1));
                                        sound(j);
                                        hittype[i].temp_data[4] = 26 * 40 + (global_random % (26 * 40));
                                    }
                                }
                            }
                        }
                    }
                    break;
            }
            ni = nexti;
        }
    }

    public static void movefallers() {
        ListNode<Sprite> ni = boardService.getStatNode(12);
        while (ni != null) {
            ListNode<Sprite> nexti = ni.getNext();
            final Sprite s = ni.get();
            final int i = ni.getIndex();
            Sector sec = boardService.getSector(s.getSectnum());

            if (hittype[i].temp_data[0] == 0) {
                s.setZ(s.getZ() - (16 << 8));
                hittype[i].temp_data[1] = s.getAng();
                int x = s.getExtra();

                int id = ifhitbyweapon(i);
                if (id >= 0) {
                    if (id == FIREEXT || id == RPG || id == RADIUSEXPLOSION || id == SEENINE || id == OOZFILTER) {
                        if (s.getExtra() <= 0) {
                            hittype[i].temp_data[0] = 1;
                            ListNode<Sprite> node = boardService.getStatNode(12);
                            while (node != null) {
                                Sprite sprite = node.get();
                                if (sprite.getHitag() == s.getHitag()) {
                                    hittype[node.getIndex()].temp_data[0] = 1;
                                    sprite.setCstat(sprite.getCstat() & (65535 - 64));
                                    if (sprite.getPicnum() == CEILINGSTEAM || sprite.getPicnum() == STEAM) {
                                        sprite.setCstat(sprite.getCstat() | 32768);
                                    }
                                }
                                node = node.getNext();
                            }
                        }
                    } else {
                        hittype[i].extra = 0;
                        s.setExtra(x);
                    }
                }
                s.setAng(hittype[i].temp_data[1]);
                s.setZ(s.getZ() + (16 << 8));
            } else if (hittype[i].temp_data[0] == 1) {
                if (s.getLotag() > 0) {
                    s.setLotag(s.getLotag() - 3);
                    if (s.getLotag() <= 0) {
                        s.setXvel((32 + engine.krand() & 63));
                        s.setZvel(-(1024 + (engine.krand() & 1023)));
                    }
                } else {
                    if (s.getXvel() > 0) {
                        s.setXvel(s.getXvel() - 8);
                        ssp(i, CLIPMASK0);
                    }

                    int x;
                    if (floorspace(s.getSectnum())) {
                        x = 0;
                    } else {
                        if (ceilingspace(s.getSectnum())) {
                            x = currentGame.getCON().gc / 6;
                        } else {
                            x = currentGame.getCON().gc;
                        }
                    }

                    if (sec != null) {
                        if (s.getZ() < (sec.getFloorz() - FOURSLEIGHT)) {
                            s.setZvel(s.getZvel() + x);
                            if (s.getZvel() > 6144) {
                                s.setZvel(6144);
                            }
                            s.setZ(s.getZ() + s.getZvel());
                        }

                        if ((sec.getFloorz() - s.getZ()) < (16 << 8)) {
                            int j = 1 + (engine.krand() & 7);
                            for (int xx = 0; xx < j; xx++) {
                                RANDOMSCRAP(s, i);
                            }
                            engine.deletesprite(i);
                        }
                    }
                }
            }

            ni = nexti;
        }
    }

    private static void Detonate(Sprite s, int i, int[] t) {
        earthquaketime = 16;

        ListNode<Sprite> ni = boardService.getStatNode(3);
        while (ni != null) {
            Sprite sp = ni.get();
            int j = ni.getIndex();
            if (s.getHitag() == sp.getHitag()) {
                if (sp.getLotag() == 13) {
                    if (hittype[j].temp_data[2] == 0) {
                        hittype[j].temp_data[2] = 1;
                    }
                } else if (sp.getLotag() == 8) {
                    hittype[j].temp_data[4] = 1;
                } else if (sp.getLotag() == 18) {
                    if (hittype[j].temp_data[0] == 0) {
                        hittype[j].temp_data[0] = 1;
                    }
                } else if (sp.getLotag() == 21) {
                    hittype[j].temp_data[0] = 1;
                }
            }
            ni = ni.getNext();
        }

        s.setZ(s.getZ() - (32 << 8));
        if ((t[3] == 1 && s.getXrepeat() != 0) || s.getLotag() == -99) {
            int x = s.getExtra();
            spawn(i, EXPLOSION2);
            hitradius(i, currentGame.getCON().seenineblastradius, x >> 2, x - (x >> 1), x - (x >> 2), x);
            spritesound(PIPEBOMB_EXPLODE, i);
        }

        if (s.getXrepeat() != 0) {
            for (int x = 0; x < 8; x++) {
                RANDOMSCRAP(s, i);
            }
        }

        engine.deletesprite(i);
    }

    public static void movestandables() {
        ListNode<Sprite> ni = boardService.getStatNode(6), nexti;

        BOLT:
        for (; ni != null; ni = nexti) {
            nexti = ni.getNext();
            final int i = ni.getIndex();
            final Sprite s = ni.get();

            int[] t = hittype[i].temp_data;
            final int sect = s.getSectnum();
            Sector sec = boardService.getSector(sect);
            if (sec == null) {
                engine.deletesprite(i);
                continue;
            }

            game.pInt.setsprinterpolate(i, s);

            if (IFWITHIN(s, CRANE, CRANE + 3)) {
                // t[0] = state
                // t[1] = checking sector number

                if (s.getXvel() != 0) {
                    getglobalz(i);
                }

                if (t[0] == 0) // Waiting to check the sector
                {
                    ListNode<Sprite> nj = boardService.getSectNode(t[1]), nextj;
                    while (nj != null) {
                        nextj = nj.getNext();
                        Sprite sp = nj.get();
                        switch (sp.getStatnum()) {
                            case 1:
                            case 2:
                            case 6:
                            case 10:
                                s.setAng(EngineUtils.getAngle(msx[t[4] + 1] - s.getX(), msy[t[4] + 1] - s.getY()));
                                engine.setsprite(nj.getIndex(), msx[t[4] + 1], msy[t[4] + 1], sp.getZ());
                                t[0]++;
                                continue BOLT;
                        }
                        nj = nextj;
                    }
                } else if (t[0] == 1) {
                    if (s.getXvel() < 184) {
                        s.setPicnum(CRANE + 1);
                        s.setXvel(s.getXvel() + 8);
                    }
                    ssp(i, CLIPMASK0);
                    if (sect == t[1]) {
                        t[0]++;
                    }
                } else if (t[0] == 2 || t[0] == 7) {
                    s.setZ(s.getZ() + (1024 + 512));

                    if (t[0] == 2) {
                        if ((sec.getFloorz() - s.getZ()) < (64 << 8)) {
                            if (s.getPicnum() > CRANE) {
                                s.setPicnum(s.getPicnum() - 1);
                            }
                        }

                        if ((sec.getFloorz() - s.getZ()) < (4096 + 1024)) {
                            t[0]++;
                        }
                    }
                    if (t[0] == 7) {
                        if ((sec.getFloorz() - s.getZ()) < (64 << 8)) {
                            if (s.getPicnum() > CRANE) {
                                s.setPicnum(s.getPicnum() - 1);
                            } else {
                                if (s.getOwner() == -2) {
                                    int p = findplayer(s);
                                    spritesound(DUKE_GRUNT, ps[p].i);
                                    if (ps[p].on_crane == i) {
                                        ps[p].on_crane = -1;
                                    }
                                }
                                t[0]++;
                                s.setOwner(-1);
                            }
                        }
                    }
                } else if (t[0] == 3) {
                    s.setPicnum(s.getPicnum() + 1);
                    if (s.getPicnum() == (CRANE + 2)) {
                        int p = checkcursectnums(t[1]);
                        if (p >= 0 && ps[p].on_ground) {
                            s.setOwner(-2);
                            ps[p].on_crane = (short) i;
                            spritesound(DUKE_GRUNT, ps[p].i);
                            ps[p].ang = BClampAngle(s.getAng() + 1024);

                            ps[p].posxv = 0;
                            ps[p].posyv = 0;
                            ps[p].poszv = 0;
                            Sprite psp = boardService.getSprite(ps[p].i);
                            if (psp != null) {
                                psp.setXvel(0);
                            }
                            ps[p].look_ang = 0;
                            ps[p].rotscrnang = 0;
                        } else {
                            ListNode<Sprite> node = boardService.getSectNode(t[1]), nextj;
                            while (node != null) {
                                nextj = node.getNext();
                                switch (node.get().getStatnum()) {
                                    case 1:
                                    case 6:
                                        s.setOwner(node.getIndex());
                                        break;
                                }
                                node = nextj;
                            }
                        }

                        t[0]++;// Grabbed the sprite
                        t[2] = 0;
                        continue;
                    }
                } else if (t[0] == 4) // Delay before going up
                {
                    t[2]++;
                    if (t[2] > 10) {
                        t[0]++;
                    }
                } else if (t[0] == 5 || t[0] == 8) {
                    if (t[0] == 8 && s.getPicnum() < (CRANE + 2)) {
                        if ((sec.getFloorz() - s.getZ()) > 8192) {
                            s.setPicnum(s.getPicnum() + 1);
                        }
                    }

                    if (s.getZ() < msx[t[4] + 2]) {
                        t[0]++;
                        s.setXvel(0);
                    } else {
                        s.setZ(s.getZ() - (1024 + 512));
                    }
                } else if (t[0] == 6) {
                    if (s.getXvel() < 192) {
                        s.setXvel(s.getXvel() + 8);
                    }
                    s.setAng(EngineUtils.getAngle(msx[t[4]] - s.getX(), msy[t[4]] - s.getY()));
                    ssp(i, CLIPMASK0);
                    if (((s.getX() - msx[t[4]]) * (s.getX() - msx[t[4]]) + (s.getY() - msy[t[4]]) * (s.getY() - msy[t[4]])) < (128 * 128)) {
                        t[0]++;
                    }
                } else if (t[0] == 9) {
                    t[0] = 0;
                }

                if (boardService.isValidSprite(msy[t[4] + 2])) {
                    game.pInt.setsprinterpolate(msy[t[4] + 2], boardService.getSprite(msy[t[4] + 2]));
                    engine.setsprite((short) msy[t[4] + 2], s.getX(), s.getY(), s.getZ() - (34 << 8));
                }

                if (s.getOwner() != -1) {
                    int p = findplayer(s);
                    int j = ifhitbyweapon(i);
                    if (j >= 0) {
                        if (s.getOwner() == -2) {
                            if (ps[p].on_crane == i) {
                                ps[p].on_crane = -1;
                            }
                        }
                        s.setOwner(-1);
                        s.setPicnum(CRANE);
                        continue;
                    }

                    if (s.getOwner() >= 0) {
                        engine.setsprite(s.getOwner(), s.getX(), s.getY(), s.getZ());

                        s.setZvel(0);
                    } else if (s.getOwner() == -2) // Transport XXX
                    {
                        ps[p].oposx = ps[p].posx;
                        ps[p].oposy = ps[p].posy;
                        ps[p].oposz = ps[p].posz;
                        ps[p].oang = ps[p].ang;
                        ps[p].opyoff = ps[p].pyoff;

                        if (IsOriginalDemo()) {
                            ps[p].posx = s.getX() - (EngineUtils.cos(((int) ps[p].ang) & 2047) >> 6);
                            ps[p].posy = s.getY() - (EngineUtils.sin((int) ps[p].ang & 2047) >> 6);
                        } else {
                            ps[p].posx = (int) (s.getX() - (BCosAngle(BClampAngle(ps[p].ang)) / 64.0f));
                            ps[p].posy = (int) (s.getY() - (BSinAngle(BClampAngle(ps[p].ang)) / 64.0f));
                        }

                        ps[p].posz = s.getZ() + (2 << 8);
                        engine.setsprite(ps[p].i, ps[p].posx, ps[p].posy, ps[p].posz);
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            ps[p].cursectnum = psp.getSectnum();
                        }
                    }
                }

                continue;
            }

            if (IFWITHIN(s, WATERFOUNTAIN, WATERFOUNTAIN + 3)) {
                if (t[0] > 0) {
                    if (t[0] < 20) {
                        t[0]++;

                        s.setPicnum(s.getPicnum() + 1);

                        if (s.getPicnum() == (WATERFOUNTAIN + 3)) {
                            s.setPicnum(WATERFOUNTAIN + 1);
                        }
                    } else {
                        findplayer(s);
                        int x = player_dist;
                        if (x > 512) {
                            t[0] = 0;
                            s.setPicnum(WATERFOUNTAIN);
                        } else {
                            t[0] = 1;
                        }
                    }
                }
                continue;
            }

            if (AFLAMABLE(s.getPicnum())) {
                if (hittype[i].temp_data[0] == 1) {
                    hittype[i].temp_data[1]++;
                    if ((hittype[i].temp_data[1] & 3) > 0) {
                        continue;
                    }

                    if (s.getPicnum() == TIRE && hittype[i].temp_data[1] == 32) {
                        s.setCstat(0);
                        int j = spawn(i, BLOODPOOL);
                        Sprite sp = boardService.getSprite(j);
                        if (sp != null) {
                            sp.setShade(127);
                        }
                    } else {
                        if (s.getShade() < 64) {
                            s.setShade(s.getShade() + 1);
                        } else {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    int j = s.getXrepeat() - (engine.krand() & 7);
                    if (j < 10) {
                        engine.deletesprite(i);
                        continue;
                    }

                    s.setXrepeat((short) j);

                    j = s.getYrepeat() - (engine.krand() & 7);
                    if (j < 4) {
                        engine.deletesprite(i);
                        continue;
                    }
                    s.setYrepeat(j);
                }
                if (s.getPicnum() == BOX) {
                    makeitfall(currentGame.getCON(), i);
                    Sector sec2 = boardService.getSector(s.getSectnum());
                    if (sec2 != sec) { // FIXME: Check and delete
                        Console.out.println("Actors(1500) sec2 != sec", OsdColor.RED);
                    }

                    if (sec2 != null) {
                        hittype[i].ceilingz = sec2.getCeilingz();
                    }
                }
                continue;
            }

            if (s.getPicnum() == TRIPBOMB) {
                if (hittype[i].temp_data[2] > 0) {
                    hittype[i].temp_data[2]--;
                    if (hittype[i].temp_data[2] == 8) {
                        spritesound(LASERTRIP_EXPLODE, i);
                        for (int j = 0; j < 5; j++) {
                            RANDOMSCRAP(s, i);
                        }
                        int x = s.getExtra();
                        hitradius(i, currentGame.getCON().tripbombblastradius, x >> 2, x >> 1, x - (x >> 2), x);

                        int j = spawn(i, EXPLOSION2);
                        Sprite spr = boardService.getSprite(j);
                        if (spr != null) {
                            spr.setAng(s.getAng());
                            spr.setXvel(348);
                            ssp(j, CLIPMASK0);
                        }

                        ListNode<Sprite> node = boardService.getStatNode(5);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (sp.getPicnum() == LASERLINE && s.getHitag() == sp.getHitag()) {
                                sp.setXrepeat(0);
                                sp.setYrepeat(0);
                            }
                            node = node.getNext();
                        }
                        engine.deletesprite(i);
                    }
                    continue;
                } else {
                    int x = s.getExtra();
                    s.setExtra(1);
                    int l = s.getAng();
                    int j = ifhitbyweapon(i);
                    if (j >= 0) {
                        hittype[i].temp_data[2] = 16;
                    }
                    s.setExtra((short) x);
                    s.setAng((short) l);
                }

                if (hittype[i].temp_data[0] < 32) {
                    findplayer(s);
                    int x = player_dist;
                    if (x > 768) {
                        hittype[i].temp_data[0]++;
                    } else if (hittype[i].temp_data[0] > 16) {
                        hittype[i].temp_data[0]++;
                    }
                }
                if (hittype[i].temp_data[0] == 32) {
                    int l = s.getAng();
                    s.setAng((short) hittype[i].temp_data[5]);

                    hittype[i].temp_data[3] = s.getX();
                    hittype[i].temp_data[4] = s.getY();
                    s.setX(s.getX() + (EngineUtils.sin((hittype[i].temp_data[5] + 512) & 2047) >> 9));
                    s.setY(s.getY() + (EngineUtils.sin((hittype[i].temp_data[5]) & 2047) >> 9));
                    s.setZ(s.getZ() - (3 << 8));
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                    int x = hitasprite(i);
                    int m = pHitInfo.hitsprite;

                    hittype[i].lastvx = x;

                    s.setAng((short) l);

                    while (x > 0) {
                        int j = spawn(i, LASERLINE);
                        Sprite sp = boardService.getSprite(j);
                        if (sp != null) {
                            engine.setsprite(j, sp.getX(), sp.getY(), sp.getZ());
                            sp.setHitag(s.getHitag());
                            hittype[j].temp_data[1] = sp.getZ();

                            s.setX(s.getX() + (EngineUtils.cos((hittype[i].temp_data[5]) & 2047) >> 4));
                            s.setY(s.getY() + (EngineUtils.sin((hittype[i].temp_data[5]) & 2047) >> 4));

                            if (x < 1024) {
                                sp.setXrepeat((short) (x >> 5));
                                break;
                            }
                        }
                        x -= 1024;
                    }

                    hittype[i].temp_data[0]++;
                    s.setX(hittype[i].temp_data[3]);
                    s.setY(hittype[i].temp_data[4]);
                    s.setZ(s.getZ() + (3 << 8));
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    hittype[i].temp_data[3] = 0;
                    if (m >= 0) {
                        hittype[i].temp_data[2] = 13;
                        spritesound(LASERTRIP_ARMING, i);
                    } else {
                        hittype[i].temp_data[2] = 0;
                    }
                }
                if (hittype[i].temp_data[0] == 33) {
                    hittype[i].temp_data[1]++;

                    hittype[i].temp_data[3] = s.getX();
                    hittype[i].temp_data[4] = s.getY();
                    s.setX(s.getX() + (EngineUtils.sin((hittype[i].temp_data[5] + 512) & 2047) >> 9));
                    s.setY(s.getY() + (EngineUtils.sin((hittype[i].temp_data[5]) & 2047) >> 9));
                    s.setZ(s.getZ() - (3 << 8));
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                    int x = hitasprite(i);

                    s.setX(hittype[i].temp_data[3]);
                    s.setY(hittype[i].temp_data[4]);
                    s.setZ(s.getZ() + (3 << 8));
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                    if (hittype[i].lastvx != x) {
                        hittype[i].temp_data[2] = 13;
                        spritesound(LASERTRIP_ARMING, i);
                    }
                }
                continue;
            }

            if (s.getPicnum() >= CRACK1 && s.getPicnum() <= CRACK4) {
                if (s.getHitag() > 0) {
                    t[0] = s.getCstat();
                    t[1] = s.getAng();
                    int j = ifhitbyweapon(i);
                    if (j == FIREEXT || j == RPG || j == RADIUSEXPLOSION || j == SEENINE || j == OOZFILTER) {

                        ListNode<Sprite> node = boardService.getStatNode(6);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (s.getHitag() == sp.getHitag() && (sp.getPicnum() == OOZFILTER || sp.getPicnum() == SEENINE)) {
                                if (sp.getShade() != -32) {
                                    sp.setShade(-32);
                                }
                            }
                            node = node.getNext();
                        }

                        Detonate(s, i, t);
                        continue;
                    } else {
                        s.setCstat((short) t[0]);
                        s.setAng((short) t[1]);
                        s.setExtra(0);
                    }
                }
                continue;
            }

            if (s.getPicnum() == FIREEXT) {
                int j = ifhitbyweapon(i);
                if (j == -1) {
                    continue;
                }

                for (int k = 0; k < 16; k++) {
                    int zv = -(engine.krand() & 4095) - (s.getZvel() >> 2);
                    int ve = (engine.krand() & 63) + 64;
                    int va = engine.krand() & 2047;
                    int pn = SCRAP3 + (engine.krand() & 3);
                    int sz = s.getZ() - (engine.krand() % (48 << 8));
                    j = EGS(s.getSectnum(), s.getX(), s.getY(), sz, pn, -8, 48, 48, va, ve, zv, i, (short) 5);
                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        sp.setPal(2);
                    }
                }

                spawn(i, EXPLOSION2);
                spritesound(PIPEBOMB_EXPLODE, i);
                spritesound(GLASS_HEAVYBREAK, i);

                if (s.getHitag() > 0) {

                    ListNode<Sprite> node = boardService.getStatNode(6);
                    while (node != null) {
                        Sprite sp = node.get();
                        if (s.getHitag() == sp.getHitag() && (sp.getPicnum() == OOZFILTER || sp.getPicnum() == SEENINE)) {
                            if (sp.getShade() != -32) {
                                sp.setShade(-32);
                            }
                        }
                        node = node.getNext();
                    }

                    int x = s.getExtra();
                    spawn(i, EXPLOSION2);
                    hitradius(i, currentGame.getCON().pipebombblastradius, x >> 2, x - (x >> 1), x - (x >> 2), x);
                    spritesound(PIPEBOMB_EXPLODE, i);

                    Detonate(s, i, t);
                    continue;
                } else {
                    hitradius(i, currentGame.getCON().seenineblastradius, 10, 15, 20, 25);
                    engine.deletesprite(i);
                }
                continue;
            }

            if (s.getPicnum() == OOZFILTER || s.getPicnum() == SEENINE || s.getPicnum() == SEENINEDEAD || s.getPicnum() == (SEENINEDEAD + 1)) {
                if (s.getShade() != -32 && s.getShade() != -33) {
                    int j = 0;
                    if (s.getXrepeat() != 0) {
                        j = (ifhitbyweapon(i) >= 0) ? 1 : 0;
                    }

                    if (j != 0 || s.getShade() == -31) {
                        if (j != 0) {
                            s.setLotag(0);
                        }

                        t[3] = 1;
                        ListNode<Sprite> node = boardService.getStatNode(6);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (s.getHitag() == sp.getHitag() && (sp.getPicnum() == SEENINE || sp.getPicnum() == OOZFILTER)) {
                                sp.setShade(-32);
                            }
                            node = node.getNext();
                        }
                    }
                } else {
                    if (s.getShade() == -32) {
                        if (s.getLotag() > 0) {
                            s.setLotag(s.getLotag() - 3);
                            if (s.getLotag() <= 0) {
                                s.setLotag(-99);
                            }
                        } else {
                            s.setShade(-33);
                        }
                    } else {
                        if (s.getXrepeat() > 0) {
                            hittype[i].temp_data[2]++;
                            if (hittype[i].temp_data[2] == 3) {
                                if (s.getPicnum() == OOZFILTER) {
                                    hittype[i].temp_data[2] = 0;
                                    Detonate(s, i, t);
                                    continue;
                                }
                                if (s.getPicnum() != (SEENINEDEAD + 1)) {
                                    hittype[i].temp_data[2] = 0;

                                    if (s.getPicnum() == SEENINEDEAD) {
                                        s.setPicnum(s.getPicnum() + 1);
                                    } else if (s.getPicnum() == SEENINE) {
                                        s.setPicnum(SEENINEDEAD);
                                    }
                                } else {
                                    Detonate(s, i, t);
                                    continue;
                                }
                            }
                            continue;
                        }

                        Detonate(s, i, t);
                    }
                }

                continue;
            }

            if (s.getPicnum() == MASTERSWITCH) {
                if (s.getYvel() == 1) {
                    s.setHitag(s.getHitag() - 1);
                    if (s.getHitag() <= 0) {
                        operatesectors(sect, i);

                        ListNode<Sprite> node = boardService.getSectNode(sect);
                        while (node != null) {
                            Sprite sp = node.get();
                            int j = node.getIndex();
                            if (sp.getStatnum() == 3) {
                                switch (sp.getLotag()) {
                                    case 2:
                                    case 21:
                                    case 31:
                                    case 32:
                                    case 36:
                                        hittype[j].temp_data[0] = 1;
                                        break;
                                    case 3:
                                        hittype[j].temp_data[4] = 1;
                                        break;
                                }
                            } else if (sp.getStatnum() == 6) {
                                switch (sp.getPicnum()) {
                                    case SEENINE:
                                    case OOZFILTER:
                                        sp.setShade(-31);
                                        break;
                                }
                            }
                            node = node.getNext();
                        }
                        engine.deletesprite(i);
                    }
                }
                continue;
            }

            switch (s.getPicnum()) {
                case VIEWSCREEN:
                case VIEWSCREEN2: {
                    if (s.getXrepeat() == 0) {
                        engine.deletesprite(i);
                        continue;
                    }

                    findplayer(s);
                    int x = player_dist;
                    if (x < 2048) {
                        if (s.getYvel() == 1) {
                            camsprite = (short) i;
                        }
                    } else if (camsprite != -1 && hittype[i].temp_data[0] == 1) {
                        camsprite = -1;
                        hittype[i].temp_data[0] = 0;
                        VIEWSCR_Lock = 199;
                    }

                    continue;
                }
                case TRASH:
                    if (s.getXvel() == 0) {
                        s.setXvel(1);
                    }
                    if (ssp(i, CLIPMASK0)) {
                        makeitfall(currentGame.getCON(), i);
                        if ((engine.krand() & 1) != 0) {
                            s.setZvel(s.getZvel() - 256);
                        }
                        if (klabs(s.getXvel()) < 48) {
                            s.setXvel(s.getXvel() + (engine.krand() & 3));
                        }
                    } else {
                        engine.deletesprite(i);
                        continue;
                    }
                    break;

                case SIDEBOLT1:
                case SIDEBOLT1 + 1:
                case SIDEBOLT1 + 2:
                case SIDEBOLT1 + 3: {
                    findplayer(s);
                    int x = player_dist;
                    if (x > 20480) {
                        continue;
                    }

                    do {
                        if (t[2] != 0) {
                            t[2]--;
                            continue BOLT;
                        }

                        if ((s.getXrepeat() | s.getYrepeat()) == 0) {
                            s.setXrepeat((short) t[0]);
                            s.setYrepeat((short) t[1]);
                        }

                        if (((engine.krand() & 8) == 0)) {
                            t[0] = s.getXrepeat();
                            t[1] = s.getYrepeat();
                            t[2] = global_random & 4;
                            s.setXrepeat(0);
                            s.setYrepeat(0);
                            continue;
                        }
                        break;
                    } while (true);

                    s.setPicnum(s.getPicnum() + 1);

                    if ((engine.krand() & 1) != 0 && sec.getFloorpicnum() == HURTRAIL) {
                        spritesound(SHORT_CIRCUIT, i);
                    }

                    if (s.getPicnum() == SIDEBOLT1 + 4) {
                        s.setPicnum(SIDEBOLT1);
                    }

                    continue;
                }
                case BOLT1:
                case BOLT1 + 1:
                case BOLT1 + 2:
                case BOLT1 + 3: {
                    findplayer(s);
                    int x = player_dist;
                    if (x > 20480) {
                        continue;
                    }

                    if (t[3] == 0) {
                        t[3] = sec.getFloorshade();
                    }

                    do {
                        if (t[2] != 0) {
                            t[2]--;
                            sec.setFloorshade(20);
                            sec.setCeilingshade(20);
                            continue BOLT;
                        }

                        if ((s.getXrepeat() | s.getYrepeat()) == 0) {
                            s.setXrepeat((short) t[0]);
                            s.setYrepeat((short) t[1]);
                        } else if (((engine.krand() & 8) == 0)) {
                            t[0] = s.getXrepeat();
                            t[1] = s.getYrepeat();
                            t[2] = global_random & 4;
                            s.setXrepeat(0);
                            s.setYrepeat(0);
                            continue;
                        }
                        break;
                    } while (true);

                    s.setPicnum(s.getPicnum() + 1);

                    int l = global_random & 7;
                    s.setXrepeat((short) (l + 8));

                    if ((l & 1) != 0) {
                        s.setCstat(s.getCstat() ^ 2);
                    }

                    if (s.getPicnum() == (BOLT1 + 1) && (engine.krand() & 7) == 0 && sec.getFloorpicnum() == HURTRAIL) {
                        spritesound(SHORT_CIRCUIT, i);
                    }

                    if (s.getPicnum() == BOLT1 + 4) {
                        s.setPicnum(BOLT1);
                    }

                    if ((s.getPicnum() & 1) != 0) {
                        sec.setFloorshade(0);
                        sec.setCeilingshade(0);
                    } else {
                        sec.setFloorshade(20);
                        sec.setCeilingshade(20);
                    }
                    continue;
                }
                case WATERDRIP:

                    if (t[1] != 0) {
                        t[1]--;
                        if (t[1] == 0) {
                            s.setCstat(s.getCstat() & 32767);
                        }
                    } else {
                        makeitfall(currentGame.getCON(), i);
                        ssp(i, CLIPMASK0);
                        if (s.getXvel() > 0) {
                            s.setXvel(s.getXvel() - 2);
                        }

                        if (s.getZvel() == 0) {
                            s.setCstat(s.getCstat() | 32768);

                            if (s.getPal() != 2 && s.getHitag() == 0) {
                                spritesound(SOMETHING_DRIPPING, i);
                            }

                            Sprite sp = boardService.getSprite(s.getOwner());
                            if (sp != null && sp.getPicnum() != WATERDRIP) {
                                engine.deletesprite(i);
                                continue;
                            } else {
                                game.pInt.setsprinterpolate(i, s);
                                s.setZ(t[0]);
                                t[1] = 48 + (engine.krand() & 31);
                            }
                        }
                    }

                    continue;

                case DOORSHOCK: {
                    int j = klabs(sec.getCeilingz() - sec.getFloorz()) >> 9;
                    s.setYrepeat((short) (j + 4));
                    s.setXrepeat(16);
                    s.setZ(sec.getFloorz());
                    continue;
                }
                case TOUCHPLATE: {
                    if (t[1] == 1 && s.getHitag() >= 0) // Move the sector floor
                    {
                        int x = sec.getFloorz();
                        if (t[3] == 1) // down
                        {
                            if (x >= t[2]) {
                                game.pInt.setfloorinterpolate(sect, sec);
                                sec.setFloorz(x);
                                t[1] = 0;
                            } else {
                                game.pInt.setfloorinterpolate(sect, sec);
                                sec.setFloorz(sec.getFloorz() + sec.getExtra());
                                int p = checkcursectnums(sect);
                                if (p >= 0) {
                                    ps[p].posz += sec.getExtra();
                                }
                            }
                        } else // up
                        {
                            if (x <= s.getZ()) {
                                game.pInt.setfloorinterpolate(sect, sec);
                                sec.setFloorz(s.getZ());
                                t[1] = 0;
                            } else {
                                game.pInt.setfloorinterpolate(sect, sec);
                                sec.setFloorz(sec.getFloorz() - sec.getExtra());
                                int p = checkcursectnums(sect);
                                if (p >= 0) {
                                    ps[p].posz -= sec.getExtra();
                                }
                            }
                        }
                        continue;
                    }

                    if (t[5] == 1) {
                        continue;
                    }

                    int p = checkcursectnums(sect);
                    if (p >= 0 && (ps[p].on_ground || s.getAng() == 512)) {
                        if (t[0] == 0 && !check_activator_motion(s.getLotag())) {
                            t[0] = 1;
                            t[1] = 1;
                            t[3] ^= 1;
                            operatemasterswitches(s.getLotag());
                            operateactivators(s.getLotag(), p);
                            if (s.getHitag() > 0) {
                                s.setHitag(s.getHitag() - 1);
                                if (s.getHitag() == 0) {
                                    t[5] = 1;
                                }
                            }
                        }
                    } else {
                        t[0] = 0;
                    }

                    if (t[1] == 1) {
                        ListNode<Sprite> node = boardService.getStatNode(6);
                        while (node != null) {
                            int j = node.getIndex();
                            Sprite sp = node.get();
                            if (j != i && sp.getPicnum() == TOUCHPLATE && sp.getLotag() == s.getLotag()) {
                                hittype[j].temp_data[1] = 1;
                                hittype[j].temp_data[3] = t[3];
                            }
                            node = node.getNext();
                        }
                    }
                    continue;
                }
                case CANWITHSOMETHING:
                case CANWITHSOMETHING2:
                case CANWITHSOMETHING3:
                case CANWITHSOMETHING4: {
                    makeitfall(currentGame.getCON(), i);
                    int j = ifhitbyweapon(i);
                    if (j >= 0) {
                        spritesound(VENT_BUST, i);
                        for (j = 0; j < 10; j++) {
                            RANDOMSCRAP(s, i);
                        }

                        if (s.getLotag() != 0) {
                            spawn(i, s.getLotag());
                        }

                        engine.deletesprite(i);
                    }
                    continue;
                }
                case EXPLODINGBARREL:
                case WOODENHORSE:
                case HORSEONSIDE:
                case FLOORFLAME:
                case FIREBARREL:
                case FIREVASE:
                case NUKEBARREL:
                case NUKEBARRELDENTED:
                case NUKEBARRELLEAKED:
                case TOILETWATER:
                case RUBBERCAN:
                case STEAM:
                case CEILINGSTEAM: {
                    int p = findplayer(s);
                    execute(currentGame.getCON(), i, p, player_dist);
                    continue;
                }
                case WATERBUBBLEMAKER: {
                    int p = findplayer(s);
                    execute(currentGame.getCON(), i, p, player_dist);
                    break;
                }
            }
        }
    }

    public static void bounce(int i) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        int xvect = mulscale(s.getXvel(), EngineUtils.sin((s.getAng() + 512) & 2047), 10);
        int yvect = mulscale(s.getXvel(), EngineUtils.sin(s.getAng() & 2047), 10);
        int zvect = s.getZvel();
        final int hitsect = s.getSectnum();
        Sector hsec = boardService.getSector(hitsect);
        if (hsec == null || hsec.getWallNode() == null) {
            return;
        }

        int daang = hsec.getWallNode().get().getWallAngle();

        int k;
        if (s.getZ() < (hittype[i].floorz + hittype[i].ceilingz) >> 1) {
            k = hsec.getCeilingheinum();
        } else {
            k = hsec.getFloorheinum();
        }

        int dax = mulscale(k, EngineUtils.sin((daang) & 2047), 14);
        int day = mulscale(k, EngineUtils.sin((daang + 1536) & 2047), 14);
        int daz = 4096;

        k = xvect * dax + yvect * day + zvect * daz;
        int l = dax * dax + day * day + daz * daz;
        if ((klabs(k) >> 14) < l) {
            k = divscale(k, l, 17);
            xvect -= mulscale(dax, k, 16);
            yvect -= mulscale(day, k, 16);
            zvect -= mulscale(daz, k, 16);
        }

        s.setZvel(zvect);
        s.setXvel(EngineUtils.sqrt(dmulscale(xvect, xvect, yvect, yvect, 8)));
        s.setAng(EngineUtils.getAngle(xvect, yvect));
    }

    public static void movetransports() {
        // Transporters
        ListNode<Sprite> ni = boardService.getStatNode(9), nexti;
        BOLT:
        for (; ni != null; ni = nexti) {
            nexti = ni.getNext();
            final Sprite sp = ni.get();
            final int i = ni.getIndex();
            final int sect = sp.getSectnum();
            final Sector sec = boardService.getSector(sect);
            if (sec == null) {
                continue;
            }

            int sectlotag = sec.getLotag();
            if (sp.getOwner() == i) {
                continue;
            }

            int onfloorz = hittype[i].temp_data[4];
            if (hittype[i].temp_data[0] > 0) {
                hittype[i].temp_data[0]--;
            }

            ListNode<Sprite> node = boardService.getSectNode(sect), nextj;
            for (; node != null; node = nextj) {
                nextj = node.getNext();
                final Sprite spr = node.get();
                final int j = node.getIndex();

                switch (spr.getStatnum()) {
                    case 10: { // Player

                        Sprite spo = boardService.getSprite(sp.getOwner());
                        if (spo != null) {
                            int p = spr.getYvel();

                            ps[p].on_warping_sector = 1;

                            if (ps[p].transporter_hold == 0 && ps[p].jumping_counter == 0) {
                                if (ps[p].on_ground && sectlotag == 0 && onfloorz != 0 && ps[p].jetpack_on == 0) {
                                    if (sp.getPal() == 0) {
                                        spawn(i, TRANSPORTERBEAM);
                                        spritesound(TELEPORTER, i);
                                    }

                                    for (int k = connecthead; k >= 0; k = connectpoint2[k]) {
                                        if (ps[k].cursectnum == spo.getSectnum()) {
                                            ps[k].frag_ps = (short) p;
                                            Sprite psp = boardService.getSprite(ps[k].i);
                                            if (psp != null) {
                                                psp.setExtra(0);
                                            }
                                        }
                                    }

                                    ps[p].ang = spo.getAng();

                                    if (spo.getOwner() != sp.getOwner()) {
                                        hittype[i].temp_data[0] = 13;
                                        hittype[sp.getOwner()].temp_data[0] = 13;
                                        ps[p].transporter_hold = 13;
                                    }

                                    ps[p].bobposx = ps[p].oposx = ps[p].posx = spo.getX();
                                    ps[p].bobposy = ps[p].oposy = ps[p].posy = spo.getY();
                                    ps[p].oposz = ps[p].posz = spo.getZ() - PHEIGHT;

                                    game.pInt.setsprinterpolate(ps[p].i, boardService.getSprite(ps[p].i));
                                    ps[p].UpdatePlayerLoc();

                                    engine.changespritesect(j, spo.getSectnum());
                                    ps[p].cursectnum = spr.getSectnum();

                                    if (sp.getPal() == 0) {
                                        int k = (short) spawn(sp.getOwner(), TRANSPORTERBEAM);
                                        spritesound(TELEPORTER, k);
                                    }

                                    break;
                                }
                            } else if (!(sectlotag == 1 && ps[p].on_ground)) {
                                break;
                            }

                            if (onfloorz == 0 && klabs(sp.getZ() - ps[p].posz) < 6144) {
                                if ((ps[p].jetpack_on == 0) || (ps[p].jetpack_on != 0 && (sync[p].bits & 1) != 0) || (ps[p].jetpack_on != 0 && (sync[p].bits & 2) != 0)) {
                                    ps[p].oposx = ps[p].posx += spo.getX() - sp.getX();
                                    ps[p].oposy = ps[p].posy += spo.getY() - sp.getY();

                                    if (ps[p].jetpack_on != 0 && ((sync[p].bits & 1) != 0 || ps[p].jetpack_on < 11)) {
                                        ps[p].posz = spo.getZ() - 6144;
                                    } else {
                                        ps[p].posz = spo.getZ() + 6144;
                                    }
                                    ps[p].oposz = ps[p].posz;

                                    game.pInt.setsprinterpolate(ps[p].i, boardService.getSprite(ps[p].i));
                                    ps[p].UpdatePlayerLoc();

                                    engine.changespritesect(j, spo.getSectnum());
                                    ps[p].cursectnum = spo.getSectnum();

                                    break;
                                }
                            }

                            int k = 0;
                            Sector owsec = boardService.getSector(spo.getSectnum());
                            if (onfloorz != 0 && sectlotag == 1 && ps[p].on_ground && ps[p].posz > (sec.getFloorz() - (16 << 8)) && ((sync[p].bits & 2) != 0 || ps[p].poszv > 2048)) {
                                k = 1;
                                if (screenpeek == p) {
                                    Sounds.stopAllSounds();
                                    clearsoundlocks();
                                }
                                Sprite psp = boardService.getSprite(ps[p].i);
                                if (psp != null) {
                                    if (psp.getExtra() > 0) {
                                        spritesound(DUKE_UNDERWATER, j);
                                    }

                                    if (owsec != null) {
                                        ps[p].oposz = ps[p].posz = owsec.getCeilingz() + (7 << 8);
                                    }

                                    game.pInt.setsprinterpolate(ps[p].i, psp);
                                    ps[p].UpdatePlayerLoc();

                                    ps[p].posxv = 4096 - (engine.krand() & 8192);
                                    ps[p].posyv = 4096 - (engine.krand() & 8192);
                                }
                            }

                            if (onfloorz != 0 && sectlotag == 2 && ps[p].posz < (sec.getCeilingz() + (6 << 8))) {
                                k = 1;
//	                            if( spr.extra <= 0) break;
                                if (screenpeek == p) {
                                    Sounds.stopAllSounds();
                                    clearsoundlocks();
                                }
                                spritesound(DUKE_GASP, j);

                                if (owsec != null) {
                                    ps[p].oposz = ps[p].posz = owsec.getFloorz() - (7 << 8);
                                }

                                game.pInt.setsprinterpolate(ps[p].i, boardService.getSprite(ps[p].i));
                                ps[p].UpdatePlayerLoc();

                                ps[p].jumping_toggle = 1;
                                ps[p].jumping_counter = 0;
                            }

                            if (k == 1) {
                                game.pInt.setsprinterpolate(ps[p].i, boardService.getSprite(ps[p].i));

                                ps[p].oposx = ps[p].posx += spo.getX() - sp.getX();
                                ps[p].oposy = ps[p].posy += spo.getY() - sp.getY();

                                if (spo.getOwner() != sp.getOwner()) {
                                    ps[p].transporter_hold = -2;
                                }
                                ps[p].cursectnum = spo.getSectnum();

                                engine.changespritesect(j, spo.getSectnum());
                                engine.setsprite(ps[p].i, ps[p].posx, ps[p].posy, ps[p].posz + PHEIGHT);

                                setpal(ps[p]);

                                if ((engine.krand() & 255) < 32) {
                                    spawn(j, WATERSPLASH2);
                                }

                                if (sectlotag == 1) {
                                    for (int l = 0; l < 9; l++) {
                                        int q = spawn(ps[p].i, WATERBUBBLE);
                                        Sprite bubble = boardService.getSprite(q);
                                        if (bubble != null) {
                                            bubble.setZ(bubble.getZ() + (engine.krand() & 16383));
                                        }
                                    }
                                }

                                ps[p].UpdatePlayerLoc();
                            }
                        }
                        break;
                    }
                    case 1:
                        switch (spr.getPicnum()) {
                            case SHARK:
                            case COMMANDER:
                            case OCTABRAIN:
                            case GREENSLIME:
                            case GREENSLIME + 1:
                            case GREENSLIME + 2:
                            case GREENSLIME + 3:
                            case GREENSLIME + 4:
                            case GREENSLIME + 5:
                            case GREENSLIME + 6:
                            case GREENSLIME + 7:
                                if (spr.getExtra() > 0) {
                                    continue;
                                }
                        }
                    case 4:
                    case 5:
                    case 12:
                    case 13:

                        int ll = klabs(spr.getZvel());

                    {
                        int warpspriteto = 0;
                        if (ll != 0 && sectlotag == 2 && spr.getZ() < (sec.getCeilingz() + ll)) {
                            warpspriteto = 1;
                        }

                        if (ll != 0 && sectlotag == 1 && spr.getZ() > (sec.getFloorz() - ll)) {
                            warpspriteto = 1;
                        }

                        if (sectlotag == 0 && (onfloorz != 0 || klabs(spr.getZ() - sp.getZ()) < 4096)) {
                            Sprite spo = boardService.getSprite(sp.getOwner());
                            if (spo != null && spo.getOwner() != sp.getOwner() && onfloorz != 0 && hittype[i].temp_data[0] > 0 && spr.getStatnum() != 5) {
                                hittype[i].temp_data[0]++;
                                continue BOLT;
                            }
                            warpspriteto = 1;
                        }

                        if (warpspriteto != 0) {
                            switch (spr.getPicnum()) {
                                case TRANSPORTERSTAR:
                                case TRANSPORTERBEAM:
                                case TRIPBOMB:
                                case BULLETHOLE:
                                case WATERSPLASH2:
                                case BURNING:
                                case BURNING2:
                                case FIRE:
                                case FIRE2:
                                case TOILETWATER:
                                case LASERLINE:
                                    continue;
                                case PLAYERONWATER:
                                    if (sectlotag == 2) {
                                        spr.setCstat(spr.getCstat() & 32767);
                                        break;
                                    }
                                default:
                                    if (spr.getStatnum() == 5 && !(sectlotag == 1 || sectlotag == 2)) {
                                        break;
                                    }

                                case WATERBUBBLE: {
                                    Sprite spo = boardService.getSprite(sp.getOwner());
                                    if (sectlotag > 0) {
                                        int k = spawn(j, WATERSPLASH2);
                                        Sprite s = boardService.getSprite(k);
                                        if (s != null && sectlotag == 1 && spr.getStatnum() == 4) {
                                            s.setXvel(spr.getXvel() >> 1);
                                            s.setAng(spr.getAng());
                                            ssp(k, CLIPMASK0);
                                        }
                                    }

                                    if (spo != null) {
                                        Sector owsec = boardService.getSector(spo.getSectnum());
                                        if (owsec == null) {
                                            break;
                                        }

                                        switch (sectlotag) {
                                            case 0:
                                                if (onfloorz != 0) {
                                                    if (spr.getStatnum() == 4 || (checkcursectnums(sect) == -1 && checkcursectnums(spo.getSectnum()) == -1)) {
                                                        game.pInt.setsprinterpolate(j, spr);

                                                        spr.setX(spr.getX() + (spo.getX() - sp.getX()));
                                                        spr.setY(spr.getY() + (spo.getY() - sp.getY()));
                                                        spr.setZ(spr.getZ() - (sp.getZ() - owsec.getFloorz()));
                                                        spr.setAng(spo.getAng());

                                                        if (sp.getPal() == 0) {
                                                            int k = spawn(i, TRANSPORTERBEAM);
                                                            spritesound(TELEPORTER, k);

                                                            k = spawn(sp.getOwner(), TRANSPORTERBEAM);
                                                            spritesound(TELEPORTER, k);
                                                        }

                                                        if (spo.getOwner() != sp.getOwner()) {
                                                            hittype[i].temp_data[0] = 13;
                                                            hittype[sp.getOwner()].temp_data[0] = 13;
                                                        }

                                                        engine.changespritesect(j, spo.getSectnum());
                                                    }
                                                } else {
                                                    game.pInt.setsprinterpolate(j, spr);

                                                    spr.setX(spr.getX() + (spo.getX() - sp.getX()));
                                                    spr.setY(spr.getY() + (spo.getY() - sp.getY()));
                                                    spr.setZ(spo.getZ() + 4096);

                                                    engine.changespritesect(j, spo.getSectnum());
                                                }
                                                break;
                                            case 1:

                                                game.pInt.setsprinterpolate(j, spr);

                                                spr.setX(spr.getX() + (spo.getX() - sp.getX()));
                                                spr.setY(spr.getY() + (spo.getY() - sp.getY()));
                                                spr.setZ(owsec.getCeilingz() + ll);

                                                engine.changespritesect(j, spo.getSectnum());

                                                break;
                                            case 2:

                                                game.pInt.setsprinterpolate(j, spr);

                                                spr.setX(spr.getX() + (spo.getX() - sp.getX()));
                                                spr.setY(spr.getY() + (spo.getY() - sp.getY()));
                                                spr.setZ(owsec.getFloorz() - ll);

                                                engine.changespritesect(j, spo.getSectnum());
                                                break;
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                }
            }
        }
    }

    public static void moveactors() {
        ListNode<Sprite> ni = boardService.getStatNode(1), nexti;

        BOLT:
        for (; ni != null; ni = nexti) {
            nexti = ni.getNext();
            final int i = ni.getIndex();
            final Sprite s = ni.get();

            final int sect = s.getSectnum();
            final Sector sec = boardService.getSector(sect);

            if (s.getXrepeat() == 0 || sec == null) {
                engine.deletesprite(i);
                continue;
            }

            int[] t = hittype[i].temp_data;
            game.pInt.clearspriteinterpolate(i);
            game.pInt.setsprinterpolate(i, s);

            switch (s.getPicnum()) {
                case FLAMETHROWERFLAME: {
                    int p = findplayer(s);
                    int x = player_dist;
                    execute(currentGame.getCON(), i, p, x);
                    t[0]++;
                    if (sec.getLotag() == 2) {
                        Sprite flame =  boardService.getSprite(spawn(i, EXPLOSION2));
                        if (flame != null) {
                            flame.setShade(127);
                            engine.deletesprite(i);
                        }
                        continue;
                    }

                    int dax = s.getX();
                    int day = s.getY();
                    int daz = s.getZ();
                    int xvel = s.getXvel();
                    int zvel = s.getZvel();

                    getglobalz(i);

                    int ds = t[0] / 6;
                    if (s.getXrepeat() < 80) {
                        int size = s.getXrepeat() + ds;
                        s.setXrepeat(size);
                        s.setYrepeat(size);
                    }
                    s.setClipdist(s.getClipdist() + ds);
                    if (t[0] <= 2) {
                        t[3] = engine.krand() % 10;
                    }
                    if (t[0] > 30) {
                        Sprite flame =  boardService.getSprite(spawn(i, EXPLOSION2));
                        if (flame != null) {
                            flame.setShade(127);
                            engine.deletesprite(i);
                        }
                        continue;
                    }

                    int moveHit = movesprite(i, (xvel * (EngineUtils.sin((s.getAng() + 512) & 2047))) >> 14, (xvel * (EngineUtils.sin(s.getAng() & 2047))) >> 14, zvel, CLIPMASK1);

                    if (s.getSectnum() < 0) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if ((moveHit & kHitTypeMask) != kHitSprite) {
                        Sector sec2 = boardService.getSector(s.getSectnum());
                        if (sec2 != null) {
                            if (s.getZ() < hittype[i].ceilingz) {
                                moveHit = kHitSector + (s.getSectnum());
                                s.setZvel(-1);
                            } else if ((s.getZ() > hittype[i].floorz && sec2.getLotag() != 1)
                                    || (s.getZ() > hittype[i].floorz + (16 << 8) && sec2.getLotag() == 1)) {
                                moveHit = kHitSector + (s.getSectnum());
                                if (sec2.getLotag() != 1) {
                                    s.setZvel(1);
                                }
                            }
                        }
                    }

                    if (moveHit != 0) {
                        s.setXvel(0);
                        s.setYvel(0);
                        s.setZvel(0);
                        if ((moveHit & kHitTypeMask) == kHitSprite) {
                            moveHit &= kHitIndexMask;
                            checkhitsprite(moveHit, i);
                            Sprite hspr = boardService.getSprite(moveHit);

                            if (hspr != null && hspr.getPicnum() == APLAYER) {
                                spritesound(moveHit, PISTOL_BODYHIT);
                            }
                        } else if ((moveHit & kHitTypeMask) == kHitWall) {
                            moveHit &= kHitIndexMask;
                            engine.setsprite(i, dax, day, daz);
                            checkhitwall(i, moveHit, s.getX(), s.getY(), s.getZ(), s.getPicnum());
                        } else if ((moveHit & kHitTypeMask) == kHitSector) {
                            engine.setsprite(i, dax, day, daz);
                            if (s.getZvel() < 0) {
                                checkhitceiling(s.getSectnum());
                            }
                        }

                        if (s.getXrepeat() >= 10) {
                            x = s.getExtra();
                            hitradius(i, currentGame.getCON().rpgblastradius, x >> 2, x >> 1, x - (x >> 2), x);
                        } else {
                            x = s.getExtra() + (global_random & 3);
                            hitradius(i, (currentGame.getCON().rpgblastradius >> 1), x >> 2, x >> 1, x - (x >> 2), x);
                        }
                    }

                    continue;
                }
                case DUCK:
                case TARGET:
                    if ((s.getCstat() & 32) != 0) {
                        t[0]++;
                        if (t[0] > 60) {
                            t[0] = 0;
                            s.setCstat(128 + 257 + 16);
                            s.setExtra(1);
                        }
                    } else {
                        int j = (short) ifhitbyweapon(i);
                        if (j >= 0) {
                            s.setCstat(32 + 128);
                            int k = 1;

                            ListNode<Sprite> node = boardService.getStatNode(1);
                            while (node != null) {
                                Sprite sp = node.get();
                                if (sp.getLotag() == s.getLotag() && sp.getPicnum() == s.getPicnum()) {
                                    if ((sp.getHitag() != 0 && (sp.getCstat() & 32) == 0) || (sp.getHitag() == 0 && (sp.getCstat() & 32) != 0)) {
                                        k = 0;
                                        break;
                                    }
                                }
                                node = node.getNext();
                            }

                            if (k == 1) {
                                operateactivators(s.getLotag(), -1);
                                operateforcefields(i, s.getLotag());
                                operatemasterswitches(s.getLotag());
                            }
                        }
                    }
                    continue;

                case RESPAWNMARKERRED:
                case RESPAWNMARKERYELLOW:
                case RESPAWNMARKERGREEN: {
                    hittype[i].temp_data[0]++;
                    if (hittype[i].temp_data[0] > currentGame.getCON().respawnitemtime) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (hittype[i].temp_data[0] >= (currentGame.getCON().respawnitemtime >> 1) && hittype[i].temp_data[0] < ((currentGame.getCON().respawnitemtime >> 1) + (currentGame.getCON().respawnitemtime >> 2))) {
                        s.setPicnum(RESPAWNMARKERYELLOW);
                    } else if (hittype[i].temp_data[0] > ((currentGame.getCON().respawnitemtime >> 1) + (currentGame.getCON().respawnitemtime >> 2))) {
                        s.setPicnum(RESPAWNMARKERGREEN);
                    }
                    makeitfall(currentGame.getCON(), i);
                    break;
                }
                case HELECOPT:
                case DUKECAR:

                    s.setZ(s.getZ() + s.getZvel());
                    t[0]++;

                    if (t[0] == 4) {
                        spritesound(WAR_AMBIENCE2, i);
                    }

                    if (t[0] > (26 * 8)) {
                        sound(RPG_EXPLODE);
                        for (int j = 0; j < 32; j++) {
                            RANDOMSCRAP(s, i);
                        }
                        earthquaketime = 16;
                        engine.deletesprite(i);
                        continue;
                    } else if ((t[0] & 3) == 0) {
                        spawn(i, EXPLOSION2);
                    }
                    ssp(i, CLIPMASK0);
                    break;
                case RAT:
                    makeitfall(currentGame.getCON(), i);
                    if (ssp(i, CLIPMASK0)) {
                        if ((engine.krand() & 255) < 3) {
                            spritesound(RATTY, i);
                        }
                        s.setAng(s.getAng() + (engine.krand() & 31) - 15 + (EngineUtils.sin((t[0] << 8) & 2047) >> 11));
                    } else {
                        hittype[i].temp_data[0]++;
                        if (hittype[i].temp_data[0] > 1) {
                            engine.deletesprite(i);
                            continue;
                        } else {
                            s.setAng((short) (engine.krand() & 2047));
                        }
                    }
                    if (s.getXvel() < 128) {
                        s.setXvel(s.getXvel() + 2);
                    }
                    s.setAng(s.getAng() + (engine.krand() & 3) - 6);
                    break;
                case QUEBALL:
                case STRIPEBALL:
                    if (s.getXvel() != 0) {

                        ListNode<Sprite> node = boardService.getStatNode(0);
                        while (node != null) {
                            ListNode<Sprite> nextj = node.getNext();
                            Sprite sp = node.get();
                            if (sp.getPicnum() == POCKET && ldist(sp, s) < 52) {
                                engine.deletesprite(i);
                                continue BOLT;
                            }
                            node = nextj;
                        }

                        int moveHit = engine.clipmove(s.getX(), s.getY(), s.getZ(), s.getSectnum(), ((((long) s.getXvel() * (EngineUtils.sin((s.getAng() + 512) & 2047))) >> 14) * TICSPERFRAME) << 11, ((((long) s.getXvel() * (EngineUtils.sin(s.getAng() & 2047))) >> 14) * TICSPERFRAME) << 11, 24, (4 << 8), (4 << 8), CLIPMASK1);

                        if (clipmove_sectnum != -1) {
                            s.setX(clipmove_x);
                            s.setY(clipmove_y);
                            s.setZ(clipmove_z);
                            s.setSectnum(clipmove_sectnum);
                        }

                        int nHitObject = moveHit & kHitTypeMask;
                        if (nHitObject != 0) {
                            if (nHitObject == kHitWall) {
                                moveHit &= kHitIndexMask;
                                Wall hwal = boardService.getWall(moveHit);
                                if (hwal != null) {
                                    int k = hwal.getWallAngle(); //EngineUtils.getAngle(boardService.getWall(boardService.getWall(moveHit).getPoint2()).getX() - boardService.getWall(moveHit).getX(), boardService.getWall(boardService.getWall(moveHit).getPoint2()).getY() - boardService.getWall(moveHit).getY());
                                    s.setAng((((k << 1) - s.getAng()) & 2047));
                                }
                            } else if (nHitObject == kHitSprite) {
                                moveHit &= kHitIndexMask;
                                checkhitsprite(i, moveHit);
                            }
                        }
                        s.setXvel(s.getXvel() - 1);
                        if (s.getXvel() < 0) {
                            s.setXvel(0);
                        }
                        if (s.getPicnum() == STRIPEBALL) {
                            s.setCstat(257);
                            s.setCstat(s.getCstat() | 4 & s.getXvel());
                            s.setCstat(s.getCstat() | 8 & s.getXvel());
                        }
                    } else {
                        int p = (short) findplayer(s);
                        int x = player_dist;
                        if (x < 1596) {
//	                        if(s.pal == 12)
                            {
                                float fj = getincangle(ps[p].ang, EngineUtils.getAngle(s.getX() - ps[p].posx, s.getY() - ps[p].posy));
                                if (fj > -64 && fj < 64 && ((sync[p].bits & (1 << 29)) != 0)) {
                                    if (ps[p].toggle_key_flag == 1) {

                                        ListNode<Sprite> node = boardService.getStatNode(1);
                                        while (node != null) {
                                            Sprite sp = node.get();
                                            if (sp.getPicnum() == QUEBALL || sp.getPicnum() == STRIPEBALL) {
                                                fj = getincangle(ps[p].ang, EngineUtils.getAngle(sp.getX() - ps[p].posx, sp.getY() - ps[p].posy));
                                                if (fj > -64 && fj < 64) {
                                                    findplayer(sp);
                                                    int l = player_dist;
                                                    if (x > l) {
                                                        break;
                                                    }
                                                }
                                            }
                                            node = node.getNext();
                                        }

                                        if (node == null) {
                                            if (s.getPal() == 12) {
                                                s.setXvel(164);
                                            } else {
                                                s.setXvel(140);
                                            }
                                            s.setAng((short) ps[p].ang);
                                            ps[p].toggle_key_flag = 2;
                                        }
                                    }
                                }
                            }
                        }
                        if (x < 512 && s.getSectnum() == ps[p].cursectnum) {
                            s.setAng(EngineUtils.getAngle(s.getX() - ps[p].posx, s.getY() - ps[p].posy));
                            s.setXvel(48);
                        }
                    }

                    break;
                case FORCESPHERE:

                    if (s.getYvel() == 0) {
                        s.setYvel(1);

                        for (int l = 512; l < (2048 - 512); l += 128) {
                            for (int j = 0; j < 2048; j += 128) {
                                int k = spawn(i, FORCESPHERE);
                                Sprite sp = boardService.getSprite(k);
                                if (sp != null) {
                                    sp.setCstat(257 + 128);
                                    sp.setClipdist(64);
                                    sp.setAng((short) j);
                                    sp.setZvel((short) (EngineUtils.sin(l & 2047) >> 5));
                                    sp.setXvel((short) (EngineUtils.cos((l) & 2047) >> 9));
                                    sp.setOwner(i);
                                }
                            }
                        }
                    }

                    if (t[3] > 0) {
                        if (s.getZvel() < 6144) {
                            s.setZvel(s.getZvel() + 192);
                        }
                        s.setZ(s.getZ() + s.getZvel());
                        if (s.getZ() > sec.getFloorz()) {
                            s.setZ(sec.getFloorz());
                        }
                        t[3]--;
                        if (t[3] == 0) {
                            engine.deletesprite(i);
                            continue;
                        }
                    } else if (t[2] > 10) {
                        ListNode<Sprite> node = boardService.getStatNode(5);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (sp.getOwner() == i && sp.getPicnum() == FORCESPHERE) {
                                hittype[node.getIndex()].temp_data[1] = 1 + (engine.krand() & 63);
                            }
                            node = node.getNext();
                        }
                        t[3] = 64;
                    }

                    continue;

                case RECON: {

                    getglobalz(i);

                    if ((sec.getCeilingstat() & 1) != 0) {
                        s.setShade(s.getShade() + ((sec.getCeilingshade() - s.getShade()) >> 1));
                    } else {
                        s.setShade(s.getShade() + ((sec.getFloorshade() - s.getShade()) >> 1));
                    }

                    if (s.getZ() < sec.getCeilingz() + (32 << 8)) {
                        s.setZ(sec.getCeilingz() + (32 << 8));
                    }

                    if (ud.multimode < 2) {
                        if (actor_tog != 0) {
                            s.setCstat(32768);
                            continue;
                        }
                    }

                    if (ifhitbyweapon(i) >= 0) {
                        if (s.getExtra() < 0 && t[0] != -1) {
                            t[0] = -1;
                            s.setExtra(0);
                        }
                        spritesound(RECO_PAIN, i);
                        RANDOMSCRAP(s, i);
                    }

                    if (t[0] == -1) {
                        s.setZ(s.getZ() + 1024);
                        t[2]++;
                        if ((t[2] & 3) == 0) {
                            spawn(i, EXPLOSION2);
                        }
                        getglobalz(i);
                        s.setAng(s.getAng() + 96);
                        s.setXvel(128);
                        if (!ssp(i, CLIPMASK0) || s.getZ() > hittype[i].floorz) {
                            for (int l = 0; l < 16; l++) {
                                RANDOMSCRAP(s, i);
                            }
                            spritesound(LASERTRIP_EXPLODE, i);
                            spawn(i, PIGCOP);
                            ps[connecthead].actors_killed++;
                            engine.deletesprite(i);
                        }
                        continue;
                    } else {
                        if (s.getZ() > hittype[i].floorz - (48 << 8)) {
                            s.setZ(hittype[i].floorz - (48 << 8));
                        }
                    }

                    int p = findplayer(s);
                    int x = player_dist;
                    int j = s.getOwner();

                    // 3 = findplayerz, 4 = shoot

                    if (t[0] >= 4) {
                        t[2]++;
                        if ((t[2] & 15) == 0) {
                            int a = s.getAng();
                            s.setAng((short) hittype[i].tempang);
                            spritesound(RECO_ATTACK, i);
                            shoot(i, FIRELASER);
                            s.setAng((short) a);
                        }
                        if (t[2] > (26 * 3) || !engine.cansee(s.getX(), s.getY(), s.getZ() - (16 << 8), s.getSectnum(), ps[p].posx, ps[p].posy, ps[p].posz, ps[p].cursectnum)) {
                            t[0] = 0;
                            t[2] = 0;
                        } else {
                            hittype[i].tempang += getincangle(hittype[i].tempang, EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY())) / 3;
                        }
                    } else if (t[0] == 2 || t[0] == 3) {
                        t[3] = 0;
                        if (s.getXvel() > 0) {
                            s.setXvel(s.getXvel() - 16);
                        } else {
                            s.setXvel(0);
                        }

                        if (t[0] == 2) {
                            int l = ps[p].posz - s.getZ();
                            if (klabs(l) < (48 << 8)) {
                                t[0] = 3;
                            } else {
                                s.setZ(s.getZ() + (sgn(ps[p].posz - s.getZ()) << 10));
                            }
                        } else {
                            t[2]++;
                            if (t[2] > (26 * 3) || !engine.cansee(s.getX(), s.getY(), s.getZ() - (16 << 8), s.getSectnum(), ps[p].posx, ps[p].posy, ps[p].posz, ps[p].cursectnum)) {
                                t[0] = 1;
                                t[2] = 0;
                            } else if ((t[2] & 15) == 0) {
                                spritesound(RECO_ATTACK, i);
                                shoot(i, FIRELASER);
                            }
                        }
                        s.setAng(s.getAng() + (getincangle(s.getAng(), EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY())) >> 2));
                    }

                    if (t[0] != 2 && t[0] != 3) {
                        Sprite js = boardService.getSprite(j);
                        int l = js == null ? 0 : ldist(js, s), a;
                        if (l <= 1524) {
                            a = s.getAng();
                            s.setXvel(s.getXvel() >> 1);
                        } else {
                            a = EngineUtils.getAngle(js.getX() - s.getX(), js.getY() - s.getY());
                        }

                        if (t[0] == 1 || t[0] == 4) // Found a locator and going with it
                        {
                            l = js == null ? 0 : dist(js, s);
                            if (l <= 1524) {
                                if (t[0] == 1) {
                                    t[0] = 0;
                                } else {
                                    t[0] = 5;
                                }
                            } else {
                                // Control speed here
                                if (s.getXvel() < 256) {
                                    s.setXvel(s.getXvel() + 32);
                                }
                            }

                            if (t[0] < 2) {
                                t[2]++;
                            }

                            if (x < 6144 && t[0] < 2 && t[2] > (26 * 4)) {
                                t[0] = 2 + (engine.krand() & 2);
                                t[2] = 0;
                                hittype[i].tempang = s.getAng();
                            }
                        }

                        if (t[0] == 0 || t[0] == 5) {
                            if (t[0] == 0) {
                                t[0] = 1;
                            } else {
                                t[0] = 4;
                            }
                            s.setOwner(LocateTheLocator(s.getHitag(), -1));
                            j = s.getOwner();
                            if (j == -1) {
                                j = hittype[i].temp_data[5];
                                s.setHitag(j);
                                s.setOwner(LocateTheLocator(j, -1));
                                j = s.getOwner();
                                if (j == -1) {
                                    engine.deletesprite(i);
                                    continue;
                                }
                            } else {
                                s.setHitag(s.getHitag() + 1);
                            }
                        }

                        t[3] = getincangle(s.getAng(), a);
                        s.setAng(s.getAng() + (t[3] >> 3));

                        js = boardService.getSprite(j); // Attention!
                        if (js != null && s.getZ() < js.getZ()) {
                            s.setZ(s.getZ() + 1024);
                        } else {
                            s.setZ(s.getZ() - 1024);
                        }
                    }

                    if (Sound[RECO_ROAM].getSoundOwnerCount() == 0) {
                        spritesound(RECO_ROAM, i);
                    }

                    ssp(i, CLIPMASK0);

                    continue;
                }
                case OOZ:
                case OOZ2: {
                    getglobalz(i);

                    int j = (hittype[i].floorz - hittype[i].ceilingz) >> 9;
                    if (j > 255) {
                        j = 255;
                    }

                    int x = 25 - (j >> 1);
                    if (x < 8) {
                        x = 8;
                    } else if (x > 48) {
                        x = 48;
                    }

                    s.setYrepeat((short) j);
                    s.setXrepeat((short) x);
                    s.setZ(hittype[i].floorz);

                    continue;
                }
                case GREENSLIME:
                case GREENSLIME + 1:
                case GREENSLIME + 2:
                case GREENSLIME + 3:
                case GREENSLIME + 4:
                case GREENSLIME + 5:
                case GREENSLIME + 6:
                case GREENSLIME + 7: {
                    if (ud.multimode < 2) {
                        if (actor_tog == 1) {
                            s.setCstat((short) 32768);
                            continue;
                        } else if (actor_tog == 2) {
                            s.setCstat(257);
                        }
                    }

                    t[1] += 128;

                    if ((sec.getFloorstat() & 1) != 0) {
                        engine.deletesprite(i);
                        continue;
                    }

                    int p = findplayer(s);
                    int x = player_dist;

                    if (x > 20480) {
                        hittype[i].timetosleep++;
                        if (hittype[i].timetosleep > SLEEPTIME) {
                            hittype[i].timetosleep = 0;
                            engine.changespritestat(i, (short) 2);
                            continue;
                        }
                    }

                    if (t[0] == -5) // FROZEN
                    {
                        t[3]++;
                        if (t[3] > 280) {
                            s.setPal(0);
                            t[0] = 0;
                            continue;
                        }
                        makeitfall(currentGame.getCON(), i);
                        s.setCstat(257);
                        s.setPicnum(GREENSLIME + 2);
                        s.setExtra(1);
                        s.setPal(1);
                        int j = ifhitbyweapon(i);
                        if (j >= 0) {
                            if (j == FREEZEBLAST) {
                                continue;
                            }
                            for (j = 16; j >= 0; j--) {
                                int vz = 1024 - (engine.krand() & 1023);
                                int ve = 32 + (engine.krand() & 63);
                                int va = engine.krand() & 2047;
                                int k = EGS(s.getSectnum(), s.getX(), s.getY(), s.getZ(), GLASSPIECES + (j % 3), -32, 36, 36, va, ve, vz, i, (short) 5);
                                Sprite ks = boardService.getSprite(k);
                                if (ks != null) {
                                    ks.setPal(1);
                                }
                            }
                            if (!gGameScreen.IsOriginalGame()) {
                                ps[connecthead].actors_killed++;
                            }
                            spritesound(GLASS_BREAKING, i);
                            engine.deletesprite(i);
                        } else if (x < 1024 && ps[p].quick_kick == 0) {
                            float fj = getincangle(ps[p].ang, EngineUtils.getAngle(s.getX() - ps[p].posx, s.getY() - ps[p].posy));
                            if (fj > -128 && fj < 128) {
                                ps[p].quick_kick = 14;
                            }
                        }

                        continue;
                    }

                    if (x < 1596) {
                        s.setCstat(0);
                    } else {
                        s.setCstat(257);
                    }

                    if (t[0] == -4) // On the player
                    {
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            if (psp.getExtra() < 1) {
                                t[0] = 0;
                                continue;
                            }

                            engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                            s.setAng((short) ps[p].ang);

                            if (((sync[p].bits & 4) != 0 || (ps[p].quick_kick > 0)) && psp.getExtra() > 0) {
                                if (ps[p].quick_kick > 0 || (ps[p].curr_weapon != HANDREMOTE_WEAPON && ps[p].curr_weapon != HANDBOMB_WEAPON && ps[p].curr_weapon != TRIPBOMB_WEAPON && ps[p].ammo_amount[ps[p].curr_weapon] >= 0)) {
                                    for (x = 0; x < 8; x++) {
                                        int vz = -(engine.krand() & 4095) - (s.getZvel() >> 2);
                                        int ve = (engine.krand() & 63) + 64;
                                        int va = engine.krand() & 2047;
                                        int j = EGS(sect, s.getX(), s.getY(), s.getZ() - (8 << 8), SCRAP3 + (engine.krand() & 3), -8, 48, 48, va, ve, vz, i, (short) 5);
                                        Sprite js = boardService.getSprite(j);
                                        if (js != null) {
                                            js.setPal(6);
                                        }
                                    }

                                    spritesound(SLIM_DYING, i);
                                    spritesound(SQUISHED, i);
                                    if ((engine.krand() & 255) < 32) {
                                        int j = spawn(i, BLOODPOOL);
                                        Sprite js = boardService.getSprite(j);
                                        if (js != null) {
                                            js.setPal(0);
                                        }
                                    }
                                    ps[connecthead].actors_killed++;
                                    t[0] = -3;
                                    if (ps[p].somethingonplayer == i) {
                                        ps[p].somethingonplayer = -1;
                                    }
                                    engine.deletesprite(i);
                                    continue;
                                }
                            }

                            s.setZ(ps[p].posz + ps[p].pyoff - t[2] + (8 << 8));

                            s.setZ(s.getZ() + ((100 - (int) ps[p].horiz) << 4));

                            if (t[2] > 512) {
                                t[2] -= 128;
                            }

                            if (t[2] < 348) {
                                t[2] += 128;
                            }

                            if (ps[p].newowner >= 0) {
                                ps[p].newowner = -1;
                                ps[p].posx = ps[p].oposx;
                                ps[p].posy = ps[p].oposy;
                                ps[p].posz = ps[p].oposz;
                                ps[p].ang = ps[p].oang;

                                game.pInt.setsprinterpolate(ps[p].i, psp);
                                ps[p].cursectnum = engine.updatesector(ps[p].posx, ps[p].posy, ps[p].cursectnum);
                                setpal(ps[p]);

                                ps[p].UpdatePlayerLoc();

                                ListNode<Sprite> n = boardService.getStatNode(1);
                                while (n != null) {
                                    Sprite sp = ni.get();
                                    if (sp.getPicnum() == CAMERA1) {
                                        sp.setYvel(0);
                                    }
                                    n = n.getNext();
                                }
                            }

                            if (t[3] > 0) {
                                short[] frames = {5, 5, 6, 6, 7, 7, 6, 5};

                                s.setPicnum((short) (GREENSLIME + frames[t[3] & 7]));

                                if (t[3] == 5) {
                                    ps[p].getPlayerSprite().setExtra(ps[p].getPlayerSprite().getExtra() - (5 + (engine.krand() & 3)));
                                    spritesound(SLIM_ATTACK, i);
                                }

                                if (t[3] < 7) {
                                    t[3]++;
                                } else {
                                    t[3] = 0;
                                }
                            } else {
                                s.setPicnum(GREENSLIME + 5);
                                if (rnd(32)) {
                                    t[3] = 1;
                                }
                            }

                            s.setXrepeat((short) (20 + (EngineUtils.sin(t[1] & 2047) >> 13)));
                            s.setYrepeat((short) (15 + (EngineUtils.sin(t[1] & 2047) >> 13)));

                            if (IsOriginalDemo()) {
                                s.setX(ps[p].posx + (EngineUtils.sin(((int) ps[p].ang + 512) & 2047) >> 7));
                                s.setY(ps[p].posy + (EngineUtils.sin((int) ps[p].ang & 2047) >> 7));
                            } else {
                                s.setX((int) (ps[p].posx + (BCosAngle(BClampAngle(ps[p].ang)) / 127.0f)));
                                s.setY((int) (ps[p].posy + (BSinAngle(BClampAngle(ps[p].ang)) / 127.0f)));
                            }
                        }
                        continue;
                    } else if (s.getXvel() < 64 && x < 768) {
                        if (ps[p].somethingonplayer == -1) {
                            ps[p].somethingonplayer = (short) i;
                            if (t[0] == 3 || t[0] == 2) // Falling downward
                            {
                                t[2] = (12 << 8);
                            } else {
                                t[2] = -(13 << 8); // Climbing up duke
                            }
                            t[0] = -4;
                        }
                    }

                    int j = ifhitbyweapon(i);
                    if (j >= 0) {
                        spritesound(SLIM_DYING, i);

                        if (gGameScreen.IsOriginalGame()) {
                            ps[connecthead].actors_killed++;
                        }

                        if (ps[p].somethingonplayer == i) {
                            ps[p].somethingonplayer = -1;
                        }

                        if (j == FREEZEBLAST) {
                            spritesound(SOMETHINGFROZE, i);
                            t[0] = -5;
                            t[3] = 0;
                            continue;
                        }
                        if (!gGameScreen.IsOriginalGame()) {
                            ps[connecthead].actors_killed++;
                        }

                        if ((engine.krand() & 255) < 32) {
                            j = spawn(i, BLOODPOOL);
                            Sprite js = boardService.getSprite(j);
                            if (js != null) {
                                js.setPal(0);
                            }
                        }

                        for (x = 0; x < 8; x++) {
                            int vz = -(engine.krand() & 4095) - (s.getZvel() >> 2);
                            int ve = (engine.krand() & 63) + 64;
                            int va = engine.krand() & 2047;
                            j = EGS(sect, s.getX(), s.getY(), s.getZ() - (8 << 8), SCRAP3 + (engine.krand() & 3), -8, 48, 48, va, ve, vz, i, (short) 5);
                            Sprite js = boardService.getSprite(j);
                            if (js != null) {
                                js.setPal(6);
                            }
                        }
                        t[0] = -3;
                        engine.deletesprite(i);
                        continue;
                    }

                    // All weap
                    if (t[0] == -1) // Shrinking down
                    {
                        makeitfall(currentGame.getCON(), i);

                        s.setCstat(s.getCstat() & 65535 - 8);
                        s.setPicnum(GREENSLIME + 4);

                        if (s.getXrepeat() > 32) {
                            s.setXrepeat(s.getXrepeat() - (engine.krand() & 7));
                        }
                        if (s.getYrepeat() > 16) {
                            s.setYrepeat(s.getYrepeat() - (engine.krand() & 7));
                        } else {
                            s.setXrepeat(40);
                            s.setYrepeat(16);
                            t[5] = -1;
                            t[0] = 0;
                        }

                        continue;
                    } else if (t[0] != -2) {
                        getglobalz(i);
                    }

                    if (t[0] == -2) // On top of somebody
                    {
                        makeitfall(currentGame.getCON(), i);
                        Sprite ts = boardService.getSprite(t[5]);
                        if (ts == null) {
                            continue;
                        }

                        ts.setXvel(0);

                        int l = ts.getAng();

                        s.setZ(ts.getZ());
                        s.setX(ts.getX() + (EngineUtils.cos(l & 2047) >> 11));
                        s.setY(ts.getY() + (EngineUtils.sin(l & 2047) >> 11));

                        s.setPicnum((short) (GREENSLIME + 2 + (global_random & 1)));

                        if (s.getYrepeat() < 64) {
                            s.setYrepeat(s.getYrepeat() + 2);
                        } else {
                            if (s.getXrepeat() < 32) {
                                s.setXrepeat(s.getXrepeat() + 4);
                            } else {
                                t[0] = -1;
                                x = ldist(s, ts);
                                if (x < 768) {
                                    ts.setXrepeat(0);
                                }
                            }
                        }

                        continue;
                    }

                    // Check randomly to see of there is an actor near
                    if (rnd(32)) {
                        ListNode<Sprite> n = boardService.getSectNode(sect);
                        while (n != null) {
                            Sprite sp = n.get();
                            switch (sp.getPicnum()) {
                                case LIZTROOP:
                                case LIZMAN:
                                case PIGCOP:
                                case NEWBEAST:
                                    if (ldist(s, sp) < 768 && (klabs(s.getZ() - sp.getZ()) < 8192)) // Gulp them
                                    {
                                        t[5] = n.getIndex();
                                        t[0] = -2;
                                        t[1] = 0;
                                        continue BOLT;
                                    }
                            }

                            n = n.getNext();
                        }
                    }

                    // Moving on the ground or ceiling

                    if (t[0] == 0 || t[0] == 2) {
                        s.setPicnum(GREENSLIME);

                        if ((engine.krand() & 511) == 0) {
                            spritesound(SLIM_ROAM, i);
                        }

                        if (t[0] == 2) {
                            s.setZvel(0);
                            s.setCstat(s.getCstat() & (65535 - 8));

                            if ((sec.getCeilingstat() & 1) != 0 || (hittype[i].ceilingz + 6144) < s.getZ()) {
                                s.setZ(s.getZ() + 2048);
                                t[0] = 3;
                                continue;
                            }
                        } else {
                            s.setCstat(s.getCstat() | 8);
                            makeitfall(currentGame.getCON(), i);
                        }

                        if ((everyothertime & 1) != 0) {
                            ssp(i, CLIPMASK0);
                        }

                        if (s.getXvel() > 96) {
                            s.setXvel(s.getXvel() - 2);
                            continue;
                        } else {
                            if (s.getXvel() < 32) {
                                s.setXvel(s.getXvel() + 4);
                            }
                            s.setXvel((short) (64 - (EngineUtils.sin((t[1] + 512) & 2047) >> 9)));

                            s.setAng(s.getAng() + (getincangle(s.getAng(), EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY())) >> 3));
                            // TJR
                        }

                        s.setXrepeat((short) (36 + (EngineUtils.sin((t[1] + 512) & 2047) >> 11)));
                        s.setYrepeat((short) (16 + (EngineUtils.sin(t[1] & 2047) >> 13)));

                        if (rnd(4) && (sec.getCeilingstat() & 1) == 0 && klabs(hittype[i].floorz - hittype[i].ceilingz) < (192 << 8)) {
                            s.setZvel(0);
                            t[0]++;
                        }

                    }

                    if (t[0] == 1) {
                        s.setPicnum(GREENSLIME);
                        if (s.getYrepeat() < 40) {
                            s.setYrepeat(s.getYrepeat() + 8);
                        }
                        if (s.getXrepeat() > 8) {
                            s.setXrepeat(s.getXrepeat() - 4);
                        }
                        if (s.getZvel() > -(2048 + 1024)) {
                            s.setZvel(s.getZvel() - 348);
                        }
                        s.setZ(s.getZ() + s.getZvel());
                        if (s.getZ() < hittype[i].ceilingz + 4096) {
                            s.setZ(hittype[i].ceilingz + 4096);
                            s.setXvel(0);
                            t[0] = 2;
                        }
                    }

                    if (t[0] == 3) {
                        s.setPicnum(GREENSLIME + 1);

                        makeitfall(currentGame.getCON(), i);

                        if (s.getZ() > hittype[i].floorz - (8 << 8)) {
                            s.setYrepeat(s.getYrepeat() - 4);
                            s.setXrepeat(s.getXrepeat() + 2);
                        } else {
                            if (s.getYrepeat() < (40 - 4)) {
                                s.setYrepeat(s.getYrepeat() + 8);
                            }
                            if (s.getXrepeat() > 8) {
                                s.setXrepeat(s.getXrepeat() - 4);
                            }
                        }

                        if (s.getZ() > hittype[i].floorz - 2048) {
                            s.setZ(hittype[i].floorz - 2048);
                            t[0] = 0;
                            s.setXvel(0);
                        }
                    }
                    continue;
                }
                case BOUNCEMINE:
                case MORTER:
                    hittype[spawn(i, FRAMEEFFECT1)].temp_data[0] = 3;

                case HEAVYHBOMB: {
                    if ((s.getCstat() & 32768) != 0) {
                        t[2]--;
                        if (t[2] <= 0) {
                            spritesound(TELEPORTER, i);
                            spawn(i, TRANSPORTERSTAR);
                            s.setCstat(257);
                        }
                        continue;
                    }

                    int p = findplayer(s);
                    int x = player_dist;

                    if (x < 1220) {
                        s.setCstat(s.getCstat() & ~257);
                    } else {
                        s.setCstat(s.getCstat() | 257);
                    }

                    int l = 0;
                    //noinspection SwitchStatementWithTooFewBranches
                    switch(l) { // goto block
                        default:
                        if (t[3] == 0) {
                            if (ifhitbyweapon(i) >= 0) {
                                t[3] = 1;
                                t[4] = 0;
                                s.setXvel(0);
                                break; // goto DETONATEB;
                            }
                        }

                        if (s.getPicnum() != BOUNCEMINE) {
                            makeitfall(currentGame.getCON(), i);

                            if (sec.getLotag() != 1 && s.getZ() >= hittype[i].floorz - (FOURSLEIGHT) && s.getYvel() < 3) {
                                if (s.getYvel() > 0 || (s.getYvel() == 0 && hittype[i].floorz == sec.getFloorz())) {
                                    spritesound(PIPEBOMB_BOUNCE, i);
                                }
                                s.setZvel((short) -((4 - s.getYvel()) << 8));
                                Sector sec2 = boardService.getSector(s.getSectnum());
                                if (sec2 != sec) { // FIXME: Check and delete
                                    Console.out.println("Actors(3598) sec2 != sec", OsdColor.RED);
                                }

                                if (sec2 != null && sec2.getLotag() == 2) {
                                    s.setZvel(s.getZvel() >> 2);
                                }
                                s.setYvel(s.getYvel() + 1);
                            }
                            if (s.getZ() < hittype[i].ceilingz) // && sec.lotag != 2 )
                            {
                                s.setZ(hittype[i].ceilingz + (3 << 8));
                                s.setZvel(0);
                            }
                        }

                        int moveHit = movesprite(i, (s.getXvel() * (EngineUtils.cos(s.getAng() & 2047))) >> 14, (s.getXvel() * (EngineUtils.sin(s.getAng() & 2047))) >> 14, s.getZvel(), CLIPMASK0);
                        Sector sec2 = boardService.getSector(s.getSectnum());
                        if (sec2 != null && sec2.getLotag() == 1 && s.getZvel() == 0) {
                            s.setZ(s.getZ() + (32 << 8));
                            if (t[5] == 0) {
                                t[5] = 1;
                                spawn(i, WATERSPLASH2);
                            }
                        } else {
                            t[5] = 0;
                        }

                        if (t[3] == 0 && (s.getPicnum() == BOUNCEMINE || s.getPicnum() == MORTER) && (moveHit != 0 || x < 844)) {
                            t[3] = 1;
                            t[4] = 0;
                            s.setXvel(0);
                            // goto DETONATEB;
                        } else {
                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (spo != null && spo.getPicnum() == APLAYER) {
                                l = spo.getYvel();
                            } else {
                                l = -1;
                            }

                            if (s.getXvel() > 0) {
                                s.setXvel(s.getXvel() - 5);
                                if (sec.getLotag() == 2) {
                                    s.setXvel(s.getXvel() - 10);
                                }

                                if (s.getXvel() < 0) {
                                    s.setXvel(0);
                                }
                                if ((s.getXvel() & 8) != 0) {
                                    s.setCstat(s.getCstat() ^ 4);
                                }
                            }

                            if ((moveHit & kHitTypeMask) == kHitWall) {
                                moveHit &= kHitIndexMask;

                                checkhitwall(i, moveHit, s.getX(), s.getY(), s.getZ(), s.getPicnum());

                                Wall hwall = boardService.getWall(moveHit);
                                if (hwall != null) {
                                    int k = hwall.getWallAngle(); //EngineUtils.getAngle(boardService.getWall(boardService.getWall(moveHit).getPoint2()).getX() - boardService.getWall(moveHit).getX(), boardService.getWall(boardService.getWall(moveHit).getPoint2()).getY() - boardService.getWall(moveHit).getY());
                                    s.setAng((short) (((k << 1) - s.getAng()) & 2047));
                                    s.setXvel(s.getXvel() >> 1);
                                }
                            }
                        }
                    }

                    if (game.isGameType(GameType.NAM) && s.getPicnum() == HEAVYHBOMB) {
                        int extra = s.getExtra();
                        s.setExtra(extra - 1);
                        if (extra <= 0) {
                            t[3] = 1;
                        }
                    }

                    // DETONATEB:
                    if ((l >= 0 && ps[l].hbomb_on == 0) || t[3] == 1) {
                        t[4]++;

                        if (t[4] == 2) {
                            x = s.getExtra();
                            int m = 0;
                            switch (s.getPicnum()) {
                                case HEAVYHBOMB:
                                    m = currentGame.getCON().pipebombblastradius;
                                    break;
                                case MORTER:
                                    m = currentGame.getCON().morterblastradius;
                                    break;
                                case BOUNCEMINE:
                                    m = currentGame.getCON().bouncemineblastradius;
                                    break;
                            }

                            hitradius(i, m, x >> 2, x >> 1, x - (x >> 2), x);
                            spawn(i, EXPLOSION2);
                            if (s.getZvel() == 0) {
                                spawn(i, EXPLOSION2BOT);
                            }
                            spritesound(PIPEBOMB_EXPLODE, i);
                            for (x = 0; x < 8; x++) {
                                RANDOMSCRAP(s, i);
                            }
                        }

                        if (s.getYrepeat() != 0) {
                            s.setYrepeat(0);
                            continue;
                        }

                        if (t[4] > 20) {
                            if (s.getOwner() != i || !ud.respawn_items) {
                                engine.deletesprite(i);
                            } else {
                                t[2] = currentGame.getCON().respawnitemtime;
                                spawn(i, RESPAWNMARKERRED);
                                s.setCstat((short) 32768);
                                s.setYrepeat(9);
                            }
                            continue;
                        }
                    } else if (s.getPicnum() == HEAVYHBOMB && x < 788 && t[0] > 7 && s.getXvel() == 0) {
                        if (engine.cansee(s.getX(), s.getY(), s.getZ() - (8 << 8), s.getSectnum(), ps[p].posx, ps[p].posy, ps[p].posz, ps[p].cursectnum)) {
                            if (ps[p].ammo_amount[HANDBOMB_WEAPON] < currentGame.getCON().max_ammo_amount[HANDBOMB_WEAPON]) {
                                if (ud.coop >= 1 && s.getOwner() == i) {
                                    for (int j = 0; j < ps[p].weapreccnt; j++) {
                                        if (ps[p].weaprecs[j] == s.getPicnum()) {
                                            continue BOLT;
                                        }
                                    }

                                    if (ps[p].weapreccnt < 16) {
                                        ps[p].weaprecs[ps[p].weapreccnt++] = s.getPicnum();
                                    }
                                }

                                addammo(HANDBOMB_WEAPON, ps[p], 1);
                                spritesound(DUKE_GET, ps[p].i);

                                if (!ps[p].gotweapon[HANDBOMB_WEAPON] || s.getOwner() == ps[p].i) {
                                    addweapon(ps[p], HANDBOMB_WEAPON);
                                }

                                Sprite spo = boardService.getSprite(s.getOwner());
                                if (spo != null && spo.getPicnum() != APLAYER) {
                                    ps[p].pals[0] = 0;
                                    ps[p].pals[1] = 32;
                                    ps[p].pals[2] = 0;
                                    ps[p].pals_time = 32;
                                }

                                if (s.getOwner() != i || !ud.respawn_items) {
                                    if (s.getOwner() == i && ud.coop >= 1) {
                                        continue;
                                    }

                                    engine.deletesprite(i);
                                    continue;
                                } else {
                                    t[2] = currentGame.getCON().respawnitemtime;
                                    spawn(i, RESPAWNMARKERRED);
                                    s.setCstat((short) 32768);
                                }
                            }
                        }
                    }

                    if (t[0] < 8) {
                        t[0]++;
                    }
                    continue;
                }
                case REACTORBURNT:
                case REACTOR2BURNT:
                    continue;

                case REACTOR:
                case REACTOR2: {
                    if (t[4] == 1) {
                        ListNode<Sprite> node = boardService.getSectNode(sect);
                        while (node != null) {
                            Sprite sp = node.get();
                            switch (sp.getPicnum()) {
                                case SECTOREFFECTOR:
                                    if (sp.getLotag() == 1) {
                                        sp.setLotag((short) 65535);
                                        sp.setHitag((short) 65535);
                                    }
                                    break;
                                case REACTOR:
                                    sp.setPicnum(REACTORBURNT);
                                    break;
                                case REACTOR2:
                                    sp.setPicnum(REACTOR2BURNT);
                                    break;
                                case REACTORSPARK:
                                case REACTOR2SPARK:
                                    sp.setCstat((short) 32768);
                                    break;
                            }
                            node = node.getNext();
                        }
                        continue;
                    }

                    if (t[1] >= 20) {
                        t[4] = 1;
                        continue;
                    }

                    int p = findplayer(s);
                    int x = player_dist;

                    t[2]++;
                    if (t[2] == 4) {
                        t[2] = 0;
                    }

                    if (x < 4096) {
                        if ((engine.krand() & 255) < 16) {
                            if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                spritesound(DUKE_LONGTERM_PAIN, ps[p].i);
                            }

                            spritesound(SHORT_CIRCUIT, i);

                            ps[p].getPlayerSprite().setExtra(ps[p].getPlayerSprite().getExtra() - 1);
                            ps[p].pals_time = 32;
                            ps[p].pals[0] = 32;
                            ps[p].pals[1] = 0;
                            ps[p].pals[2] = 0;
                        }
                        t[0] += 128;
                        if (t[3] == 0) {
                            t[3] = 1;
                        }
                    } else {
                        t[3] = 0;
                    }

                    if (t[1] != 0) {
                        t[1]++;

                        t[4] = s.getZ();
                        s.setZ(sec.getFloorz());
                        int dz = sec.getFloorz() - sec.getCeilingz();
                        if (dz != 0) {
                            s.setZ(s.getZ() - (engine.krand() % dz));
                        }

                        switch (t[1]) {
                            case 3: {
                                // Turn on all of those flashing sectoreffector.
                                hitradius(i, 4096, currentGame.getCON().impact_damage << 2, currentGame.getCON().impact_damage << 2, currentGame.getCON().impact_damage << 2, currentGame.getCON().impact_damage << 2);

                                ListNode<Sprite> node = boardService.getStatNode(6);
                                while (node != null) {
                                    Sprite sp = node.get();
                                    if (sp.getPicnum() == MASTERSWITCH) {
                                        if (sp.getHitag() == s.getHitag()) {
                                            if (sp.getYvel() == 0) {
                                                sp.setYvel(1);
                                            }
                                        }
                                    }
                                    node = node.getNext();
                                }
                                break;
                            }
                            case 4:
                            case 7:
                            case 10:
                            case 15:
                                ListNode<Sprite> node = boardService.getSectNode(sect);
                                while (node != null) {
                                    ListNode<Sprite> next = node.getNext();
                                    int j = node.getIndex();
                                    if (j != i) {
                                        engine.deletesprite(j);
                                        break;
                                    }
                                    node = next;
                                }
                                break;
                        }
                        for (x = 0; x < 16; x++) {
                            RANDOMSCRAP(s, i);
                        }

                        s.setZ(t[4]);
                        t[4] = 0;

                    } else {
                        if (ifhitbyweapon(i) >= 0) {
                            for (x = 0; x < 32; x++) {
                                RANDOMSCRAP(s, i);
                            }
                            if (s.getExtra() < 0) {
                                t[1] = 1;
                            }
                        }
                    }
                    continue;
                }
                case CAMERA1: {
                    if (t[0] == 0) {
                        t[1] += 8;
                        if (currentGame.getCON().camerashitable != 0) {
                            if (ifhitbyweapon(i) >= 0) {
                                t[0] = 1; // static
                                s.setCstat((short) 32768);
                                for (int x = 0; x < 5; x++) {
                                    RANDOMSCRAP(s, i);
                                }
                                continue;
                            }
                        }

                        if (s.getHitag() > 0) {
                            if (t[1] < s.getHitag()) {
                                s.setAng(s.getAng() + 8);
                            } else if (t[1] < (s.getHitag() * 3)) {
                                s.setAng(s.getAng() - 8);
                            } else if (t[1] < (s.getHitag() << 2)) {
                                s.setAng(s.getAng() + 8);
                            } else {
                                t[1] = 8;
                                s.setAng(s.getAng() + 16);
                            }
                        }
                    }
                    continue;
                }
            }

            if (ud.multimode < 2 && badguy(s)) {
                if (actor_tog == 1) {
                    s.setCstat((short) 32768);
                    continue;
                } else if (actor_tog == 2) {
                    s.setCstat(257);
                }
            }

            int p = findplayer(s);
            int x = player_dist;
            execute(currentGame.getCON(), i, p, x);
        }
    }

    public static void moveexplosions() // STATNUM 5
    {
        ListNode<Sprite> ni = boardService.getStatNode(5), nexti;
        for (; ni != null; ni = nexti) {
            nexti = ni.getNext();
            final int i = ni.getIndex();

            int[] t = hittype[i].temp_data;
            final Sprite s = ni.get();
            int sect = s.getSectnum();

            if (sect < 0 || s.getXrepeat() == 0) {
                engine.deletesprite(i);
                continue;
            }

            game.pInt.setsprinterpolate(i, s);

            switch (s.getPicnum()) {
                case FIREFLYFLYINGEFFECT: {
                    if (currentGame.getCON().type != 20) // Twentieth Anniversary World Tour
                    {
                        continue;
                    }

                    int p = findplayer(s);
                    int x = player_dist;
                    execute(currentGame.getCON(), i, p, x);

                    Sprite owner = boardService.getSprite(s.getOwner());
                    if (owner == null) {
                        continue;
                    }

                    if (owner.getPicnum() != FIREFLY) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (owner.getXrepeat() >= 24 || owner.getPal() == 1) {
                        s.setCstat(s.getCstat() | 0x8000);
                    } else {
                        s.setCstat(s.getCstat() & ~0x8000);
                    }

                    double dx = owner.getX() - ps[p].getPlayerSprite().getX();
                    double dy = owner.getY() - ps[p].getPlayerSprite().getY();
                    double dist = Math.sqrt(dx * dx + dy * dy);
                    if (dist != 0.0) {
                        dx /= dist;
                        dy /= dist;
                    }

                    s.setX((int) (owner.getX() - (dx * -10.0)));
                    s.setY((int) (owner.getY() - (dy * -10.0)));
                    s.setZ(owner.getZ() + 2048);

                    if (owner.getExtra() <= 0) {
                        engine.deletesprite(i);
                        continue;
                    }

                    break;
                }
                case NEON1:
                case NEON2:
                case NEON3:
                case NEON4:
                case NEON5:
                case NEON6:

                    if ((global_random / (s.getLotag() + 1) & 31) > 4) {
                        s.setShade(-127);
                    } else {
                        s.setShade(127);
                    }
                    continue;

                case BLOODSPLAT1:
                case BLOODSPLAT2:
                case BLOODSPLAT3:
                case BLOODSPLAT4:

                    if (t[0] == 7 * 26) {
                        continue;
                    }
                    s.setZ(s.getZ() + 16 + (engine.krand() & 15));
                    t[0]++;
                    if ((t[0] % 9) == 0) {
                        s.setYrepeat(s.getYrepeat() + 1);
                    }
                    continue;

                case NUKEBUTTON:
                case NUKEBUTTON + 1:
                case NUKEBUTTON + 2:
                case NUKEBUTTON + 3:

                    if (t[0] != 0) {
                        Sprite spo = boardService.getSprite(s.getOwner());

                        t[0]++;
                        if (spo != null) {
                            if (t[0] == 8) {
                                s.setPicnum(NUKEBUTTON + 1);
                            } else if (t[0] == 16) {
                                s.setPicnum(NUKEBUTTON + 2);
                                ps[spo.getYvel()].fist_incs = 1;
                            }

                            if (ps[spo.getYvel()].fist_incs == 26) {
                                s.setPicnum(NUKEBUTTON + 3);
                            }
                        }
                    }
                    continue;

                case FORCESPHERE: {
                    Sprite spo = boardService.getSprite(s.getOwner());

                    int l = s.getXrepeat();
                    if (t[1] > 0) {
                        t[1]--;
                        if (t[1] == 0) {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    if (hittype[s.getOwner()].temp_data[1] == 0) {
                        if (t[0] < 64) {
                            t[0]++;
                            l += 3;
                        }
                    } else if (t[0] > 64) {
                        t[0]--;
                        l -= 3;
                    }

                    if (spo != null) {
                        s.setX(spo.getX());
                        s.setY(spo.getY());
                        s.setZ(spo.getZ());
                        s.setAng(s.getAng() + hittype[s.getOwner()].temp_data[0]);
                    }

                    if (l > 64) {
                        l = 64;
                    } else if (l < 1) {
                        l = 1;
                    }

                    s.setXrepeat((short) l);
                    s.setYrepeat((short) l);
                    s.setShade((byte) ((l >> 1) - 48));

                    for (int j = t[0]; j > 0; j--) {
                        ssp(i, CLIPMASK0);
                    }
                    continue;
                }
                case WATERSPLASH2:

                    t[0]++;
                    if (t[0] == 1) {
                        Sector sec = boardService.getSector(sect);
                        if (sec != null && sec.getLotag() != 1 && sec.getLotag() != 2) {
                            engine.deletesprite(i);
                            continue;
                        }

                        if (Sound[ITEM_SPLASH].getSoundOwnerCount() == 0) {
                            spritesound(ITEM_SPLASH, i);
                        }
                    }
                    if (t[0] == 3) {
                        t[0] = 0;
                        t[1]++;
                    }
                    if (t[1] == 5) {
                        engine.deletesprite(i);
                    }
                    continue;

                case FRAMEEFFECT1:
                    Sprite spo = boardService.getSprite(s.getOwner());
                    if (spo != null) {
                        t[0]++;

                        if (t[0] > 7) {
                            engine.deletesprite(i);
                            continue;
                        } else if (t[0] > 4) {
                            s.setCstat(s.getCstat() | 512 + 2);
                        } else if (t[0] > 2) {
                            s.setCstat(s.getCstat() | 2);
                        }
                        s.setXoffset(spo.getXoffset());
                        s.setYoffset(spo.getYoffset());
                    }
                    continue;
                case INNERJAW:
                case INNERJAW + 1: {
                    int p = findplayer(s);
                    int x = player_dist;
                    if (x < 512) {
                        ps[p].pals_time = 32;
                        ps[p].pals[0] = 32;
                        ps[p].pals[1] = 0;
                        ps[p].pals[2] = 0;
                        ps[p].getPlayerSprite().setExtra(ps[p].getPlayerSprite().getExtra() - 4);
                    }
                }
                case FIRELASER:
                    if (s.getExtra() != 999) {
                        s.setExtra(999);
                    } else {
                        engine.deletesprite(i);
                        continue;
                    }
                    break;
                case TONGUE:
                    engine.deletesprite(i);
                    continue;
                case MONEY + 1:
                case MAIL + 1:
                case PAPER + 1:
                    int z = engine.getflorzofslope(s.getSectnum(), s.getX(), s.getY());
                    s.setZ(z);
                    hittype[i].floorz = z;
                    break;
                case MONEY:
                case MAIL:
                case PAPER: {

                    s.setXvel((short) ((engine.krand() & 7) + (EngineUtils.sin(hittype[i].temp_data[0] & 2047) >> 9)));
                    hittype[i].temp_data[0] += (engine.krand() & 63);
                    if ((hittype[i].temp_data[0] & 2047) > 512 && (hittype[i].temp_data[0] & 2047) < 1596) {
                        Sector sec = boardService.getSector(sect);
                        if (sec != null && sec.getLotag() == 2) {
                            if (s.getZvel() < 64) {
                                s.setZvel(s.getZvel() + (currentGame.getCON().gc >> 5) + (engine.krand() & 7));
                            }
                        } else if (s.getZvel() < 144) {
                            s.setZvel(s.getZvel() + (currentGame.getCON().gc >> 5) + (engine.krand() & 7));
                        }
                    }

                    ssp(i, CLIPMASK0);

                    if ((engine.krand() & 3) == 0) {
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    }

                    if (s.getSectnum() == -1) {
                        engine.deletesprite(i);
                        continue;
                    }
                    int l = engine.getflorzofslope(s.getSectnum(), s.getX(), s.getY());

                    if (s.getZ() > l) {
                        s.setZ(l);

                        insertspriteq(i);
                        s.setPicnum(s.getPicnum() + 1);

                        ListNode<Sprite> node = boardService.getStatNode(5);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (sp.getPicnum() == BLOODPOOL) {
                                if (ldist(s, sp) < 348) {
                                    s.setPal(2);
                                    break;
                                }
                            }
                            node = node.getNext();
                        }
                    }

                    break;
                }
                case JIBS1:
                case JIBS2:
                case JIBS3:
                case JIBS4:
                case JIBS5:
                case JIBS6:
                case HEADJIB1:
                case ARMJIB1:
                case LEGJIB1:
                case LIZMANHEAD1:
                case LIZMANARM1:
                case LIZMANLEG1:
                case DUKETORSO:
                case DUKEGUN:
                case DUKELEG: {

                    if (s.getXvel() > 0) {
                        s.setXvel(s.getXvel() - 1);
                    } else {
                        s.setXvel(0);
                    }

                    if (t[5] < 30 * 10) {
                        t[5]++;
                    } else {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (s.getZvel() > 1024 && s.getZvel() < 1280) {
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                        sect = s.getSectnum();
                    }

                    int l = engine.getflorzofslope(sect, s.getX(), s.getY());
                    int x = engine.getceilzofslope(sect, s.getX(), s.getY());
                    Sector sec = boardService.getSector(sect);
                    if (x == l || sec == null) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (s.getZ() < l - (2 << 8)) {
                        if (t[1] < 2) {
                            t[1]++;
                        } else if (sec.getLotag() != 2) {
                            t[1] = 0;
                            if (s.getPicnum() == DUKELEG || s.getPicnum() == DUKETORSO || s.getPicnum() == DUKEGUN) {
                                if (t[0] > 6) {
                                    t[0] = 0;
                                } else {
                                    t[0]++;
                                }
                            } else {
                                if (t[0] > 2) {
                                    t[0] = 0;
                                } else {
                                    t[0]++;
                                }
                            }
                        }

                        if (s.getZvel() < 6144) {
                            if (sec.getLotag() == 2) {
                                if (s.getZvel() < 1024) {
                                    s.setZvel(s.getZvel() + 48);
                                } else {
                                    s.setZvel(1024);
                                }
                            } else {
                                s.setZvel(s.getZvel() + currentGame.getCON().gc - 50);
                            }
                        }

                        s.setX(s.getX() + ((s.getXvel() * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 14));
                        s.setY(s.getY() + ((s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14));
                        s.setZ(s.getZ() + s.getZvel());

                    } else {
                        if (t[2] == 0) {
                            if ((sec.getFloorstat() & 2) != 0) {
                                engine.deletesprite(i);
                                continue;
                            }
                            t[2]++;
                        }
                        l = engine.getflorzofslope(s.getSectnum(), s.getX(), s.getY());

                        s.setZ(l - (2 << 8));
                        s.setXvel(0);

                        if (s.getPicnum() == JIBS6) {
                            t[1]++;
                            if ((t[1] & 3) == 0 && t[0] < 7) {
                                t[0]++;
                            }
                            if (t[1] > 20) {
                                engine.deletesprite(i);
                                continue;
                            }
                        } else {
                            s.setPicnum(JIBS6);
                            t[0] = 0;
                            t[1] = 0;
                        }
                    }
                    continue;
                }
                case BLOODPOOL:
                case PUKE: {
                    Sector sec = boardService.getSector(sect);

                    if (t[0] == 0) {
                        t[0] = 1;
                        if (sec == null || (sec.getFloorstat() & 2) != 0) {
                            engine.deletesprite(i);
                            continue;
                        } else {
                            insertspriteq(i);
                        }
                    }

                    makeitfall(currentGame.getCON(), i);

                    int p = findplayer(s);
                    int x = player_dist;

                    s.setZ(hittype[i].floorz - (FOURSLEIGHT));

                    if (t[2] < 32) {
                        t[2]++;
                        if (hittype[i].picnum == TIRE) {
                            if (s.getXrepeat() < 64 && s.getYrepeat() < 64) {
                                s.setXrepeat(s.getXrepeat() + (engine.krand() & 3));
                                s.setYrepeat(s.getYrepeat() + (engine.krand() & 3));
                            }
                        } else {
                            if (s.getXrepeat() < 32 && s.getYrepeat() < 32) {
                                s.setXrepeat(s.getXrepeat() + (engine.krand() & 3));
                                s.setYrepeat(s.getYrepeat() + (engine.krand() & 3));
                            }
                        }
                    }

                    if (x < 844 && s.getXrepeat() > 6 && s.getYrepeat() > 6) {
                        if (s.getPal() == 0 && (engine.krand() & 255) < 16 && s.getPicnum() != PUKE) {
                            if (ps[p].boot_amount > 0) {
                                ps[p].boot_amount--;
                            } else {
                                if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                    spritesound(DUKE_LONGTERM_PAIN, ps[p].i);
                                }
                                ps[p].getPlayerSprite().setExtra(ps[p].getPlayerSprite().getExtra() - 1);
                                ps[p].pals_time = 32;
                                ps[p].pals[0] = 16;
                                ps[p].pals[1] = 0;
                                ps[p].pals[2] = 0;
                            }
                        }

                        if (t[1] == 1) {
                            continue;
                        }
                        t[1] = 1;

                        if (hittype[i].picnum == TIRE) {
                            ps[p].footprintcount = 10;
                        } else {
                            ps[p].footprintcount = 3;
                        }

                        ps[p].footprintpal = s.getPal();
                        ps[p].footprintshade = s.getShade();

                        if (t[2] == 32) {
                            s.setXrepeat(s.getXrepeat() - 6);
                            s.setYrepeat(s.getYrepeat() - 6);
                        }
                    } else {
                        t[1] = 0;
                    }
                    continue;
                }
                case LAVAPOOL:
                case ONFIRE:
                case BURNEDCORPSE:
                case LAVAPOOLBUBBLE:
                case WHISPYSMOKE:
                    if (currentGame.getCON().type != 20) // Twentieth Anniversary World Tour
                    {
                        continue;
                    }

                case BURNING:
                case BURNING2:
                case FECES:
                case WATERBUBBLE:
                case SMALLSMOKE:
                case EXPLOSION2:
                case SHRINKEREXPLOSION:
                case EXPLOSION2BOT:
                case BLOOD:
                case LASERSITE:
                case FORCERIPPLE:
                case TRANSPORTERSTAR:
                case TRANSPORTERBEAM:
                    int p = findplayer(s);
                    int x = player_dist;
                    execute(currentGame.getCON(), i, p, x);
                    continue;

                case SHELL:
                case SHOTGUNSHELL: {

                    ssp(i, CLIPMASK0);
                    Sector sec = boardService.getSector(sect);
                    if (sec == null || (sec.getFloorz() + (24 << 8)) < s.getZ()) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (sec.getLotag() == 2) {
                        t[1]++;
                        if (t[1] > 8) {
                            t[1] = 0;
                            t[0]++;
                            t[0] &= 3;
                        }
                        if (s.getZvel() < 128) {
                            s.setZvel(s.getZvel() + (currentGame.getCON().gc / 13)); // 8
                        } else {
                            s.setZvel(s.getZvel() - 64);
                        }
                        if (s.getXvel() > 0) {
                            s.setXvel(s.getXvel() - 4);
                        } else {
                            s.setXvel(0);
                        }
                    } else {
                        t[1]++;
                        if (t[1] > 3) {
                            t[1] = 0;
                            t[0]++;
                            t[0] &= 3;
                        }
                        if (s.getZvel() < 512) {
                            s.setZvel(s.getZvel() + (currentGame.getCON().gc / 3)); // 52;
                        }
                        if (s.getXvel() > 0) {
                            s.setXvel(s.getXvel() - 1);
                        } else {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    continue;
                }
                case GLASSPIECES:
                case GLASSPIECES + 1:
                case GLASSPIECES + 2: {
                    makeitfall(currentGame.getCON(), i);
                    Sector sec = boardService.getSector(sect);

                    if (s.getZvel() > 4096) {
                        s.setZvel(4096);
                    }
                    if (sec == null) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (s.getZ() == hittype[i].floorz - (FOURSLEIGHT) && t[0] < 3) {
                        s.setZvel((short) (-((3 - t[0]) << 8) - (engine.krand() & 511)));
                        if (sec.getLotag() == 2) {
                            s.setZvel(s.getZvel() >> 1);
                        }
                        s.setXrepeat(s.getXrepeat() >> 1);
                        s.setYrepeat(s.getYrepeat() >> 1);
                        if (rnd(96)) {
                            engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                        }
                        t[0]++;// Number of bounces
                    } else if (t[0] == 3) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (s.getXvel() > 0) {
                        s.setXvel(s.getXvel() - 2);
                        s.setCstat((short) ((s.getXvel() & 3) << 2));
                    } else {
                        s.setXvel(0);
                    }

                    ssp(i, CLIPMASK0);

                    continue;
                }
            }

            if (IFWITHIN(s, SCRAP6, SCRAP5 + 3)) {
                if (s.getXvel() > 0) {
                    s.setXvel(s.getXvel() - 1);
                } else {
                    s.setXvel(0);
                }

                if (s.getZvel() > 1024 && s.getZvel() < 1280) {
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    sect = s.getSectnum();
                }

                Sector sec = boardService.getSector(sect);
                if (sec != null && s.getZ() < sec.getFloorz() - (2 << 8)) {
                    if (t[1] < 1) {
                        t[1]++;
                    } else {
                        t[1] = 0;

                        if (s.getPicnum() < SCRAP6 + 8) {
                            if (t[0] > 6) {
                                t[0] = 0;
                            } else {
                                t[0]++;
                            }
                        } else {
                            if (t[0] > 2) {
                                t[0] = 0;
                            } else {
                                t[0]++;
                            }
                        }
                    }
                    if (s.getZvel() < 4096) {
                        s.setZvel(s.getZvel() + currentGame.getCON().gc - 50);
                    }
                    s.setX(s.getX() + ((s.getXvel() * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 14));
                    s.setY(s.getY() + ((s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14));
                    s.setZ(s.getZ() + s.getZvel());
                } else {
                    if (s.getPicnum() == SCRAP1 && s.getYvel() > 0) {
                        int j = spawn(i, s.getYvel());
                        engine.setsprite(j, s.getX(), s.getY(), s.getZ());
                        getglobalz(j);
                        Sprite js = boardService.getSprite(j);
                        if (js != null) {
                            js.setLotag(0);
                            js.setHitag(0);
                        }
                    }
                    engine.deletesprite(i);
                }
            }
        }
    }

    public static void moveeffectors() // STATNUM 3
    {
        fricxv = fricyv = 0;
        ListNode<Sprite> ni = boardService.getStatNode(3), nexti;

        BOLT:
        for (; ni != null; ni = nexti) {
            nexti = ni.getNext();
            Sprite s = ni.get();
            final int i = ni.getIndex();
            final Sector sc = boardService.getSector(s.getSectnum());

            int st = s.getLotag();
            int sh = s.getHitag();
            int[] t = hittype[i].temp_data;

            switch (st) {
                case 0: {
                    int zchange = 0;
                    final int j = s.getOwner();
                    final Sprite js = boardService.getSprite(j);

                    if (js == null || js.getLotag() == (short) 65535) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if (sc == null) {
                        break;
                    }

                    int q = sc.getExtra() >> 3;
                    int l = 0;

                    if (sc.getLotag() == 30) {
                        q >>= 2;

                        if (s.getExtra() == 1) {
                            if (hittype[i].tempang < 256) {
                                hittype[i].tempang += 4;
                                if (hittype[i].tempang >= 256) {
                                    callsound(s.getSectnum(), i);
                                }
                                if (s.getClipdist() != 0) {
                                    l = 1;
                                } else {
                                    l = -1;
                                }
                            } else {
                                hittype[i].tempang = 256;
                            }

                            if (sc.getFloorz() > s.getZ()) // z's are touching
                            {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() - 512);
                                zchange = -512;
                                if (sc.getFloorz() < s.getZ()) {
                                    sc.setFloorz(s.getZ());
                                }
                            } else if (sc.getFloorz() < s.getZ()) // z's are touching
                            {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() + 512);
                                zchange = 512;
                                if (sc.getFloorz() > s.getZ()) {
                                    sc.setFloorz(s.getZ());
                                }
                            }
                        } else if (s.getExtra() == 3) {
                            if (hittype[i].tempang > 0) {
                                hittype[i].tempang -= 4;
                                if (hittype[i].tempang <= 0) {
                                    callsound(s.getSectnum(), i);
                                }
                                if (s.getClipdist() != 0) {
                                    l = -1;
                                } else {
                                    l = 1;
                                }
                            } else {
                                hittype[i].tempang = 0;
                            }

                            if (sc.getFloorz() > hittype[i].temp_data[3]) // z's are touching
                            {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() - 512);
                                zchange = -512;
                                if (sc.getFloorz() < hittype[i].temp_data[3]) {
                                    sc.setFloorz(hittype[i].temp_data[3]);
                                }
                            } else if (sc.getFloorz() < hittype[i].temp_data[3]) // z's are touching
                            {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() + 512);
                                zchange = 512;
                                if (sc.getFloorz() > hittype[i].temp_data[3]) {
                                    sc.setFloorz(hittype[i].temp_data[3]);
                                }
                            }
                        }

                    } else {
                        if (hittype[j].temp_data[0] == 0) {
                            break;
                        }
                        if (hittype[j].temp_data[0] == 2) {
                            engine.deletesprite(i);
                            continue;
                        }

                        if (js.getAng() > 1024) {
                            l = -1;
                        } else {
                            l = 1;
                        }
                        if (t[3] == 0) {
                            t[3] = ldist(s, js);
                        }
                        s.setXvel((short) t[3]);
                        s.setX(js.getX());
                        s.setY(js.getY());
                    }
                    s.setAng(s.getAng() + (l * q));
                    t[2] += (l * q);

                    if (l != 0 && (sc.getFloorstat() & 64) != 0) {
                        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                            if (ps[p].cursectnum == s.getSectnum() && ps[p].on_ground) {
                                ps[p].ang += (l * q);
                                ps[p].ang = BClampAngle(ps[p].ang);
                                ps[p].posz += zchange;

                                Point rp = EngineUtils.rotatepoint(js.getX(), js.getY(), ps[p].posx, ps[p].posy, (short) (q * l));

                                int x = rp.getX();
                                int y = rp.getY();
                                ps[p].bobposx += x - ps[p].posx;
                                ps[p].bobposy += y - ps[p].posy;

                                ps[p].posx = x;
                                ps[p].posy = y;

                                Sprite psp = boardService.getSprite(ps[p].i);
                                if (psp != null && psp.getExtra() <= 0) {
                                    psp.setX(x);
                                    psp.setY(y);
                                }
                            }
                        }

                        ListNode<Sprite> node = boardService.getSectNode(s.getSectnum());
                        while (node != null) {
                            int p = node.getIndex();
                            Sprite sp = node.get();
                            if (sp.getStatnum() != 3 && sp.getStatnum() != 4) {
                                if (sp.getPicnum() != LASERLINE) {
                                    if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                        node = node.getNext();
                                        continue;
                                    }

                                    game.pInt.setsprinterpolate(p, sp);

                                    sp.setAng(sp.getAng() + (l * q));
                                    sp.setAng(sp.getAng() & 2047);
                                    sp.setZ(sp.getZ() + zchange);

                                    Point rp = EngineUtils.rotatepoint(js.getX(), js.getY(), sp.getX(), sp.getY(), (short) (q * l));
                                    sp.setX(rp.getX());
                                    sp.setY(rp.getY());

                                }
                            }
                            node = node.getNext();
                        }
                    }

                    ms(i);
                }

                break;
                case 1: // Nothing for now used as the pivot
                    if (s.getOwner() == -1) // Init
                    {
                        s.setOwner(i);

                        ListNode<Sprite> node = boardService.getStatNode(3);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (sp.getLotag() == 19 && sp.getHitag() == sh) {
                                t[0] = 0;
                                break;
                            }
                            node = node.getNext();
                        }
                    }

                    break;
                case 6: {
                    if (sc == null) {
                        break;
                    }

                    int k = sc.getExtra();
                    if (t[4] > 0) {
                        t[4]--;
                        if (t[4] >= (k - (k >> 3))) {
                            s.setXvel(s.getXvel() - (k >> 5));
                        }
                        if (t[4] > ((k >> 1) - 1) && t[4] < (k - (k >> 3))) {
                            s.setXvel(0);
                        }
                        if (t[4] < (k >> 1)) {
                            s.setXvel(s.getXvel() + (k >> 5));
                        }
                        if (t[4] < ((k >> 1) - (k >> 3))) {
                            t[4] = 0;
                            s.setXvel(k);
                        }
                    } else {
                        s.setXvel(k);
                    }

                    ListNode<Sprite> node = boardService.getStatNode(3);
                    while (node != null) {
                        Sprite sp = node.get();
                        int j = node.getIndex();
                        if ((sp.getLotag() == 14) && (sh == sp.getHitag()) && (hittype[j].temp_data[0] == t[0])) {
                            sp.setXvel(s.getXvel());
//	                        if( t[4] == 1 )
                            {
                                if (hittype[j].temp_data[5] == 0) {
                                    hittype[j].temp_data[5] = dist(sp, s);
                                }
                                int x = sgn(dist(sp, s) - hittype[j].temp_data[5]);
                                if (sp.getExtra() != 0) {
                                    x = -x;
                                }
                                s.setXvel(s.getXvel() + x);
                            }
                            hittype[j].temp_data[4] = t[4];
                        }
                        node = node.getNext();
                    }
                }
                // fall to case 14
                case 14: {
                    if (s.getOwner() == -1) {
                        s.setOwner((short) LocateTheLocator((short) t[3], (short) t[0]));
                    }

                    if (s.getOwner() == -1) {
                        throw new AssertException("Could not find any locators for SE# 6 and 14 with a hitag of " + t[3]);
                    }

                    Sprite so = boardService.getSprite(s.getOwner());
                    int j = so != null ? ldist(so, s) : 0;
                    if (j < 1024) {
                        if (st == 6) {
                            if (so != null && (so.getHitag() & 1) != 0) {
                                t[4] = sc.getExtra(); // Slow it down
                            }
                        }
                        t[3]++;
                        s.setOwner((short) LocateTheLocator(t[3], t[0]));
                        if (s.getOwner() == -1) {
                            t[3] = 0;
                            s.setOwner((short) LocateTheLocator(0, t[0]));
                        }
                    }

                    if (sc != null && s.getXvel() != 0) {
                        so = boardService.getSprite(s.getOwner()); // Attension!
                        int x = so != null ? EngineUtils.getAngle(so.getX() - s.getX(), so.getY() - s.getY()) : s.getAng();
                        int q = getincangle(s.getAng(), x) >> 3;

                        t[2] += q;
                        s.setAng(s.getAng() + q);

                        if (s.getXvel() == sc.getExtra()) {
                            if ((sc.getFloorstat() & 1) == 0 && (sc.getCeilingstat() & 1) == 0) {
                                if (Sound[hittype[i].lastvx].getSoundOwnerCount() == 0) {
                                    spritesound(hittype[i].lastvx, i);
                                }
                            } else if (!ud.monsters_off && sc.getFloorpal() == 0 && (sc.getFloorstat() & 1) != 0 && rnd(8)) {
                                int p = findplayer(s);
                                x = player_dist;
                                if (x < 20480) {
                                    int oldAng = s.getAng();
                                    s.setAng(EngineUtils.getAngle(s.getX() - ps[p].posx, s.getY() - ps[p].posy));
                                    shoot(i, RPG);
                                    s.setAng(oldAng);
                                }
                            }
                        }

                        if (s.getXvel() <= 64 && (sc.getFloorstat() & 1) == 0 && (sc.getCeilingstat() & 1) == 0) {
                            stopsound(hittype[i].lastvx, i);
                        }

                        if ((sc.getFloorz() - sc.getCeilingz()) < (108 << 8)) {
                            if (!ud.clipping && s.getXvel() >= 192) {
                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    Sprite psp = boardService.getSprite(ps[p].i);
                                    if (psp != null && psp.getExtra() > 0) {
                                        int k = ps[p].cursectnum;
                                        k = engine.updatesector(ps[p].posx, ps[p].posy, k);
                                        if ((k == -1 && !ud.clipping) || (k == s.getSectnum() && ps[p].cursectnum != s.getSectnum())) {
                                            ps[p].posx = s.getX();
                                            ps[p].posy = s.getY();
                                            ps[p].cursectnum = s.getSectnum();
                                            System.err.println("aa?");
                                            engine.setsprite(ps[p].i, s.getX(), s.getY(), s.getZ());
                                            quickkill(ps[p]);
                                        }
                                    }
                                }
                            }
                        }

                        int m = (s.getXvel() * EngineUtils.cos(s.getAng() & 2047)) >> 14;
                        x = (s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14;

                        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                            Sector psec = boardService.getSector(ps[p].cursectnum);
                            if (psec != null && psec.getLotag() != 2) {
                                if (po[p].os == s.getSectnum()) {
                                    po[p].ox += m;
                                    po[p].oy += x;
                                }

                                Sprite psp = boardService.getSprite(ps[p].i);
                                if (psp != null && s.getSectnum() == psp.getSectnum()) {
                                    Point out = EngineUtils.rotatepoint(s.getX(), s.getY(), ps[p].posx, ps[p].posy, (short) q);
                                    ps[p].posx = out.getX();
                                    ps[p].posy = out.getY();

                                    ps[p].posx += m;
                                    ps[p].posy += x;

                                    ps[p].bobposx += m;
                                    ps[p].bobposy += x;

                                    ps[p].ang += q;
                                    ps[p].ang = BClampAngle(ps[p].ang);

                                    if (psp.getExtra() <= 0) {
                                        ps[p].getPlayerSprite().setX(ps[p].posx);
                                        ps[p].getPlayerSprite().setY(ps[p].posy);
                                    }
                                }
                            }
                        }

                        ListNode<Sprite> node = boardService.getSectNode(s.getSectnum());
                        while (node != null) {
                            Sprite sp = node.get();
                            Sector sec = boardService.getSector(sp.getSectnum());
                            if (sec != null && sp.getStatnum() != 10 && sec.getLotag() != 2 && sp.getPicnum() != SECTOREFFECTOR && sp.getPicnum() != LOCATORS) {
                                Point rp = EngineUtils.rotatepoint(s.getX(), s.getY(), sp.getX(), sp.getY(), (short) q);

                                game.pInt.setsprinterpolate(node.getIndex(), sp);

                                sp.setX(rp.getX());
                                sp.setY(rp.getY());

                                sp.setX(sp.getX() + m);
                                sp.setY(sp.getY() + x);

                                sp.setAng(sp.getAng() + q);
                            }
                            node = node.getNext();
                        }

                        ms(i);
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                        if ((sc.getFloorz() - sc.getCeilingz()) < (108 << 8)) {
                            if (!ud.clipping && s.getXvel() >= 192) {
                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    Sprite psp = boardService.getSprite(ps[p].i);

                                    if (psp != null && psp.getExtra() > 0) {
                                        int k = ps[p].cursectnum;
                                        k = engine.updatesector(ps[p].posx, ps[p].posy, k);
                                        if ((k == -1 && !ud.clipping) || (k == s.getSectnum() && ps[p].cursectnum != s.getSectnum())) {
                                            ps[p].oposx = ps[p].posx = s.getX();
                                            ps[p].oposy = ps[p].posy = s.getY();
                                            ps[p].cursectnum = s.getSectnum();

                                            System.err.println("aaa?");

                                            engine.setsprite(ps[p].i, s.getX(), s.getY(), s.getZ());
                                            quickkill(ps[p]);
                                        }
                                    }
                                }
                            }

                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (spo != null) {
                                node = boardService.getSectNode(spo.getSectnum());
                                while (node != null) {
                                    ListNode<Sprite> next = node.getNext();
                                    Sprite sp = node.get();
                                    if (sp.getStatnum() == 1 && badguy(sp) && sp.getPicnum() != SECTOREFFECTOR && sp.getPicnum() != LOCATORS) {
                                        int k = sp.getSectnum();
                                        k = engine.updatesector(sp.getX(), sp.getY(), k);
                                        if (sp.getExtra() >= 0 && k == s.getSectnum()) {
                                            gutsdir(sp, JIBS6, 72, myconnectindex);
                                            spritesound(SQUISHED, i);
                                            engine.deletesprite(node.getIndex());
                                        }
                                    }
                                    node = next;
                                }
                            }
                        }
                    }

                    break;
                }
                case 30: {
                    Sprite spo = boardService.getSprite(s.getOwner());
                    if (spo == null) {
                        t[3] ^= 1;
                        s.setOwner((short) LocateTheLocator(t[3], t[0]));
                    } else {
                        if (t[4] == 1) // Starting to go
                        {
                            if (ldist(spo, s) < (2048 - 128)) {
                                t[4] = 2;
                            } else {
                                if (s.getXvel() == 0) {
                                    operateactivators(s.getHitag() + (t[3] == 0 ? 1 : 0), -1);
                                }
                                if (s.getXvel() < 256) {
                                    s.setXvel(s.getXvel() + 16);
                                }
                            }
                        }
                        if (t[4] == 2) {
                            int l = FindDistance2D(spo.getX() - s.getX(), spo.getY() - s.getY());
                            if (l <= 128) {
                                s.setXvel(0);
                            }

                            if (s.getXvel() > 0) {
                                s.setXvel(s.getXvel() - 16);
                            } else {
                                s.setXvel(0);
                                operateactivators(s.getHitag() + (short) t[3], -1);
                                s.setOwner(-1);
                                s.setAng(s.getAng() + 1024);
                                t[4] = 0;
                                operateforcefields(i, s.getHitag());

                                ListNode<Sprite> node = boardService.getSectNode(s.getSectnum());
                                while (node != null) {
                                    Sprite sp = node.get();
                                    if (sp.getPicnum() != SECTOREFFECTOR && sp.getPicnum() != LOCATORS) {

                                        game.pInt.setsprinterpolate(node.getIndex(), sp);
                                    }
                                    node = node.getNext();
                                }

                            }
                        }
                    }

                    if (s.getXvel() != 0) {
                        int l = (s.getXvel() * EngineUtils.cos((s.getAng()) & 2047)) >> 14;
                        int x = (s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14;

                        if (sc != null && (sc.getFloorz() - sc.getCeilingz()) < (108 << 8)) {
                            if (!ud.clipping) {
                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    Sprite psp = boardService.getSprite(ps[p].i);

                                    if (psp != null && psp.getExtra() > 0) {
                                        int k = ps[p].cursectnum;
                                        k = engine.updatesector(ps[p].posx, ps[p].posy, k);
                                        if ((k == -1 && !ud.clipping) || (k == s.getSectnum() && ps[p].cursectnum != s.getSectnum())) {
                                            ps[p].posx = s.getX();
                                            ps[p].posy = s.getY();
                                            ps[p].cursectnum = s.getSectnum();

                                            System.err.println("aaaa?");

                                            engine.setsprite(ps[p].i, s.getX(), s.getY(), s.getZ());
                                            quickkill(ps[p]);
                                        }
                                    }
                                }
                            }
                        }

                        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                            Sprite psp = boardService.getSprite(ps[p].i);
                            if (psp != null && psp.getSectnum() == s.getSectnum()) {
                                ps[p].posx += l;
                                ps[p].posy += x;

                                ps[p].bobposx += l;
                                ps[p].bobposy += x;
                            }

                            if (po[p].os == s.getSectnum()) {
                                po[p].ox += l;
                                po[p].oy += x;
                            }
                        }

                        ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                        while (nj != null) {
                            Sprite sp = nj.get();
                            if (sp.getPicnum() != SECTOREFFECTOR && sp.getPicnum() != LOCATORS) {
                                game.pInt.setsprinterpolate(nj.getIndex(), sp);

                                sp.setX(sp.getX() + l);
                                sp.setY(sp.getY() + x);
                            }
                            nj = nj.getNext();
                        }

                        ms(i);
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                        if (sc != null && (sc.getFloorz() - sc.getCeilingz()) < (108 << 8)) {
                            if (!ud.clipping) {
                                for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                    Sprite psp = boardService.getSprite(ps[p].i);

                                    if (psp != null && psp.getExtra() > 0) {
                                        int k = ps[p].cursectnum;
                                        k = engine.updatesector(ps[p].posx, ps[p].posy, k);
                                        if ((k == -1 && !ud.clipping) || (k == s.getSectnum() && ps[p].cursectnum != s.getSectnum())) {
                                            ps[p].posx = s.getX();
                                            ps[p].posy = s.getY();

                                            ps[p].oposx = ps[p].posx;
                                            ps[p].oposy = ps[p].posy;

                                            ps[p].cursectnum = s.getSectnum();

                                            System.err.println("aaaaa?");

                                            engine.setsprite(ps[p].i, s.getX(), s.getY(), s.getZ());
                                            quickkill(ps[p]);
                                        }
                                    }
                                }
                            }

                            spo = boardService.getSprite(s.getOwner());
                            if (spo != null) {
                                nj = boardService.getSectNode(spo.getSectnum());
                                while (nj != null) {
                                    ListNode<Sprite> nl = nj.getNext();
                                    Sprite sp = nj.get();
                                    if (sp.getStatnum() == 1 && badguy(sp) && sp.getPicnum() != SECTOREFFECTOR && sp.getPicnum() != LOCATORS) {
                                        // if(sp.sectnum != s.sectnum)
                                        {
                                            int k = sp.getSectnum();
                                            k = engine.updatesector(sp.getX(), sp.getY(), k);
                                            if (sp.getExtra() >= 0 && k == s.getSectnum()) {
                                                gutsdir(sp, JIBS6, 24, myconnectindex);
                                                spritesound(SQUISHED, nj.getIndex());
                                                engine.deletesprite(nj.getIndex());
                                            }
                                        }

                                    }
                                    nj = nl;
                                }
                            }
                        }
                    }

                    break;
                }
                case 2: // Quakes
                    if (t[4] > 0 && t[0] == 0) {
                        if (t[4] < sh) {
                            t[4]++;
                        } else {
                            t[0] = 1;
                        }
                    }

                    if (t[0] > 0) {
                        t[0]++;

                        s.setXvel(3);

                        if (t[0] > 96) {
                            t[0] = -1; // Stop the quake
                            t[4] = -1;
                            engine.deletesprite(i);
                            continue;
                        } else {
                            if ((t[0] & 31) == 8) {
                                earthquaketime = 48;
                                spritesound(EARTHQUAKE, ps[screenpeek].i);
                            }

                            if (sc != null) {
                                if (klabs(sc.getFloorheinum() - t[5]) < 8) {
                                    sc.setFloorheinum((short) t[5]);
                                } else {
                                    sc.setFloorheinum(sc.getFloorheinum() + (sgn(t[5] - sc.getFloorheinum()) << 4));
                                }
                            }
                        }

                        int m = (s.getXvel() * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 14;
                        int x = (s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14;

                        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                            if (ps[p].cursectnum == s.getSectnum() && ps[p].on_ground) {
                                ps[p].posx += m;
                                ps[p].posy += x;

                                ps[p].bobposx += m;
                                ps[p].bobposy += x;
                            }
                        }

                        ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                        while (nj != null) {
                            ListNode<Sprite> nextj = nj.getNext();
                            Sprite sp = nj.get();
                            int j = nj.getIndex();
                            if (sp.getPicnum() != SECTOREFFECTOR) {

                                game.pInt.setsprinterpolate(j, sp);

                                sp.setX(sp.getX() + m);
                                sp.setY(sp.getY() + x);
                                engine.setsprite((short) j, sp.getX(), sp.getY(), sp.getZ());
                            }
                            nj = nextj;
                        }
                        ms(i);
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    }
                    break;

                // Flashing sector lights after reactor EXPLOSION2

                case 3: {

                    if (sc == null || t[4] == 0) {
                        break;
                    }

                    if ((global_random / (sh + 1) & 31) < 4 && t[2] == 0) {
                        sc.setCeilingpal((short) (s.getOwner() >> 8));
                        sc.setFloorpal((short) (s.getOwner() & 0xff));
                        t[0] = s.getShade() + (global_random & 15);
                    } else {
                        sc.setCeilingpal(s.getPal());
                        sc.setFloorpal(s.getPal());
                        t[0] = t[3];
                    }

                    sc.setCeilingshade((byte) t[0]);
                    sc.setFloorshade((byte) t[0]);

                    for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        if (wal.getHitag() != 1) {
                            wal.setShade(t[0]);
                            Wall nw = boardService.getWall(wal.getNextwall());
                            if ((wal.getCstat() & 2) != 0 && nw != null) {
                                nw.setShade(wal.getShade());
                            }
                        }
                    }

                    break;
                }
                case 4: {
                    if (sc == null) {
                        break;
                    }

                    int j = 0;
                    if ((global_random / (sh + 1) & 31) < 4) {
                        t[1] = s.getShade() + (global_random & 15);// Got really bright
                        t[0] = s.getShade() + (global_random & 15);
                        sc.setCeilingpal((short) (s.getOwner() >> 8));
                        sc.setFloorpal((short) (s.getOwner() & 0xff));
                        j = 1;
                    } else {
                        t[1] = t[2];
                        t[0] = t[3];

                        sc.setCeilingpal(s.getPal());
                        sc.setFloorpal(s.getPal());
                    }

                    sc.setFloorshade((byte) t[1]);
                    sc.setCeilingshade((byte) t[1]);

                    for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        if (j != 0) {
                            wal.setPal((short) (s.getOwner() & 0xff));
                        } else {
                            wal.setPal(s.getPal());
                        }

                        if (wal.getHitag() != 1) {
                            wal.setShade((byte) t[0]);
                            Wall nw = boardService.getWall(wal.getNextwall());
                            if ((wal.getCstat() & 2) != 0 && nw != null) {
                                nw.setShade(wal.getShade());
                            }
                        }
                    }

                    ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                    while (nj != null) {
                        Sprite sp = nj.get();
                        if ((sp.getCstat() & 16) != 0) {
                            if ((sc.getCeilingstat() & 1) != 0) {
                                sp.setShade(sc.getCeilingshade());
                            } else {
                                sp.setShade(sc.getFloorshade());
                            }
                        }

                        nj = nj.getNext();
                    }

                    if (t[4] != 0) {
                        engine.deletesprite(i);
                        continue;
                    }

                    break;
                }
                // BOSS
                case 5: {
                    int p = findplayer(s);
                    int x = player_dist;
                    if (x < 8192) {
                        int olda = s.getAng();
                        s.setAng(EngineUtils.getAngle(s.getX() - ps[p].posx, s.getY() - ps[p].posy));
                        shoot(i, FIRELASER);
                        s.setAng(olda);
                    }

                    if (s.getOwner() == -1) // Start search
                    {
                        int q = 0;
                        t[4] = 0;
                        int l = 0x7fffffff;
                        while (true) // Find the shortest dist
                        {
                            s.setOwner((short) LocateTheLocator((short) t[4], -1)); // t[0] hold sectnum

                            if (s.getOwner() == -1) {
                                break;
                            }

                            Sprite psp = boardService.getSprite(ps[p].i);
                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (psp != null && spo != null) {
                                int m = ldist(psp, spo);
                                if (l > m) {
                                    q = s.getOwner();
                                    l = m;
                                }
                            }

                            t[4]++;
                        }

                        Sprite qs = boardService.getSprite(q);
                        if (qs != null) {
                            s.setOwner(q);
                            s.setZvel((ksgn(qs.getZ() - s.getZ()) << 4));
                        }
                    }

                    if (sc == null) {
                        break;
                    }

                    Sprite spo = boardService.getSprite(s.getOwner());
                    if (spo != null) {
                        if (ldist(spo, s) < 1024) {
                            short ta = s.getAng();
                            s.setAng(EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY()));
                            s.setAng(ta);
                            s.setOwner(-1);
                            continue;

                        } else {
                            s.setXvel(256);
                        }

                        x = EngineUtils.getAngle(spo.getX() - s.getX(), spo.getY() - s.getY());
                        int q = getincangle(s.getAng(), x) >> 3;
                        s.setAng(s.getAng() + q);

                        if (rnd(32)) {
                            t[2] += q;
                            sc.setCeilingshade(127);
                        } else {
                            t[2] += getincangle(t[2] + 512, EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY())) >> 2;
                            sc.setCeilingshade(0);
                        }
                    }

                    if (ifhitbyweapon(i) >= 0) {
                        t[3]++;
                        if (t[3] == 5) {
                            s.setZvel(s.getZvel() + 1024);
                            FTA(7, ps[myconnectindex]);
                        }
                    }

                    s.setZ(s.getZ() + s.getZvel());
                    game.pInt.setceilinterpolate(s.getSectnum(), sc);
                    sc.setCeilingz(sc.getCeilingz() + s.getZvel());
                    Sector tsec = boardService.getSector(t[0]);
                    if (tsec != null) {
                        game.pInt.setceilinterpolate(t[0], tsec);
                        tsec.setCeilingz(tsec.getCeilingz() + s.getZvel());
                    }
                    ms(i);
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    break;
                }
                case 8:
                case 9: {

                    // work only if its moving

                    int j;

                    if (hittype[i].temp_data[4] != 0) {
                        hittype[i].temp_data[4]++;
                        if (hittype[i].temp_data[4] > 8) {
                            engine.deletesprite(i);
                            continue;
                        }
                        j = 1;
                    } else {
                        j = getanimationgoal(sc, CEILZ);
                    }

                    if (j >= 0) {
                        int x;
                        if ((sc != null && (sc.getLotag() & 0x8000) != 0) || hittype[i].temp_data[4] != 0) {
                            x = -t[3];
                        } else {
                            x = t[3];
                        }

                        if (st == 9) {
                            x = -x;
                        }

                        ListNode<Sprite> node = boardService.getStatNode(3);
                        while (node != null) {
                            Sprite sp = node.get();
                            j = node.getIndex();
                            if (((sp.getLotag()) == st) && (sp.getHitag()) == sh) {
                                final int sn = sp.getSectnum();
                                final Sector sec = boardService.getSector(sn);
                                int m = sp.getShade();
                                if (sec != null) {
                                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                                        Wall wal = wn.get();

                                        if (wal.getHitag() != 1) {
                                            wal.setShade(wal.getShade() + x);

                                            if (wal.getShade() < m) {
                                                wal.setShade((byte) m);
                                            } else if (wal.getShade() > hittype[j].temp_data[2]) {
                                                wal.setShade((byte) hittype[j].temp_data[2]);
                                            }

                                            Wall nwal = boardService.getWall(wal.getNextwall());
                                            if (nwal != null) {
                                                if (nwal.getHitag() != 1) {
                                                    nwal.setShade(wal.getShade());
                                                }
                                            }
                                        }
                                    }

                                    sec.setFloorshade(sec.getFloorshade() + x);
                                    sec.setCeilingshade(sec.getCeilingshade() + x);

                                    if (sec.getFloorshade() < m) {
                                        sec.setFloorshade((byte) m);
                                    } else if (sec.getFloorshade() > hittype[j].temp_data[0]) {
                                        sec.setFloorshade((byte) hittype[j].temp_data[0]);
                                    }

                                    if (sec.getCeilingshade() < m) {
                                        sec.setCeilingshade((byte) m);
                                    } else if (sec.getCeilingshade() > hittype[j].temp_data[1]) {
                                        sec.setCeilingshade((byte) hittype[j].temp_data[1]);
                                    }
                                }
                            }
                            node = node.getNext();
                        }
                    }
                    break;
                }
                case 10: {
                    if (sc == null) {
                        break;
                    }

                    if ((sc.getLotag() & 0xff) == 27 || (sc.getFloorz() > sc.getCeilingz() && (sc.getLotag() & 0xff) != 23) || sc.getLotag() == (short) 32791) {
                        int j = 1;

                        if ((sc.getLotag() & 0xff) != 27) {
                            for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                                if (sc.getLotag() != 30 && sc.getLotag() != 31 && sc.getLotag() != 0) {
                                    Sprite psp = boardService.getSprite(ps[p].i);
                                    if (psp != null && s.getSectnum() == psp.getSectnum()) {
                                        j = 0;
                                    }
                                }
                            }
                        }

                        if (j == 1) {
                            if (t[0] > sh) {
                                switch (sc.getLotag()) {
                                    case 20:
                                    case 21:
                                    case 22:
                                    case 26:
                                        if (getanimationgoal(sc, CEILZ) >= 0) {
                                            break;
                                        }
                                    default:
                                        activatebysector(s.getSectnum(), i);
                                        t[0] = 0;
                                        break;
                                }
                            } else {
                                t[0]++;
                            }
                        }
                    } else {
                        t[0] = 0;
                    }
                    break;
                }
                case 11: // Swingdoor
                {
                    if (t[5] > 0) {
                        t[5]--;
                        break;
                    }

                    if (sc != null && t[4] != 0) {
                        for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                            int j = wn.getIndex();
                            ListNode<Sprite> node = boardService.getStatNode(1);
                            while (node != null) {
                                Sprite sp = node.get();
                                if (sp.getExtra() > 0 && badguy(sp) && engine.clipinsidebox(sp.getX(), sp.getY(), j, 256) == 1) {
                                    continue BOLT;
                                }
                                node = node.getNext();
                            }

                            node = boardService.getStatNode(10);
                            while (node != null) {
                                Sprite sp = node.get();
                                if (sp.getOwner() >= 0 && engine.clipinsidebox(sp.getX(), sp.getY(), j, 144) == 1) {
                                    t[5] = 8; // Delay
                                    short k = (short) ((s.getYvel() >> 3) * t[3]);
                                    t[2] -= k;
                                    t[4] -= k;
                                    ms(i);
                                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                                    continue BOLT;
                                }
                                node = node.getNext();
                            }
                        }

                        short k = (short) ((s.getYvel() >> 3) * t[3]);
                        t[2] += k;
                        t[4] += k;
                        ms(i);
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                        if (t[4] <= -511 || t[4] >= 512) {
                            t[4] = 0;
                            t[2] &= 0xffffff00;
                            ms(i);
                            engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                            break;
                        }
                    }
                    break;
                }
                case 12:
                    if (sc == null) {
                        break;
                    }

                    if (t[0] == 3 || t[3] == 1) // Lights going off
                    {
                        sc.setFloorpal(0);
                        sc.setCeilingpal(0);

                        for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                            Wall wal = wn.get();
                            if (wal.getHitag() != 1) {
                                wal.setShade((byte) t[1]);
                                wal.setPal(0);
                            }
                        }

                        sc.setFloorshade((byte) t[1]);
                        sc.setCeilingshade((byte) t[2]);
                        t[0] = 0;

                        ListNode<Sprite> node = boardService.getSectNode(s.getSectnum());
                        while (node != null) {
                            Sprite sp = node.get();
                            if ((sp.getCstat() & 16) != 0) {
                                if ((sc.getCeilingstat() & 1) != 0) {
                                    sp.setShade(sc.getCeilingshade());
                                } else {
                                    sp.setShade(sc.getFloorshade());
                                }
                            }
                            node = node.getNext();
                        }

                        if (t[3] == 1) {
                            engine.deletesprite(i);
                            continue;
                        }
                    }
                    if (t[0] == 1) // Lights flickering on
                    {
                        if (sc.getFloorshade() > s.getShade()) {
                            sc.setFloorpal(s.getPal());
                            sc.setCeilingpal(s.getPal());

                            sc.setFloorshade(sc.getFloorshade() - 2);
                            sc.setCeilingshade(sc.getCeilingshade() - 2);

                            for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                if (wal.getHitag() != 1) {
                                    wal.setPal(s.getPal());
                                    wal.setShade(wal.getShade() - 2);
                                }
                            }
                        } else {
                            t[0] = 2;
                        }

                        ListNode<Sprite> node = boardService.getSectNode(s.getSectnum());
                        while (node != null) {
                            Sprite sp = node.get();
                            if ((sp.getCstat() & 16) != 0) {
                                if ((sc.getCeilingstat() & 1) != 0) {
                                    sp.setShade(sc.getCeilingshade());
                                } else {
                                    sp.setShade(sc.getFloorshade());
                                }
                            }
                            node = node.getNext();
                        }
                    }
                    break;

                case 13:
                    if (sc != null && t[2] != 0) {
                        int j = (s.getYvel() << 5) | 1;

                        if (s.getAng() == 512) {
                            if (s.getOwner() != 0) {
                                game.pInt.setceilinterpolate(s.getSectnum(), sc);
                                if (klabs(t[0] - sc.getCeilingz()) >= j) {
                                    sc.setCeilingz(sc.getCeilingz() + sgn(t[0] - sc.getCeilingz()) * j);
                                } else {
                                    sc.setCeilingz(t[0]);
                                }
                            } else {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                if (klabs(t[1] - sc.getFloorz()) >= j) {
                                    sc.setFloorz(sc.getFloorz() + sgn(t[1] - sc.getFloorz()) * j);
                                } else {
                                    sc.setFloorz(t[1]);
                                }
                            }
                        } else {
                            game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                            game.pInt.setceilinterpolate(s.getSectnum(), sc);
                            if (klabs(t[1] - sc.getFloorz()) >= j) {
                                sc.setFloorz(sc.getFloorz() + sgn(t[1] - sc.getFloorz()) * j);
                            } else {
                                sc.setFloorz(t[1]);
                            }
                            if (klabs(t[0] - sc.getCeilingz()) >= j) {
                                sc.setCeilingz(sc.getCeilingz() + sgn(t[0] - sc.getCeilingz()) * j);
                            }
                            sc.setCeilingz(t[0]);
                        }

                        if (t[3] == 1) {
                            // Change the shades

                            t[3]++;
                            sc.setCeilingstat(sc.getCeilingstat() ^ 1);

                            if (s.getAng() == 512) {
                                for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                                    Wall wal = wn.get();
                                    wal.setShade(s.getShade());
                                }

                                sc.setFloorshade(s.getShade());

                                Sector sec = boardService.getSector(ps[connecthead].one_parallax_sectnum);
                                if (sec != null) {
                                    sc.setCeilingpicnum(sec.getCeilingpicnum());
                                    sc.setCeilingshade(sec.getCeilingshade());
                                }
                            }
                        }
                        t[2]++;
                        if (t[2] > 256) {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    if (t[2] == 4 && s.getAng() != 512) {
                        for (int x = 0; x < 7; x++) {
                            RANDOMSCRAP(s, i);
                        }
                    }
                    break;

                case 15:

                    if (t[4] != 0) {
                        s.setXvel(16);

                        if (t[4] == 1) // Opening
                        {
                            if (t[3] >= (s.getYvel() >> 3)) {
                                t[4] = 0; // Turn off the sliders
                                callsound(s.getSectnum(), i);
                                break;
                            }
                            t[3]++;
                        } else if (t[4] == 2) {
                            if (t[3] < 1) {
                                t[4] = 0;
                                callsound(s.getSectnum(), i);
                                break;
                            }
                            t[3]--;
                        }

                        ms(i);
                        engine.setsprite(i, s.getX(), s.getY(), s.getZ());
                    }
                    break;

                case 16: // Reactor
                    if (sc == null) {
                        break;
                    }

                    t[2] += 32;
                    if (sc.getFloorz() < sc.getCeilingz()) {
                        s.setShade(0);
                    } else if (sc.getCeilingz() < t[3]) {

                        // The following code check to see if
                        // there is any other sprites in the sector.
                        // If there isn't, then kill this sectoreffector
                        // itself.....

                        ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                        while (nj != null) {
                            Sprite sp = nj.get();
                            if (sp.getPicnum() == REACTOR || sp.getPicnum() == REACTOR2) {
                                break;
                            }
                            nj = nj.getNext();
                        }
                        if (nj == null) {
                            engine.deletesprite(i);
                            continue;
                        } else {
                            s.setShade(1);
                        }
                    }

                    game.pInt.setceilinterpolate(s.getSectnum(), sc);
                    if (s.getShade() != 0) {
                        sc.setCeilingz(sc.getCeilingz() + 1024);
                    } else {
                        sc.setCeilingz(sc.getCeilingz() - 512);
                    }

                    ms(i);
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                    break;

                case 17: {
                    if (sc == null) {
                        break;
                    }

                    int q = t[0] * (s.getYvel() << 2);

                    game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                    game.pInt.setceilinterpolate(s.getSectnum(), sc);
                    sc.setCeilingz(sc.getCeilingz() + q);
                    sc.setFloorz(sc.getFloorz() + q);

                    ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                    while (nj != null) {
                        Sprite sp = nj.get();
                        int j = nj.getIndex();
                        if (sp.getStatnum() == 10 && sp.getOwner() >= 0) {
                            int p = sp.getYvel();
                            ps[p].posz += q;
                            ps[p].truefz += q;
                            ps[p].truecz += q;
                            if (numplayers > 1 || mFakeMultiplayer) {
                                ps[p].oposz = ps[p].posz;
                            }
                        }
                        if (sp.getStatnum() != 3) {
                            game.pInt.setsprinterpolate(j, sp);

                            sp.setZ(sp.getZ() + q);
                        }

                        hittype[j].floorz = sc.getFloorz();
                        hittype[j].ceilingz = sc.getCeilingz();

                        nj = nj.getNext();
                    }

                    if (t[0] != 0) // If in motion
                    {
                        if (klabs(sc.getFloorz() - t[2]) <= s.getYvel()) {
                            activatewarpelevators(i, 0);
                            break;
                        }

                        if (t[0] == -1) {
                            if (sc.getFloorz() > t[3]) {
                                break;
                            }
                        } else if (sc.getCeilingz() < t[4]) {
                            break;
                        }

                        if (t[1] == 0) {
                            break;
                        }
                        t[1] = 0;

                        int j = -1;
                        ListNode<Sprite> node = boardService.getStatNode(3);
                        while (node != null) {
                            Sprite sp = node.get();
                            j = node.getIndex();
                            if (i != j && (sp.getLotag()) == 17) {
                                Sector sec = boardService.getSector(sp.getSectnum());
                                if (sec != null && (sc.getHitag() - t[0]) == (sec.getHitag()) && sh == (sp.getHitag())) {
                                    break;
                                }
                            }
                            node = node.getNext();
                        }

                        if (node == null) {
                            break;
                        }

                        Sprite sprite = boardService.getSprite(j);
                        if (sprite == null) {
                            break;
                        }

                        Sector jsec = boardService.getSector(sprite.getSectnum());
                        if (jsec == null) {
                            break;
                        }

                        ListNode<Sprite> nk = boardService.getSectNode(s.getSectnum());
                        while (nk != null) {
                            Sprite sp = nk.get();
                            ListNode<Sprite> nextk = nk.getNext();
                            int k = nk.getIndex();

                            if (sp.getStatnum() == 10 && sp.getOwner() >= 0) {
                                int p = sp.getYvel();

                                ps[p].posx += sprite.getX() - s.getX();
                                ps[p].posy += sprite.getY() - s.getY();
                                ps[p].posz = jsec.getFloorz() - (sc.getFloorz() - ps[p].posz);

                                hittype[k].floorz = jsec.getFloorz();
                                hittype[k].ceilingz = jsec.getCeilingz();

                                ps[p].bobposx = ps[p].oposx = ps[p].posx;
                                ps[p].bobposy = ps[p].oposy = ps[p].posy;
                                ps[p].oposz = ps[p].posz;

                                ps[p].truefz = hittype[k].floorz;
                                ps[p].truecz = hittype[k].ceilingz;
                                ps[p].bobcounter = 0;

                                game.pInt.setsprinterpolate(ps[p].i, boardService.getSprite(ps[p].i));
                                engine.changespritesect(k, sprite.getSectnum());
                                ps[p].cursectnum = sprite.getSectnum();

                                ps[p].UpdatePlayerLoc();
                            } else if (sp.getStatnum() != 3) {

                                game.pInt.setsprinterpolate(k, sp);

                                sp.setX(sp.getX() + sprite.getX() - s.getX());
                                sp.setY(sp.getY() + sprite.getY() - s.getY());
                                sp.setZ(jsec.getFloorz() - (sc.getFloorz() - sp.getZ()));

                                engine.changespritesect(k, sprite.getSectnum());
                                engine.setsprite(k, sp.getX(), sp.getY(), sp.getZ());

                                hittype[k].floorz = jsec.getFloorz();
                                hittype[k].ceilingz = jsec.getCeilingz();

                            }
                            nk = nextk;
                        }
                    }
                    break;
                }
                case 18:
                    if (sc != null && t[0] != 0) {
                        if (s.getPal() != 0) {
                            if (s.getAng() == 512) {
                                game.pInt.setceilinterpolate(s.getSectnum(), sc);
                                sc.setCeilingz(sc.getCeilingz() - sc.getExtra());
                                if (sc.getCeilingz() <= t[1]) {
                                    sc.setCeilingz(t[1]);
                                    engine.deletesprite(i);
                                    continue;
                                }
                            } else {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() + sc.getExtra());
                                ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                                while (nj != null) {
                                    Sprite sp = nj.get();
                                    int j = nj.getIndex();
                                    if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                        if (ps[sp.getYvel()].on_ground) {
                                            ps[sp.getYvel()].posz += sc.getExtra();
                                        }
                                    }
                                    if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
//                                    	hittype[j].bposz = sp.z;

                                        game.pInt.setsprinterpolate(j, sp);
                                        sp.setZ(sp.getZ() + sc.getExtra());
                                        hittype[j].floorz = sc.getFloorz();
                                    }
                                    nj = nj.getNext();
                                }
                                if (sc.getFloorz() >= t[1]) {
                                    sc.setFloorz(t[1]);
                                    engine.deletesprite(i);
                                    continue;
                                }
                            }
                        } else {
                            if (s.getAng() == 512) {
                                game.pInt.setceilinterpolate(s.getSectnum(), sc);
                                sc.setCeilingz(sc.getCeilingz() + sc.getExtra());
                                if (sc.getCeilingz() >= s.getZ()) {
                                    sc.setCeilingz(s.getZ());
                                    engine.deletesprite(i);
                                    continue;
                                }
                            } else {
                                game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                                sc.setFloorz(sc.getFloorz() - sc.getExtra());
                                ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                                while (nj != null) {
                                    Sprite sp = nj.get();
                                    int j = nj.getIndex();
                                    if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                        if (ps[sp.getYvel()].on_ground) {
                                            ps[sp.getYvel()].posz -= sc.getExtra();
                                        }
                                    }
                                    if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
//                                    	hittype[j].bposz = sp.z;

                                        game.pInt.setsprinterpolate(j, sp);
                                        sp.setZ(sp.getZ() - sc.getExtra());
                                        hittype[j].floorz = sc.getFloorz();
                                    }
                                    nj = nj.getNext();
                                }
                                if (sc.getFloorz() <= s.getZ()) {
                                    sc.setFloorz(s.getZ());
                                    engine.deletesprite(i);
                                    continue;
                                }
                            }
                        }

                        t[2]++;
                        if (t[2] >= s.getHitag()) {
                            t[2] = 0;
                            t[0] = 0;
                        }
                    }
                    break;

                case 19: // Battlestar galactia shields

                    if (sc != null && t[0] != 0) {
                        if (t[0] == 1) {
                            t[0]++;

                            for (ListNode<Wall> wn = sc.getWallNode(); wn != null; wn = wn.getNext()) {
                                Wall wal = wn.get();
                                if (wal.getOverpicnum() == BIGFORCE) {
                                    wal.setCstat(wal.getCstat() & (128 + 32 + 8 + 4 + 2));
                                    wal.setOverpicnum(0);
                                    Wall nwal = boardService.getWall(wal.getNextwall());
                                    if (nwal != null) {
                                        nwal.setOverpicnum(0);
                                        nwal.setCstat(nwal.getCstat() & (128 + 32 + 8 + 4 + 2));
                                    }
                                }
                            }
                        }

                        game.pInt.setceilinterpolate(s.getSectnum(), sc);
                        if (sc.getCeilingz() < sc.getFloorz()) {
                            sc.setCeilingz(sc.getCeilingz() + s.getYvel());
                        } else {
                            sc.setCeilingz(sc.getFloorz());

                            ListNode<Sprite> node = boardService.getStatNode(3);
                            while (node != null) {
                                Sprite sp = node.get();
                                if (sp.getLotag() == 0 && sp.getHitag() == sh) {
                                    Sprite spo = boardService.getSprite(sp.getOwner());
                                    if (spo != null) {
                                        int q = spo.getSectnum();
                                        Sector qsec = boardService.getSector(q);
                                        Sector sec = boardService.getSector(sp.getSectnum());
                                        if (sec != null && qsec != null) {
                                            int pal = qsec.getFloorpal();
                                            sec.setCeilingpal(pal);
                                            sec.setFloorpal(pal);
                                            int shade = qsec.getFloorshade();
                                            sec.setCeilingshade(shade);
                                            sec.setFloorshade(shade);

                                            hittype[sp.getOwner()].temp_data[0] = 2;
                                        }
                                    }
                                }
                                node = node.getNext();
                            }
                            engine.deletesprite(i);
                            continue;
                        }
                    } else // Not hit yet
                    {
                        if (ifhitsectors(s.getSectnum()) >= 0) {
                            FTA(8, ps[myconnectindex]);

                            ListNode<Sprite> node = boardService.getStatNode(3);
                            while (node != null) {
                                int l = node.getIndex();
                                Sprite sp = node.get();
                                int x = sp.getLotag() & 0x7fff;
                                switch (x) {
                                    case 0:
                                        if (sp.getHitag() == sh) {
                                            int q = sp.getSectnum();

                                            Sector sec = boardService.getSector(q);
                                            Sprite spo = boardService.getSprite(sp.getOwner());
                                            if (sec != null && spo != null) {
                                                int pal = spo.getPal();
                                                sec.setCeilingpal(pal);
                                                sec.setFloorpal(pal);
                                                int shade = spo.getShade();
                                                sec.setCeilingshade(shade);
                                                sec.setFloorshade(shade);
                                            }
                                        }
                                        break;
                                    case 1:
                                    case 12:
                                    case 19:
                                        if (sh == sp.getHitag()) {
                                            if (hittype[l].temp_data[0] == 0) {
                                                hittype[l].temp_data[0] = 1; // Shut them all on
                                                sp.setOwner(i);
                                            }
                                        }

                                        break;
                                }
                                node = node.getNext();
                            }
                        }
                    }

                    break;

                case 20: // Extend-o-bridge
                {
                    if (t[0] == 0) {
                        break;
                    }
                    if (t[0] == 1) {
                        s.setXvel(8);
                    } else {
                        s.setXvel(-8);
                    }

                    if (s.getXvel() != 0) // Moving
                    {
                        int x = (s.getXvel() * EngineUtils.cos(s.getAng() & 2047)) >> 14;
                        int l = (s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14;

                        t[3] += s.getXvel();

                        s.setX(s.getX() + x);
                        s.setY(s.getY() + l);

                        if (t[3] <= 0 || (t[3] >> 6) >= (s.getYvel() >> 6)) {
                            s.setX(s.getX() - x);
                            s.setY(s.getY() - l);
                            t[0] = 0;
                            callsound(s.getSectnum(), i);
                            break;
                        }

                        ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                        while (nj != null) {
                            Sprite sp = nj.get();
                            ListNode<Sprite> nextj = nj.getNext();
                            int j = nj.getIndex();
                            if (sp.getStatnum() != 3 && sp.getZvel() == 0) {

                                game.pInt.setsprinterpolate(j, sp);

                                sp.setX(sp.getX() + x);
                                sp.setY(sp.getY() + l);
                                engine.setsprite(j, sp.getX(), sp.getY(), sp.getZ());
                                Sector sec = boardService.getSector(sp.getSectnum());
                                if (sec != null && (sec.getFloorstat() & 2) != 0) {
                                    if (sp.getStatnum() == 2) {
                                        makeitfall(currentGame.getCON(), j);
                                    }
                                }
                            }
                            nj = nextj;
                        }

                        Wall w1 = boardService.getWall(t[1]);
                        Wall w2 = boardService.getWall(t[2]);

                        if (w1 != null) {
                            engine.dragpoint(t[1], w1.getX() + x, w1.getY() + l);
                        }

                        if (w2 != null) {
                            engine.dragpoint(t[2], w2.getX() + x, w2.getY() + l);
                        }

                        for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                            if (ps[p].cursectnum == s.getSectnum() && ps[p].on_ground) {
                                ps[p].posx += x;
                                ps[p].posy += l;

                                engine.setsprite(ps[p].i, ps[p].posx, ps[p].posy, ps[p].posz + PHEIGHT);
                            }
                        }

                        if (sc != null) {
                            sc.setFloorxpanning(sc.getFloorxpanning() - (x >> 3));
                            sc.setFloorypanning(sc.getFloorypanning() - (l >> 3));

                            sc.setCeilingxpanning(sc.getCeilingxpanning() - (x >> 3));
                            sc.setCeilingypanning(sc.getCeilingypanning() - (l >> 3));
                        }
                    }

                    break;
                }
                case 21: { // Cascading effect

                    if (sc == null || t[0] == 0) {
                        break;
                    }

                    int l = sc.getFloorz();
                    if (s.getAng() == 1536) {
                        l = sc.getCeilingz();
                    }

                    if (t[0] == 1) // Decide if the s.sectnum should go up or down
                    {
                        s.setZvel((ksgn(s.getZ() - l) * (s.getYvel() << 4)));
                        t[0]++;
                    }

                    if (sc.getExtra() == 0) {
                        l += s.getZvel();
                        if (s.getAng() == 1536) {
                            game.pInt.setceilinterpolate(s.getSectnum(), sc);
                            sc.setCeilingz(l);
                        } else {
                            game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                            sc.setFloorz(l);
                        }

                        if (klabs(l - s.getZ()) < 1024) {
                            l = s.getZ();
                            if (s.getAng() == 1536) {
                                sc.setCeilingz(l);
                            } else {
                                sc.setFloorz(l);
                            }
                            engine.deletesprite(i);
                            continue;
                        }
                    } else {
                        sc.setExtra(sc.getExtra() - 1);
                    }
                    break;
                }
                case 22:

                    if (t[1] != 0) {
                        if (sc != null && getanimationgoal(boardService.getSector(t[0]), CEILZ) >= 0) {
                            game.pInt.setceilinterpolate(s.getSectnum(), sc);
                            sc.setCeilingz(sc.getCeilingz() + sc.getExtra() * 9);
                        } else {
                            t[1] = 0;
                        }
                    }
                    break;

                case 24:
                case 34: {
                    if (t[4] != 0) {
                        break;
                    }

                    int x = (s.getYvel() * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 18;
                    int l = (s.getYvel() * EngineUtils.sin(s.getAng() & 2047)) >> 18;

                    ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                    while (nj != null) {
                        Sprite sp = nj.get();
                        ListNode<Sprite> nextj = nj.getNext();
                        int j = nj.getIndex();
                        if (sp.getZvel() >= 0) {
                            switch (sp.getStatnum()) {
                                case 5:
                                    switch (sp.getPicnum()) {
                                        case BLOODPOOL:
                                        case PUKE:
                                        case FOOTPRINTS:
                                        case FOOTPRINTS2:
                                        case FOOTPRINTS3:
                                        case FOOTPRINTS4:
                                        case BULLETHOLE:
                                        case BLOODSPLAT1:
                                        case BLOODSPLAT2:
                                        case BLOODSPLAT3:
                                        case BLOODSPLAT4:
                                            sp.setXrepeat(0);
                                            sp.setYrepeat(0);
                                            nj = nextj;
                                            continue;
                                        case LASERLINE:
                                            nj = nextj;
                                            continue;
                                    }
                                case 6:
                                    if (sp.getPicnum() == TRIPBOMB) {
                                        break;
                                    }
                                case 1:
                                case 0:
                                    if (sp.getPicnum() == BOLT1 || sp.getPicnum() == BOLT1 + 1 || sp.getPicnum() == BOLT1 + 2 || sp.getPicnum() == BOLT1 + 3 || sp.getPicnum() == SIDEBOLT1 || sp.getPicnum() == SIDEBOLT1 + 1 || sp.getPicnum() == SIDEBOLT1 + 2 || sp.getPicnum() == SIDEBOLT1 + 3 || wallswitchcheck(j)) {
                                        break;
                                    }

                                    if (!(sp.getPicnum() >= CRANE && sp.getPicnum() <= (CRANE + 3))) {
                                        if (sp.getZ() > (hittype[j].floorz - (16 << 8))) {

                                            game.pInt.setsprinterpolate(j, sp);

                                            sp.setX(sp.getX() + (x >> 2));
                                            sp.setY(sp.getY() + (l >> 2));

                                            engine.setsprite(j, sp.getX(), sp.getY(), sp.getZ());

                                            Sector sec = boardService.getSector(sp.getSectnum());
                                            if (sec != null && (sec.getFloorstat() & 2) != 0) {
                                                if (sp.getStatnum() == 2) {
                                                    makeitfall(currentGame.getCON(), j);
                                                }
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                        nj = nextj;
                    }

                    int p = myconnectindex;
                    if (ps[p].cursectnum == s.getSectnum() && ps[p].on_ground) {
                        if (klabs(ps[p].posz - ps[p].truefz) < PHEIGHT + (9 << 8)) {
                            fricxv += x << 3;
                            fricyv += l << 3;
                        }
                    }

                    if (sc != null) {
                        sc.setFloorxpanning(sc.getFloorxpanning() + (s.getYvel() >> 7));
                    }

                    break;
                }
                case 35:
                    if (sc == null) {
                        break;
                    }

                    if (sc.getCeilingz() > s.getZ()) {
                        for (int j = 0; j < 8; j++) {
                            s.setAng(s.getAng() + (engine.krand() & 511));
                            int k = spawn(i, SMALLSMOKE);
                            Sprite sp = boardService.getSprite(k);
                            if (sp != null) {
                                sp.setXvel((96 + (engine.krand() & 127)));
                                ssp(k, CLIPMASK0);
                                engine.setsprite(k, sp.getX(), sp.getY(), sp.getZ());
                                if (rnd(16)) {
                                    spawn(i, EXPLOSION2);
                                }
                            }
                        }
                    }

                    switch (t[0]) {
                        case 0:
                            game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                            game.pInt.setceilinterpolate(s.getSectnum(), sc);
                            sc.setCeilingz(sc.getCeilingz() + s.getYvel());
                            if (sc.getCeilingz() > sc.getFloorz()) {
                                sc.setFloorz(sc.getCeilingz());
                            }
                            if (sc.getCeilingz() > s.getZ() + (32 << 8)) {
                                t[0]++;
                            }
                            break;
                        case 1:
                            game.pInt.setceilinterpolate(s.getSectnum(), sc);
                            sc.setCeilingz(sc.getCeilingz() - (s.getYvel() << 2));
                            if (sc.getCeilingz() < t[4]) {
                                sc.setCeilingz(t[4]);
                                t[0] = 0;
                            }
                            break;
                    }
                    break;

                case 25: // PISTONS

                    if (sc == null || t[4] == 0) {
                        break;
                    }

                    if (sc.getFloorz() <= sc.getCeilingz()) {
                        s.setShade(0);
                    } else if (sc.getCeilingz() <= t[3]) {
                        s.setShade(1);
                    }

                    game.pInt.setceilinterpolate(s.getSectnum(), sc);
                    if (s.getShade() != 0) {
                        sc.setCeilingz(sc.getCeilingz() + (s.getYvel() << 4));
                        if (sc.getCeilingz() > sc.getFloorz()) {
                            sc.setCeilingz(sc.getFloorz());
                        }
                    } else {
                        sc.setCeilingz(sc.getCeilingz() - (s.getYvel() << 4));
                        if (sc.getCeilingz() < t[3]) {
                            sc.setCeilingz(t[3]);
                        }
                    }

                    break;

                case 26: {
                    if (sc == null) {
                        break;
                    }

                    s.setXvel(32);
                    int l = (s.getXvel() * EngineUtils.cos(s.getAng() & 2047)) >> 14;
                    int x = (s.getXvel() * EngineUtils.sin(s.getAng() & 2047)) >> 14;

                    s.setShade(s.getShade() + 1);
                    game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                    if (s.getShade() > 7) {
                        s.setX(t[3]);
                        s.setY(t[4]);
                        sc.setFloorz(sc.getFloorz() - ((s.getZvel() * s.getShade()) - s.getZvel()));
                        s.setShade(0);
                    } else {
                        sc.setFloorz(sc.getFloorz() + s.getZvel());
                    }

                    ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                    while (nj != null) {
                        Sprite sp = nj.get();
                        ListNode<Sprite> nextj = nj.getNext();
                        int j = nj.getIndex();
                        if (sp.getStatnum() != 3 && sp.getStatnum() != 10) {

                            game.pInt.setsprinterpolate(j, sp);

                            sp.setX(sp.getX() + l);
                            sp.setY(sp.getY() + x);

                            sp.setZ(sp.getZ() + s.getZvel());
                            engine.setsprite(j, sp.getX(), sp.getY(), sp.getZ());
                        }
                        nj = nextj;
                    }

                    if (ps[myconnectindex].getPlayerSprite().getSectnum() == s.getSectnum() && ps[myconnectindex].on_ground) {
                        fricxv += l << 5;
                        fricyv += x << 5;
                    }

                    for (int p = connecthead; p >= 0; p = connectpoint2[p]) {
                        if (ps[p].getPlayerSprite().getSectnum() == s.getSectnum() && ps[p].on_ground) {
                            ps[p].posz += s.getZvel();
                        }
                    }

                    ms(i);
                    engine.setsprite(i, s.getX(), s.getY(), s.getZ());

                    break;
                }
                case 27: {

                    if (ud.recstat == DEMOSTAT_NULL) { // demo camera
                        break;
                    }

                    hittype[i].tempang = s.getAng();

                    int p = (short) findplayer(s);
                    int x = player_dist;
                    Sprite psp = boardService.getSprite(ps[p].i);
                    if (psp != null && psp.getExtra() > 0 && myconnectindex == screenpeek) {
                        if (t[0] < 0) {
                            ud.camerasprite = i;
                            t[0]++;
                        } else if (DemoScreen.isDemoPlaying() && ps[p].newowner == -1) {
                            if (engine.cansee(s.getX(), s.getY(), s.getZ(), s.getSectnum(), ps[p].posx, ps[p].posy, ps[p].posz, ps[p].cursectnum)) {
                                if (x < (sh & 0xFFFF)) {
                                    ud.camerasprite = i;
                                    t[0] = 999;
                                    s.setAng(s.getAng() + (getincangle(s.getAng(), EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY())) >> 3));
                                    s.setYvel((short) (100 + ((s.getZ() - ps[p].posz) / 257)));

                                } else if (t[0] == 999) {
                                    if (ud.camerasprite == i) {
                                        t[0] = 0;
                                    } else {
                                        t[0] = -10;
                                    }
                                    ud.camerasprite = i;
                                }
                            } else {
                                s.setAng(EngineUtils.getAngle(ps[p].posx - s.getX(), ps[p].posy - s.getY()));

                                if (t[0] == 999) {
                                    if (ud.camerasprite == i) {
                                        t[0] = 0;
                                    } else {
                                        t[0] = -20;
                                    }
                                    ud.camerasprite = i;
                                }
                            }
                        }
                    }
                    break;
                }
                case 28: {
                    if (t[5] > 0) {
                        t[5]--;
                        break;
                    }

                    if (hittype[i].temp_data[0] == 0) {
                        findplayer(s);
                        int x = player_dist;
                        if (x > 15500) {
                            break;
                        }
                        hittype[i].temp_data[0] = 1;
                        hittype[i].temp_data[1] = 64 + (engine.krand() & 511);
                        hittype[i].temp_data[2] = 0;
                    } else {
                        hittype[i].temp_data[2]++;
                        if (hittype[i].temp_data[2] > hittype[i].temp_data[1]) {
                            hittype[i].temp_data[0] = 0;
                            visibility = currentGame.getCON().const_visibility;
                            break;
                        } else if (hittype[i].temp_data[2] == (hittype[i].temp_data[1] >> 1)) {
                            spritesound(THUNDER, i);
                        } else if (hittype[i].temp_data[2] == (hittype[i].temp_data[1] >> 3)) {
                            spritesound(LIGHTNING_SLAP, i);
                        } else if (hittype[i].temp_data[2] == (hittype[i].temp_data[1] >> 2)) {
                            ListNode<Sprite> node = boardService.getStatNode(0);
                            while (node != null) {
                                Sprite sp = node.get();
                                if (sp.getPicnum() == NATURALLIGHTNING && sp.getHitag() == s.getHitag()) {
                                    sp.setCstat(sp.getCstat() | 32768);
                                }
                                node = node.getNext();
                            }
                        } else if (hittype[i].temp_data[2] > (hittype[i].temp_data[1] >> 3) && hittype[i].temp_data[2] < (hittype[i].temp_data[1] >> 2)) {
                            int j;
                            if (engine.cansee(s.getX(), s.getY(), s.getZ(), s.getSectnum(), ps[screenpeek].posx, ps[screenpeek].posy, ps[screenpeek].posz, ps[screenpeek].cursectnum)) {
                                j = 1;
                            } else {
                                j = 0;
                            }

                            if (rnd(192) && (hittype[i].temp_data[2] & 1) != 0) {
                                if (j != 0) {
                                    gVisibility = 0;
                                }
                            } else if (j != 0) {
                                gVisibility = currentGame.getCON().const_visibility;
                            }

                            ListNode<Sprite> node = boardService.getStatNode(0);
                            while (node != null) {
                                Sprite sp = node.get();
                                j = node.getIndex();
                                if (sp.getPicnum() == NATURALLIGHTNING && sp.getHitag() == s.getHitag()) {
                                    if (rnd(32) && (hittype[i].temp_data[2] & 1) != 0) {
                                        sp.setCstat(sp.getCstat() & 32767);
                                        spawn(j, SMALLSMOKE);

                                        int p = findplayer(s);
                                        Sprite psp = boardService.getSprite(ps[p].i);
                                        if (psp == null) {
                                            break;
                                        }

                                        int x = ldist(psp, sp);
                                        if (x < 768) {
                                            if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                                spritesound(DUKE_LONGTERM_PAIN, ps[p].i);
                                            }
                                            spritesound(SHORT_CIRCUIT, ps[p].i);
                                            ps[p].getPlayerSprite().setExtra(ps[p].getPlayerSprite().getExtra() - (8 + (engine.krand() & 7)));
                                            ps[p].pals_time = 32;
                                            ps[p].pals[0] = 16;
                                            ps[p].pals[1] = 0;
                                            ps[p].pals[2] = 0;
                                        }
                                        break;
                                    } else {
                                        sp.setCstat(sp.getCstat() | 32768);
                                    }
                                }

                                node = node.getNext();
                            }
                        }
                    }
                    break;
                }
                case 29: {
                    s.setHitag(s.getHitag() + 64);
                    int l = mulscale(s.getYvel(), EngineUtils.sin(s.getHitag() & 2047), 12);
                    if (sc != null) {
                        sc.setFloorz(s.getZ() + l);
                    }
                    break;
                }
                case 31: {// True Drop Floor
                    if (t[0] == 1) {
                        // Choose dir

                        if (t[3] > 0) {
                            t[3]--;
                            break;
                        }

                        game.pInt.setfloorinterpolate(s.getSectnum(), sc);
                        ListNode<Sprite> nj = boardService.getSectNode(s.getSectnum());
                        while (nj != null) {
                            Sprite sp = nj.get();
//                            hittype[j].bposz = sp.z;

                            game.pInt.setsprinterpolate(nj.getIndex(), sp);
                            nj = nj.getNext();
                        }

                        if (sc == null) {
                            break;
                        }

                        if (t[2] == 1) // Retract
                        {
                            if (s.getAng() != 1536) {
                                if (klabs(sc.getFloorz() - s.getZ()) < s.getYvel()) {
                                    sc.setFloorz(s.getZ());
                                    t[2] = 0;
                                    t[0] = 0;
                                    t[3] = s.getHitag();
                                    callsound(s.getSectnum(), i);
                                } else {
                                    int l = sgn(s.getZ() - sc.getFloorz()) * s.getYvel();
                                    sc.setFloorz(sc.getFloorz() + l);

                                    nj = boardService.getSectNode(s.getSectnum());
                                    while (nj != null) {
                                        Sprite sp = nj.get();
                                        if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                            if (ps[sp.getYvel()].on_ground) {
                                                ps[sp.getYvel()].posz += l;
                                            }
                                        }
                                        if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
                                            sp.setZ(sp.getZ() + l);
                                            hittype[nj.getIndex()].floorz = sc.getFloorz();
                                        }
                                        nj = nj.getNext();
                                    }
                                }
                            } else {
                                if (klabs(sc.getFloorz() - t[1]) < s.getYvel()) {
                                    sc.setFloorz(t[1]);
                                    callsound(s.getSectnum(), i);
                                    t[2] = 0;
                                    t[0] = 0;
                                    t[3] = s.getHitag();
                                } else {
                                    int l = sgn(t[1] - sc.getFloorz()) * s.getYvel();
                                    sc.setFloorz(sc.getFloorz() + l);

                                    nj = boardService.getSectNode(s.getSectnum());
                                    while (nj != null) {
                                        Sprite sp = nj.get();
                                        if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                            if (ps[sp.getYvel()].on_ground) {
                                                ps[sp.getYvel()].posz += l;
                                            }
                                        }
                                        if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
                                            sp.setZ(sp.getZ() + l);
                                            hittype[nj.getIndex()].floorz = sc.getFloorz();
                                        }
                                        nj = nj.getNext();
                                    }
                                }
                            }
                            break;
                        }

                        if ((s.getAng() & 2047) == 1536) {
                            if (klabs(s.getZ() - sc.getFloorz()) < s.getYvel()) {
                                callsound(s.getSectnum(), i);
                                t[0] = 0;
                                t[2] = 1;
                                t[3] = s.getHitag();
                            } else {
                                int l = sgn(s.getZ() - sc.getFloorz()) * s.getYvel();
                                sc.setFloorz(sc.getFloorz() + l);

                                nj = boardService.getSectNode(s.getSectnum());
                                while (nj != null) {
                                    Sprite sp = nj.get();
                                    if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                        if (ps[sp.getYvel()].on_ground) {
                                            ps[sp.getYvel()].posz += l;
                                        }
                                    }
                                    if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
                                        sp.setZ(sp.getZ() + l);
                                        hittype[nj.getIndex()].floorz = sc.getFloorz();
                                    }
                                    nj = nj.getNext();
                                }
                            }
                        } else {
                            if (klabs(sc.getFloorz() - t[1]) < s.getYvel()) {
                                t[0] = 0;
                                callsound(s.getSectnum(), i);
                                t[2] = 1;
                                t[3] = s.getHitag();
                            } else {
                                int l = sgn(s.getZ() - t[1]) * s.getYvel();
                                sc.setFloorz(sc.getFloorz() - l);

                                nj = boardService.getSectNode(s.getSectnum());
                                while (nj != null) {
                                    Sprite sp = nj.get();
                                    if (sp.getPicnum() == APLAYER && sp.getOwner() >= 0) {
                                        if (ps[sp.getYvel()].on_ground) {
                                            ps[sp.getYvel()].posz -= l;
                                        }
                                    }
                                    if (sp.getZvel() == 0 && sp.getStatnum() != 3 && sp.getStatnum() != 4) {
                                        sp.setZ(sp.getZ() - l);
                                        hittype[nj.getIndex()].floorz = sc.getFloorz();
                                    }
                                    nj = nj.getNext();
                                }
                            }
                        }
                    }
                    break;
                }
                case 32: { // True Drop Ceiling
                    if (sc != null && t[0] == 1) {
                        // Choose dir
                        game.pInt.setceilinterpolate(s.getSectnum(), sc);
                        if (t[2] == 1) // Retract
                        {
                            if (s.getAng() != 1536) {
                                if (klabs(sc.getCeilingz() - s.getZ()) < (s.getYvel() << 1)) {
                                    sc.setCeilingz(s.getZ());
                                    callsound(s.getSectnum(), i);
                                    t[2] = 0;
                                    t[0] = 0;
                                } else {
                                    sc.setCeilingz(sc.getCeilingz() + sgn(s.getZ() - sc.getCeilingz()) * s.getYvel());
                                }
                            } else {
                                if (klabs(sc.getCeilingz() - t[1]) < (s.getYvel() << 1)) {
                                    sc.setCeilingz(t[1]);
                                    callsound(s.getSectnum(), i);
                                    t[2] = 0;
                                    t[0] = 0;
                                } else {
                                    sc.setCeilingz(sc.getCeilingz() + sgn(t[1] - sc.getCeilingz()) * s.getYvel());
                                }
                            }
                            break;
                        }

                        if ((s.getAng() & 2047) == 1536) {
                            if (klabs(sc.getCeilingz() - s.getZ()) < (s.getYvel() << 1)) {
                                t[0] = 0;
                                t[2] ^= 1;
                                callsound(s.getSectnum(), i);
                                sc.setCeilingz(s.getZ());
                            } else {
                                sc.setCeilingz(sc.getCeilingz() + sgn(s.getZ() - sc.getCeilingz()) * s.getYvel());
                            }
                        } else {
                            if (klabs(sc.getCeilingz() - t[1]) < (s.getYvel() << 1)) {
                                t[0] = 0;
                                t[2] ^= 1;
                                callsound(s.getSectnum(), i);
                            } else {
                                sc.setCeilingz(sc.getCeilingz() - sgn(s.getZ() - t[1]) * s.getYvel());
                            }
                        }
                    }
                    break;
                }
                case 33:
                    if (earthquaketime > 0 && (engine.krand() & 7) == 0) {
                        RANDOMSCRAP(s, i);
                    }
                    break;
                case 36:

                    if (t[0] != 0) {
                        if (sc != null && t[0] == 1) {
                            shoot(i, sc.getExtra());
                        } else if (t[0] == 26 * 5) {
                            t[0] = 0;
                        }
                        t[0]++;
                    }
                    break;

                case 128: // SE to control glass breakage

                    Wall wal = boardService.getWall(t[2]);
//                  #GDX 30.06.2024: this check is always true
                    if (wal != null /*&& (wal.getCstat() | 32) != 0*/) {
                        wal.setCstat(wal.getCstat() & (255 - 32));
                        wal.setCstat(wal.getCstat() | 16);

                        Wall nwal = boardService.getWall(wal.getNextwall());
                        if (nwal != null) {
                            nwal.setCstat(nwal.getCstat() & (255 - 32));
                            nwal.setCstat(nwal.getCstat() | 16);
                        }
                    } else {
                        break;
                    }

                    wal.setOverpicnum(wal.getOverpicnum() + 1);
                    Wall nwal = boardService.getWall(wal.getNextwall());
                    if (nwal != null) {
                        nwal.setOverpicnum(nwal.getOverpicnum() + 1);
                    }

                    if (t[0] < t[1]) {
                        t[0]++;
                    } else {
                        wal.setCstat(wal.getCstat() & (128 + 32 + 8 + 4 + 2));
                        if (nwal != null) {
                            nwal.setCstat(nwal.getCstat() & (128 + 32 + 8 + 4 + 2));
                        }
                        engine.deletesprite(i);
                        continue;
                    }
                    break;

                case 130: {
                    if (t[0] > 80) {
                        engine.deletesprite(i);
                        continue;
                    } else {
                        t[0]++;
                    }

                    if (sc != null) {
                        int x = sc.getFloorz() - sc.getCeilingz();
                        if (rnd(64)) {
                            int k = spawn(i, EXPLOSION2);
                            int size = (2 + (engine.krand() & 7));
                            Sprite ks = boardService.getSprite(k);
                            if (ks != null) {
                                ks.setXrepeat(size);
                                ks.setYrepeat(size);
                                ks.setZ(sc.getFloorz() - (engine.krand() % x));
                                ks.setAng(ks.getAng() + 256 - (engine.krand() % 511));
                                ks.setXvel((short) (engine.krand() & 127));
                                ssp(k, CLIPMASK0);
                            }
                        }
                    }
                    break;
                }
                case 131: {
                    if (t[0] > 40) {
                        engine.deletesprite(i);
                        continue;
                    } else {
                        t[0]++;
                    }

                    if (sc != null) {
                        int x = sc.getFloorz() - sc.getCeilingz();
                        if (rnd(32)) {
                            int k = (short) spawn(i, EXPLOSION2);
                            int size = 2 + (engine.krand() & 3);
                            Sprite ks = boardService.getSprite(k);
                            if (ks != null) {
                                ks.setXrepeat(size);
                                ks.setYrepeat(size);
                                ks.setZ(sc.getFloorz() - (engine.krand() % x));
                                ks.setAng(ks.getAng() + 256 - (engine.krand() % 511));
                                ks.setXvel((short) (engine.krand() & 127));
                                ssp(k, CLIPMASK0);
                            }
                        }
                    }
                    break;
                }
            }
        }

        for (ListNode<Sprite> node = boardService.getStatNode(3); node != null; node = node.getNext()) {
            Sprite s = node.get();
            if (s.getLotag() != 29) {
                continue;
            }

            Sector sc = boardService.getSector(s.getSectnum());
            if (sc == null || sc.getWallnum() != 4) {
                continue;
            }

            Wall wal = boardService.getWall(sc.getWallptr() + 2);
            if (wal == null) {
                continue;
            }

            Sector sec = boardService.getSector(wal.getNextsector());
            if (sec == null) {
                continue;
            }

            engine.alignflorslope(s.getSectnum(), wal.getX(), wal.getY(), sec.getFloorz());
        }
    }

}
