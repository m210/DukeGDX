//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import com.badlogic.gdx.utils.IntArray;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.exceptions.InitializationException;
import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Script.Scriptfile;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.filehandle.grp.GrpFile;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Duke3D.Screens.DemoScreen;
import ru.m210projects.Duke3D.Types.EpisodeInfo;
import ru.m210projects.Duke3D.Types.GameType;
import ru.m210projects.Duke3D.Types.MapInfo;
import ru.m210projects.Duke3D.Types.Script;
import ru.m210projects.Duke3D.filehandle.EpisodeEntry;
import ru.m210projects.Duke3D.filehandle.UserEntry;

import java.io.FileNotFoundException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.BClampAngle;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Strhandler.*;
import static ru.m210projects.Duke3D.Actors.*;
import static ru.m210projects.Duke3D.Gameutils.neartag;
import static ru.m210projects.Duke3D.Gameutils.rnd;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.*;
import static ru.m210projects.Duke3D.Premap.*;
import static ru.m210projects.Duke3D.ResourceHandler.episodeManager;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;
import static ru.m210projects.Duke3D.Spawn.*;
import static ru.m210projects.Duke3D.View.*;
import static ru.m210projects.Duke3D.Weapons.*;

public class Gamedef {

    public static final int[] params = new int[31];
    //	private static int conversion = 13;
    public static final int MAXSCRIPTSIZE = 20460;
//    // Player Actions.;
//    public static final int pstanding = 1;
//    public static final int pwalking = 2;
//    public static final int prunning = 4;
//    public static final int pducking = 8;
//    public static final int pfalling = 16;
//    public static final int pjumping = 32;
//    public static final int phigher = 64;
//    public static final int pwalkingback = 128;
//    public static final int prunningback = 256;
//    public static final int pkicking = 512;
//    public static final int pshrunk = 1024;
//    public static final int pjetpack = 2048;
//    public static final int ponsteroids = 4096;
//    public static final int ponground = 8192;
//    public static final int palive = 16384;
//    public static final int pdead = 32768;
//    public static final int pfacing = 65536;
//    // Defines for 'useractor' keyword;
//    public static final int notenemy = 0;
//    public static final int enemy = 1;
//    public static final int enemystayput = 2;
    // Defines the motion characteristics of an actor;
    public static final int face_player = 1;
    public static final int geth = 2;
    public static final int getv = 4;
    public static final int random_angle = 8;
    public static final int face_player_slow = 16;
    public static final int spin = 32;
    public static final int face_player_smart = 64;
    public static final int fleeenemy = 128;
    public static final int jumptoplayer = 257;
    public static final int seekplayer = 512;
    public static final int furthestdir = 1024;
    public static final int dodgebullet = 4096;
//    public static final String[] defaultcons = {"GAME.CON", "USER.CON", "DEFS.CON"};
    public static final char[][] keyw = {"definelevelname".toCharArray(), // 0
            "actor".toCharArray(), // 1 [#] ok
            "addammo".toCharArray(), // 2 [#]
            "ifrnd".toCharArray(), // 3 [C]
            "enda".toCharArray(), // 4 [:] ok
            "ifcansee".toCharArray(), // 5 [C] ok
            "ifhitweapon".toCharArray(), // 6 [#]
            "action".toCharArray(), // 7 [#]
            "ifpdistl".toCharArray(), // 8 [#] ok
            "ifpdistg".toCharArray(), // 9 [#]
            "else".toCharArray(), // 10 [#] ok
            "strength".toCharArray(), // 11 [#]
            "break".toCharArray(), // 12 [#]
            "shoot".toCharArray(), // 13 [#]
            "palfrom".toCharArray(), // 14 [#]
            "sound".toCharArray(), // 15 [filename.voc]
            "fall".toCharArray(), // 16 [] ok
            "state".toCharArray(), // 17 ok
            "ends".toCharArray(), // 18 ok
            "define".toCharArray(), // 19
            "//".toCharArray(), // 20
            "ifai".toCharArray(), // 21 ok
            "killit".toCharArray(), // 22 ok
            "addweapon".toCharArray(), // 23
            "ai".toCharArray(), // 24
            "addphealth".toCharArray(), // 25
            "ifdead".toCharArray(), // 26
            "ifsquished".toCharArray(), // 27
            "sizeto".toCharArray(), // 28
            "{".toCharArray(), // 29
            "}".toCharArray(), // 30
            "spawn".toCharArray(), // 31
            "move".toCharArray(), // 32
            "ifwasweapon".toCharArray(), // 33
            "ifaction".toCharArray(), // 34
            "ifactioncount".toCharArray(), // 35
            "resetactioncount".toCharArray(), // 36
            "debris".toCharArray(), // 37
            "pstomp".toCharArray(), // 38
            "/*".toCharArray(), // 39
            "cstat".toCharArray(), // 40
            "ifmove".toCharArray(), // 41
            "resetplayer".toCharArray(), // 42
            "ifonwater".toCharArray(), // 43
            "ifinwater".toCharArray(), // 44
            "ifcanshoottarget".toCharArray(), // 45
            "ifcount".toCharArray(), // 46
            "resetcount".toCharArray(), // 47
            "addinventory".toCharArray(), // 48
            "ifactornotstayput".toCharArray(), // 49
            "hitradius".toCharArray(), // 50
            "ifp".toCharArray(), // 51
            "count".toCharArray(), // 52
            "ifactor".toCharArray(), // 53
            "music".toCharArray(), // 54
            "include".toCharArray(), // 55 ok
            "ifstrength".toCharArray(), // 56
            "definesound".toCharArray(), // 57
            "guts".toCharArray(), // 58
            "ifspawnedby".toCharArray(), // 59
            "gamestartup".toCharArray(), // 60
            "wackplayer".toCharArray(), // 61
            "ifgapzl".toCharArray(), // 62
            "ifhitspace".toCharArray(), // 63 ok
            "ifoutside".toCharArray(), // 64
            "ifmultiplayer".toCharArray(), // 65
            "operate".toCharArray(), // 66
            "ifinspace".toCharArray(), // 67
            "debug".toCharArray(), // 68
            "endofgame".toCharArray(), // 69
            "ifbulletnear".toCharArray(), // 70
            "ifrespawn".toCharArray(), // 71
            "iffloordistl".toCharArray(), // 72 ok
            "ifceilingdistl".toCharArray(), // 73
            "spritepal".toCharArray(), // 74
            "ifpinventory".toCharArray(), // 75
            "betaname".toCharArray(), // 76
            "cactor".toCharArray(), // 77 ok
            "ifphealthl".toCharArray(), // 78
            "definequote".toCharArray(), // 79
            "quote".toCharArray(), // 80
            "ifinouterspace".toCharArray(), // 81
            "ifnotmoving".toCharArray(), // 82
            "respawnhitag".toCharArray(), // 83
            "tip".toCharArray(), // 84
            "ifspritepal".toCharArray(), // 85
            "money".toCharArray(), // 86
            "soundonce".toCharArray(), // 87
            "addkills".toCharArray(), // 88
            "stopsound".toCharArray(), // 89
            "ifawayfromwall".toCharArray(), // 90
            "ifcanseetarget".toCharArray(), // 91
            "globalsound".toCharArray(), // 92
            "lotsofglass".toCharArray(), // 93
            "ifgotweaponce".toCharArray(), // 94
            "getlastpal".toCharArray(), // 95
            "pkick".toCharArray(), // 96
            "mikesnd".toCharArray(), // 97
            "useractor".toCharArray(), // 98
            "sizeat".toCharArray(), // 99 ok
            "addstrength".toCharArray(), // 100 [#]
            "cstator".toCharArray(), // 101
            "mail".toCharArray(), // 102
            "paper".toCharArray(), // 103
            "tossweapon".toCharArray(), // 104
            "sleeptime".toCharArray(), // 105
            "nullop".toCharArray(), // 106
            "definevolumename".toCharArray(), // 107
            "defineskillname".toCharArray(), // 108
            "ifnosounds".toCharArray(), // 109
            "clipdist".toCharArray(), // 110
            "ifangdiffl".toCharArray(), // 111
            "ifplaybackon".toCharArray(), // 112 Twentieth Anniversary World Tour
    };
    public static final int NUMKEYWORDS = keyw.length;
    private static final char[] tempbuf = new char[2048];
    public static final String confilename = "GAME.CON";
    public static int insptr;
    public static int scriptptr, error, warning, killit_flag;

    public static char[] label;
    public static int labelcnt;
    public static final IntArray labelcode = new IntArray();
    public static short checking_ifelse, parsing_state;
    public static String last_used_text;
    public static short num_squigilly_brackets;
    public static int last_used_size;
    public static short g_i, g_p;
    public static int g_x;
    public static Sprite g_sp;
    public static int[] g_t;
    public static int furthest_x, furthest_y;
    private static char[] text;
    private static int textptr = 0;
    private static int parsing_actor;
    private static short line_number;

    public static float getincangle(float a, float na) {
        a = BClampAngle(a);
        na = BClampAngle(na);

        if (!(Math.abs(a - na) < 1024)) {
            if (na > 1024) {
                na -= 2048;
            }
            if (a > 1024) {
                a -= 2048;
            }

            na -= 2048;
            a -= 2048;
        }
        return (na - a);
    }

    public static int getincangle(int a, int na) {
        a &= 2047;
        na &= 2047;

        if (klabs(a - na) >= 1024) {
            if (na > 1024) {
                na -= 2048;
            }
            if (a > 1024) {
                a -= 2048;
            }

            na -= 2048;
            a -= 2048;
        }
        return (na - a);
    }

    public static boolean ispecial(char c) {
        if (c == 0x0a) {
            line_number++;
            return true;
        }

        return c == ' ' || c == 0x0d || c == '\t';
    }

    public static boolean isaltok(char c) {
        return (isalnum(c) || c == '{' || c == '}' || c == '/' || c == '*' || c == '-' || c == '_' || c == '.');
    }

    public static boolean isaltok(byte c) {
        return (Character.isLetterOrDigit(c) || c == '{' || c == '}' || c == '/' || c == '*' || c == '-' || c == '_' || c == '.');
    }

    public static boolean isalnum(char c) {
        return Character.isLetterOrDigit(c);
    }

    /**
     * Move sprite to floor
     */
    public static void getglobalz(int i) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        final Sector sec = boardService.getSector(s.getSectnum());
        if (sec == null) {
            return;
        }

        if (s.getStatnum() == 10 || s.getStatnum() == 6 || s.getStatnum() == 2 || s.getStatnum() == 1 || s.getStatnum() == 4) {
            int zr;
            if (s.getStatnum() == 4) {
                zr = 4;
            } else {
                zr = 127;
            }

            engine.getzrange(s.getX(), s.getY(), s.getZ() - (FOURSLEIGHT), s.getSectnum(), zr, CLIPMASK0);
            hittype[i].ceilingz = zr_ceilz;
            hittype[i].floorz = zr_florz;
            int lz = zr_florhit;
            if ((lz & kHitTypeMask) == kHitSprite) {
                lz &= kHitIndexMask;
                Sprite hsp = boardService.getSprite(lz);
                if (hsp != null && (hsp.getCstat() & 48) == 0) {
                    if (badguy(hsp) && hsp.getPal() != 1) {
                        if (s.getStatnum() != 4) {
                            hittype[i].dispicnum = -4; // No shadows on actors
                            s.setXvel(-256);
                            ssp(i, CLIPMASK0);
                        }
                    } else if (hsp.getPicnum() == APLAYER && badguy(s)) {
                        hittype[i].dispicnum = -4; // No shadows on actors
                        s.setXvel(-256);
                        ssp(i, CLIPMASK0);
                    } else if (s.getStatnum() == 4 && hsp.getPicnum() == APLAYER) {
                        if (s.getOwner() == lz) {
                            hittype[i].ceilingz = sec.getCeilingz();
                            hittype[i].floorz = sec.getFloorz();
                        }
                    }
                }
            }
        } else {
            hittype[i].ceilingz = sec.getCeilingz();
            hittype[i].floorz = sec.getFloorz();
        }
    }

    public static void makeitfall(Script con, int i) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        final Sector sec = boardService.getSector(s.getSectnum());
        if (sec == null) {
            return;
        }

        int c;
        if (floorspace(s.getSectnum())) {
            c = 0;
        } else {
            if (ceilingspace(s.getSectnum()) || sec.getLotag() == 2) {
                c = con.gc / 6;
            } else {
                c = con.gc;
            }
        }

        if ((s.getStatnum() == 1 || s.getStatnum() == 10 || s.getStatnum() == 2 || s.getStatnum() == 6)) {
            engine.getzrange(s.getX(), s.getY(), s.getZ() - (FOURSLEIGHT), s.getSectnum(), 127, CLIPMASK0);
            hittype[i].ceilingz = zr_ceilz;
            hittype[i].floorz = zr_florz;
        } else {
            hittype[i].ceilingz = sec.getCeilingz();
            hittype[i].floorz = sec.getFloorz();
        }

        if (s.getZ() < hittype[i].floorz - (FOURSLEIGHT)) {
            if (sec.getLotag() == 2 && s.getZvel() > 3122) {
                s.setZvel(3144);
            }
            if (s.getZvel() < 6144) {
                s.setZvel(s.getZvel() + c);
            } else {
                s.setZvel(6144);
            }
            s.setZ(s.getZ() + s.getZvel());
        }
        if (s.getZ() >= hittype[i].floorz - (FOURSLEIGHT)) {
            s.setZ(hittype[i].floorz - FOURSLEIGHT);
            s.setZvel(0);
        }
    }

    public static void getlabel() {
        while (!isalnum(text[textptr])) {
            if (text[textptr] == 0x0a) {
                line_number++;
            }
            textptr++;
            if (text[textptr] == 0) {
                return;
            }
        }

        int len = 0;
        while (true) {
            if (ispecial(text[textptr + ++len])) {
                break;
            }
        }

        if (label.length <= (labelcnt << 6) + len) // resize
        {
            char[] newItems = new char[(labelcnt << 6) + len + 1];
            System.arraycopy(label, 0, newItems, 0, label.length);
            label = newItems;
        }

        System.arraycopy(text, textptr, label, (labelcnt << 6), len);
        label[(labelcnt << 6) + len] = 0;
        textptr += len;
    }

    public static int keyword() {
        int temptextptr = textptr;

        while (!isaltok(text[temptextptr])) {
            temptextptr++;
            if (text[temptextptr] == 0) {
                return 0;
            }
        }

        int i = 0;
        while (isaltok(text[temptextptr])) {
            tempbuf[i] = text[temptextptr++];
            i++;
        }
        tempbuf[i] = 0;

        for (i = 0; i < NUMKEYWORDS; i++) {
            if (Bstrcmp(tempbuf, keyw[i]) == 0) {
                return i;
            }
        }

        return -1;
    }

    public static int transword(Script con) // Returns its code #
    {
        while (!isaltok(text[textptr])) {
            if (text[textptr] == 0x0a) {
                line_number++;
            }
            if (text[textptr] == 0) {
                return -1;
            }
            textptr++;
        }

        int l = 0;
        while (isaltok(text[textptr + l])) {
            tempbuf[l] = text[textptr + l];
            l++;
        }

        tempbuf[l] = 0;

        for (int i = 0; i < NUMKEYWORDS; i++) {
            if (Bstrcmp(tempbuf, keyw[i]) == 0) {
                con.script[scriptptr] = i; // set to struct script
                textptr += l;
                scriptptr++;
                return i;
            }
        }

        textptr += l;
        if (tempbuf[0] == '{' && tempbuf[1] != 0) {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting a SPACE or CR between '{' and '" + new String(tempbuf, 1, l) + "'.", OsdColor.RED);
        } else if (tempbuf[0] == '}' && tempbuf[1] != 0) {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting a SPACE or CR between '}' and '" + new String(tempbuf, 1, l) + "'.", OsdColor.RED);
        } else if (tempbuf[0] == '/' && tempbuf[1] == '/' && tempbuf[2] != 0) {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting a SPACE between '//' and '" + new String(tempbuf, 2, l) + "'.", OsdColor.RED);
        } else if (tempbuf[0] == '/' && tempbuf[1] == '*' && tempbuf[2] != 0) {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting a SPACE between '/*' and '" + new String(tempbuf, 2, l) + "'.", OsdColor.RED);
        } else if (tempbuf[0] == '*' && tempbuf[1] == '/' && tempbuf[2] != 0) {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting a SPACE between '*/' and '" + new String(tempbuf, 2, l) + "'.", OsdColor.RED);
        } else {
            Console.out.println("  * ERROR!(L" + line_number + ") Expecting key word, but found '" + new String(tempbuf, 0, l) + "'.", OsdColor.RED);
        }

        error++;
        return -1;
    }

    public static void transnum(Script con) {
        while (!isaltok(text[textptr])) {
            if (text[textptr] == 0x0a) {
                line_number++;
            }
            textptr++;
            if (text[textptr] == 0) {
                return;
            }
        }

        int l = 0;
        while (isaltok(text[textptr + l])) {
            tempbuf[l] = text[textptr + l];
            l++;
        }

        tempbuf[l] = 0;
        for (int i = 0; i < NUMKEYWORDS; i++) {
            if (Bstrcmp(label, (labelcnt << 6), keyw[i], 0) == 0) {
                error++;
                Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[(labelcnt << 6)] + "' is a key word.", OsdColor.RED);
                textptr += l;
            }
        }

        for (int i = 0; i < labelcnt; i++) {
            if (Bstrcmp(tempbuf, 0, label, i << 6) == 0) {
                con.script[scriptptr] = labelcode.get(i);
                scriptptr++;
                textptr += l;
                return;
            }
        }

        if (!Character.isDigit(text[textptr]) && text[textptr] != '-') {
            Console.out.println("  * ERROR!(L" + line_number + ") Parameter '" + new String(tempbuf, 0, l) + "' is undefined.", OsdColor.RED);
            error++;
            textptr += l;
            return;
        }

        try {
            con.script[scriptptr] = Integer.parseInt(new String(text, textptr, l));
        } catch (Exception e) {
            Console.out.println("  * ERROR!(L" + line_number + ") Parameter '" + new String(tempbuf, 0, l) + "' is undefined.", OsdColor.RED);
            error++;
            textptr += l;
            return;
        }

        scriptptr++;
        textptr += l;
    }

    public static boolean parsecommand(Script con) {
        int i, j, k;
        char temp_ifelse_check;
        short temp_line_number;
        int tempscrptr;

        if (error > 12 || (text[textptr] == '\0') || (text[textptr + 1] == '\0')) {
            return true;
        }

        int tw = transword(con);

        switch (tw) {
            default:
            case -1:
                return false; // End
            case 39: // Rem endrem
                scriptptr--;
                j = line_number;
                do {
                    textptr++;
                    if (text[textptr] == 0x0a) {
                        line_number++;
                    }
                    if (text[textptr] == 0) {
                        Console.out.println("  * ERROR!(L" + j + ") Found '/*' with no '*/'.", OsdColor.RED);
                        error++;
                        return false;
                    }
                } while (text[textptr] != '*' || text[textptr + 1] != '/');
                textptr += 2;
                return false;

            case 17:
                if (parsing_actor == 0 && parsing_state == 0) {
                    getlabel();
                    scriptptr--;
                    labelcode.add(scriptptr);
                    labelcnt = labelcode.size;

                    parsing_state = 1;

                    return false;
                }

                getlabel();

                for (i = 0; i < NUMKEYWORDS; i++) {
                    if (Bstrcmp(label, labelcnt << 6, keyw[i], 0) == 0) {
                        error++;
                        Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[labelcnt << 6] + "' is a key word.", OsdColor.RED);
                        return false;
                    }
                }

                for (j = 0; j < labelcnt; j++) {
                    if (Bstrcmp(label, (j << 6), label, (labelcnt << 6)) == 0) {
                        con.script[scriptptr] = labelcode.get(j);
                        break;
                    }
                }

                if (j == labelcnt) {
                    Console.out.println("  * ERROR!(L" + line_number + ") State '" + label[labelcnt << 6] + "' not found.", OsdColor.RED);
                    error++;
                }
                scriptptr++;
                return false;
            case 15:
            case 92:
            case 87:
            case 89:
            case 93:

            case 11:
            case 13:
            case 25:
            case 31:
            case 40:
            case 52:
            case 69:
            case 74:
            case 77:
            case 80:
            case 86:
            case 88:
            case 68:
            case 100:
            case 101:
            case 102:
            case 103:
            case 105:
            case 110:
                transnum(con);
                return false;
            case 18:
                if (parsing_state == 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'ends' with no 'state'.", OsdColor.RED);
                    error++;
                }

                if (num_squigilly_brackets > 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found more '{' than '}' before 'ends'.", OsdColor.RED);
                    error++;
                }
                if (num_squigilly_brackets < 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found more '}' than '{' before 'ends'.", OsdColor.RED);
                    error++;
                }
                parsing_state = 0;

                return false;
            case 19:
                getlabel();
                // Check to see it's already defined
                for (i = 0; i < NUMKEYWORDS; i++) {
                    if (Bstrcmp(label, labelcnt << 6, keyw[i], 0) == 0) {
                        error++;
                        Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[labelcnt << 6] + "' is a key word.", OsdColor.RED);
                        return false;
                    }
                }

                for (i = 0; i < labelcnt; i++) {
                    if (Bstrcmp(label, labelcnt << 6, label, i << 6) == 0) {
                        warning++;
                        Console.out.println("  * WARNING.(L" + line_number + ") Duplicate definition '" + label[labelcnt << 6] + "' ignored.", OsdColor.RED);
                        break;
                    }
                }
                transnum(con);
                if (i == labelcnt) {
                    labelcode.add(con.script[scriptptr - 1]);
                    labelcnt = labelcode.size;
                }
//	                labelcode[labelcnt++] = con.script[scriptptr-1];
                scriptptr -= 2;
                return false;
            case 14:
                for (j = 0; j < 4; j++) {
                    if (keyword() == -1) {
                        transnum(con);
                    } else {
                        break;
                    }
                }

                while (j < 4) {
                    con.script[scriptptr] = 0;
                    scriptptr++;
                    j++;
                }
                return false;
            case 32:
                if (parsing_actor != 0 || parsing_state != 0) {
                    transnum(con);

                    j = 0;
                    while (keyword() == -1) {
                        transnum(con);
                        scriptptr--;
                        j |= con.script[scriptptr];
                    }
                    con.script[scriptptr] = j;
                    scriptptr++;
                } else {
                    scriptptr--;
                    getlabel();

                    // Check to see it's already defined
                    for (i = 0; i < NUMKEYWORDS; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), keyw[i], 0) == 0) {
                            error++;
                            Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[labelcnt << 6] + "' is a key word.", OsdColor.RED);
                            return false;
                        }
                    }

                    for (i = 0; i < labelcnt; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), label, (i << 6)) == 0) {
                            warning++;
                            Console.out.println("  * WARNING.(L" + line_number + ") Duplicate move '" + label[labelcnt << 6] + "' ignored.", OsdColor.RED);
                            break;
                        }
                    }
                    if (i == labelcnt) {
                        labelcode.add(scriptptr);
                        labelcnt = labelcode.size;
                    }
//	                    labelcode[labelcnt++] = scriptptr;
                    for (j = 0; j < 2; j++) {
                        if (keyword() >= 0) {
                            break;
                        }
                        transnum(con);
                    }
                    for (k = j; k < 2; k++) {
                        con.script[scriptptr] = 0;
                        scriptptr++;
                    }
                }
                return false;
            case 54: {
                scriptptr--;
                transnum(con); // Volume Number (0/4)
                scriptptr--;

                k = con.script[scriptptr] - 1;
                i = 0;
                if (k >= 0) // if it's background music
                {
                    while (keyword() == -1) {
                        while (!isaltok(text[textptr])) {
                            if (text[textptr] == 0x0a) {
                                line_number++;
                            }
                            textptr++;
                            if (text[textptr] == 0) {
                                break;
                            }
                        }
                        int startptr = textptr;
                        j = 0;
                        while (isaltok(text[textptr + j])) {
                            j++;
                        }

                        con.music_fn[k][i] = new String(text, startptr, j);
                        textptr += j;
                        if (i > 9) {
                            break;
                        }
                        i++;
                    }
                } else {
                    while (keyword() == -1) {
                        while (!isaltok(text[textptr])) {
                            if (text[textptr] == 0x0a) {
                                line_number++;
                            }
                            textptr++;
                            if (text[textptr] == 0) {
                                break;
                            }
                        }
                        int startptr = textptr;
                        j = 0;
                        while (isaltok(text[textptr + j])) {
                            j++;
                        }
                        con.env_music_fn[i] = new String(text, startptr, j);

                        textptr += j;
                        if (i > 9) {
                            break;
                        }
                        i++;
                    }
                }
            }
            return false;
            case 55: {
                scriptptr--;
                while (!isaltok(text[textptr])) {
                    if (text[textptr] == 0x0a) {
                        line_number++;
                    }
                    textptr++;
                    if (text[textptr] == 0) {
                        break;
                    }
                }
                j = 0;
                while (isaltok(text[textptr])) {
                    tempbuf[j] = text[textptr++];
                    j++;
                }
                tempbuf[j] = '\0';

                String name = new String(tempbuf, 0, j).trim();
                Entry fp = game.getCache().getEntry(name, !loadfromgrouponly);
                if (!fp.exists()) {
                    error++;
                    Console.out.println("  * ERROR!(L" + line_number + ") Could not find '" + label[labelcnt << 6] + "'.", OsdColor.RED);
                    return false;
                }

                j = (int) fp.getSize();
                byte[] buf = new byte[j + 1];
                System.arraycopy(fp.getBytes(), 0, buf, 0, j);

                Console.out.println("Including: '" + name + "'.");

                temp_line_number = line_number;
                line_number = 1;
                temp_ifelse_check = (char) checking_ifelse;
                checking_ifelse = 0;

                char[] origtext = text;
                int origtptr = textptr;
                textptr = 0;

                last_used_text = new String(buf);
                last_used_size = j;

                text = last_used_text.toCharArray();

                text[j] = 0;

                boolean done;
                do {
                    done = parsecommand(con);
                } while (!done);

                text = origtext;
                textptr = origtptr;
                line_number = temp_line_number;
                checking_ifelse = (short) temp_ifelse_check;

                return false;
            }
            case 24:
                if (parsing_actor != 0 || parsing_state != 0) {
                    transnum(con);
                } else {
                    scriptptr--;
                    getlabel();

                    for (i = 0; i < NUMKEYWORDS; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), keyw[i], 0) == 0) {
                            error++;
                            Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[labelcnt << 6] + "' is a key word.", OsdColor.RED);
                            return false;
                        }
                    }

                    for (i = 0; i < labelcnt; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), label, (i << 6)) == 0) {
                            warning++;
                            Console.out.println("  * WARNING.(L" + line_number + ") Duplicate ai '" + label[labelcnt << 6] + "' ignored.", OsdColor.RED);
                            break;
                        }
                    }

                    if (i == labelcnt) {
                        labelcode.add(scriptptr);
                        labelcnt = labelcode.size;
                    }
//	                    labelcode[labelcnt++] = scriptptr;

                    for (j = 0; j < 3; j++) {
                        if (keyword() >= 0) {
                            break;
                        }
                        if (j == 2) {
                            k = 0;
                            while (keyword() == -1) {
                                transnum(con);
                                scriptptr--;
                                k |= con.script[scriptptr];
                            }
                            con.script[scriptptr] = k;
                            scriptptr++;
                            return false;
                        } else {
                            transnum(con);
                        }
                    }
                    for (k = j; k < 3; k++) {
                        con.script[scriptptr] = 0;
                        scriptptr++;
                    }
                }
                return false;
            case 7:
                if (parsing_actor != 0 || parsing_state != 0) {
                    transnum(con);
                } else {
                    scriptptr--;
                    getlabel();
                    // Check to see it's already defined

                    for (i = 0; i < NUMKEYWORDS; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), keyw[i], 0) == 0) {
                            error++;
                            Console.out.println("  * ERROR!(L" + line_number + ") Symbol '" + label[labelcnt << 6] + "' is a key word.", OsdColor.RED);
                            return false;
                        }
                    }

                    for (i = 0; i < labelcnt; i++) {
                        if (Bstrcmp(label, (labelcnt << 6), label, (i << 6)) == 0) {
                            warning++;
                            Console.out.println("  * WARNING.(L" + line_number + ") Duplicate action '" + label[labelcnt << 6] + "' ignored.", OsdColor.RED);
                            break;
                        }
                    }

                    if (i == labelcnt) {
                        labelcode.add(scriptptr);
                        labelcnt = labelcode.size;
                    }
//	                    labelcode[labelcnt++] = scriptptr;

                    for (j = 0; j < 5; j++) {
                        if (keyword() >= 0) {
                            break;
                        }
                        transnum(con);
                    }
                    for (k = j; k < 5; k++) {
                        con.script[scriptptr] = 0;
                        scriptptr++;
                    }
                }
                return false;

            case 1:
                if (parsing_state != 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'actor' within 'state'.", OsdColor.RED);
                    error++;
                }

                if (parsing_actor != 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'actor' within 'actor'.", OsdColor.RED);
                    error++;
                }

                num_squigilly_brackets = 0;
                scriptptr--;
                parsing_actor = scriptptr;

                transnum(con);
                scriptptr--;
                con.actorscrptr[con.script[scriptptr]] = parsing_actor;

                for (j = 0; j < 4; j++) {
                    con.script[parsing_actor + j] = 0;
                    if (j == 3) {
                        j = 0;
                        while (keyword() == -1) {
                            transnum(con);
                            scriptptr--;
                            j |= con.script[scriptptr];
                        }
                        con.script[scriptptr] = j;
                        scriptptr++;
                        break;
                    } else {
                        if (keyword() >= 0) {
                            for (i = 4 - j; i > 0; i--) {
                                con.script[scriptptr++] = 0;
                            }
                            break;
                        }
                        transnum(con);

                        con.script[parsing_actor + j] = con.script[scriptptr - 1];
                    }
                }

                checking_ifelse = 0;

                return false;

            case 98:

                if (parsing_state != 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'useritem' within 'state'.", OsdColor.RED);
                    error++;
                }

                if (parsing_actor != 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'useritem' within 'actor'.", OsdColor.RED);
                    error++;
                }

                num_squigilly_brackets = 0;
                scriptptr--;
                parsing_actor = scriptptr;

                transnum(con);
                scriptptr--;
                j = con.script[scriptptr];
                transnum(con);
                scriptptr--;
                con.actorscrptr[con.script[scriptptr]] = parsing_actor;
                con.actortype[con.script[scriptptr]] = (short) j;

                for (j = 0; j < 4; j++) {
                    con.script[parsing_actor + j] = 0;
                    if (j == 3) {
                        j = 0;
                        while (keyword() == -1) {
                            transnum(con);
                            scriptptr--;
                            j |= con.script[scriptptr];
                        }
                        con.script[scriptptr] = j;
                        scriptptr++;
                        break;
                    } else {
                        if (keyword() >= 0) {
                            for (i = 4 - j; i > 0; i--) {
                                con.script[scriptptr++] = 0;
                            }
                            break;
                        }
                        transnum(con);
                        con.script[parsing_actor + j] = con.script[scriptptr - 1];
                    }
                }

                checking_ifelse = 0;
                return false;

            case 2:
            case 23:
            case 28:
            case 99:
            case 37:
            case 48:
            case 58:
                transnum(con);
                transnum(con);
                break;
            case 50:
                transnum(con);
                transnum(con);
                transnum(con);
                transnum(con);
                transnum(con);
                break;
            case 10:
                if (checking_ifelse != 0) {
                    checking_ifelse--;
                    tempscrptr = scriptptr;
                    scriptptr++; // Leave a spot for the fail location
                    parsecommand(con);

                    con.script[tempscrptr] = scriptptr;
                } else {
                    scriptptr--;
                    error++;
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'else' with no 'if'.", OsdColor.RED);
                }

                return false;

            case 75:
                transnum(con);
            case 3:
            case 8:
            case 9:
            case 21:
            case 33:
            case 34:
            case 35:
            case 41:
            case 46:
            case 53:
            case 56:
            case 59:
            case 62:
            case 72:
            case 73:
            case 78:
            case 85:
            case 94:
            case 111:
                transnum(con);
            case 43:
            case 44:
            case 49:
            case 5:
            case 6:
            case 27:
            case 26:
            case 45:
            case 51:
            case 63:
            case 64:
            case 65:
            case 67:
            case 70:
            case 71:
            case 81:
            case 82:
            case 90:
            case 91:
            case 109:
            case 112: // Twentieth Anniversary World Tour

                if (tw == 51) {
                    j = 0;
                    do {
                        transnum(con);
                        scriptptr--;
                        j |= con.script[scriptptr];
                    } while (keyword() == -1);
                    con.script[scriptptr] = j;
                    scriptptr++;
                }

                tempscrptr = scriptptr;
                scriptptr++; // Leave a spot for the fail location

                do {
                    j = keyword();
                    if (j == 20 || j == 39) {
                        parsecommand(con);
                    }
                } while (j == 20 || j == 39);

                parsecommand(con);
                con.script[tempscrptr] = scriptptr;
                checking_ifelse++;
                return false;
            case 29: {
                num_squigilly_brackets++;
                boolean done;
                do {
                    done = parsecommand(con);
                } while (!done);
                return false;
            }
            case 30:
                num_squigilly_brackets--;
                if (num_squigilly_brackets < 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found more '}' than '{'.", OsdColor.RED);
                    error++;
                }
                return true;
            case 76:
                scriptptr--;
                j = 0;
                while (text[textptr] != 0x0a && text[textptr] != 0) // JBF 20040127: end of file checked
                {
                    con.betaname[j] = text[textptr];
                    j++;
                    textptr++;
                }
                con.betaname[j] = 0;
                return false;
            case 20:
                scriptptr--; // Negate the rem
                while (text[textptr] != 0x0a && text[textptr] != 0) // JBF 20040127: end of file checked
                {
                    textptr++;
                }

                return false;

            case 107:
                scriptptr--;
                transnum(con);
                scriptptr--;
                j = con.script[scriptptr];
                while (text[textptr] == ' ') {
                    textptr++;
                }

                i = 0;
                while (text[textptr] != 0x0a && text[textptr] != 0) {
                    con.volume_names[j][i] = Character.toUpperCase(text[textptr]);
                    textptr++;
                    i++;
                    if (i >= 32) {
                        Console.out.println("  * ERROR!(L" + line_number + ") Volume name exceeds character size limit of 32.", OsdColor.RED);
                        error++;
                        while (text[textptr] != 0x0a && text[textptr] != 0) {
                            textptr++;
                        }
                        break;
                    }
                }
                con.volume_names[j][i - 1] = '\0';
                con.nEpisodes = Math.max(con.nEpisodes, j + 1);
                return false;
            case 108:
                scriptptr--;
                transnum(con);
                scriptptr--;
                j = con.script[scriptptr];
                while (text[textptr] == ' ') {
                    textptr++;
                }

                i = 0;

                while (text[textptr] != 0x0a && text[textptr] != 0) {
                    con.skill_names[j][i] = Character.toUpperCase(text[textptr]);
                    textptr++;
                    i++;
                    if (i >= 32) {
                        Console.out.println("  * ERROR!(L" + line_number + ") Skill name exceeds character size limit of 32.", OsdColor.RED);
                        error++;
                        while (text[textptr] != 0x0a && text[textptr] != 0) {
                            textptr++;
                        }
                        break;
                    }
                }
                con.skill_names[j][i - 1] = '\0';
                con.nSkills = Math.max(con.nSkills, j + 1);
                return false;
            case 0:
                scriptptr--;
                transnum(con);
                scriptptr--;
                j = con.script[scriptptr];
                transnum(con);
                scriptptr--;
                k = con.script[scriptptr];
                while (text[textptr] == ' ') {
                    textptr++;
                }

                i = 0;
                while (text[textptr] != ' ' && text[textptr] != 0x0a && text[textptr] != 0) {
                    con.level_file_names[j * 11 + k][i] = text[textptr];
                    textptr++;
                    i++;
                    if (i > 127) {
                        Console.out.println("  * ERROR!(L" + line_number + ") Level file name exceeds character size limit of 128.", OsdColor.RED);
                        error++;
                        while (text[textptr] != ' ' && text[textptr] != 0) {
                            textptr++;
                        }
                        break;
                    }
                }
                con.level_names[j * 11 + k][i - 1] = '\0';

                while (text[textptr] == ' ') {
                    textptr++;
                }

                con.partime[j * 11 + k] = (((text[textptr] - '0') * 10 + (text[textptr + 1] - '0')) * 26 * 60) + (((text[textptr + 3] - '0') * 10 + (text[textptr + 4] - '0')) * 26);

                textptr += 5;
                while (text[textptr] == ' ') {
                    textptr++;
                }

                con.designertime[j * 11 + k] = (((text[textptr] - '0') * 10 + (text[textptr + 1] - '0')) * 26 * 60) + (((text[textptr + 3] - '0') * 10 + (text[textptr + 4] - '0')) * 26);

                textptr += 5;
                while (text[textptr] == ' ') {
                    textptr++;
                }

                i = 0;

                while (text[textptr] != 0x0a && text[textptr] != 0) {
                    con.level_names[j * 11 + k][i] = Character.toUpperCase(text[textptr]);
                    textptr++;
                    i++;
                    if (i >= 32) {
                        Console.out.println("  * ERROR!(L" + line_number + ") Level name exceeds character size limit of 32.", OsdColor.RED);
                        error++;
                        while (text[textptr] != 0x0a && text[textptr] != 0) {
                            textptr++;
                        }
                        break;
                    }
                }
                con.level_names[j * 11 + k][i - 1] = '\0';
                con.nMaps[j] = Math.max(con.nMaps[j], k + 1);
                return false;
            case 79:
                scriptptr--;
                transnum(con);
                k = con.script[scriptptr - 1];
                if (k >= NUMOFFIRSTTIMEACTIVE) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Quote amount exceeds limit of " + NUMOFFIRSTTIMEACTIVE + " characters.", OsdColor.RED);
                    error++;
                    break;
                }
                scriptptr--;
                i = 0;
                while (text[textptr] == ' ') {
                    textptr++;
                }

                while (text[textptr] != 0x0a && text[textptr] != 0) {
                    con.fta_quotes[k][i] = text[textptr];
                    textptr++;
                    i++;

                    if (i >= 64) {
                        Console.out.println("  * ERROR!(L" + line_number + ") Quote exceeds character size limit of 64.", OsdColor.RED);
                        error++;
                        while (text[textptr] != 0x0a) {
                            textptr++;
                        }
                        break;
                    }
                }
                con.fta_quotes[k][i] = '\0';

                String quote = toLowerCase(new String(con.fta_quotes[k]).trim());
                if (quote.contains("press space")) {
                    quote = quote.replace("press space", "press \"open\"");
                    quote.getChars(0, quote.length(), con.fta_quotes[k], 0);
                    con.fta_quotes[k][quote.length()] = '\0';
                }

                return false;
            case 57: // definesound
                scriptptr--;
                transnum(con);
                k = con.script[scriptptr - 1];
                if (k >= NUM_SOUNDS) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Exceeded sound limit of " + NUM_SOUNDS + ".", OsdColor.RED);
                    error++;
                }
                scriptptr--;
                i = 0;
                while (text[textptr] == ' ' && text[textptr] != '\t' && text[textptr] != 0) {
                    textptr++;
                }

                int soundptr = textptr;
                while (text[textptr] != ' ' && text[textptr] != '\t' && text[textptr] != 0) {
                    textptr++;
                    i++;
                    if (i >= 128) {
                        Console.out.println(new String(text, soundptr, i));
                        Console.out.println("  * ERROR!(L" + line_number + ") Sound filename exceeded limit of 128 characters.", OsdColor.RED);
                        error++;
                        while (text[textptr] != ' ' && text[textptr] != '\t') {
                            textptr++;
                        }
                        break;
                    }
                }

                con.sounds[k] = FileUtils.getCorrectPath(new String(text, soundptr, i));

                transnum(con);
                con.soundps[k] = (short) con.script[scriptptr - 1];
                scriptptr--;
                transnum(con);
                con.soundpe[k] = (short) con.script[scriptptr - 1];
                scriptptr--;
                transnum(con);
                con.soundpr[k] = (short) con.script[scriptptr - 1];
                scriptptr--;
                transnum(con);
                con.soundm[k] = (short) con.script[scriptptr - 1];
                scriptptr--;
                transnum(con);
                con.soundvo[k] = (short) con.script[scriptptr - 1];
                scriptptr--;
                return false;
            case 4:
                if (parsing_actor == 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found 'enda' without defining 'actor'.", OsdColor.RED);
                    error++;
                }

                if (num_squigilly_brackets > 0) {
                    Console.out.println("  * ERROR!(L" + line_number + ") Found more '{' than '}' before 'enda'.", OsdColor.RED);
                    error++;
                }
                parsing_actor = 0;

                return false;
            case 12:
            case 16:
            case 84:
            case 22: // KILLIT
            case 36:
            case 38:
            case 42:
            case 47:
            case 61:
            case 66:
            case 83:
            case 95:
            case 96:
            case 97:
            case 104:
            case 106:
                return false;
            case 60:

                for (j = 0; j < 31; j++) {
                    transnum(con);
                    scriptptr--;
                    params[j] = con.script[scriptptr];

                    if (j != 25 && j != 29) {
                        continue;
                    }

                    if (error > 0) {
                        return false;
                    }

                    if (keyword() != -1) {
                        break;
                    } else {
                        if (j == 25) {
                            con.type = 14;
                        } else {
                            con.type = 20;
                        }
                    }
                }

                j = 0;
                con.const_visibility = params[j++];
                con.impact_damage = params[j++];
                con.max_player_health = params[j++];
                con.max_armour_amount = params[j++];
                con.respawnactortime = params[j++];
                con.respawnitemtime = params[j++];
                con.dukefriction = params[j++];
                if (con.type != 13) {
                    con.gc = params[j++];
                }
                con.rpgblastradius = params[j++];
                con.pipebombblastradius = params[j++];
                con.shrinkerblastradius = params[j++];
                con.tripbombblastradius = params[j++];
                con.morterblastradius = params[j++];
                con.bouncemineblastradius = params[j++];
                con.seenineblastradius = params[j++];
                con.max_ammo_amount[PISTOL_WEAPON] = params[j++];
                con.max_ammo_amount[SHOTGUN_WEAPON] = params[j++];
                con.max_ammo_amount[CHAINGUN_WEAPON] = params[j++];
                con.max_ammo_amount[RPG_WEAPON] = params[j++];
                con.max_ammo_amount[HANDBOMB_WEAPON] = params[j++];
                con.max_ammo_amount[SHRINKER_WEAPON] = params[j++];
                con.max_ammo_amount[DEVISTATOR_WEAPON] = params[j++];
                con.max_ammo_amount[TRIPBOMB_WEAPON] = params[j++];
                con.max_ammo_amount[FREEZE_WEAPON] = params[j++];
                if (con.type != 13) {
                    con.max_ammo_amount[GROW_WEAPON] = params[j++];
                }
                con.camerashitable = (char) params[j++];
                con.numfreezebounces = params[j++];
                con.freezerhurtowner = (char) params[j++];
                if (con.type != 13) {
                    con.spriteqamount = (short) params[j++];
                    if (con.spriteqamount > 1024) {
                        con.spriteqamount = 1024;
                    } else if (con.spriteqamount < 0) {
                        con.spriteqamount = 0;
                    }

                    con.lasermode = (char) params[j++];
                }

                if (con.type == 20) // Twentieth Anniversary World Tour
                {
                    con.max_ammo_amount[FLAMETHROWER_WEAPON] = params[j];
                }

                scriptptr++;
                return false;
        }
        return false;
    }

    public static void passone(Script con) {
        while (true) {
            if (parsecommand(con)) {
                break;
            }
        }

        if ((error + warning) > 12) {
            Console.out.println("  * ERROR! Too many warnings or errors.", OsdColor.RED);
        }
    }

    public static Script loadefs(String filenam) throws InitializationException {
        Entry fp = game.getCache().getEntry(filenam, !loadfromgrouponly);
        if (!fp.exists()) {
            throw new InitializationException("\nMissing con file(s).");
        }

        Console.out.println("Compiling: " + filenam + ".");

        int fs = (int) fp.getSize();

        byte[] buf = new byte[fs + 1];
        label = new char[131072];
        System.arraycopy(fp.getBytes(), 0, buf, 0, fs);

        last_used_text = new String(buf);
        last_used_size = fs;
        text = last_used_text.toCharArray();
        if (fs < text.length) {
            text[fs] = 0;
        }

        Script con = new Script();

        labelcnt = 0;
        scriptptr = 1;
        warning = 0;
        error = 0;
        line_number = 1;
        try {
            passone(con); // Tokenize
        } catch (Exception e) {
            e.printStackTrace();
            error = 1;
        }

        if ((warning | error) != 0) {
            Console.out.println("Found " + warning + " warning(s), " + error + " error(s).");
        }

        if (error != 0) {
            throw new InitializationException("\nCompilation error in " + filenam + ".");
        } else {
            Console.out.println("Code Size:" + (((scriptptr) << 2) - 4) + " bytes(" + labelcnt + " labels).");
        }

        return con;
    }

    public static boolean dodge(Sprite s) {
        int bx, by, mx, my, bxvect, byvect, mxvect, myvect, d;

        mx = s.getX();
        my = s.getY();
        mxvect = EngineUtils.sin((s.getAng() + 512) & 2047);
        myvect = EngineUtils.sin(s.getAng() & 2047);

        for (ListNode<Sprite> node = boardService.getStatNode(4); node != null; node = node.getNext()) // weapons list
        {
            Sprite sp = node.get();
            if (sp.getOwner() == node.getIndex() || sp.getSectnum() != s.getSectnum()) {
                continue;
            }

            bx = sp.getX() - mx;
            by = sp.getY() - my;
            bxvect = EngineUtils.cos(sp.getAng());
            byvect = EngineUtils.sin(sp.getAng());

            if (mxvect * bx + myvect * by >= 0) {
                if (bxvect * bx + byvect * by < 0) {
                    d = bxvect * by - byvect * bx;
                    if (klabs(d) < 65536 * 64) {
                        s.setAng(s.getAng() - (512 + (engine.krand() & 1024)));
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static short furthestangle(int i, int angs) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return 0;
        }

        long greatestd = -(1 << 30);
        int angincs = (2048 / angs);

        if (s.getPicnum() != APLAYER) {
            if ((g_t[0] & 63) > 2) {
                return (short) (s.getAng() + 1024);
            }
        }

        int furthest_angle = 0;
        for (int j = s.getAng(); j < (2048 + s.getAng()); j += angincs) {
            engine.hitscan(s.getX(), s.getY(), s.getZ() - (8 << 8), s.getSectnum(), EngineUtils.sin((j + 512) & 2047), EngineUtils.sin(j & 2047), 0, pHitInfo, CLIPMASK1);

            long d = klabs(pHitInfo.hitx - s.getX()) + klabs(pHitInfo.hity - s.getY());
            if (d > greatestd) {
                greatestd = d;
                furthest_angle = j;
            }
        }
        return (short) (furthest_angle & 2047);
    }

    public static int furthestcanseepoint(int i, Sprite ts, int dax, int day) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return -1;
        }

        furthest_x = dax;
        furthest_y = day;

        if ((g_t[0] & 63) != 0) {
            return -1;
        }

        short angincs = 1024;
        if (ud.multimode >= 2 || ud.player_skill >= 3) {
            angincs = (short) (2048 / (1 + (engine.krand() & 1)));
        }

        for (int j = ts.getAng(); j < (2048 + ts.getAng()); j += (angincs - (engine.krand() & 511))) {
            engine.hitscan(ts.getX(), ts.getY(), ts.getZ() - (16 << 8), ts.getSectnum(), EngineUtils.cos(j & 2047), EngineUtils.sin(j & 2047), 16384 - (engine.krand() & 32767), pHitInfo, CLIPMASK1);

            long d = klabs(pHitInfo.hitx - ts.getX()) + klabs(pHitInfo.hity - ts.getY());
            long da = klabs(pHitInfo.hitx - s.getX()) + klabs(pHitInfo.hity - s.getY());

            if (d < da) {
                if (engine.cansee(pHitInfo.hitx, pHitInfo.hity, pHitInfo.hitz, pHitInfo.hitsect, s.getX(), s.getY(), s.getZ() - (16 << 8), s.getSectnum())) {
                    furthest_x = pHitInfo.hitx;
                    furthest_y = pHitInfo.hity;
                    return pHitInfo.hitsect;
                }
            }
        }
        return -1;
    }

    public static void alterang(Script con, int a) {
        int ticselapsed = (g_t[0]) & 31;

        int aang = g_sp.getAng();

        g_sp.setXvel(g_sp.getXvel() + (con.script[g_t[1]] - g_sp.getXvel()) / 5);
        if (g_sp.getZvel() < 648) {
            g_sp.setZvel(g_sp.getZvel() + ((con.script[g_t[1] + 1] << 4) - g_sp.getZvel()) / 5);
        }

        if ((a & seekplayer) != 0) {
            int j = ps[g_p].holoduke_on;
            Sprite shd = boardService.getSprite(j);

            if (shd != null && engine.cansee(shd.getX(), shd.getY(), shd.getZ(), shd.getSectnum(), g_sp.getX(), g_sp.getY(), g_sp.getZ(), g_sp.getSectnum())) {
                g_sp.setOwner(j);
            } else {
                g_sp.setOwner(ps[g_p].i);
            }

            int goalang = 0;
            Sprite spo = boardService.getSprite(g_sp.getOwner());
            if (spo != null) {
                if (spo.getPicnum() == APLAYER) {
                    goalang = EngineUtils.getAngle(hittype[g_i].lastvx - g_sp.getX(), hittype[g_i].lastvy - g_sp.getY());
                } else {
                    goalang = EngineUtils.getAngle(spo.getX() - g_sp.getX(), spo.getY() - g_sp.getY());
                }
            }

            if (g_sp.getXvel() != 0 && g_sp.getPicnum() != DRONE) {
                int angdif = getincangle(aang, goalang);
                if (ticselapsed < 2) {
                    if (klabs(angdif) < 256) {
                        j = 128 - (engine.krand() & 256);
                        g_sp.setAng(g_sp.getAng() + j);
                        if (hits(g_i) < 844) {
                            g_sp.setAng(g_sp.getAng() - j);
                        }
                    }
                } else if (ticselapsed > 18 && ticselapsed < 26) // choose
                {
                    if (klabs(angdif >> 2) < 128) {
                        g_sp.setAng(goalang);
                    } else {
                        g_sp.setAng(g_sp.getAng() + (angdif >> 2));
                    }
                }
            } else {
                g_sp.setAng(goalang);
            }
        }

        if (ticselapsed < 1) {
            int j = 2;
            if ((a & furthestdir) != 0) {
                int goalang = furthestangle(g_i, j);
                g_sp.setAng((short) goalang);
                g_sp.setOwner(ps[g_p].i);
            }

            if ((a & fleeenemy) != 0) {
                int goalang = furthestangle(g_i, j);
                g_sp.setAng((short) goalang);
            }
        }
    }

    public static void move(Script con) {
        int l;
        int goalang, angdif;
        int daxvel;

        short a = g_sp.getHitag();

        if (a == -1) {
            a = 0;
        }

        g_t[0]++;

        if ((a & face_player) != 0) {
            if (ps[g_p].newowner >= 0) {
                goalang = EngineUtils.getAngle(ps[g_p].oposx - g_sp.getX(), ps[g_p].oposy - g_sp.getY());
            } else {
                goalang = EngineUtils.getAngle(ps[g_p].posx - g_sp.getX(), ps[g_p].posy - g_sp.getY());
            }
            angdif = (short) (getincangle(g_sp.getAng(), goalang) >> 2);
            if (angdif > -8 && angdif < 0) {
                angdif = 0;
            }
            g_sp.setAng(g_sp.getAng() + angdif);
        }

        if ((a & spin) != 0) {
            g_sp.setAng(g_sp.getAng() + (EngineUtils.sin(((g_t[0] << 3) & 2047)) >> 6));
        }

        if ((a & face_player_slow) != 0) {
            if (ps[g_p].newowner >= 0) {
                goalang = EngineUtils.getAngle(ps[g_p].oposx - g_sp.getX(), ps[g_p].oposy - g_sp.getY());
            } else {
                goalang = EngineUtils.getAngle(ps[g_p].posx - g_sp.getX(), ps[g_p].posy - g_sp.getY());
            }
            angdif = (short) (ksgn(getincangle(g_sp.getAng(), goalang)) << 5);
            if (angdif > -32 && angdif < 0) {
                angdif = 0;
                g_sp.setAng(goalang);
            }
            g_sp.setAng(g_sp.getAng() + angdif);
        }

        if ((a & jumptoplayer) == jumptoplayer) {
            if (g_t[0] < 16) {
                g_sp.setZvel(g_sp.getZvel() - (EngineUtils.sin((512 + (g_t[0] << 4)) & 2047) >> 5));
            }
        }

        if ((a & face_player_smart) != 0) {
            int newx = ps[g_p].posx + (ps[g_p].posxv / 768);
            int newy = ps[g_p].posy + (ps[g_p].posyv / 768);
            goalang = EngineUtils.getAngle(newx - g_sp.getX(), newy - g_sp.getY());
            angdif = (short) (getincangle(g_sp.getAng(), goalang) >> 2);
            if (angdif > -8 && angdif < 0) {
                angdif = 0;
            }
            g_sp.setAng(g_sp.getAng() + angdif);
        }

        if (g_t[1] == 0 || a == 0) {
            ILoc oldLoc = game.pInt.getsprinterpolate(g_i);
            if (oldLoc != null && ((badguy(g_sp) && g_sp.getExtra() <= 0) || (oldLoc.x != g_sp.getX()) || (oldLoc.y != g_sp.getY()))) {
                oldLoc.x = g_sp.getX();
                oldLoc.y = g_sp.getY();
                engine.setsprite(g_i, g_sp.getX(), g_sp.getY(), g_sp.getZ());
            }
            return;
        }

        if ((a & geth) != 0) {
            g_sp.setXvel(g_sp.getXvel() + ((con.script[g_t[1]] - g_sp.getXvel()) >> 1));
        }
        if ((a & getv) != 0) {
            g_sp.setZvel(g_sp.getZvel() + (((con.script[g_t[1] + 1] << 4) - g_sp.getZvel()) >> 1));
        }

        if ((a & dodgebullet) != 0) {
            dodge(g_sp);
        }

        if (g_sp.getPicnum() != APLAYER) {
            alterang(con, a);
        }

        if (g_sp.getXvel() > -6 && g_sp.getXvel() < 6) {
            g_sp.setXvel(0);
        }

        a = (short) (badguy(g_sp) ? 1 : 0);

        if (g_sp.getXvel() != 0 || g_sp.getZvel() != 0) {
            if (a != 0 && g_sp.getPicnum() != ROTATEGUN) {
                if ((g_sp.getPicnum() == DRONE || g_sp.getPicnum() == COMMANDER) && g_sp.getExtra() > 0) {
                    if (g_sp.getPicnum() == COMMANDER) {
                        hittype[g_i].floorz = l = engine.getflorzofslope(g_sp.getSectnum(), g_sp.getX(), g_sp.getY());
                        if (g_sp.getZ() > (l - (8 << 8))) {
                            if (g_sp.getZ() > (l - (8 << 8))) {
                                g_sp.setZ(l - (8 << 8));
                            }
                            g_sp.setZvel(0);
                        }

                        hittype[g_i].ceilingz = l = engine.getceilzofslope(g_sp.getSectnum(), g_sp.getX(), g_sp.getY());
                        if ((g_sp.getZ() - l) < (80 << 8)) {
                            g_sp.setZ(l + (80 << 8));
                            g_sp.setZvel(0);
                        }
                    } else {
                        if (g_sp.getZvel() > 0) {
                            hittype[g_i].floorz = l = engine.getflorzofslope(g_sp.getSectnum(), g_sp.getX(), g_sp.getY());
                            if (g_sp.getZ() > (l - (30 << 8))) {
                                g_sp.setZ(l - (30 << 8));
                            }
                        } else {
                            hittype[g_i].ceilingz = l = engine.getceilzofslope(g_sp.getSectnum(), g_sp.getX(), g_sp.getY());
                            if ((g_sp.getZ() - l) < (50 << 8)) {
                                g_sp.setZ(l + (50 << 8));
                                g_sp.setZvel(0);
                            }
                        }
                    }
                } else if (g_sp.getPicnum() != ORGANTIC) {
                    if (g_sp.getZvel() > 0 && hittype[g_i].floorz < g_sp.getZ()) {
                        g_sp.setZ(hittype[g_i].floorz);
                    }
                    if (g_sp.getZvel() < 0) {
                        l = engine.getceilzofslope(g_sp.getSectnum(), g_sp.getX(), g_sp.getY());
                        if ((g_sp.getZ() - l) < (66 << 8)) {
                            g_sp.setZ(l + (66 << 8));
                            g_sp.setZvel(g_sp.getZvel() >> 1);
                        }
                    }
                }
            } else if (g_sp.getPicnum() == APLAYER) {
                if ((g_sp.getZ() - hittype[g_i].ceilingz) < (32 << 8)) {
                    g_sp.setZ(hittype[g_i].ceilingz + (32 << 8));
                }
            }

            daxvel = g_sp.getXvel();
            angdif = g_sp.getAng();

            if (a != 0 && g_sp.getPicnum() != ROTATEGUN) {
                if (g_x < 960 && g_sp.getXrepeat() > 16) {
                    daxvel = -(1024 - g_x);
                    angdif = EngineUtils.getAngle(ps[g_p].posx - g_sp.getX(), ps[g_p].posy - g_sp.getY());

                    if (g_x < 512) {
                        ps[g_p].posxv = 0;
                        ps[g_p].posyv = 0;
                    } else {
                        ps[g_p].posxv = mulscale(ps[g_p].posxv, con.dukefriction - 0x2000, 16);
                        ps[g_p].posyv = mulscale(ps[g_p].posyv, con.dukefriction - 0x2000, 16);
                    }
                } else if (g_sp.getPicnum() != DRONE && g_sp.getPicnum() != SHARK && g_sp.getPicnum() != COMMANDER) {
                    ILoc oldLoc = game.pInt.getsprinterpolate(g_i);
                    if ((oldLoc != null && oldLoc.z != g_sp.getZ()) || (ud.multimode < 2 && ud.player_skill < 2)) {
                        if ((g_t[0] & 1) != 0 || ps[g_p].actorsqu == g_i) {
                            return;
                        } else {
                            daxvel <<= 1;
                        }
                    } else {
                        if ((g_t[0] & 3) != 0 || ps[g_p].actorsqu == g_i) {
                            return;
                        } else {
                            daxvel <<= 2;
                        }
                    }
                }
            }

            hittype[g_i].movflag = movesprite(g_i, (daxvel * (EngineUtils.sin((angdif + 512) & 2047))) >> 14, (daxvel * (EngineUtils.sin(angdif & 2047))) >> 14, g_sp.getZvel(), CLIPMASK0);
        }

        Sector sec = boardService.getSector(g_sp.getSectnum());
        if (sec != null && a != 0) {
            if ((sec.getCeilingstat() & 1) != 0) {
                g_sp.setShade(g_sp.getShade() + ((sec.getCeilingshade() - g_sp.getShade()) >> 1));
            } else {
                g_sp.setShade(g_sp.getShade() + ((sec.getFloorshade() - g_sp.getShade()) >> 1));
            }

            if (sec.getFloorpicnum() == MIRROR) {
                engine.deletesprite(g_i);
            }
        }
    }

    public static void parseifelse(Script con, boolean condition) {
        if (condition) {
            insptr += 2;
            parse(con);
        } else {
            insptr = con.script[insptr + 1];
            if (con.script[insptr] == 10) {
                insptr += 2;
                parse(con);
            }
        }
    }

    public static boolean parse(Script con) {
        int j, l, s;
        boolean cansee;

        if (killit_flag != 0) {
            return true;
        }

        switch (con.script[insptr]) {
            case 3:
                insptr++;
                parseifelse(con, rnd(con.script[insptr]));
                break;
            case 45:

                if (g_x > 1024) {
                    int temphit, sclip, angdif;

                    if (badguy(g_sp) && g_sp.getXrepeat() > 56) {
                        sclip = 3084;
                        angdif = 48;
                    } else {
                        sclip = 768;
                        angdif = 16;
                    }

                    j = hitasprite(g_i);
                    temphit = pHitInfo.hitsprite;
                    if (j == (1 << 30)) {
                        parseifelse(con, true);
                        break;
                    }

                    if (j > sclip) {
                        Sprite hsp = boardService.getSprite(temphit);
                        if (hsp != null && hsp.getPicnum() == g_sp.getPicnum()) {
                            j = 0;
                        } else {
                            g_sp.setAng(g_sp.getAng() + angdif);
                            j = hitasprite(g_i);
                            temphit = pHitInfo.hitsprite;
                            hsp = boardService.getSprite(temphit); // Attension!
                            g_sp.setAng(g_sp.getAng() - angdif);
                            if (j > sclip) {
                                if (hsp != null && hsp.getPicnum() == g_sp.getPicnum()) {
                                    j = 0;
                                } else {
                                    g_sp.setAng(g_sp.getAng() - angdif);
                                    j = hitasprite(g_i);
                                    temphit = pHitInfo.hitsprite;
                                    hsp = boardService.getSprite(temphit); // Attension!
                                    g_sp.setAng(g_sp.getAng() + angdif);
                                    if (j > 768) {
                                        if (hsp != null && hsp.getPicnum() == g_sp.getPicnum()) {
                                            j = 0;
                                        } else {
                                            j = 1;
                                        }
                                    } else {
                                        j = 0;
                                    }
                                }
                            } else {
                                j = 0;
                            }
                        }
                    } else {
                        j = 0;
                    }
                } else {
                    j = 1;
                }

                parseifelse(con, j != 0);
                break;
            case 91: {
                Sprite psp = boardService.getSprite(ps[g_p].i);
                if (psp == null) {
                    break;
                }

                cansee = engine.cansee(g_sp.getX(),
                        g_sp.getY(),
                        g_sp.getZ() - ((engine.krand() & 41) << 8),
                        g_sp.getSectnum(),
                        ps[g_p].posx,
                        ps[g_p].posy,
                        ps[g_p].posz, psp.getSectnum());
                parseifelse(con, cansee);
                if (cansee) {
                    hittype[g_i].timetosleep = SLEEPTIME;
                }
                break;
            }
            case 49:
                parseifelse(con, hittype[g_i].actorstayput == -1);
                break;
            case 5: {
                Sprite spr;
                if (ps[g_p].holoduke_on >= 0) {
                    spr = boardService.getSprite(ps[g_p].holoduke_on);
                    cansee = spr != null && engine.cansee(g_sp.getX(), g_sp.getY(), g_sp.getZ() - (engine.krand() & ((32 << 8) - 1)), g_sp.getSectnum(), spr.getX(), spr.getY(), spr.getZ(), spr.getSectnum());
                    if (!cansee) {
                        spr = boardService.getSprite(ps[g_p].i);
                    }
                } else {
                    spr = boardService.getSprite(ps[g_p].i);
                }

                if (spr == null) {
                    break;
                }

                cansee = engine.cansee(g_sp.getX(), g_sp.getY(), g_sp.getZ() - (engine.krand() & ((47 << 8))), g_sp.getSectnum(), spr.getX(), spr.getY(), spr.getZ() - (24 << 8), spr.getSectnum());

                if (!cansee) {
                    j = furthestcanseepoint(g_i, spr, hittype[g_i].lastvx, hittype[g_i].lastvy);

                    hittype[g_i].lastvx = furthest_x;
                    hittype[g_i].lastvy = furthest_y;

                    cansee = j != -1;
                } else {
                    hittype[g_i].lastvx = spr.getX();
                    hittype[g_i].lastvy = spr.getY();
                }

                if (cansee && (g_sp.getStatnum() == 1 || g_sp.getStatnum() == 6)) {
                    hittype[g_i].timetosleep = SLEEPTIME;
                }
                parseifelse(con, cansee);
                break;
            }

            case 6:
                parseifelse(con, ifhitbyweapon(g_i) >= 0);
                break;
            case 27:
                parseifelse(con, ifsquished(g_i, g_p));
                break;
            case 26: {
                j = g_sp.getExtra();
                if (g_sp.getPicnum() == APLAYER) {
                    j--;
                }
                parseifelse(con, j < 0);
            }
            break;
            case 24:
                insptr++;
                g_t[5] = con.script[insptr];
                g_t[4] = con.script[g_t[5]]; // Action
                g_t[1] = con.script[g_t[5] + 1]; // move
                g_sp.setHitag((short) (con.script[g_t[5] + 2])); // Ai
                g_t[0] = g_t[2] = g_t[3] = 0;
                if ((g_sp.getHitag() & random_angle) != 0) {
                    g_sp.setAng((short) (engine.krand() & 2047));
                }
                insptr++;
                break;
            case 7:
                insptr++;
                g_t[2] = 0;
                g_t[3] = 0;
                g_t[4] = con.script[insptr];
                insptr++;
                break;

            case 8:
                insptr++;
                parseifelse(con, g_x < con.script[insptr]);
                if (g_x > MAXSLEEPDIST && hittype[g_i].timetosleep == 0) {
                    hittype[g_i].timetosleep = SLEEPTIME;
                }
                break;
            case 9:
                insptr++;
                parseifelse(con, g_x > con.script[insptr]);
                if (g_x > MAXSLEEPDIST && hittype[g_i].timetosleep == 0) {
                    hittype[g_i].timetosleep = SLEEPTIME;
                }
                break;
            case 10:
                insptr = con.script[insptr + 1];
                break;
            case 100:
                insptr++;
                g_sp.setExtra(g_sp.getExtra() + con.script[insptr]);
                insptr++;
                break;
            case 11:
                insptr++;
                g_sp.setExtra((short) con.script[insptr]);
                insptr++;
                break;
            case 94:
                insptr++;

                if (ud.coop >= 1 && ud.multimode > 1) {
                    if (con.script[insptr] == 0) {
                        for (j = 0; j < ps[g_p].weapreccnt; j++) {
                            if (ps[g_p].weaprecs[j] == g_sp.getPicnum()) {
                                break;
                            }
                        }

                        parseifelse(con, j < ps[g_p].weapreccnt && g_sp.getOwner() == g_i);
                    } else if (ps[g_p].weapreccnt < 16) {
                        ps[g_p].weaprecs[ps[g_p].weapreccnt++] = g_sp.getPicnum();
                        parseifelse(con, g_sp.getOwner() == g_i);
                    }
                } else {
                    parseifelse(con, false);
                }
                break;
            case 95:
                insptr++;
                if (g_sp.getPicnum() == APLAYER) {
                    g_sp.setPal(ps[g_sp.getYvel()].palookup);
                } else {
                    if (g_sp.getPicnum() == EGG && hittype[g_i].temp_data[5] == EGG + 2 && g_sp.getPal() == 1) {
                        ps[connecthead].max_actors_killed++; // revive the egg
                        hittype[g_i].temp_data[5] = 0;
                    }
                    g_sp.setPal((short) hittype[g_i].tempang);
                }
                hittype[g_i].tempang = 0;
                break;
            case 104:
                insptr++;
                checkweapons(ps[g_sp.getYvel()]);
                break;
            case 106:
                insptr++;
                break;
            case 97:
                insptr++;
                if (Sound[g_sp.getYvel()].getSoundOwnerCount() == 0) {
                    spritesound(g_sp.getYvel(), g_i);
                }
                break;
            case 96:
                insptr++;

                if (ud.multimode > 1 && g_sp.getPicnum() == APLAYER) {
                    if (ps[otherp].quick_kick == 0) {
                        ps[otherp].quick_kick = 14;
                    }
                } else if (g_sp.getPicnum() != APLAYER && ps[g_p].quick_kick == 0) {
                    ps[g_p].quick_kick = 14;
                }
                break;
            case 28:
                insptr++;

                // JBF 20030805: As I understand it, if xrepeat becomes 0 it basically kills the
                // sprite, which is why the "sizeto 0 41" calls in 1.3d became "sizeto 4 41" in
                // 1.4, so instead of patching the CONs I'll surruptitiously patch the code here
                if (con.type == 13 && con.script[insptr] == 0) {
                    con.script[insptr] = 4;
                }

                j = ((con.script[insptr]) - g_sp.getXrepeat()) << 1;
                g_sp.setXrepeat(g_sp.getXrepeat() + ksgn(j));

                insptr++;

                if ((g_sp.getPicnum() == APLAYER && g_sp.getYrepeat() < 36) || con.script[insptr] < g_sp.getYrepeat() || ((g_sp.getYrepeat() * (engine.getTile(g_sp.getPicnum()).getHeight() + 8)) << 2) < (hittype[g_i].floorz - hittype[g_i].ceilingz)) {
                    j = ((con.script[insptr]) - g_sp.getYrepeat()) << 1;
                    if (klabs(j) != 0) {
                        g_sp.setYrepeat(g_sp.getYrepeat() + ksgn(j));
                    }
                }

                insptr++;

                break;
            case 99:
                insptr++;
                g_sp.setXrepeat((short) con.script[insptr]);
                insptr++;
                g_sp.setYrepeat((short) con.script[insptr]);
                insptr++;
                break;
            case 13:
                insptr++;
                shoot(g_i, con.script[insptr]);
                insptr++;
                break;
            case 87:
                insptr++;
                if (Sound[con.script[insptr]].getSoundOwnerCount() == 0) {
                    spritesound((short) con.script[insptr], g_i);
                }
                insptr++;
                break;
            case 89:
                insptr++;
                if (Sound[con.script[insptr]].getSoundOwnerCount() > 0) {
                    stopsound((short) con.script[insptr]);
                }
                insptr++;
                break;
            case 92:
                insptr++;
                if (g_p == screenpeek || ud.coop == 1) {
                    spritesound((short) con.script[insptr], ps[screenpeek].i);
                }
                insptr++;
                break;
            case 15:
                insptr++;
                spritesound((short) con.script[insptr], g_i);
                insptr++;
                break;
            case 84:
                insptr++;
                ps[g_p].tipincs = 26;
                break;
            case 16:
                insptr++;
                g_sp.setXoffset(0);
                g_sp.setYoffset(0);
            {
                Sector sec = boardService.getSector(g_sp.getSectnum());
                if (sec == null) {
                    break;
                }

                long c;
                if (floorspace(g_sp.getSectnum())) {
                    c = 0;
                } else {
                    if (ceilingspace(g_sp.getSectnum()) || sec.getLotag() == 2) {
                        c = con.gc / 6;
                    } else {
                        c = con.gc;
                    }
                }

                if (hittype[g_i].cgg <= 0 || (sec.getFloorstat() & 2) != 0) {
                    getglobalz(g_i);
                    hittype[g_i].cgg = 6;
                } else {
                    hittype[g_i].cgg--;
                }

                if (g_sp.getZ() < (hittype[g_i].floorz - FOURSLEIGHT)) {
                    g_sp.setZvel((int) (g_sp.getZvel() + c));
                    g_sp.setZ(g_sp.getZ() + g_sp.getZvel());

                    if (g_sp.getZvel() > 6144) {
                        g_sp.setZvel(6144);
                    }
                } else {
                    g_sp.setZ(hittype[g_i].floorz - FOURSLEIGHT);

                    if (badguy(g_sp) || (g_sp.getPicnum() == APLAYER && g_sp.getOwner() >= 0)) {

                        if (g_sp.getZvel() > 3084 && g_sp.getExtra() <= 1) {
                            if (g_sp.getPal() != 1 && g_sp.getPicnum() != DRONE) {
                                if (g_sp.getPicnum() != APLAYER || g_sp.getExtra() <= 0) {
                                    guts(g_sp, JIBS6, 15, g_p);
                                    spritesound(SQUISHED, g_i);
                                    spawn(g_i, BLOODPOOL);
                                }
                            }

                            hittype[g_i].picnum = SHOTSPARK1;
                            hittype[g_i].extra = 1;
                            g_sp.setZvel(0);
                        } else if (g_sp.getZvel() > 2048 && sec.getLotag() != 1) {
                            short pushsect = g_sp.getSectnum();
                            engine.pushmove(g_sp.getX(), g_sp.getY(), g_sp.getZ(), pushsect, 128, (4 << 8), (4 << 8), CLIPMASK0);

                            g_sp.setX(pushmove_x);
                            g_sp.setY(pushmove_y);
                            g_sp.setZ(pushmove_z);
                            pushsect = pushmove_sectnum;
                            if (pushsect != g_sp.getSectnum() && pushsect != -1) {
                                engine.changespritesect(g_i, pushsect);
                            }

                            spritesound(THUD, g_i);
                        }
                    }
                    if (sec.getLotag() == 1) {
                        switch (g_sp.getPicnum()) {
                            case OCTABRAIN:
                            case COMMANDER:
                            case DRONE:
                                break;
                            default:
                                g_sp.setZ(g_sp.getZ() + (24 << 8));
                                break;
                        }
                    } else {
                        g_sp.setZvel(0);
                    }
                }
            }

            break;
            case 4:
            case 12:
            case 18:
                return true;
            case 30:
                insptr++;
                return true;
            case 2:
                insptr++;
                int ammo = con.script[insptr];
                if (ammo < MAX_WEAPONS) {
                    if (ps[g_p].ammo_amount[con.script[insptr]] >= con.max_ammo_amount[con.script[insptr]]) {
                        killit_flag = 2;
                        break;
                    }
                    addammo(con.script[insptr], ps[g_p], con.script[insptr + 1]);
                    if (ps[g_p].curr_weapon == KNEE_WEAPON) {
                        if (ps[g_p].gotweapon[con.script[insptr]]) {
                            addweapon(ps[g_p], con.script[insptr]);
                        }
                    }
                }
                insptr += 2;
                break;
            case 86:
                insptr++;
                lotsofmoney(g_sp, con.script[insptr]);
                insptr++;
                break;
            case 102:
                insptr++;
                lotsofmail(g_sp, con.script[insptr]);
                insptr++;
                break;
            case 105:
                insptr++;
                hittype[g_i].timetosleep = (short) con.script[insptr];
                insptr++;
                break;
            case 103:
                insptr++;
                lotsofpaper(g_sp, con.script[insptr]);
                insptr++;
                break;
            case 88:
                insptr++;
                if (hittype[g_i].temp_data[5] != EGG + 2 || g_sp.getPal() != 1) {
                    ps[connecthead].actors_killed += (short) con.script[insptr];
                    if (ud.coop == 1) {
                        ps[g_p].frag += (short) con.script[insptr];
                    }
                }
                hittype[g_i].actorstayput = -1;
                insptr++;
                break;
            case 93:
                insptr++;
                spriteglass(g_i, con.script[insptr]);
                insptr++;
                break;
            case 22:
                insptr++;
                killit_flag = 1;
                break;
            case 23:
                insptr++;
                int weap = con.script[insptr];
                if (weap < MAX_WEAPONS) {
                    if (!ps[g_p].gotweapon[weap]) {
                        addweapon(ps[g_p], weap);
                    } else if (ps[g_p].ammo_amount[weap] >= con.max_ammo_amount[weap]) {
                        killit_flag = 2;
                        break;
                    }
                    addammo(weap, ps[g_p], con.script[insptr + 1]);
                    if (ps[g_p].curr_weapon == KNEE_WEAPON) {
                        if (ps[g_p].gotweapon[weap]) {
                            addweapon(ps[g_p], weap);
                        }
                    }
                }
                insptr += 2;
                break;
            case 68:
                insptr++;
                Console.out.println("" + con.script[insptr]);
                insptr++;
                break;
            case 69:
                insptr++;
                ps[g_p].timebeforeexit = (short) con.script[insptr];
                ps[g_p].customexitsound = -1;
                ud.eog = 1;
                insptr++;
                break;
            case 25: {
                insptr++;

                if (ps[g_p].newowner >= 0) {
                    ps[g_p].newowner = -1;
                    ps[g_p].posx = ps[g_p].oposx;
                    ps[g_p].posy = ps[g_p].oposy;
                    ps[g_p].posz = ps[g_p].oposz;
                    ps[g_p].ang = ps[g_p].oang;
                    ps[g_p].cursectnum = engine.updatesector(ps[g_p].posx, ps[g_p].posy, ps[g_p].cursectnum);

                    setpal(ps[g_p]);

                    for (ListNode<Sprite> node = boardService.getStatNode(1); node != null; node = node.getNext()) {
                        Sprite sp = node.get();
                        if (sp.getPicnum() == CAMERA1) {
                            sp.setYvel(0);
                        }
                    }
                }
                Sprite psp = boardService.getSprite(ps[g_p].i);
                if (psp == null) {
                    break;
                }

                j = psp.getExtra();
                if (g_sp.getPicnum() != ATOMICHEALTH) {
                    if (j > con.max_player_health && con.script[insptr] > 0) {
                        insptr++;
                        break;
                    } else {
                        if (j > 0) {
                            j += con.script[insptr];
                        }
                        if (j > con.max_player_health && con.script[insptr] > 0) {
                            j = con.max_player_health;
                        }
                    }
                } else {
                    if (j > 0) {
                        j += con.script[insptr];
                    }
                    if (j > (con.max_player_health << 1)) {
                        j = (con.max_player_health << 1);
                    }
                }

                if (j < 0) {
                    j = 0;
                }

                if (!ud.god) {
                    if (con.script[insptr] > 0) {
                        if ((j - con.script[insptr]) < (con.max_player_health >> 2) && j >= (con.max_player_health >> 2)) {
                            spritesound(DUKE_GOTHEALTHATLOW, ps[g_p].i);
                        }

                        ps[g_p].last_extra = (short) j;
                    }

                    psp.setExtra((short) j);
                }

                insptr++;
                break;
            }
            case 17: {
                int tempscrptr = insptr + 2;
                insptr = con.script[insptr + 1];
                while (true) {
                    if (parse(con)) {
                        break;
                    }
                }
                insptr = tempscrptr;
            }
            break;
            case 29:
                insptr++;
                while (true) {
                    if (parse(con)) {
                        break;
                    }
                }
                break;
            case 32:
                g_t[0] = 0;
                insptr++;
                g_t[1] = con.script[insptr];
                insptr++;
                g_sp.setHitag((short) con.script[insptr]);
                insptr++;
                if ((g_sp.getHitag() & random_angle) != 0) {
                    g_sp.setAng((short) (engine.krand() & 2047));
                }
                break;
            case 31: {
                insptr++;
                Sector g_sec = boardService.getSector(g_sp.getSectnum());
                if (g_sec != null) {
                    if (g_sp.getPicnum() == EGG && con.script[insptr] == GREENSLIME) {
                        hittype[g_i].temp_data[5] = EGG + 2;
                    }
                    spawn(g_i, con.script[insptr]);
                }
                insptr++;
                break;
            }
            case 33: // ifwasweapon
            case 59: // ifspawnedby
                insptr++;
                parseifelse(con, hittype[g_i].picnum == con.script[insptr]);
                break;
            case 21:
                insptr++;
                parseifelse(con, g_t[5] == con.script[insptr]);
                break;
            case 34:
                insptr++;
                parseifelse(con, g_t[4] == con.script[insptr]);
                break;
            case 35:
                insptr++;
                parseifelse(con, g_t[2] >= con.script[insptr]);
                break;
            case 36:
                insptr++;
                g_t[2] = 0;
                break;
            case 37: {
                int dnum;

                insptr++;
                dnum = con.script[insptr];
                insptr++;

                Sector g_sec = boardService.getSector(g_sp.getSectnum());
                if (g_sec != null) {
                    for (j = (con.script[insptr]) - 1; j >= 0; j--) {
                        if (g_sp.getPicnum() == BLIMP && dnum == SCRAP1) {
                            s = 0;
                        } else {
                            s = (engine.krand() % 3);
                        }

                        int vz = -(engine.krand() & 2047);
                        int ve = (engine.krand() & 127) + 32;
                        int va = engine.krand() & 2047;
                        int vy = 32 + (engine.krand() & 15);
                        int vx = 32 + (engine.krand() & 15);
                        int sz = g_sp.getZ() - (8 << 8) - (engine.krand() & 8191);
                        int sy = g_sp.getY() + (engine.krand() & 255) - 128;
                        int sx = g_sp.getX() + (engine.krand() & 255) - 128;

                        l = EGS(g_sp.getSectnum(), sx, sy, sz, dnum + s, g_sp.getShade(), vx, vy, va, ve, vz, g_i, (short) 5);
                        Sprite sp = boardService.getSprite(l);
                        if (sp != null) {
                            if (g_sp.getPicnum() == BLIMP && dnum == SCRAP1) {
                                sp.setYvel(weaponsandammosprites[j % 14]);
                            } else {
                                sp.setYvel(-1);
                            }
                            sp.setPal(g_sp.getPal());
                        }
                    }
                }
                insptr++;
            }
            break;
            case 52:
                insptr++;
                g_t[0] = (short) con.script[insptr];
                insptr++;
                break;
            case 101:
                insptr++;
                g_sp.setCstat(g_sp.getCstat() | (short) con.script[insptr]);
                insptr++;
                break;
            case 110:
                insptr++;
                g_sp.setClipdist((short) con.script[insptr]);
                insptr++;
                break;
            case 40:
                insptr++;
                g_sp.setCstat((short) con.script[insptr]);
                insptr++;
                break;
            case 41:
                insptr++;
                parseifelse(con, g_t[1] == con.script[insptr]);
                break;
            case 112: // Twentieth Anniversary World Tour
                parseifelse(con, false);
                break;
            case 42:
                insptr++;
                if (ud.multimode < 2) {
                    if (DemoScreen.isDemoPlaying()) {
                        break;
                    }

                    if (!gGameScreen.enterlevel(gGameScreen.getTitle(), ud.volume_number, ud.level_number)) {
                        game.show();
                        return false;
                    }
                    killit_flag = 2;
                } else {
                    pickrandomspot(g_p);
                    game.pInt.clearspriteinterpolate(g_i);
                    game.pInt.setsprinterpolate(g_i, boardService.getSprite(ps[g_p].i));
                    g_sp.setX(ps[g_p].bobposx = ps[g_p].oposx = ps[g_p].posx);
                    g_sp.setY(ps[g_p].bobposy = ps[g_p].oposy = ps[g_p].posy);
                    g_sp.setZ(ps[g_p].oposz = ps[g_p].posz);
                    ps[g_p].cursectnum = engine.updatesector(ps[g_p].posx, ps[g_p].posy, ps[g_p].cursectnum);
                    engine.setsprite(ps[g_p].i, ps[g_p].posx, ps[g_p].posy, ps[g_p].posz + PHEIGHT);
                    g_sp.setCstat(257);

                    g_sp.setShade(-12);
                    g_sp.setClipdist(64);
                    g_sp.setXrepeat(42);
                    g_sp.setYrepeat(36);
                    g_sp.setOwner(g_i);
                    g_sp.setXoffset(0);
                    g_sp.setPal(ps[g_p].palookup);
                    g_sp.setExtra(con.max_player_health);
                    ps[g_p].last_extra = (short) con.max_player_health;
                    ps[g_p].wantweaponfire = -1;
                    ps[g_p].horiz = 100;
                    ps[g_p].on_crane = -1;
                    ps[g_p].frag_ps = g_p;
                    ps[g_p].horizoff = 0;
                    ps[g_p].opyoff = 0;
                    ps[g_p].wackedbyactor = -1;
                    ps[g_p].shield_amount = (short) con.max_armour_amount;
                    ps[g_p].dead_flag = 0;
                    ps[g_p].pals_time = 0;
                    ps[g_p].footprintcount = 0;
                    ps[g_p].weapreccnt = 0;
                    ps[g_p].posxv = ps[g_p].posyv = 0;
                    ps[g_p].rotscrnang = 0;

                    ps[g_p].falling_counter = 0;

                    hittype[g_i].extra = -1;
                    hittype[g_i].owner = g_i;

                    hittype[g_i].cgg = 0;
                    hittype[g_i].movflag = 0;
                    hittype[g_i].tempang = 0;
                    hittype[g_i].actorstayput = -1;
                    hittype[g_i].dispicnum = 0;
                    hittype[g_i].owner = ps[g_p].i;

                    resetinventory(g_p);
                    resetweapons(g_p);

                    fta = 0;
                    ftq = 0;
                    cameradist = 0;
                    cameraclock = engine.getTotalClock();
                }
                setpal(ps[g_p]);

                break;
            case 43: {
                Sector sec = boardService.getSector(g_sp.getSectnum());
                if (sec != null) {
                    parseifelse(con, klabs(g_sp.getZ() - sec.getFloorz()) < (32 << 8) && sec.getLotag() == 1);
                }
                break;
            }
            case 44: {
                Sector sec = boardService.getSector(g_sp.getSectnum());
                if (sec != null) {
                    parseifelse(con, sec.getLotag() == 2);
                }
                break;
            }
            case 46: {
                insptr++;
                parseifelse(con, g_t[0] >= con.script[insptr]);
                break;
            }
            case 53:
                insptr++;
                parseifelse(con, g_sp.getPicnum() == con.script[insptr]);
                break;
            case 47:
                insptr++;
                g_t[0] = 0;
                break;
            case 48:
                insptr += 2;
                switch (con.script[insptr - 1]) {
                    case 0:
                        ps[g_p].steroids_amount = (short) con.script[insptr];
                        ps[g_p].inven_icon = 2;
                        break;
                    case 1:
                        ps[g_p].shield_amount += (short) con.script[insptr]; // 100;
                        if (ps[g_p].shield_amount > con.max_player_health) {
                            ps[g_p].shield_amount = (short) con.max_player_health;
                        }
                        break;
                    case 2:
                        ps[g_p].scuba_amount = (short) con.script[insptr];// 1600;
                        ps[g_p].inven_icon = 6;
                        break;
                    case 3:
                        ps[g_p].holoduke_amount = (short) con.script[insptr];// 1600;
                        ps[g_p].inven_icon = 3;
                        break;
                    case 4:
                        ps[g_p].jetpack_amount = (short) con.script[insptr];// 1600;
                        ps[g_p].inven_icon = 4;
                        break;
                    case 6:
                        switch (g_sp.getPal()) {
                            case 0:
                                ps[g_p].got_access |= 1;
                                break;
                            case 21:
                                ps[g_p].got_access |= 2;
                                break;
                            case 23:
                                ps[g_p].got_access |= 4;
                                break;
                        }
                        break;
                    case 7:
                        ps[g_p].heat_amount = (short) con.script[insptr];
                        ps[g_p].inven_icon = 5;
                        break;
                    case 9:
                        ps[g_p].inven_icon = 1;
                        ps[g_p].firstaid_amount = (short) con.script[insptr];
                        break;
                    case 10:
                        ps[g_p].inven_icon = 7;
                        ps[g_p].boot_amount = (short) con.script[insptr];
                        break;
                }
                insptr++;
                break;
            case 50:
                hitradius(g_i, con.script[insptr + 1], con.script[insptr + 2], con.script[insptr + 3], con.script[insptr + 4], con.script[insptr + 5]);
                insptr += 6;
                break;
            case 51: {
                insptr++;

                l = con.script[insptr];
                j = 0;

                s = g_sp.getXvel();
                Sprite psp = boardService.getSprite(ps[g_p].i);

                if (((l & 8) != 0) && ps[g_p].on_ground && (sync[g_p].bits & 2) != 0) {
                    j = 1;
                } else if (((l & 16) != 0) && ps[g_p].jumping_counter == 0 && !ps[g_p].on_ground && ps[g_p].poszv > 2048) {
                    j = 1;
                } else if (((l & 32) != 0) && ps[g_p].jumping_counter > 348) {
                    j = 1;
                } else if (((l & 1) != 0) && s >= 0 && s < 8) {
                    j = 1;
                } else if (((l & 2) != 0) && s >= 8 && (sync[g_p].bits & (1 << 5)) == 0) {
                    j = 1;
                } else if (((l & 4) != 0) && s >= 8 && (sync[g_p].bits & (1 << 5)) != 0) {
                    j = 1;
                } else if (((l & 64) != 0) && ps[g_p].posz < (g_sp.getZ() - (48 << 8))) {
                    j = 1;
                } else if (((l & 128) != 0) && s <= -8 && (sync[g_p].bits & (1 << 5)) == 0) {
                    j = 1;
                } else if (((l & 256) != 0) && s <= -8 && (sync[g_p].bits & (1 << 5)) != 0) {
                    j = 1;
                } else if (((l & 512) != 0) && (ps[g_p].quick_kick > 0 || (ps[g_p].curr_weapon == KNEE_WEAPON && ps[g_p].kickback_pic > 0))) {
                    j = 1;
                } else if (((l & 1024) != 0) && psp != null && psp.getXrepeat() < 32) {
                    j = 1;
                } else if (((l & 2048) != 0) && ps[g_p].jetpack_on != 0) {
                    j = 1;
                } else if (((l & 4096) != 0) && ps[g_p].steroids_amount > 0 && ps[g_p].steroids_amount < 400) {
                    j = 1;
                } else if (((l & 8192) != 0) && ps[g_p].on_ground) {
                    j = 1;
                } else if (((l & 16384) != 0) && ps[g_p].getPlayerSprite().getXrepeat() > 32 && ps[g_p].getPlayerSprite().getExtra() > 0 && ps[g_p].timebeforeexit == 0) {
                    j = 1;
                } else if (((l & 32768) != 0) && psp != null && psp.getExtra() <= 0) {
                    j = 1;
                } else if (((l & 65536)) != 0) {
                    if (g_sp.getPicnum() == APLAYER && ud.multimode > 1) {
                        j = getincangle((int) ps[otherp].ang, EngineUtils.getAngle(ps[g_p].posx - ps[otherp].posx, ps[g_p].posy - ps[otherp].posy));
                    } else {
                        j = getincangle((int) ps[g_p].ang, EngineUtils.getAngle(g_sp.getX() - ps[g_p].posx, g_sp.getY() - ps[g_p].posy));
                    }

                    if (j > -128 && j < 128) {
                        j = 1;
                    } else {
                        j = 0;
                    }
                }

                parseifelse(con, j != 0);

            }
            break;
            case 56:
                insptr++;
                parseifelse(con, g_sp.getExtra() <= con.script[insptr]);
                break;
            case 58:
                insptr += 2;
                guts(g_sp, con.script[insptr - 1], con.script[insptr], g_p);
                insptr++;
                break;
            case 61:
                insptr++;
                forceplayerangle(ps[g_p]);
                return false;
            case 62:
                insptr++;
                parseifelse(con, ((hittype[g_i].floorz - hittype[g_i].ceilingz) >> 8) < con.script[insptr]);
                break;
            case 63:
                parseifelse(con, (sync[g_p].bits & (1 << 29)) != 0);
                break;
            case 64: {
                Sector sec = boardService.getSector(g_sp.getSectnum());
                if (sec != null) {
                    parseifelse(con, (sec.getCeilingstat() & 1) != 0);
                }
            }
            break;
            case 65:
                parseifelse(con, ud.multimode > 1);
                break;
            case 66:
                insptr++;
                Sector sec = boardService.getSector(g_sp.getSectnum());
                if (sec != null) {
                    if (sec.getLotag() == 0) {
                        neartag(g_sp.getX(), g_sp.getY(), g_sp.getZ() - (32 << 8), g_sp.getSectnum(), g_sp.getAng(), 768, 1);
                        Sector nearsec = boardService.getSector(neartagsector);
                        if (nearsec != null && isanearoperator(nearsec.getLotag())) {
                            if ((nearsec.getLotag() & 0xff) == 23 || nearsec.getFloorz() == nearsec.getCeilingz()) {
                                if ((nearsec.getLotag() & 16384) == 0) {
                                    if ((nearsec.getLotag() & 32768) == 0) {
                                        ListNode<Sprite> node = boardService.getSectNode(neartagsector);
                                        while (node != null) {
                                            Sprite sp = node.get();
                                            if (sp.getPicnum() == ACTIVATOR) {
                                                break;
                                            }
                                            node = node.getNext();
                                        }

                                        if (node == null) {
                                            operatesectors(neartagsector, g_i);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case 67:
                parseifelse(con, ceilingspace(g_sp.getSectnum()));
                break;

            case 74:
                insptr++;
                if (g_sp.getPicnum() != APLAYER) {
                    hittype[g_i].tempang = g_sp.getPal();
                }
                g_sp.setPal(con.script[insptr]);
                insptr++;
                break;

            case 77:
                insptr++;
                g_sp.setPicnum(con.script[insptr]);
                insptr++;
                break;

            case 70:
                parseifelse(con, dodge(g_sp));
                break;
            case 71:
                if (badguy(g_sp)) {
                    parseifelse(con, ud.respawn_monsters);
                } else if (inventory(g_sp)) {
                    parseifelse(con, ud.respawn_inventory);
                } else {
                    parseifelse(con, ud.respawn_items);
                }
                break;
            case 72:
                insptr++;
                parseifelse(con, (hittype[g_i].floorz - g_sp.getZ()) <= ((con.script[insptr]) << 8));
                break;
            case 73:
                insptr++;
                parseifelse(con, (g_sp.getZ() - hittype[g_i].ceilingz) <= ((con.script[insptr]) << 8));
                break;
            case 14:

                insptr++;
                ps[g_p].pals_time = con.script[insptr];
                insptr++;
                for (j = 0; j < 3; j++) {
                    ps[g_p].pals[j] = (short) con.script[insptr];
                    insptr++;
                }
                break;
            case 78:
                insptr++;
                parseifelse(con, ps[g_p].getPlayerSprite().getExtra() < con.script[insptr]);
                break;

            case 75: {
                insptr++;
                j = 0;
                switch (con.script[insptr++]) {
                    case 0:
                        if (ps[g_p].steroids_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 1:
                        if (ps[g_p].shield_amount != con.max_player_health) {
                            j = 1;
                        }
                        break;
                    case 2:
                        if (ps[g_p].scuba_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 3:
                        if (ps[g_p].holoduke_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 4:
                        if (ps[g_p].jetpack_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 6:
                        switch (g_sp.getPal()) {
                            case 0:
                                if ((ps[g_p].got_access & 1) != 0) {
                                    j = 1;
                                }
                                break;
                            case 21:
                                if ((ps[g_p].got_access & 2) != 0) {
                                    j = 1;
                                }
                                break;
                            case 23:
                                if ((ps[g_p].got_access & 4) != 0) {
                                    j = 1;
                                }
                                break;
                        }
                        break;
                    case 7:
                        if (ps[g_p].heat_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 9:
                        if (ps[g_p].firstaid_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                    case 10:
                        if (ps[g_p].boot_amount != con.script[insptr]) {
                            j = 1;
                        }
                        break;
                }

                parseifelse(con, j != 0);
                break;
            }
            case 38: {
                insptr++;
                Sprite psp = boardService.getSprite(ps[g_p].i);
                if (psp != null && ps[g_p].knee_incs == 0 && psp.getXrepeat() >= 40) {
                    if (engine.cansee(g_sp.getX(), g_sp.getY(), g_sp.getZ() - (4 << 8), g_sp.getSectnum(), ps[g_p].posx, ps[g_p].posy, ps[g_p].posz + (16 << 8), psp.getSectnum())) {
                        ps[g_p].knee_incs = 1;
                        if (ps[g_p].weapon_pos == 0) {
                            ps[g_p].weapon_pos = -1;
                        }
                        ps[g_p].actorsqu = g_i;
                    }
                }
                break;
            }
            case 90: {
                int s1;

                s1 = g_sp.getSectnum();

                j = 0;

                s1 = engine.updatesector(g_sp.getX() + 108, g_sp.getY() + 108, s1);
                if (s1 == g_sp.getSectnum()) {
                    s1 = engine.updatesector(g_sp.getX() - 108, g_sp.getY() - 108, s1);
                    if (s1 == g_sp.getSectnum()) {
                        s1 = engine.updatesector(g_sp.getX() + 108, g_sp.getY() - 108, s1);
                        if (s1 == g_sp.getSectnum()) {
                            s1 = engine.updatesector(g_sp.getX() - 108, g_sp.getY() + 108, s1);
                            if (s1 == g_sp.getSectnum()) {
                                j = 1;
                            }
                        }
                    }
                }
                parseifelse(con, j != 0);
            }

            break;
            case 80:
                insptr++;
                FTA(con.script[insptr], ps[g_p]);
                insptr++;
                break;
            case 81:
                parseifelse(con, floorspace(g_sp.getSectnum()));
                break;
            case 82:
                // hit wall or sprite
                int hitType = hittype[g_i].movflag & HIT_TYPE_MASK;
                parseifelse(con, hitType == HIT_WALL || hitType == HIT_SPRITE);
                break;
            case 83:
                insptr++;
                switch (g_sp.getPicnum()) {
                    case FEM1:
                    case FEM2:
                    case FEM3:
                    case FEM4:
                    case FEM5:
                    case FEM6:
                    case FEM7:
                    case FEM8:
                    case FEM9:
                    case FEM10:
                    case PODFEM1:
                    case NAKED1:
                    case STATUE:
                        if (g_sp.getYvel() != 0) {
                            operaterespawns(g_sp.getYvel());
                        }
                        break;
                    default:
                        if (g_sp.getHitag() >= 0) {
                            operaterespawns(g_sp.getHitag());
                        }
                        break;
                }
                break;
            case 85:
                insptr++;
                parseifelse(con, g_sp.getPal() == con.script[insptr]);
                break;

            case 111:
                insptr++;
                j = klabs((int) getincangle(ps[g_p].ang, g_sp.getAng()));
                parseifelse(con, j <= con.script[insptr]);
                break;

            case 109:

                for (j = 1; j < NUM_SOUNDS; j++) {
                    if (Sound[j].getSoundOwner(0).i == g_i) {
                        break;
                    }
                }

                parseifelse(con, j == NUM_SOUNDS);
                break;
            default:
                killit_flag = 1;
                break;
        }
        return false;
    }

    public static void execute(Script con, int i, int p, int x) {
        boolean done;

        g_i = (short) i;
        g_p = (short) p;
        g_x = x;
        g_sp = boardService.getSprite(g_i);
        g_t = hittype[g_i].temp_data;

        if (g_sp == null) {
            return;
        }

        if (con.actorscrptr[g_sp.getPicnum()] == 0) {
            return;
        }

        insptr = 4 + con.actorscrptr[g_sp.getPicnum()];

        killit_flag = 0;

        Sector g_sec = boardService.getSector(g_sp.getSectnum());
        if (g_sec == null) {
            if (badguy(g_sp)) {
                ps[connecthead].actors_killed++;
                if (ud.coop == 1) {
                    ps[g_p].frag++;
                }
            }

            engine.deletesprite(g_i);
            return;
        }

        if (g_t[4] != 0) {
            g_sp.setLotag(g_sp.getLotag() + TICSPERFRAME);
            if (g_sp.getLotag() > con.script[g_t[4] + 4]) {
                g_t[2]++;
                g_sp.setLotag(0);
                g_t[3] += con.script[g_t[4] + 3];
            }
            if (klabs(g_t[3]) >= klabs(con.script[g_t[4] + 1] * con.script[g_t[4] + 3])) {
                g_t[3] = 0;
            }
        }

        do {
            done = parse(con);
        } while (!done);

        if (killit_flag == 1) {
            if (ps[g_p].actorsqu == g_i) {
                ps[g_p].actorsqu = -1;
            }
            engine.deletesprite(g_i);
        } else {
            move(con);
            if (g_sp.getStatnum() == 1) {
                if (badguy(g_sp)) {
                    if (g_sp.getXrepeat() > 60) {
                        return;
                    }
                    if (ud.respawn_monsters && g_sp.getExtra() <= 0) {
                        return;
                    }
                } else if (ud.respawn_items && (g_sp.getCstat() & 32768) != 0) {
                    return;
                }

                if (hittype[g_i].timetosleep > 1) {
                    hittype[g_i].timetosleep--;
                } else if (hittype[g_i].timetosleep == 1) {
                    engine.changespritestat(g_i, (short) 2);
                }
            } else if (g_sp.getStatnum() == 6) {
                switch (g_sp.getPicnum()) {
                    case RUBBERCAN:
                    case EXPLODINGBARREL:
                    case WOODENHORSE:
                    case HORSEONSIDE:
                    case CANWITHSOMETHING:
                    case FIREBARREL:
                    case NUKEBARREL:
                    case NUKEBARRELDENTED:
                    case NUKEBARRELLEAKED:
                    case TRIPBOMB:
                    case EGG:
                        if (hittype[g_i].timetosleep > 1) {
                            hittype[g_i].timetosleep--;
                        } else if (hittype[g_i].timetosleep == 1) {
                            engine.changespritestat(g_i, (short) 2);
                        }
                        break;
                }
            }
        }
    }

    public static void compilecons() throws InitializationException {
        Script con = loadefs(confilename);

        try {
            FileEntry grpEntry = game.getCache().getGameDirectory().getEntry(game.mainGrp);
            if (!grpEntry.exists()) {
                throw new FileNotFoundException();
            }

            if (game.isGameType(GameType.NAM)) {
                Entry conEntry = game.getCache().getEntry(confilename, true);
                if (conEntry.exists()) {
                    GrpFile group = (GrpFile) game.getCache().getGroup(game.mainGrp);
                    group.addEntry(new UserEntry(conEntry));
                }
            }

            List<EpisodeEntry> entryList = episodeManager.getEpisodeEntries(grpEntry);
            EpisodeEntry entry = entryList.get(0);
            if (!entry.getConFile().getName().equalsIgnoreCase(confilename)) {
                throw new RuntimeException("Can't initialize script file: " + confilename);
            }
            defGame = episodeManager.getEpisode(entry);
        } catch (Exception e) {
            throw new RuntimeException("[ " + e.getClass().getSimpleName() + "]: " + "Unknown error!", e);
        }

        switch (con.type) {
            case 13:
                Console.out.println("Looks like Standard CON files.");

                buildString(con.volume_names[0], 0, "L.A. MELTDOWN");
                buildString(con.volume_names[1], 0, "LUNAR APOCALYPSE");
                buildString(con.volume_names[2], 0, "SHRAPNEL CITY");

                buildString(con.skill_names[0], 0, "PIECE OF CAKE");
                buildString(con.skill_names[1], 0, "LET'S ROCK");
                buildString(con.skill_names[2], 0, "COME GET SOME");
                buildString(con.skill_names[3], 0, "DAMN I'M GOOD");
                con.nEpisodes = 3;
                con.nSkills = 4;
                break;
            case 14:
                con.PLUTOPAK = true;
                Console.out.println("Looks like Atomic Edition CON files.");
                break;
            case 20:
                con.PLUTOPAK = true;
                Console.out.println("Looks like Twentieth Anniversary World Tour CON files.");
                handleWTCons(con);
                break;
        }

        defGame.setCON(con);
        defGame.title = "Default";

        // Create episode info
        defGame.nEpisodes = con.nEpisodes;
        for (int i = 0; i < con.nEpisodes; i++) {
            defGame.episodes[i] = new EpisodeInfo(new String(con.volume_names[i]).trim());
            defGame.episodes[i].nMaps = con.nMaps[i];
            for (int j = 0; j < con.nMaps[i]; j++) {
                Path path = FileUtils.getPath(new String(con.level_file_names[i * 11 + j]).trim());
                if (!defGame.episodes[i].setMapInfo(j, new MapInfo(path, new String(con.level_names[i * 11 + j]).trim(), con.partime[i * 11 + j], con.designertime[i * 11 + j]))) {
                    Console.out.println("Warning! Can't set map info for index " + j, OsdColor.YELLOW); // #GDX 31.12.2024
                }
            }
        }

        for (int i = 0; i < con.nSkills; i++) {
            defGame.skillnames[i] = new String(con.skill_names[i]).trim();
        }

        currentGame = defGame;
    }

    private static void handleWTCons(Script con) {
        Console.out.println("Trying to open an english locale file");
        Entry res = game.getCache().getGameDirectory().getEntry(FileUtils.getPath("locale", "english", "strings.txt"));
        if (res.exists()) {
            Console.out.println("Parsing the locale file");
            LocaleScript scr = new LocaleScript(res);

            for (int i = 0; i < NUMOFFIRSTTIMEACTIVE; i++) // quotes
            {
                String quote = scr.getLocale(toLowerCase(new String(con.fta_quotes[i]).trim()));
                if (quote != null) {
                    int len = Math.min(con.fta_quotes[i].length - 1, quote.length());
                    quote.getChars(0, len, con.fta_quotes[i], 0);
                    con.fta_quotes[i][len] = '\0';
                }
            }

            for (int i = 0; i < con.volume_names.length; i++) // episode names
            {
                String quote = scr.getLocale(toLowerCase(new String(con.volume_names[i]).trim()));
                if (quote != null) {
                    int len = Math.min(con.volume_names[i].length - 1, quote.length());
                    quote.getChars(0, len, con.volume_names[i], 0);
                    con.volume_names[i][len] = '\0';
                }
            }

            for (int i = 0; i < con.level_names.length; i++) // level names
            {
                String quote = scr.getLocale(toLowerCase(new String(con.level_names[i]).trim()));
                if (quote != null) {
                    int len = Math.min(con.level_names[i].length - 1, quote.length());
                    quote.getChars(0, len, con.level_names[i], 0);
                    con.level_names[i][len] = '\0';
                }
            }
        }
    }

    public static byte[] preparescript(byte[] buf) {
        if (buf == null) {
            return null;
        }

        int index = -1;
        while ((index = indexOf("//", buf, index + 1)) != -1) {
            int textptr = index + 2;
            while (buf[textptr] != 0x0a) {
                buf[textptr] = 0;
                textptr++;
                if (textptr >= buf.length) {
                    return buf;
                }
            }
        }

        while ((index = indexOf("/*", buf, index + 1)) != -1) {
            int textptr = index + 2;
            do {
                buf[textptr] = 0;
                textptr++;
                if (textptr >= buf.length) {
                    return buf;
                }
            } while (buf[textptr] != '*' || buf[textptr + 1] != '/');
        }

        return buf;
    }

    // For user episodes

    public static Script loaduserdef(Entry fp) {
        if (!fp.exists()) {
            return null;
        }
        int fs = (int) fp.getSize();
        Console.out.println("Compiling: " + fp.getName() + ".");

        byte[] buf = new byte[fs + 1];
        label = new char[131072];

        System.arraycopy(fp.getBytes(), 0, buf, 0, fs);

        parsing_actor = 0;
        parsing_state = 0;
        num_squigilly_brackets = 0;
        checking_ifelse = 0;
        killit_flag = 0;

        textptr = 0;
        last_used_text = new String(buf);
        last_used_size = fs;
        text = last_used_text.toCharArray();
        if (fs < text.length) {
            text[fs] = 0;
        }

        Script con = new Script();

        Arrays.fill(con.actorscrptr, 0);
        Arrays.fill(con.actortype, (short) 0);

        labelcode.clear();
        labelcnt = 0;
        scriptptr = 1;
        warning = 0;
        error = 0;
        line_number = 1;

        try {
            passone(con); // Tokenize
        } catch (Exception e) {
            e.printStackTrace();
            error = 1;
            return null;
        }

        switch (con.type) {
            case 13:
                Console.out.println("Looks like Standard CON files.");

                buildString(con.volume_names[0], 0, "L.A. MELTDOWN");
                buildString(con.volume_names[1], 0, "LUNAR APOCALYPSE");
                buildString(con.volume_names[2], 0, "SHRAPNEL CITY");

                buildString(con.skill_names[0], 0, "PIECE OF CAKE");
                buildString(con.skill_names[1], 0, "LET'S ROCK");
                buildString(con.skill_names[2], 0, "COME GET SOME");
                buildString(con.skill_names[3], 0, "DAMN I'M GOOD");
                break;
            case 14:
                con.PLUTOPAK = true;
                Console.out.println("Looks like Atomic Edition CON files.");
                break;
            case 20:
                con.PLUTOPAK = true;
                Console.out.println("Looks like Twentieth Anniversary World Tour CON files.");
                handleWTCons(con);
                break;
        }

        if ((warning | error) != 0) {
            Console.out.println("Found " + warning + " warning(s), " + error + " error(s).");
        }

        if (error != 0) {
            return null;
        } else {
            Console.out.println("Code Size:" + (((scriptptr) << 2) - 4) + " bytes(" + labelcnt + " labels).");
        }

        return con;
    }

    private static class LocaleScript extends Scriptfile {

        private final HashMap<String, String> locale;

        public LocaleScript(Entry data) {
            super("Locale", data);

            locale = new HashMap<>();

            while (!eof()) {
                String tag = getstring();
                String text = getstring();
                if (tag != null && text != null) {
                    locale.put(tag.trim(), text);
                }
            }
        }

        public String getLocale(String txt) {
            return locale.get(txt);
        }
    }
}
