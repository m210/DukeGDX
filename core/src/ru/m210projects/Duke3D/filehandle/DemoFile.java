// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.filehandle;

import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Duke3D.Input;
import ru.m210projects.Duke3D.Types.GameInfo;

import java.io.InputStream;

import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.game;
import static ru.m210projects.Duke3D.Types.SafeLoader.findAddon;

public class DemoFile {

    public int rcnt;
    public Input[][] recsync;
    public int reccnt;
    public int version;
    public int volume_number, level_number, player_skill;
    public int coop;
    public int ffire;
    public int multimode;
    public boolean monsters_off;
    public boolean respawn_monsters;
    public boolean respawn_items;
    public boolean respawn_inventory;
    public int playerai;
    public final String[] user_name = new String[MAXPLAYERS];
    public int auto_run;
    public String boardfilename;
    public final int[] aim_mode = new int[MAXPLAYERS];
    public final int[] auto_aim = new int[MAXPLAYERS];

    public int[] armor;
    public int[] health;
    public boolean[][] gotweapon;
    public int[][] ammo_amount;
    public int[] firstaid_amount;
    public int[] steroids_amount;
    public int[] holoduke_amount;
    public int[] jetpack_amount;
    public int[] heat_amount;
    public int[] scuba_amount;
    public int[] boot_amount;
    public int[] inven_icon;
    public GameInfo addon;

    public DemoFile(InputStream is) throws Exception {
        rcnt = 0;

        reccnt = StreamUtils.readInt(is);
        version = StreamUtils.readUnsignedByte(is);

        if (version != BYTEVERSION15 && version != BYTEVERSION15 + 1 && version != JFBYTEVERSION && version != GDXBYTEVERSION) { // || (ud.reccnt < 512) )
            throw new Exception("Wrong version!");
        }

        if (reccnt == 0) {
            throw new Exception("reccnt == 0");
        }

        volume_number = StreamUtils.readUnsignedByte(is);
        level_number = StreamUtils.readUnsignedByte(is);
        player_skill = StreamUtils.readUnsignedByte(is);

        coop = StreamUtils.readUnsignedByte(is);
        ffire = StreamUtils.readUnsignedByte(is);
        multimode = StreamUtils.readShort(is);
        monsters_off = StreamUtils.readShort(is) == 1;
        respawn_monsters = StreamUtils.readInt(is) == 1;
        respawn_items = StreamUtils.readInt(is) == 1;
        respawn_inventory = StreamUtils.readInt(is) == 1;
        playerai = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPLAYERS; i++) {
            user_name[i] = StreamUtils.readString(is, 32);
        }
        auto_run = StreamUtils.readInt(is);
        int MAX_PATH = 128;
        if (version >= JFBYTEVERSION) {
            MAX_PATH = 260;
        }

        String name = FileUtils.getCorrectPath(StreamUtils.readString(is, MAX_PATH));
        if (!name.isEmpty() && game.getCache().getEntry(name, true).exists()) {
            boardfilename = name;
            level_number = 7;
            volume_number = 0;
        }

        if (version == GDXBYTEVERSION) {
            boolean isPacked = StreamUtils.readBoolean(is);
            String addonFileName = StreamUtils.readDataString(is).toLowerCase();
            String addonPackedConName = null;
            if (isPacked) {
                addonPackedConName = StreamUtils.readDataString(is).toLowerCase();
            }
            addon = findAddon(addonFileName, addonPackedConName);

            armor = new int[multimode];
            health = new int[multimode];
            gotweapon = new boolean[multimode][MAX_WEAPONS];
            ammo_amount = new int[multimode][MAX_WEAPONS];
            firstaid_amount = new int[multimode];
            steroids_amount = new int[multimode];
            holoduke_amount = new int[multimode];
            jetpack_amount = new int[multimode];
            heat_amount = new int[multimode];
            scuba_amount = new int[multimode];
            boot_amount = new int[multimode];
            inven_icon = new int[multimode];

            for (int i = 0; i < multimode; i++) {
                armor[i] = StreamUtils.readInt(is);
                health[i] = StreamUtils.readInt(is);
                for(int j = 0; j < MAX_WEAPONS; j++) {
                    gotweapon[i][j] = StreamUtils.readBoolean(is);
                    ammo_amount[i][j] = StreamUtils.readInt(is);
                }
                firstaid_amount[i] = StreamUtils.readInt(is);
                steroids_amount[i] = StreamUtils.readInt(is);
                holoduke_amount[i] = StreamUtils.readInt(is);
                jetpack_amount[i] = StreamUtils.readInt(is);
                heat_amount[i] = StreamUtils.readInt(is);
                scuba_amount[i] = StreamUtils.readInt(is);
                boot_amount[i] = StreamUtils.readInt(is);
                inven_icon[i] = StreamUtils.readInt(is);
            }
        }

        for (int i = 0; i < multimode; i++) {
            aim_mode[i] = StreamUtils.readUnsignedByte(is);
            if (version >= JFBYTEVERSION) {
                auto_aim[i] = StreamUtils.readUnsignedByte(is);
            }
        }

        recsync = new Input[reccnt][MAXPLAYERS];
        LZWInputStream lis = new LZWInputStream(is, Input.sizeof(version) * multimode);
        for (int c = 0; c < reccnt; c++) {
            for (int i = 0; i < multimode; i++) {
                recsync[c][i] = new Input().readObject(lis, version);
            }
        }
    }

}
