package ru.m210projects.Duke3D.filehandle;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.Directory;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Duke3D.Input;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.*;
import static ru.m210projects.Build.Engine.MAXPLAYERS;
import static ru.m210projects.Build.net.Mmulti.connecthead;
import static ru.m210projects.Build.net.Mmulti.connectpoint2;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;

public class DemoRecorder {

    private final OutputStream os;
    private final Path filepath;
    public int reccnt;
    public int totalreccnt;
    public int recversion;

    public DemoRecorder(FileOutputStream os, Path filepath, int nVersion) throws IOException {
        this.filepath = filepath;
        StreamUtils.writeInt(os, 0);
        StreamUtils.writeByte(os, nVersion);

        StreamUtils.writeByte(os, ud.volume_number);
        StreamUtils.writeByte(os, ud.level_number);
        StreamUtils.writeByte(os, ud.player_skill);
        StreamUtils.writeByte(os, ud.coop);
        StreamUtils.writeByte(os, ud.ffire);
        StreamUtils.writeShort(os, ud.multimode);
        StreamUtils.writeShort(os, ud.monsters_off ? 1 : 0);
        StreamUtils.writeInt(os, ud.respawn_monsters ? 1 : 0);
        StreamUtils.writeInt(os, ud.respawn_items ? 1 : 0);
        StreamUtils.writeInt(os, ud.respawn_inventory ? 1 : 0);
        StreamUtils.writeInt(os, ud.playerai);

        for (int i = 0; i < MAXPLAYERS; i++) {
            StreamUtils.writeString(os, ud.user_name[i], 32);
        }

        StreamUtils.writeInt(os, ud.auto_run);
        int MAX_PATH = 128;
        if (nVersion >= JFBYTEVERSION) {
            MAX_PATH = 260;
        }

        if (mUserFlag == UserFlag.UserMap && boardfilename != null && boardfilename.exists()) {
            StreamUtils.writeString(os, boardfilename.getName(), MAX_PATH);
        } else {
            StreamUtils.writeBytes(os, new byte[MAX_PATH]);
        }

        if (nVersion >= GDXBYTEVERSION) {
            if (mUserFlag == Main.UserFlag.Addon && currentGame != null) {
                EpisodeEntry episodeEntry = currentGame.getEpisodeEntry();
                boolean isPacked = episodeEntry.isPackageEpisode();
                StreamUtils.writeBoolean(os, isPacked);
                StreamUtils.writeDataString(os, episodeEntry.getFileEntry().getRelativePath().toString());
                if (isPacked) {
                    StreamUtils.writeDataString(os, episodeEntry.getConFile().getName());
                }
            } else {
                StreamUtils.writeBoolean(os, false); // packed
                StreamUtils.writeInt(os, 0); // name length
            }

            for (int i = 0; i < ud.multimode; i++) {
                PlayerStruct p = ps[i];
                Sprite psp = boardService.getSprite(p.i);
                StreamUtils.writeInt(os, p.shield_amount);
                StreamUtils.writeInt(os, psp != null ? psp.getExtra() : 100);

                for(int j = 0; j < MAX_WEAPONS; j++) {
                    StreamUtils.writeBoolean(os, p.gotweapon[j]);
                    StreamUtils.writeInt(os, p.ammo_amount[j]);
                }
                StreamUtils.writeInt(os, p.firstaid_amount);
                StreamUtils.writeInt(os, p.steroids_amount);
                StreamUtils.writeInt(os, p.holoduke_amount);
                StreamUtils.writeInt(os, p.jetpack_amount);
                StreamUtils.writeInt(os, p.heat_amount);
                StreamUtils.writeInt(os, p.scuba_amount);
                StreamUtils.writeInt(os, p.boot_amount);
                StreamUtils.writeInt(os, p.inven_icon);
            }
        }

        for (int i = 0; i < ud.multimode; i++) {
            StreamUtils.writeByte(os, ps[i].aim_mode);
            if (nVersion >= JFBYTEVERSION) // JBF 20031126
            {
                StreamUtils.writeByte(os, ps[i].auto_aim);
            }
        }

        totalreccnt = 0;
        reccnt = 0;
        recversion = nVersion;
        this.os = new LZWOutputStream(new BufferedOutputStream(os, RECSYNCBUFSIZ * Input.sizeof(BYTEVERSION)), Input.sizeof(BYTEVERSION));
    }

    public void record() {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            try {
                sync[i].writeObject(os, recversion);
                reccnt++;
                if (reccnt >= RECSYNCBUFSIZ) {
                    os.flush();
                    reccnt = 0;
                }
                totalreccnt++;
            } catch (Exception e) {
                Console.out.println(e.toString(), OsdColor.RED);
                close();
            }
        }
    }

    public void close() {
        try {
            os.close();
            try (OutputStream out = Files.newOutputStream(filepath, WRITE)) {
                StreamUtils.writeInt(out, totalreccnt);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Console.out.println("Stop recording");
        Directory dir = game.getCache().getGameDirectory();

        Entry entry = dir.addEntry(filepath);
        if (entry.exists()) {
            List<Entry> demos = gDemoScreen.demofiles.computeIfAbsent(dir, e -> new ArrayList<>());
            demos.add(entry);
        }
    }
}
