//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Renderer.RenderType;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.exceptions.WarningException;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Duke3D.Screens.DemoScreen;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Duke3D.Animate.gAnimationCount;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.*;
import static ru.m210projects.Duke3D.ResourceHandler.InitSpecialTextures;
import static ru.m210projects.Duke3D.Sounds.*;
import static ru.m210projects.Duke3D.Spawn.*;
import static ru.m210projects.Duke3D.View.animatesprites;

public class Premap {

    public static final short[] lotags = new short[65];
    public static final byte[] packbuf = new byte[576];
    public static final byte[] oldgotpics = new byte[(MAXTILES + 7) >> 3];
    public static final short[] rorsector = new short[16];
    public static final byte[] rortype = new byte[16];
    public static int rorcnt;
    public static short which_palookup = 9;
    public static final PlayerInfo[] info = new PlayerInfo[MAXPLAYERS];

    public static void getsound(int num) {
        if (cfg.isNoSound()) {
            return;
        }

        Entry entry = getSampleEntry(num);
        if (!entry.exists()) {
            return;
        }

        soundsiz[num] = (int) entry.getSize();
        if (currentGame.getCON().type != 20 || ((ud.level_number == 0 && ud.volume_number == 0
                && (num == 189 || num == 232 || num == 99 || num == 233 || num == 17)) || (entry.getSize() < 12288))) {
            Sound[num].lock = 2;
            loadSample(entry, num);
        }
    }

    public static void xyzmirror(int i, DynamicArtEntry wn) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        Renderer renderer = game.getRenderer();
        byte[] gotpic = renderer.getRenderedPics();
        System.arraycopy(gotpic, 0, oldgotpics, 0, gotpic.length);

        renderer.setviewtotile(wn);

        renderer.drawrooms(sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 100 + sp.getShade(),
                sp.getSectnum());
        display_mirror = 1;
        animatesprites(sp.getX(), sp.getY(), sp.getZ(), sp.getAng(), 65536);
        display_mirror = 0;
        renderer.drawmasks();

        renderer.setviewback();

        if (renderer.getType() == RenderType.Software) {
            engine.getTileManager().squarerotatetile(wn);
        }

        System.arraycopy(oldgotpics, 0, gotpic, 0, gotpic.length);
    }

    public static void pickrandomspot(int snum) {
        int i;

        PlayerStruct p = ps[snum];

        if (ud.multimode > 1 && ud.coop != 1) {
            i = engine.krand() % numplayersprites;
        } else {
            i = snum;
        }

        p.bobposx = p.oposx = p.posx = po[i].ox;
        p.bobposy = p.oposy = p.posy = po[i].oy;
        p.oposz = p.posz = po[i].oz;
        p.ang = po[i].oa;
        p.cursectnum = po[i].os;
    }

    public static void resetplayerstats(int snum) {
        PlayerStruct p = ps[snum];

        ud.showallmap = 0;
        p.dead_flag = 0;
        p.wackedbyactor = -1;
        p.falling_counter = 0;
        p.quick_kick = 0;
        p.subweapon = 0;
        p.last_full_weapon = 0;
        p.tipincs = 0;
        p.buttonpalette = 0;
        p.actorsqu = -1;
        p.invdisptime = 0;
        p.refresh_inventory = false;
        p.last_pissed_time = 0;
        p.holster_weapon = 0;
        p.pycount = 0;
        p.pyoff = 0;
        p.opyoff = 0;
        p.loogcnt = 0;
        p.angvel = 0;
        p.weapon_sway = 0;
        p.extra_extra8 = 0;
        p.show_empty_weapon = 0;
        p.dummyplayersprite = -1;
        p.crack_time = 0;
        p.hbomb_hold_delay = 0;
        p.transporter_hold = 0;
        p.wantweaponfire = -1;
        p.hurt_delay = 0;
        p.footprintcount = 0;
        p.footprintpal = 0;
        p.footprintshade = 0;
        p.jumping_toggle = 0;
        p.ohoriz = p.horiz = 140;
        p.horizoff = 0;
        p.bobcounter = 0;
        p.on_ground = false;
        p.player_par = 0;
        p.return_to_center = 9;
        p.airleft = 15 * 26;
        p.rapid_fire_hold = 0;
        p.toggle_key_flag = 0;
        p.access_spritenum = -1;
        if (ud.multimode > 1 && ud.coop != 1) {
            p.got_access = 7;
        } else {
            p.got_access = 0;
        }
        p.random_club_frame = 0;
        p.on_warping_sector = 0;
        p.spritebridge = 0;
        p.palette = engine.getPaletteManager().getBasePalette();

        if (p.steroids_amount < 400) {
            p.steroids_amount = 0;
            p.inven_icon = 0;
        }
        p.heat_on = 0;
        p.holoduke_on = -1;

        p.look_ang = (short) (512 - ((ud.level_number & 1) << 10));

        p.rotscrnang = 0;
        p.newowner = -1;
        p.jumping_counter = 0;
        p.hard_landing = 0;
        p.posxv = 0;
        p.posyv = 0;
        p.poszv = 0;
        fricxv = 0;
        fricyv = 0;
        p.somethingonplayer = -1;
        p.one_eighty_count = 0;
        p.cheat_phase = 0;

        p.on_crane = -1;

        if (p.curr_weapon == PISTOL_WEAPON) {
            p.kickback_pic = 5;
        } else {
            p.kickback_pic = 0;
        }

        p.weapon_pos = 6;
        p.walking_snd_toggle = 0;
        p.weapon_ang = 0;

        p.knuckle_incs = 1;
        p.fist_incs = 0;
        p.knee_incs = 0;
        p.jetpack_on = 0;
        setpal(p);

        p.numloogs = 0; // GDX 31.10.2018
        p.exitx = 0;
        p.exity = 0;
        p.truefz = 0;
        p.truecz = 0;
        p.randomflamex = 0;
        p.access_incs = 0;
        p.access_wallnum = 0;
        p.interface_toggle_flag = 0;
        p.scream_voice = null;
        p.crouch_toggle = 0;
        p.last_used_weapon = 0;
    }

    public static void resetweapons(int snum) {
        int weapon;
        PlayerStruct p = ps[snum];

        for (weapon = PISTOL_WEAPON; weapon < MAX_WEAPONS; weapon++) {
            p.gotweapon[weapon] = false;
        }
        for (weapon = PISTOL_WEAPON; weapon < MAX_WEAPONS; weapon++) {
            p.ammo_amount[weapon] = 0;
        }
        Arrays.fill(p.weaprecs, (short) 0);

        p.weapon_pos = 6;
        p.kickback_pic = 5;
        p.curr_weapon = PISTOL_WEAPON;
        p.gotweapon[PISTOL_WEAPON] = true;
        p.gotweapon[KNEE_WEAPON] = true;
        p.ammo_amount[PISTOL_WEAPON] = 48;
        p.gotweapon[HANDREMOTE_WEAPON] = true;
        p.last_weapon = -1;

        p.show_empty_weapon = 0;
        p.last_pissed_time = 0;
        p.holster_weapon = 0;
    }

    public static void resetinventory(int snum) {
        PlayerStruct p = ps[snum];

        p.inven_icon = 0;
        p.boot_amount = 0;
        p.scuba_on = 0;
        p.scuba_amount = 0;
        p.heat_amount = 0;
        p.heat_on = 0;
        p.jetpack_on = 0;
        p.jetpack_amount = 0;
        p.shield_amount = (short) currentGame.getCON().max_armour_amount;
        p.holoduke_on = -1;
        p.holoduke_amount = 0;
        p.firstaid_amount = 0;
        p.steroids_amount = 0;
        p.inven_icon = 0;
    }

    public static void resetprestat(int snum) {
        PlayerStruct p = ps[snum];

        spriteqloc = 0;
        for (int i = 0; i < currentGame.getCON().spriteqamount; i++) {
            spriteq[i] = -1;
        }

        p.hbomb_on = 0;
        p.cheat_phase = 0;
        p.pals_time = 0;
        p.toggle_key_flag = 0;
        p.secret_rooms = 0;
        p.max_secret_rooms = 0;
        p.actors_killed = 0;
        p.max_actors_killed = 0;
        p.lastrandomspot = 0;
        p.weapon_pos = 6;
        p.kickback_pic = 5;
        p.last_weapon = -1;
        p.weapreccnt = 0;
        p.show_empty_weapon = 0;
        p.holster_weapon = 0;
        p.last_pissed_time = 0;

        p.one_parallax_sectnum = -1;

        screenpeek = myconnectindex;
        numanimwalls = 0;
        numcyclers = 0;
        gAnimationCount = 0;
        parallaxtype = 0;
        engine.srand(17);
        game.gPaused = false;
        ud.camerasprite = -1;
        ud.eog = 0;
        tempwallptr = 0;
        camsprite = -1;
        earthquaketime = 0;

        startofdynamicinterpolations = 0;

        if (((uGameFlags & MODE_EOL) != MODE_EOL && numplayers < 2) || (ud.coop != 1 && numplayers > 1)) {
            resetweapons(snum);
            resetinventory(snum);
        } else if (p.curr_weapon == HANDREMOTE_WEAPON) {
            p.ammo_amount[HANDBOMB_WEAPON]++;
            p.curr_weapon = HANDBOMB_WEAPON;
        }

        p.timebeforeexit = 0;
        p.customexitsound = 0;

        Arrays.fill(p.pals, (short) 0);
    }

    public static void setupbackdrop(short sky) {
        Renderer renderer = game.getRenderer();
        Arrays.fill(pskyoff, (short) 0);

        int parallaxyscale = renderer.getParallaxScale();
        if (parallaxyscale != 65536) {
            parallaxyscale = 32768;
        }
        renderer.setParallaxOffset(0);

        switch (sky) {
            case CLOUDYOCEAN:
                parallaxyscale = 65536;
                break;
            case MOONSKY1:
                pskyoff[6] = 1;
                pskyoff[1] = 2;
                pskyoff[4] = 2;
                pskyoff[2] = 3;
                break;
            case BIGORBIT1: // orbit
                pskyoff[5] = 1;
                pskyoff[6] = 2;
                pskyoff[7] = 3;
                pskyoff[2] = 4;
                break;
            case LA:
                parallaxyscale = 16384 + 1024;
                pskyoff[0] = 1;
                pskyoff[1] = 2;
                pskyoff[2] = 1;
                pskyoff[3] = 3;
                pskyoff[4] = 4;
                pskyoff[5] = 0;
                pskyoff[6] = 2;
                pskyoff[7] = 3;
                break;

            case 5284: // Twentieth Anniversary World Tour skies
            case 5412:
            case 5420:
            case 5450:
            case 5540:
            case 5548:
            case 5556:
            case 5720:
            case 5814:
                if (currentGame.getCON().type == 20) {
                    parallaxyscale = 65536;
                    if (sky != 5284) {
                        renderer.setParallaxOffset(48);
                    }

                    switch (sky) {
                        case 5450:
                            for (int i = 0; i < 8; i++) {
                                pskyoff[i] = (short) ((i + 7) & 7);
                            }
                            break;
                        case 5556:
                            for (int i = 0; i < 8; i++) {
                                pskyoff[i] = (short) ((i + 1) & 7);
                            }
                            break;
                        case 5720:
                            for (int i = 0; i < 8; i++) {
                                pskyoff[i] = (short) ((i + 4) & 7);
                            }
                            break;
                        default:
                            for (int i = 0; i < 8; i++) {
                                pskyoff[i] = (short) i;
                            }
                            break;
                    }
                }
                break;
        }
        renderer.setParallaxScale(parallaxyscale);

        Arrays.fill(zeropskyoff, (short) 0);
        System.arraycopy(pskyoff, 0, zeropskyoff, 0, MAXPSKYTILES);

        pskybits = 3;
    }

    public static void prelevel() {
//        int j, startwall, endwall, lotaglist;
        Arrays.fill(lotags, (short) 0);

        show2dsector.clear();
        show2dwall.clear();
        show2dsprite.clear();

        resetprestat(0);
        numclouds = 0;
        gVisibility = currentGame.getCON().const_visibility;

        Sector[] sectors = boardService.getBoard().getSectors();
        for (int i = 0; i < sectors.length; i++) {
            Sector sec = sectors[i];
            sec.setExtra(256);

            switch (sec.getLotag()) {
                case 20:
                case 22:
                    if (sec.getFloorz() > sec.getCeilingz()) {
                        sec.setLotag(sec.getLotag() | 32768);
                    }
                    continue;
            }

            if ((sec.getCeilingstat() & 1) != 0) {
                setupbackdrop(sec.getCeilingpicnum());

                if (sec.getCeilingpicnum() == CLOUDYSKIES && numclouds < 127) {
                    clouds[numclouds++] = (short) i;
                }

                if (ps[0].one_parallax_sectnum == -1) {
                    ps[0].one_parallax_sectnum = (short) i;
                }
            }

            if (sec.getLotag() == 32767) // Found a secret room
            {
                ps[0].max_secret_rooms++;
                continue;
            }

            if (sec.getLotag() == -1) {
                Wall wal = boardService.getWall(sec.getWallptr());
                if (wal != null) {
                    ps[0].exitx = wal.getX();
                    ps[0].exity = wal.getY();
                }
            }
        }

        ListNode<Sprite> node = boardService.getStatNode(0);
        while (node != null) {
            ListNode<Sprite> nexti = node.getNext();
            Sprite sp = node.get();

            if (sp.getLotag() == -1 && (sp.getCstat() & 16) != 0) {
                ps[0].exitx = sp.getX();
                ps[0].exity = sp.getY();
            } else {
                Sector sec = boardService.getSector(sp.getSectnum());
                if (sec != null) {
                    switch (sp.getPicnum()) {
                        case GPSPEED:
                            sec.setExtra(sp.getLotag());
                            engine.deletesprite(node.getIndex());
                            break;

                        case CYCLER:
                            if (numcyclers >= MAXCYCLERS) {
                                throw new WarningException("\nToo many cycling sectors.");
                            }
                            cyclers[numcyclers][0] = sp.getSectnum();
                            cyclers[numcyclers][1] = sp.getLotag();
                            cyclers[numcyclers][2] = sp.getShade();
                            cyclers[numcyclers][3] = sec.getFloorshade();
                            cyclers[numcyclers][4] = sp.getHitag();
                            cyclers[numcyclers][5] = (short) ((sp.getAng() == 1536) ? 1 : 0);
                            numcyclers++;
                            engine.deletesprite(node.getIndex());
                            break;
                    }
                }
            }
            node = nexti;
        }

        List<Sprite> sprites = boardService.getBoard().getSprites();
        for (int i = 0; i < sprites.size(); i++) {
            Sprite sp = sprites.get(i);
            if (sp.getStatnum() < MAXSTATUS) {
                if (sp.getPicnum() == SECTOREFFECTOR && sp.getLotag() == 14) {
                    continue;
                }
                spawn(-1, i);
            }
        }

        for (int i = 0; i < sprites.size(); i++) {
            Sprite sp = sprites.get(i);
            if (sp.getStatnum() < MAXSTATUS) {
                if (sp.getPicnum() == SECTOREFFECTOR && sp.getLotag() == 14) {
                    spawn(-1, i);
                }
            }
        }

        int lotaglist = 0;
        for (ListNode<Sprite> n = boardService.getStatNode(0); n != null; n = n.getNext()) {
            Sprite sp = n.get();
            switch (sp.getPicnum()) {
                case DIPSWITCH:
                case DIPSWITCH2:
                case ACCESSSWITCH:
                case PULLSWITCH:
                case HANDSWITCH:
                case SLOTDOOR:
                case LIGHTSWITCH:
                case SPACELIGHTSWITCH:
                case SPACEDOORSWITCH:
                case FRANKENSTINESWITCH:
                case LIGHTSWITCH2:
                case POWERSWITCH1:
                case LOCKSWITCH1:
                case POWERSWITCH2:
                    break;
                case DIPSWITCH + 1:
                case DIPSWITCH2 + 1:
                case PULLSWITCH + 1:
                case HANDSWITCH + 1:
                case SLOTDOOR + 1:
                case LIGHTSWITCH + 1:
                case SPACELIGHTSWITCH + 1:
                case SPACEDOORSWITCH + 1:
                case FRANKENSTINESWITCH + 1:
                case LIGHTSWITCH2 + 1:
                case POWERSWITCH1 + 1:
                case LOCKSWITCH1 + 1:
                case POWERSWITCH2 + 1: {
                    int j = 0;
                    for (; j < lotaglist; j++) {
                        if (sp.getLotag() == lotags[j]) {
                            break;
                        }
                    }

                    if (j == lotaglist) {
                        lotags[lotaglist] = sp.getLotag();
                        lotaglist++;
                        if (lotaglist > 64) {
                            throw new WarningException("\nToo many switches (64 max).");
                        }

                        for (ListNode<Sprite> n2 = boardService.getStatNode(3); n2 != null; n2 = n2.getNext()) {
                            Sprite sp2 = n2.get();
                            if (sp2.getLotag() == 12 && sp2.getHitag() == sp.getLotag()) {
                                hittype[n2.getIndex()].temp_data[0] = 1;
                            }
                        }
                    }
                    break;
                }
            }
        }

        mirrorcnt = 0;

        Wall[] walls = boardService.getBoard().getWalls();
        for (int i = 0; i < walls.length; i++) {
            Wall wal = walls[i];

            if (wal.getOverpicnum() == MIRROR && (wal.getCstat() & 32) != 0) {
                if (mirrorcnt > 63) {
                    throw new WarningException("\nToo many mirrors (64 max.)");
                }

                Sector nextsector = boardService.getSector(wal.getNextsector());
                if ((nextsector != null) && nextsector.getCeilingpicnum() != MIRROR) {
                    nextsector.setCeilingpicnum(MIRROR);
                    nextsector.setFloorpicnum(MIRROR);
                    mirrorwall[mirrorcnt] = (short) i;
                    mirrorsector[mirrorcnt] = wal.getNextsector();
                    mirrorcnt++;
                    continue;
                }
            }

            if (numanimwalls >= MAXANIMWALLS) {
                throw new WarningException("\nToo many 'anim' walls (max 512.)");
            }

            animwall[numanimwalls].tag = 0;
            animwall[numanimwalls].wallnum = 0;

            switch (wal.getOverpicnum()) {
                case FANSHADOW:
                case FANSPRITE:
                    Wall wal0 = boardService.getWall(0); // original typo wall->cstat |= 65 instead of wal->cstat |= 65;
                    if (wal0 != null) {
                        wal0.setCstat(wal0.getCstat() | 65);
                    }
                    animwall[numanimwalls].wallnum = (short) i;
                    numanimwalls++;
                    break;

                case W_FORCEFIELD:
                case W_FORCEFIELD + 1:
                case W_FORCEFIELD + 2:
                    if (wal.getShade() > 31) {
                        wal.setCstat(0);
                    } else {
                        wal.setCstat(wal.getCstat() | 85 + 256);
                    }

                    Wall nextwal = boardService.getWall(wal.getNextwall());
                    if (wal.getLotag() != 0 && nextwal != null) {
                        nextwal.setLotag(wal.getLotag());
                    }
                case BIGFORCE:

                    animwall[numanimwalls].wallnum = (short) i;
                    numanimwalls++;

                    continue;
            }

            wal.setExtra(-1);

            switch (wal.getPicnum()) {
                case W_TECHWALL1:
                case W_TECHWALL2:
                case W_TECHWALL3:
                case W_TECHWALL4:
                    animwall[numanimwalls].wallnum = (short) i;
                    numanimwalls++;
                    break;
                case SCREENBREAK6:
                case SCREENBREAK7:
                case SCREENBREAK8:
                    animwall[numanimwalls].wallnum = (short) i;
                    animwall[numanimwalls].tag = -1;
                    numanimwalls++;
                    break;

                case FEMPIC1:
                case FEMPIC2:
                case FEMPIC3:
                    wal.setExtra(wal.getPicnum());
                    animwall[numanimwalls].tag = -1;
                    if (ud.lockout != 0) {
                        if (wal.getPicnum() == FEMPIC1) {
                            wal.setPicnum(BLANKSCREEN);
                        } else {
                            wal.setPicnum(SCREENBREAK6);
                        }
                    }

                    animwall[numanimwalls].wallnum = (short) i;
                    animwall[numanimwalls].tag = wal.getPicnum();
                    numanimwalls++;
                    break;

                case SCREENBREAK1:
                case SCREENBREAK2:
                case SCREENBREAK3:
                case SCREENBREAK4:
                case SCREENBREAK5:

                case SCREENBREAK9:
                case SCREENBREAK10:
                case SCREENBREAK11:
                case SCREENBREAK12:
                case SCREENBREAK13:
                case SCREENBREAK14:
                case SCREENBREAK15:
                case SCREENBREAK16:
                case SCREENBREAK17:
                case SCREENBREAK18:
                case SCREENBREAK19:

                    animwall[numanimwalls].wallnum = (short) i;
                    animwall[numanimwalls].tag = wal.getPicnum();
                    numanimwalls++;
                    break;
            }
        }

        // Invalidate textures in sector behind mirror
        for (int i = 0; i < mirrorcnt; i++) {
            Sector sec = boardService.getSector(mirrorsector[i]);
            if (sec == null) {
                continue;
            }

            for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wall = wn.get();
                wall.setPicnum(MIRROR);
                wall.setOverpicnum(MIRROR);
            }
        }

        InitSpecialTextures();
    }

    public static void resetpspritevars() {
        EGS(ps[0].cursectnum, ps[0].posx, ps[0].posy, ps[0].posz, APLAYER, 0, 0, 0, (short) ps[0].ang, 0, 0, 0,
                (short) 10);

        if (!DemoScreen.isDemoPlaying()) {
            for (int i = 0; i < MAXPLAYERS; i++) {
                if (info[i] == null) {
                    info[i] = new PlayerInfo();
                }
                info[i].set(ps[i]);
            }
        }

        resetplayerstats(0);

        for (int i = 1; i < MAXPLAYERS; i++) {
            ps[i].copy(ps[0]);
        }

        if (!DemoScreen.isDemoPlaying()) {
            for (int i = 0; i < MAXPLAYERS; i++) {
                info[i].restore(ps[i]);
                if (ps[i].curr_weapon == PISTOL_WEAPON) {
                    ps[i].kickback_pic = 5;
                } else {
                    ps[i].kickback_pic = 0;
                }
            }
        }

        numplayersprites = 0;

        which_palookup = 9;
        int j = connecthead;
        ListNode<Sprite> n = boardService.getStatNode(10);
        while (n != null) {
            ListNode<Sprite> nexti = n.getNext();
            Sprite s = n.get();
            int i = n.getIndex();

            if (numplayersprites == MAXPLAYERS) {
                throw new WarningException("\nToo many player sprites (max 16.)");
            }

            po[numplayersprites].ox = s.getX();
            po[numplayersprites].oy = s.getY();
            po[numplayersprites].oz = s.getZ();
            po[numplayersprites].oa = s.getAng();
            po[numplayersprites].os = s.getSectnum();

            numplayersprites++;
            if (j >= 0) {
                s.setOwner(i);
                s.setShade(0);
                s.setXrepeat(42);
                s.setYrepeat(36);
                s.setCstat(1 + 256);
                s.setXoffset(0);
                s.setClipdist(64);

                if ((uGameFlags & MODE_EOL) != MODE_EOL || ps[j].last_extra == 0) {
                    ps[j].last_extra = (short) currentGame.getCON().max_player_health;
                    s.setExtra((short) currentGame.getCON().max_player_health);
                } else {
                    s.setExtra(ps[j].last_extra);
                }

                s.setYvel(j);

                if (s.getPal() == 0) {
                    s.setPal(ps[j].palookup = which_palookup);
                    which_palookup++;
                    if (which_palookup >= 17) {
                        which_palookup = 9;
                    }
                } else {
                    ps[j].palookup = s.getPal();
                }

                ps[j].i = (short) i;
                ps[j].frag_ps = (short) j;
                hittype[i].owner = i;

                ps[j].bobposx = ps[j].oposx = ps[j].posx = s.getX();
                ps[j].bobposy = ps[j].oposy = ps[j].posy = s.getY();
                ps[j].oposz = ps[j].posz = s.getZ();
                ps[j].oang = ps[j].ang = s.getAng();
                ps[j].cursectnum = engine.updatesector(s.getX(), s.getY(), ps[j].cursectnum);
                j = connectpoint2[j];
            } else {
                engine.deletesprite(i);
            }
            n = nexti;
        }

        for (int i = numplayersprites; i < MAXPLAYERS; i++) {
            ps[i].copy(ps[i % numplayersprites]); // players without position
        }
    }

    public static void clearfrags() {
        for (int i = 0; i < MAXPLAYERS; i++) {
            ps[i].frag = ps[i].fraggedself = 0;
            Arrays.fill(frags[i], (short) 0);
        }
    }

    public static void genspriteremaps() throws IOException {
        int j;
        int look_pos;

        Entry entry = game.getCache().getEntry("lookup.dat", true);
        if (!entry.exists()) {
            throw new FileNotFoundException("\nERROR: File 'LOOKUP.DAT' not found.");
        }

        try (InputStream is = entry.getInputStream()) {
            int numl = StreamUtils.readUnsignedByte(is);

            PaletteManager paletteManager = engine.getPaletteManager();
            for (j = 0; j < numl; j++) {
                look_pos = StreamUtils.readUnsignedByte(is);
                StreamUtils.readBytes(is, tempbuf, 256);
                paletteManager.makePalookup(look_pos, tempbuf, 0, 0, 0, 1);
            }

            StreamUtils.readBytes(is, waterpal);
            StreamUtils.readBytes(is, slimepal);
            StreamUtils.readBytes(is, titlepal);
            StreamUtils.readBytes(is, drealms);
            StreamUtils.readBytes(is, endingpal);

            byte[] palette = paletteManager.getBasePalette();

            palette[765] = palette[766] = palette[767] = 0;
            slimepal[765] = slimepal[766] = slimepal[767] = 0;
            waterpal[765] = waterpal[766] = waterpal[767] = 0;
        } catch (Exception e) {
            Console.out.println("Warning! File 'LOOKUP.DAT' reading error!", OsdColor.YELLOW);
        }
    }

    public static void checknextlevel() {
        if (ud.level_number >= currentGame.episodes[ud.volume_number].nMaps) {
            if (ud.volume_number == 0 && currentGame.nEpisodes > 0) {
                ud.level_number = 0;
                ud.volume_number = 1;
            } else {
                ud.level_number = 0;
            }
        }
    }

    public static void LeaveMap() {
        System.err.println("LeaveMap");
        if (numplayers > 1 && game.pNet.bufferJitter >= 0 && myconnectindex == connecthead) {
            for (int i = 0; i <= game.pNet.bufferJitter; i++) {
                game.pNet.GetNetworkInput(); // wait for other player before level end
            }
        }

        if (!game.pNet.WaitForAllPlayers(5000)) {
            game.pNet.NetDisconnect(myconnectindex);
            return;
        }

        uGameFlags |= MODE_EOL;
        if (mUserFlag != UserFlag.UserMap && (uGameFlags & MODE_END) == 0
                && ud.level_number >= currentGame.episodes[ud.volume_number].nMaps) {
            uGameFlags |= MODE_END;
        }

        gDemoScreen.onStopRecord();
    }
}

