//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;


import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Script.TextureHDInfo;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Engine.MAXPALOOKUPS;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Build.Pragmas.scale;
import static ru.m210projects.Build.Render.AbstractRenderer.DEFAULT_SCREEN_FADE;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.slimepal;
import static ru.m210projects.Duke3D.Player.waterpal;

public class Screen {

    public static int changepalette;
    public static boolean restorepalette;
    public static int screensize;

    public static void vscrn(int size) {
        Renderer renderer = game.getRenderer();
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();

        if (size < 0) {
            size = 0;
        } else if (size > 3) {
            size = 3;
        }

        int x1 = 0;
        int y1 = 0;
        int y2 = 200;

        if (size == 3) {
            y2 -= (32);
        }

        y1 = scale(y1, ydim, 200);
        y2 = scale(y2, ydim, 200);

        renderer.setview(x1, y1, xdim - 1, y2 - 1);
        screensize = size;
    }

    public static void setgamepalette(PlayerStruct player, byte[] pal) {
        if (player != ps[screenpeek]) {
            // another head
            player.palette = pal;
            return;
        }
        player.palette = pal;
        engine.setbrightness(ud.brightness >> 2, pal);
        DEFAULT_SCREEN_FADE.set(0,0,0,0);

        if (game.currentDef != null) {
            TextureHDInfo hdInfo = game.currentDef.texInfo;
            if (pal == waterpal) {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 160, 160, 255, 0);
            } else if (pal == slimepal) {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 180, 200, 50, 0);
            } else {
                hdInfo.setPaletteTint(MAXPALOOKUPS - 1, 255, 255, 255, 0);
            }
        }
    }

    public static void palto(int r, int g, int b, int count) {
        DEFAULT_SCREEN_FADE.set(r, g, b, count);
    }

    public static void scrReset() {
        DEFAULT_SCREEN_FADE.set(0,0,0,0);
        setgamepalette(ps[myconnectindex], engine.getPaletteManager().getBasePalette());
    }

    public static void myospal(int x, int y, int tilenum, int shade, int orientation, int p) {
        Renderer renderer = game.getRenderer();
        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();

        short a = 0;
        if ((orientation & 4) != 0) {
            a = 1024;
        }
        renderer.rotatesprite(x << 16, y << 16, 65536, a, tilenum, shade, p, 10 | orientation, windowx1, windowy1, windowx2, windowy2);
    }

    public static void myos(int x, int y, int tilenum, int shade, int orientation) {
        Renderer renderer = game.getRenderer();
        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();

        int a = 0;
        if ((orientation & 4) != 0) {
            a = 1024;
        }

        Sector sec = boardService.getSector(ps[screenpeek].cursectnum);
        int p = sec != null ? sec.getFloorpal() : 0;
        renderer.rotatesprite(x << 16, y << 16, 65536, a, tilenum, shade, p, 10 | orientation, windowx1, windowy1, windowx2, windowy2);
    }

    public static void weaponnum(int ind, int x, int y, int num1, int num2, int ha) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite((x - 7) << 16, y << 16, 65536, 0, THREEBYFIVE + ind + 1, ha - 10, 7, 10);
        renderer.rotatesprite((x - 3) << 16, y << 16, 65536, 0, THREEBYFIVE + 10, ha, 0, 10);
        renderer.rotatesprite((x + 9) << 16, y << 16, 65536, 0, THREEBYFIVE + 11, ha, 0, 10);

        if (num1 > 99) {
            num1 = 99;
        }
        if (num2 > 99) {
            num2 = 99;
        }

        Bitoa(num1, buf);
        if (num1 > 9) {
            renderer.rotatesprite((x) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
        } else {
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
        }

        Bitoa(num2, buf);
        if (num2 > 9) {
            renderer.rotatesprite((x + 13) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 17) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
        } else {
            renderer.rotatesprite((x + 13) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
        }
    }

    public static void weaponnum999(int ind, int x, int y, int num1, int num2, int ha) {
        Renderer renderer = game.getRenderer();
        renderer.rotatesprite((x - 7) << 16, y << 16, 65536, 0, THREEBYFIVE + ind + 1, ha - 10, 7, 10);
        renderer.rotatesprite((x - 4) << 16, y << 16, 65536, 0, THREEBYFIVE + 10, ha, 0, 10);
        renderer.rotatesprite((x + 13) << 16, y << 16, 65536, 0, THREEBYFIVE + 11, ha, 0, 10);

        Bitoa(num1, buf);
        if (num1 > 99) {
            renderer.rotatesprite((x) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 8) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[2] - '0', ha, 0, 10);
        } else if (num1 > 9) {
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 8) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
        } else {
            renderer.rotatesprite((x + 8) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
        }

        Bitoa(num2, buf);
        if (num2 > 99) {
            renderer.rotatesprite((x + 17) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 21) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 25) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[2] - '0', ha, 0, 10);
        } else if (num2 > 9) {
            renderer.rotatesprite((x + 17) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
            renderer.rotatesprite((x + 21) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[1] - '0', ha, 0, 10);
        } else {
            renderer.rotatesprite((x + 25) << 16, y << 16, 65536, 0, THREEBYFIVE + buf[0] - '0', ha, 0, 10);
        }
    }

    public static void weapon_amounts(PlayerStruct p, int x, int y) {
        int cw = p.curr_weapon;
        weaponnum999(PISTOL_WEAPON, x, y, p.ammo_amount[PISTOL_WEAPON], currentGame.getCON().max_ammo_amount[PISTOL_WEAPON], 12 - ((cw == PISTOL_WEAPON) ? 20 : 0));

        weaponnum999(SHOTGUN_WEAPON, x, y + 6, p.ammo_amount[SHOTGUN_WEAPON], currentGame.getCON().max_ammo_amount[SHOTGUN_WEAPON], (!p.gotweapon[SHOTGUN_WEAPON] ? 9 : 0) + 12 - ((cw == SHOTGUN_WEAPON) ? 18 : 0));

        weaponnum999(CHAINGUN_WEAPON, x, y + 12, p.ammo_amount[CHAINGUN_WEAPON], currentGame.getCON().max_ammo_amount[CHAINGUN_WEAPON], (!p.gotweapon[CHAINGUN_WEAPON] ? 9 : 0) + 12 - ((cw == CHAINGUN_WEAPON) ? 18 : 0));

        weaponnum(RPG_WEAPON, x + 39, y, p.ammo_amount[RPG_WEAPON], currentGame.getCON().max_ammo_amount[RPG_WEAPON], (!p.gotweapon[RPG_WEAPON] ? 9 : 0) + 12 - ((cw == RPG_WEAPON) ? 19 : 0));

        weaponnum(HANDBOMB_WEAPON, x + 39, y + 6, p.ammo_amount[HANDBOMB_WEAPON], currentGame.getCON().max_ammo_amount[HANDBOMB_WEAPON], (((p.ammo_amount[HANDBOMB_WEAPON] == 0) ? 1 : 0) | (!p.gotweapon[HANDBOMB_WEAPON] ? 1 : 0)) * 9 + 12 - ((cw == HANDBOMB_WEAPON || cw == HANDREMOTE_WEAPON) ? 19 : 0));

        if ((p.subweapon & (1 << GROW_WEAPON)) != 0) {
            weaponnum(SHRINKER_WEAPON, x + 39, y + 12, p.ammo_amount[GROW_WEAPON], currentGame.getCON().max_ammo_amount[GROW_WEAPON], (!p.gotweapon[GROW_WEAPON] ? 9 : 0) + 12 - ((cw == GROW_WEAPON) ? 18 : 0));
        } else {
            weaponnum(SHRINKER_WEAPON, x + 39, y + 12, p.ammo_amount[SHRINKER_WEAPON], currentGame.getCON().max_ammo_amount[SHRINKER_WEAPON], (!p.gotweapon[SHRINKER_WEAPON] ? 9 : 0) + 12 - ((cw == SHRINKER_WEAPON) ? 18 : 0));
        }

        weaponnum(DEVISTATOR_WEAPON, x + 70, y, p.ammo_amount[DEVISTATOR_WEAPON], currentGame.getCON().max_ammo_amount[DEVISTATOR_WEAPON], (!p.gotweapon[DEVISTATOR_WEAPON] ? 9 : 0) + 12 - ((cw == DEVISTATOR_WEAPON) ? 18 : 0));

        weaponnum(TRIPBOMB_WEAPON, x + 70, y + 6, p.ammo_amount[TRIPBOMB_WEAPON], currentGame.getCON().max_ammo_amount[TRIPBOMB_WEAPON], (!p.gotweapon[TRIPBOMB_WEAPON] ? 9 : 0) + 12 - ((cw == TRIPBOMB_WEAPON) ? 18 : 0));

        weaponnum(-1, x + 70, y + 12, p.ammo_amount[FREEZE_WEAPON], currentGame.getCON().max_ammo_amount[FREEZE_WEAPON], (!p.gotweapon[FREEZE_WEAPON] ? 9 : 0) + 12 - ((cw == FREEZE_WEAPON) ? 18 : 0));
    }

    public static void patchstatusbar(int x1, int y1, int x2, int y2) {
        Renderer renderer = game.getRenderer();
        if (ud.screen_size >= 2) {
            ArtEntry pic = engine.getTile(BIGHOLE);
            if (pic.hasSize()) {
                int xdim = renderer.getWidth();
                int ydim = renderer.getHeight();

                int framesx = xdim / pic.getWidth();
                int framesy = ydim - scale(engine.getTile(BOTTOMSTATUSBAR).getHeight(), ydim, 200);

                int x = 0;
                for (int i = 0; i <= framesx; i++) {
                    renderer.rotatesprite(x << 16, framesy << 16, 0x10000, 0, BIGHOLE, 0, 0, 8 | 16 | 256);
                    x += pic.getWidth();
                }
            }
        }
        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();
        renderer.rotatesprite(160 << 16, 197 << 16, 65536, 0, BOTTOMSTATUSBAR, 4, 0, 10 + 64, scale(x1, xdim, 320), scale(y1, ydim, 200), scale(x2, xdim, 320) - 1, scale(y2, ydim, 200) - 1);
    }

    public static void displayinventory(PlayerStruct p) {
        Renderer renderer = game.getRenderer();
        int n, j, xoff, y;

        j = 0;

        n = (p.jetpack_amount > 0) ? 1 << 3 : 0;
        if ((n & 8) != 0) {
            j++;
        }
        n |= (p.scuba_amount > 0) ? 1 << 5 : 0;
        if ((n & 32) != 0) {
            j++;
        }
        n |= (p.steroids_amount > 0) ? 1 << 1 : 0;
        if ((n & 2) != 0) {
            j++;
        }
        n |= (p.holoduke_amount > 0) ? 1 << 2 : 0;
        if ((n & 4) != 0) {
            j++;
        }
        n |= (p.firstaid_amount > 0) ? 1 : 0;
        if ((n & 1) != 0) {
            j++;
        }
        n |= (p.heat_amount > 0) ? 1 << 4 : 0;
        if ((n & 16) != 0) {
            j++;
        }
        n |= (p.boot_amount > 0) ? 1 << 6 : 0;
        if ((n & 64) != 0) {
            j++;
        }

        xoff = 160 - (j * 11);

        j = 0;

        if (ud.screen_size > 1) {
            y = 134;
        } else {
            y = 178;
        }

        if (ud.screen_size == 1) {
            if (ud.multimode > 1) {
                xoff += 56;
            } else {
                xoff += 65;
            }
        }

        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();
        while (j <= 9) {
            if ((n & (1 << j)) != 0) {
                switch (n & (1 << j)) {
                    case 1:
                        renderer.rotatesprite(xoff << 16, y << 16, 65536, 0, FIRSTAID_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 2:
                        renderer.rotatesprite((xoff + 1) << 16, y << 16, 65536, 0, STEROIDS_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 4:
                        renderer.rotatesprite((xoff + 2) << 16, y << 16, 65536, 0, HOLODUKE_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 8:
                        renderer.rotatesprite(xoff << 16, y << 16, 65536, 0, JETPACK_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 16:
                        renderer.rotatesprite(xoff << 16, y << 16, 65536, 0, HEAT_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 32:
                        renderer.rotatesprite(xoff << 16, y << 16, 65536, 0, AIRTANK_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                    case 64:
                        renderer.rotatesprite(xoff << 16, (y - 1) << 16, 65536, 0, BOOT_ICON, 0, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                        break;
                }

                xoff += 22;

                if (p.inven_icon == j + 1) {
                    renderer.rotatesprite((xoff - 2) << 16, (y + 19) << 16, 65536, 1024, ARROW, -32, 0, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                }
            }

            j++;
        }
    }

    public static void invennum(int x, int y, int num1, int ha, int sbits) {
        Renderer renderer = game.getRenderer();
        char[] dabuf = Globals.buf;

        Bitoa(num1, dabuf);

        if (num1 > 99) {
            renderer.rotatesprite((x - 4) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[0] - '0', ha, 0, sbits);
            renderer.rotatesprite((x) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[1] - '0', ha, 0, sbits);
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[2] - '0', ha, 0, sbits);
        } else if (num1 > 9) {
            renderer.rotatesprite((x) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[0] - '0', ha, 0, sbits);
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[1] - '0', ha, 0, sbits);
        } else {
            renderer.rotatesprite((x + 4) << 16, y << 16, 65536, 0, THREEBYFIVE + dabuf[0] - '0', ha, 0, sbits);
        }
    }

    public static void digitalnumber(int x, int y, int n, int s, int cs) {
        Renderer renderer = game.getRenderer();
        int i, j, k, p, c;
        char[] b = Globals.buf;
        i = Bitoa(n, b); // ltoa(n,b,10);
        j = 0;

        for (k = 0; k < i; k++) {
            p = DIGITALNUM + b[k] - '0';
            j += engine.getTile(p).getWidth() + 1;
        }
        c = x - (j >> 1);

        j = 0;
        for (k = 0; k < i; k++) {
            p = DIGITALNUM + b[k] - '0';
            renderer.rotatesprite((c + j) << 16, y << 16, 65536, 0, p, s, 0, cs);
            j += engine.getTile(p).getWidth() + 1;
        }
    }

}
