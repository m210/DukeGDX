//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.klabs;
import static ru.m210projects.Duke3D.Actors.badguy;
import static ru.m210projects.Duke3D.Actors.badguypic;
import static ru.m210projects.Duke3D.Animate.*;
import static ru.m210projects.Duke3D.Gameutils.neartag;
import static ru.m210projects.Duke3D.Gameutils.*;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.*;
import static ru.m210projects.Duke3D.Premap.LeaveMap;
import static ru.m210projects.Duke3D.Premap.xyzmirror;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.*;
import static ru.m210projects.Duke3D.Spawn.*;
import static ru.m210projects.Duke3D.Types.ANIMATION.*;
import static ru.m210projects.Duke3D.View.FTA;
import static ru.m210projects.Duke3D.Weapons.shoot;

public class DSector {

    public static int player_dist;
    public static final int[] wallfind = new int[2];
    private static boolean haltsoundhack;

    public static void moveclouds() {
        if (engine.getTotalClock() > cloudtotalclock || engine.getTotalClock() < (cloudtotalclock - 7)) {
            cloudtotalclock = engine.getTotalClock() + 6;

            for (int i = 0; i < numclouds; i++) {
                cloudx[i] += (short) (BCosAngle(BClampAngle(ps[screenpeek].ang)) / 512.0f);
                cloudy[i] += (short) (BSinAngle(BClampAngle(ps[screenpeek].ang)) / 512.0f);

                Sector sec = boardService.getSector(clouds[i]);
                if  (sec != null) {
                    sec.setCeilingxpanning((short) (cloudx[i] >> 6));
                    sec.setCeilingypanning((short) (cloudy[i] >> 6));
                }
            }
        }
    }

    public static boolean ceilingspace(int sectnum) {
        Sector sec = boardService.getSector(sectnum);
        if (sec != null && (sec.getCeilingstat() & 1) != 0 && sec.getCeilingpal() == 0) {
            switch (sec.getCeilingpicnum()) {
                case MOONSKY1:
                case BIGORBIT1:
                    return true;
            }
        }
        return false;
    }

    public static boolean floorspace(int sectnum) {
        Sector sec = boardService.getSector(sectnum);
        if (sec != null && (sec.getFloorstat() & 1) != 0 && sec.getCeilingpal() == 0) {
            switch (sec.getFloorpicnum()) {
                case MOONSKY1:
                case BIGORBIT1:
                    return true;
            }
        }
        return false;
    }

    public static boolean wallswitchcheck(int i) {
        Sprite spr = boardService.getSprite(i);
        if (spr == null) {
            return false;
        }

        switch (spr.getPicnum()) {
            case HANDPRINTSWITCH:
            case HANDPRINTSWITCH + 1:
            case ALIENSWITCH:
            case ALIENSWITCH + 1:
            case MULTISWITCH:
            case MULTISWITCH + 1:
            case MULTISWITCH + 2:
            case MULTISWITCH + 3:
            case ACCESSSWITCH:
            case ACCESSSWITCH2:
            case PULLSWITCH:
            case PULLSWITCH + 1:
            case HANDSWITCH:
            case HANDSWITCH + 1:
            case SLOTDOOR:
            case SLOTDOOR + 1:
            case LIGHTSWITCH:
            case LIGHTSWITCH + 1:
            case SPACELIGHTSWITCH:
            case SPACELIGHTSWITCH + 1:
            case SPACEDOORSWITCH:
            case SPACEDOORSWITCH + 1:
            case FRANKENSTINESWITCH:
            case FRANKENSTINESWITCH + 1:
            case LIGHTSWITCH2:
            case LIGHTSWITCH2 + 1:
            case POWERSWITCH1:
            case POWERSWITCH1 + 1:
            case LOCKSWITCH1:
            case LOCKSWITCH1 + 1:
            case POWERSWITCH2:
            case POWERSWITCH2 + 1:
            case DIPSWITCH:
            case DIPSWITCH + 1:
            case DIPSWITCH2:
            case DIPSWITCH2 + 1:
            case TECHSWITCH:
            case TECHSWITCH + 1:
            case DIPSWITCH3:
            case DIPSWITCH3 + 1:
                return true;
        }
        return false;
    }

    public static int callsound(int sn, int whatsprite) {
        if (haltsoundhack) {
            haltsoundhack = false;
            return -1;
        }

        ListNode<Sprite> n = boardService.getSectNode(sn);
        while (n != null) {
            Sprite sp = n.get();
            if (sp.getPicnum() == MUSICANDSFX && sp.getLotag() < 1000) {
                int i = n.getIndex();
                if (whatsprite == -1) {
                    whatsprite = i;
                }

                if (hittype[i].temp_data[0] == 0) {
                    if (sp.getLotag() < NUM_SOUNDS && (currentGame.getCON().soundm[sp.getLotag()] & 16) == 0) {
                        if (sp.getLotag() != 0) {
                            spritesound(sp.getLotag(), whatsprite);
                            if (sp.getHitag() != 0 && sp.getLotag() != sp.getHitag()
                                    && sp.getHitag() < NUM_SOUNDS) {
                                stopsound(sp.getHitag());
                            }
                        }

                        Sector sec = boardService.getSector(sp.getSectnum());
                        if (sec != null && (sec.getLotag() & 0xff) != 22) {
                            hittype[i].temp_data[0] = 1;
                        }
                    }
                } else if (sp.getHitag() < NUM_SOUNDS) {
                    if (sp.getHitag() != 0) {
                        spritesound(sp.getHitag(), whatsprite);
                    }
                    if ((currentGame.getCON().soundm[sp.getLotag()] & 1) != 0
                            || (sp.getHitag() != 0 && sp.getHitag() != sp.getLotag())) {
                        stopsound(sp.getLotag());
                    }
                    hittype[i].temp_data[0] = 0;
                }
                return sp.getLotag();
            }
            n = n.getNext();
        }
        return -1;
    }

    public static boolean check_activator_motion(int lotag) {
        ListNode<Sprite> n = boardService.getStatNode(8);
        while (n != null) {
            Sprite s = n.get();
            if (s.getLotag() == lotag) {
                for (int j = gAnimationCount - 1; j >= 0; j--) {
                    if (s.getSectnum() == gAnimationData[j].sect) {
                        return (true);
                    }
                }

                ListNode<Sprite> n2 = boardService.getStatNode(3);
                while (n2 != null) {
                    Sprite sp = n2.get();
                    int j = n2.getIndex();
                    if (s.getSectnum() == sp.getSectnum()) {
                        switch (sp.getLotag()) {
                            case 11:
                            case 30:
                                if (hittype[j].temp_data[4] != 0) {
                                    return (true);
                                }
                                break;
                            case 20:
                            case 31:
                            case 32:
                            case 18:
                                if (hittype[j].temp_data[0] != 0) {
                                    return (true);
                                }
                                break;
                        }
                    }
                    n2 = n2.getNext();
                }
            }
            n = n.getNext();
        }
        return (false);
    }

    public static boolean isadoorwall(int dapic) {
        switch (dapic) {
            case DOORTILE1:
            case DOORTILE2:
            case DOORTILE3:
            case DOORTILE4:
            case DOORTILE5:
            case DOORTILE6:
            case DOORTILE7:
            case DOORTILE8:
            case DOORTILE9:
            case DOORTILE10:
            case DOORTILE11:
            case DOORTILE12:
            case DOORTILE14:
            case DOORTILE15:
            case DOORTILE16:
            case DOORTILE17:
            case DOORTILE18:
            case DOORTILE19:
            case DOORTILE20:
            case DOORTILE21:
            case DOORTILE22:
            case DOORTILE23:
                return true;
        }
        return false;
    }

    public static boolean isanunderoperator(int lotag) {
        switch (lotag & 0xff) {
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 22:
            case 26:
                return true;
        }
        return false;
    }

    public static boolean isanearoperator(int lotag) {
        switch (lotag & 0xff) {
            case 9:
            case 15:
            case 16:
            case 17:
            case 18:
            case 19:
            case 20:
            case 21:
            case 22:
            case 23:
            case 25:
            case 26:
            case 29:// Toothed door
                return true;
        }
        return false;
    }

    public static int ldist(Sprite s1, Sprite s2) {
        int vx = s1.getX() - s2.getX();
        int vy = s1.getY() - s2.getY();
        return (FindDistance2D(vx, vy) + 1);
    }

    public static int dist(Sprite s1, Sprite s2) {
        int vx = s1.getX() - s2.getX();
        int vy = s1.getY() - s2.getY();
        int vz = s1.getZ() - s2.getZ();
        return (FindDistance3D(vx, vy, vz >> 4));
    }

    public static int findplayer(Sprite s) {
        if (ud.multimode < 2) {
            player_dist = klabs(ps[myconnectindex].oposx - s.getX()) + klabs(ps[myconnectindex].oposy - s.getY())
                    + ((klabs(ps[myconnectindex].oposz - s.getZ() + (28 << 8))) >> 4);
            return myconnectindex;
        }

        int closest = 0x7fffffff;
        int closest_player = 0;

        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            Sprite psp = boardService.getSprite(ps[j].i);
            int x = klabs(ps[j].oposx - s.getX()) + klabs(ps[j].oposy - s.getY()) + ((klabs(ps[j].oposz - s.getZ() + (28 << 8))) >> 4);
            if (psp != null && x < closest && psp.getExtra() > 0) {
                closest_player = j;
                closest = x;
            }
        }

        player_dist = closest;
        return closest_player;
    }

    public static int findotherplayer(int p) {
        int closest = 0x7fffffff;
        int closest_player = p;

        for (int j = connecthead; j >= 0; j = connectpoint2[j]) {
            Sprite psp = boardService.getSprite(ps[j].i);
            if (p != j && psp != null && psp.getExtra() > 0) {
                int x = klabs(ps[j].oposx - ps[p].posx) + klabs(ps[j].oposy - ps[p].posy)
                        + (klabs(ps[j].oposz - ps[p].posz) >> 4);

                if (x < closest) {
                    closest_player = j;
                    closest = x;
                }
            }
        }

        player_dist = closest;
        return closest_player;
    }

    public static int checkcursectnums(int sect) {
        for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
            Sprite psp = boardService.getSprite(ps[i].i);
            if (psp != null && psp.getSectnum() == sect) {
                return i;
            }
        }
        return -1;
    }

    public static void animatecamsprite() {
        if (camsprite <= 0) {
            return;
        }

        int i = camsprite;

        if (hittype[i].temp_data[0] >= 11) {
            hittype[i].temp_data[0] = 0;

            Sprite spo = boardService.getSprite(i);
            Sprite psp = boardService.getSprite(ps[screenpeek].i);

            if (spo != null) {
                if (ps[screenpeek].newowner >= 0) {
                    spo.setOwner(ps[screenpeek].newowner);
                } else if (psp != null && spo.getOwner() >= 0 && dist(psp, spo) < 2048) {
                    ArtEntry viewScrEntry = engine.getTile(TILE_VIEWSCR);
                    if (!(viewScrEntry instanceof DynamicArtEntry) || !viewScrEntry.exists()) {
                        ArtEntry pic = engine.getTile(spo.getPicnum());
                        if (pic.hasSize()) {
                            engine.allocatepermanenttile(TILE_VIEWSCR, pic.getWidth(), pic.getHeight());
                        } else {
                            engine.allocatepermanenttile(TILE_VIEWSCR, 96, 96);
                        }
                    } else {
                        VIEWSCR_Lock = 255;
                    }

                    xyzmirror(spo.getOwner(), engine.getTileManager().getDynamicTile(TILE_VIEWSCR));
                }
            }
        } else {
            hittype[i].temp_data[0]++;
        }
    }

    public static void animatewalls() {
        for (int p = 0; p < numanimwalls; p++) {
            final int i = animwall[p].wallnum;
            Wall wal = boardService.getWall(i);
            if (wal == null) {
                continue;
            }

            int j = wal.getPicnum();
            switch (j) {
                case SCREENBREAK1:
                case SCREENBREAK2:
                case SCREENBREAK3:
                case SCREENBREAK4:
                case SCREENBREAK5:

                case SCREENBREAK9:
                case SCREENBREAK10:
                case SCREENBREAK11:
                case SCREENBREAK12:
                case SCREENBREAK13:
                case SCREENBREAK14:
                case SCREENBREAK15:
                case SCREENBREAK16:
                case SCREENBREAK17:
                case SCREENBREAK18:
                case SCREENBREAK19:

                    if ((engine.krand() & 255) < 16) {
                        animwall[p].tag = wal.getPicnum();
                        wal.setPicnum(SCREENBREAK6);
                    }

                    continue;

                case SCREENBREAK6:
                case SCREENBREAK7:
                case SCREENBREAK8:

                    if (animwall[p].tag >= 0 && wal.getExtra() != FEMPIC2 && wal.getExtra() != FEMPIC3) {
                        wal.setPicnum((short) animwall[p].tag);
                    } else {
                        wal.setPicnum(wal.getPicnum() + 1);
                        if (wal.getPicnum() == (SCREENBREAK6 + 3)) {
                            wal.setPicnum(SCREENBREAK6);
                        }
                    }
                    continue;

            }

            if ((wal.getCstat() & 16) != 0) {
                switch (wal.getOverpicnum()) {
                    case W_FORCEFIELD:
                    case W_FORCEFIELD + 1:
                    case W_FORCEFIELD + 2:

                        int t = animwall[p].tag;

                        if ((wal.getCstat() & 254) != 0) {
                            wal.setXpanning(wal.getXpanning() - (t >> 10));
                            wal.setYpanning(wal.getYpanning() - (t >> 10));

                            if (wal.getExtra() == 1) {
                                wal.setExtra(0);
                                animwall[p].tag = 0;
                            } else {
                                animwall[p].tag += 128;
                            }

                            if (animwall[p].tag < (128 << 4)) {
                                if ((animwall[p].tag & 128) != 0) {
                                    wal.setOverpicnum(W_FORCEFIELD);
                                } else {
                                    wal.setOverpicnum(W_FORCEFIELD + 1);
                                }
                            } else {
                                if ((engine.krand() & 255) < 32) {
                                    animwall[p].tag = 128 << (engine.krand() & 3);
                                } else {
                                    wal.setOverpicnum(W_FORCEFIELD + 1);
                                }
                            }
                        }

                        break;
                }
            }
        }
    }

    public static boolean activatewarpelevators(final int s, int d) // Parm = sectoreffectornum
    {
        Sprite spr = boardService.getSprite(s);
        if (spr == null) {
            return false;
        }

        int sn = spr.getSectnum();
        Sector sec = boardService.getSector(sn);
        if (sec == null) {
            return false;
        }

        // See if the sector exists
        ListNode<Sprite> n = boardService.getStatNode(3);
        while (n != null) {
            Sprite sp = n.get();
            if (sp.getLotag() == 17) {
                if (sp.getHitag() == spr.getHitag()) {
                    Sector sec2 = boardService.getSector(sp.getSectnum());
                    if (sec2 != null) {
                        if ((klabs(sec.getFloorz() - hittype[s].temp_data[2]) > sp.getYvel())
                                || (sec2.getHitag() == (sec.getHitag() - d))) {
                            break;
                        }
                    }
                }
            }
            n = n.getNext();
        }

        if (n == null) {
            return true; // No find
        } else {
            if (d == 0) {
                spritesound(ELEVATOR_OFF, s);
            } else {
                spritesound(ELEVATOR_ON, s);
            }
        }

        n = boardService.getStatNode(3);
        while (n != null) {
            Sprite sp = n.get();
            int i = n.getIndex();
            if (sp.getLotag() == 17) {
                if (sp.getHitag() == spr.getHitag()) {
                    hittype[i].temp_data[0] = d;
                    hittype[i].temp_data[1] = d; // Make all check warp
                }
            }
            n = n.getNext();
        }
        return false;
    }

    public static void operatesectors(final int sn, final int ii) {
        Sector ssec = boardService.getSector(sn);
        if (ssec == null) {
            return;
        }

        switch (ssec.getLotag() & (0xffff - 49152)) {
            case 30: {
                int j = ssec.getHitag();
                Sprite s = boardService.getSprite(j);
                if (s == null) {
                    break;
                }

                if (hittype[j].tempang == 0 || hittype[j].tempang == 256) {
                    callsound(sn, ii);
                }
                if (s.getExtra() == 1) {
                    s.setExtra(3);
                } else {
                    s.setExtra(1);
                }
                break;
            }
            case 31: {
                int j = ssec.getHitag();
                if (hittype[j].temp_data[4] == 0) {
                    hittype[j].temp_data[4] = 1;
                }

                callsound(sn, ii);
                break;
            }
            case 26: { // The split doors
                int i = getanimationgoal(ssec, CEILZ);
                if (i == -1) // if the door has stopped
                {
                    haltsoundhack = true;
                    ssec.setLotag(ssec.getLotag() & 0xff00);
                    ssec.setLotag(ssec.getLotag() | 22);
                    operatesectors(sn, ii);
                    ssec.setLotag(ssec.getLotag() & 0xff00);
                    ssec.setLotag(ssec.getLotag() | 9);
                    operatesectors(sn, ii);
                    ssec.setLotag(ssec.getLotag() & 0xff00);
                    ssec.setLotag(ssec.getLotag() | 26);
                }
                return;
            }
            case 9: {
                int dax, day, dax2, day2, sp;
                sp = ssec.getExtra() >> 4;

                // first find center point by averaging all points
                dax = 0;
                day = 0;
                for (ListNode<Wall> wn = ssec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal = wn.get();
                    dax += wal.getX();
                    day += wal.getY();
                }
                int startwall = ssec.getWallptr();
                int endwall = ssec.getEndWall();

                dax /= (endwall - startwall + 1);
                day /= (endwall - startwall + 1);

                // find any points with either same x or same y coordinate
                // as center (dax, day) - should be 2 points found.
                wallfind[0] = -1;
                wallfind[1] = -1;
                for (int i = startwall; i <= endwall; i++) {
                    Wall wal = boardService.getWall(i);
                    if (wal != null && ((wal.getX() == dax) || (wal.getY() == day))) {
                        if (wallfind[0] == -1) {
                            wallfind[0] = i;
                        } else {
                            wallfind[1] = i;
                        }
                    }
                }

                for (int j = 0; j < 2; j++) {
                    Wall wal = boardService.getWall(wallfind[j]);
                    if (wal != null && (wal.getX() == dax) && (wal.getY() == day)) {
                        // find what direction door should open by averaging the
                        // 2 neighboring points of wallfind[0] & wallfind[1].
                        int i = wallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall wal2 = boardService.getWall(i);
                        if (wal2 == null) {
                            continue;
                        }

                        dax2 = ((wal2.getX() + wal.getWall2().getX()) >> 1) - wal.getX();
                        day2 = ((wal2.getY() + wal.getWall2().getY()) >> 1) - wal.getY();
                        if (dax2 != 0) {
                            dax2 = wal.getWall2().getWall2().getX();
                            dax2 -= wal.getWall2().getX();
                            setanimation(sn, wallfind[j], wal.getX() + dax2, sp, WALLX);
                            setanimation(sn, i, wal2.getX() + dax2, sp, WALLX);
                            setanimation(sn, wal.getPoint2(), wal.getWall2().getX() + dax2, sp, WALLX);
                            callsound(sn, ii);
                        } else if (day2 != 0) {
                            day2 = wal.getWall2().getWall2().getY();
                            day2 -= wal.getWall2().getY();
                            setanimation(sn, wallfind[j], wal.getY() + day2, sp, WALLY);
                            setanimation(sn, i, wal2.getY() + day2, sp, WALLY);
                            setanimation(sn, wal.getPoint2(), wal.getWall2().getY() + day2, sp, WALLY);
                            callsound(sn, ii);
                        }
                    } else if (wal != null) {
                        int i = wallfind[j] - 1;
                        if (i < startwall) {
                            i = endwall;
                        }
                        Wall wal2 = boardService.getWall(i);
                        if (wal2 == null) {
                            continue;
                        }

                        dax2 = ((wal2.getX() + wal.getWall2().getX()) >> 1) - wal.getX();
                        day2 = ((wal2.getY() + wal.getWall2().getY()) >> 1) - wal.getY();
                        if (dax2 != 0) {
                            // setanimation(short animsect,long *animptr, long thegoal, long thevel)
                            setanimation(sn, wallfind[j], dax, sp, WALLX);
                            setanimation(sn, i, dax + dax2, sp, WALLX);
                            setanimation(sn, wal.getPoint2(), dax + dax2, sp, WALLX);
                            callsound(sn, ii);
                        } else if (day2 != 0) {
                            setanimation(sn, wallfind[j], day, sp, WALLY);
                            setanimation(sn, i, day + day2, sp, WALLY);
                            setanimation(sn, wal.getPoint2(), day + day2, sp, WALLY);
                            callsound(sn, ii);
                        }
                    }
                }

            }
            return;

            case 15: {// Warping elevators
                Sprite s = boardService.getSprite(ii);
                if (s == null || s.getPicnum() != APLAYER) {
                    return;
                }

                ListNode<Sprite> n = boardService.getSectNode(sn);
                while (n != null) {
                    if (n.get().getPicnum() == SECTOREFFECTOR && n.get().getLotag() == 17) {
                        break;
                    }
                    n = n.getNext();
                }

                if (n == null) {
                    return;
                }

                int i = n.getIndex();
                if (s.getSectnum() == sn) {
                    if (activatewarpelevators(i, -1)) {
                        activatewarpelevators(i, 1);
                    } else if (activatewarpelevators(i, 1)) {
                        activatewarpelevators(i, -1);
                    }
                } else {
                    if (ssec.getFloorz() > n.get().getZ()) {
                        activatewarpelevators(i, -1);
                    } else {
                        activatewarpelevators(i, 1);
                    }
                }
                return;
            }
            case 16:
            case 17: {
                int i = getanimationgoal(ssec, FLOORZ);
                if (i == -1) {
                    i = engine.nextsectorneighborz(sn, ssec.getFloorz(), 1, 1);
                    if (i == -1) {
                        i = engine.nextsectorneighborz(sn, ssec.getFloorz(), 1, -1);
                        if (i == -1) {
                            return;
                        }
                    }
                    Sector sec = boardService.getSector(i);
                    if (sec != null) {
                        int j = sec.getFloorz();
                        setanimation(sn, sn, j, ssec.getExtra(), FLOORZ);
                        callsound(sn, ii);
                    }
                }
                return;
            }
            case 18:
            case 19: {

                int i = getanimationgoal(ssec, FLOORZ);

                if (i == -1) {
                    i = engine.nextsectorneighborz(sn, ssec.getFloorz(), 1, -1);
                    if (i == -1) {
                        i = engine.nextsectorneighborz(sn, ssec.getFloorz(), 1, 1);
                    }
                    if (i == -1) {
                        return;
                    }

                    Sector sec = boardService.getSector(i);
                    if (sec != null) {
                        int j = sec.getFloorz();
                        int q = ssec.getExtra();
                        int l = ssec.getCeilingz() - ssec.getFloorz();
                        setanimation(sn, sn, j, q, FLOORZ);
                        setanimation(sn, sn, j + l, q, CEILZ);
                        callsound(sn, ii);
                    }
                }
                return;
            }
            case 29: {
                int j = 0;
                if ((ssec.getLotag() & 0x8000) != 0) {
                    Sector sec = boardService.getSector(engine.nextsectorneighborz(sn, ssec.getCeilingz(), 1, 1));
                    if (sec != null) {
                        j = sec.getFloorz();
                    }
                } else {
                    Sector sec = boardService.getSector(engine.nextsectorneighborz(sn, ssec.getCeilingz(), -1, -1));
                    if (sec != null) {
                        j = sec.getCeilingz();
                    }
                }

                ListNode<Sprite> ni = boardService.getStatNode(3); // Effectors
                while (ni != null) {
                    Sprite sp = ni.get();
                    if ((sp.getLotag() == 22) && (sp.getHitag() == ssec.getHitag())) {
                        Sector sec = boardService.getSector(sp.getSectnum());
                        if (sec != null) {
                            sec.setExtra(-sec.getExtra());
                            int i = ni.getIndex();
                            hittype[i].temp_data[0] = sn;
                            hittype[i].temp_data[1] = 1;
                        }
                    }
                    ni = ni.getNext();
                }

                ssec.setLotag(ssec.getLotag() ^ 0x8000);

                setanimation(sn, sn, j, ssec.getExtra(), CEILZ);

                callsound(sn, ii);

                return;
            }
            case 20: {
                int j = 0;
                boolean REDODOOR;
                do {
                    REDODOOR = false;
                    if ((ssec.getLotag() & 0x8000) != 0) {
                        ListNode<Sprite> node = boardService.getSectNode(sn);
                        while (node != null) {
                            Sprite sp = node.get();
                            if (sp.getStatnum() == 3 && sp.getLotag() == 9) {
                                j = sp.getZ();
                                break;
                            }
                            node = node.getNext();
                        }
                        if (node == null) {
                            j = ssec.getFloorz();
                        }
                    } else {
                        j = engine.nextsectorneighborz(sn, ssec.getCeilingz(), -1, -1);
                        Sector sec = boardService.getSector(j);
                        if (sec != null) {
                            j = sec.getCeilingz();
                        } else {
                            ssec.setLotag(ssec.getLotag() | 32768);
                            REDODOOR = true;
                        }
                    }
                } while (REDODOOR);

                ssec.setLotag(ssec.getLotag() ^ 0x8000);

                setanimation(sn, sn, j, ssec.getExtra(), CEILZ);
                callsound(sn, ii);

                return;
            }
            case 21: {
                int i = getanimationgoal(ssec, FLOORZ);
                if (i >= 0) {
                    if (gAnimationData[i].goal == ssec.getCeilingz()) {
                        Sector sec = boardService.getSector(engine.nextsectorneighborz(sn, ssec.getCeilingz(), 1, 1));
                        if (sec != null) {
                            gAnimationData[i].goal = sec.getFloorz();
                        }
                    } else {
                        gAnimationData[i].goal = ssec.getCeilingz();
                    }
                } else {
                    int j = ssec.getCeilingz();
                    if (ssec.getCeilingz() == ssec.getFloorz()) {
                        Sector sec = boardService.getSector(engine.nextsectorneighborz(sn, ssec.getCeilingz(), 1, 1));
                        if (sec != null) {
                            j = sec.getFloorz();
                        }
                    }

                    ssec.setLotag(ssec.getLotag() ^ 0x8000);
                    if (setanimation(sn, sn, j, ssec.getExtra(), FLOORZ) >= 0) {
                        callsound(sn, ii);
                    }
                }
                return;
            }
            case 22:

                // REDODOOR22:

                if ((ssec.getLotag() & 0x8000) != 0) {
                    int q = (ssec.getCeilingz() + ssec.getFloorz()) >> 1;
                    setanimation(sn, sn, q, ssec.getExtra(), FLOORZ);
                    setanimation(sn, sn, q, ssec.getExtra(), CEILZ);
                } else {
                    int fsect = engine.nextsectorneighborz(sn, ssec.getFloorz(), 1, 1);
                    Sector sec = boardService.getSector(fsect);
                    if (sec != null) {
                        int q = sec.getFloorz();
                        setanimation(sn, sn, q, ssec.getExtra(), FLOORZ);
                    }
                    int csect = engine.nextsectorneighborz(sn, ssec.getCeilingz(), -1, -1);
                    sec = boardService.getSector(csect);
                    if (sec != null) {
                        int q = sec.getCeilingz();
                        setanimation(sn, sn, q, ssec.getExtra(), CEILZ);
                    }
                }

                ssec.setLotag(ssec.getLotag() ^ 0x8000);

                callsound(sn, ii);

                return;

            case 23: { // Swingdoor
                int q = 0;

                ListNode<Sprite> ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    int i = ni.getIndex();
                    if (sp.getLotag() == 11 && sp.getSectnum() == sn && hittype[i].temp_data[4] == 0) {
                        break;
                    }
                    ni = ni.getNext();
                }

                if (ni == null) {
                    return;
                }

                final Sprite s = ni.get();
                Sector seci = boardService.getSector(s.getSectnum());
                if (seci == null) {
                    return;
                }

                int l = seci.getLotag() & 0x8000;
                ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    Sector sec = boardService.getSector(sp.getSectnum());

                    int i = ni.getIndex();
                    if (sec != null && l == (sec.getLotag() & 0x8000) && sp.getLotag() == 11
                            && s.getHitag() == sp.getHitag() && hittype[i].temp_data[4] == 0) {
                        if ((sec.getLotag() & 0x8000) != 0) {
                            sec.setLotag(sec.getLotag() & 0x7fff);
                        } else {
                            sec.setLotag(sec.getLotag() | 0x8000);
                        }
                        hittype[i].temp_data[4] = 1;
                        hittype[i].temp_data[3] = -hittype[i].temp_data[3];
                        if (q == 0) {
                            callsound(sn, i);
                            q = 1;
                        }
                    }
                    ni = ni.getNext();
                }

                return;
            }
            case 25: {// Subway type sliding doors

                ListNode<Sprite> ni = boardService.getStatNode(3);
                while (ni != null)// Find the sprite
                {
                    Sprite sp = ni.get();
                    if ((sp.getLotag()) == 15 && sp.getSectnum() == sn) {
                        break; // Found the sectoreffector.
                    }
                    ni = ni.getNext();
                }

                if (ni == null) {
                    return;
                }

                Sprite spr = ni.get();
                ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    int i = ni.getIndex();
                    if (sp.getHitag() == spr.getHitag()) {
                        if (sp.getLotag() == 15) {
                            Sector sec = boardService.getSector(sp.getSectnum());
                            if (sec != null) {
                                sec.setLotag(sec.getLotag() ^ 0x8000); // Toggle the open or close
                                sp.setAng(sp.getAng() + 1024);
                                if (hittype[i].temp_data[4] != 0) {
                                    callsound(sp.getSectnum(), i);
                                }
                                callsound(sp.getSectnum(), i);
                                if ((sec.getLotag() & 0x8000) != 0) {
                                    hittype[i].temp_data[4] = 1;
                                } else {
                                    hittype[i].temp_data[4] = 2;
                                }
                            }
                        }
                    }
                    ni = ni.getNext();
                }
                return;
            }
            case 27: { // Extended bridge
                ListNode<Sprite> ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    if ((sp.getLotag() & 0xff) == 20 && sp.getSectnum() == sn) { // Bridge
                        ssec.setLotag(ssec.getLotag() ^ 0x8000);
                        if ((ssec.getLotag() & 0x8000) != 0) { // OPENING
                            hittype[ni.getIndex()].temp_data[0] = 1;
                        } else {
                            hittype[ni.getIndex()].temp_data[0] = 2;
                        }
                        callsound(sn, ii);
                        break;
                    }
                    ni = ni.getNext();
                }
                return;
            }
            case 28: {
                // activate the rest of them
                int j = -1;
                ListNode<Sprite> ni = boardService.getSectNode(sn);
                while (ni != null) {
                    Sprite sp = ni.get();
                    if (sp.getStatnum() == 3 && (sp.getLotag() & 0xff) == 21) {
                        break; // Found it
                    }
                    ni = ni.getNext();
                }

                if (ni != null) {
                    j = ni.get().getHitag();
                }

                ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    int l = ni.getIndex();
                    if ((sp.getLotag() & 0xff) == 21 && hittype[l].temp_data[0] == 0 && (sp.getHitag()) == j) {
                        hittype[l].temp_data[0] = 1;
                    }
                    ni = ni.getNext();
                }
                callsound(sn, ii);
            }
        }
    }

    public static void operaterespawns(int low) {
        ListNode<Sprite> ni = boardService.getStatNode(11);
        while (ni != null) {
            ListNode<Sprite> nextni = ni.getNext();
            Sprite sp = ni.get();
            if (sp.getLotag() == low) {
                if (sp.getPicnum() == RESPAWN) {
                    if (sp.getHitag() < 0 || sp.getHitag() >= MAXTILES
                            || (badguypic(sp.getHitag()) && ud.monsters_off)) {
                        break;
                    }

                    int j = spawn(ni.getIndex(), TRANSPORTERSTAR);
                    Sprite sj = boardService.getSprite(j);
                    if (sj != null) {
                        sj.setZ(sj.getZ() - (32 << 8));
                    }
                    sp.setExtra(66 - 12); // Just a way to killit
                }
            }
            ni = nextni;
        }
    }

    public static void operateactivators(int low, int snum) {
        for (int i = numcyclers - 1; i >= 0; i--) {
            short[] p = cyclers[i];

            if (p[4] == low) {
                p[5] ^= 1;

                Sector sec = boardService.getSector(p[0]);
                if (sec != null) {
                    sec.setCeilingshade(p[3]);
                    sec.setFloorshade(p[3]);
                    for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                        Wall wal = wn.get();
                        wal.setShade((byte) p[3]);
                    }
                }
            }
        }

        int k = -1;
        ListNode<Sprite> ni = boardService.getStatNode(8);
        while (ni != null) {
            Sprite sp = ni.get();
            Sector sec = boardService.getSector(sp.getSectnum());

            if (sec != null && sp.getLotag() == low) {
                if (sp.getPicnum() == ACTIVATORLOCKED) {
                    if ((sec.getLotag() & 16384) != 0) {
                        sec.setLotag(sec.getLotag() & 65535 - 16384);
                    } else {
                        sec.setLotag(sec.getLotag() | 16384);
                    }

                    if (snum >= 0) {
                        if ((sec.getLotag() & 16384) != 0) {
                            FTA(4, ps[snum]);
                        } else {
                            FTA(8, ps[snum]);
                        }
                    }
                } else {
                    switch (sp.getHitag()) {
                        case 0:
                            break;
                        case 1:
                            if (sec.getFloorz() != sec.getCeilingz()) {
                                ni = ni.getNext();
                                continue;
                            }
                            break;
                        case 2:
                            if (sec.getFloorz() == sec.getCeilingz()) {
                                ni = ni.getNext();
                                continue;
                            }
                            break;
                    }

                    if (sec.getLotag() < 3) {
                        ListNode<Sprite> node = boardService.getSectNode(sp.getSectnum());
                        while (node != null) {
                            Sprite jspr = node.get();
                            if (jspr.getStatnum() == 3) {
                                switch (jspr.getLotag()) {
                                    case 36:
                                    case 31:
                                    case 32:
                                    case 18:
                                        int j = node.getIndex();
                                        hittype[j].temp_data[0] = 1 - hittype[j].temp_data[0];
                                        callsound(sp.getSectnum(), j);
                                        break;
                                }
                            }
                            node = node.getNext();
                        }
                    }

                    int i = ni.getIndex();
                    if (k == -1 && (sec.getLotag() & 0xff) == 22) {
                        k = (short) callsound(sp.getSectnum(), i);
                    }
                    operatesectors(sp.getSectnum(), i);
                }
            }
            ni = ni.getNext();
        }

        operaterespawns(low);
    }

    public static void operatemasterswitches(int low) {
        ListNode<Sprite> node = boardService.getStatNode(6);
        while (node != null) {
            Sprite sp = node.get();
            if (sp.getPicnum() == MASTERSWITCH && sp.getLotag() == low && sp.getYvel() == 0) {
                sp.setYvel(1);
            }
            node = node.getNext();
        }
    }

    public static void operateforcefields(int s, int low) {
        for (int p = numanimwalls; p >= 0; p--) {
            int i = animwall[p].wallnum;
            Wall wal = boardService.getWall(i);
            if (wal == null) {
                continue;
            }

            if (low == wal.getLotag() || low == -1) {
                switch (wal.getOverpicnum()) {
                    case W_FORCEFIELD:
                    case W_FORCEFIELD + 1:
                    case W_FORCEFIELD + 2:
                    case BIGFORCE:

                        animwall[p].tag = 0;

                        if (wal.getCstat() != 0) {
                            wal.setCstat(0);

                            Sprite sp = boardService.getSprite(s);
                            if (sp != null && sp.getPicnum() == SECTOREFFECTOR && sp.getLotag() == 30) {
                                wal.setLotag(0);
                            }
                        } else {
                            wal.setCstat(85);
                        }
                        break;
                }
            }
        }
    }

    public static boolean checkhitswitch(int snum, int w, int switchtype) {
        if (w < 0) {
            return false;
        }
        int correctdips = 1;
        int numdips = 0;

        int switchpal;
        int lotag, hitag, picnum;
        int sx, sy;
        if (switchtype == 1) // A wall sprite
        {
            Sprite s = boardService.getSprite(w);
            if (s == null) {
                return false;
            }

            lotag = s.getLotag();
            if (lotag == 0) {
                return false;
            }
            hitag = s.getHitag();
            sx = s.getX();
            sy = s.getY();
            picnum = s.getPicnum();
            switchpal = s.getPal();
        } else {
            Wall wal = boardService.getWall(w);
            if (wal == null) {
                return false;
            }

            lotag = wal.getLotag();
            if (lotag == 0) {
                return false;
            }
            hitag = wal.getHitag();
            sx = wal.getX();
            sy = wal.getY();
            picnum = wal.getPicnum();
            switchpal = wal.getPal();
        }

        switch (picnum) {
            case DIPSWITCH:
            case DIPSWITCH + 1:
            case TECHSWITCH:
            case TECHSWITCH + 1:
            case ALIENSWITCH:
            case ALIENSWITCH + 1:
                break;
            case DEVELOPERCOMMENTARY + 1: // Twentieth Anniversary World Tour
                if (switchtype == 1) {
                    Sprite s = boardService.getSprite(w);
                    if (s != null) {
                        s.setPicnum(s.getPicnum() - 1);
                        StopCommentary(pCommentary);
                    }
                    return true;
                }
                return false;
            case DEVELOPERCOMMENTARY: // Twentieth Anniversary World Tour
                if (switchtype == 1) {
                    Sprite s = boardService.getSprite(w);
                    if (s != null && StartCommentary(lotag, w)) {
                        s.setPicnum(s.getPicnum() + 1);
                    }
                    return true;
                }
                return false;
            case ACCESSSWITCH:
            case ACCESSSWITCH2:
                if (ps[snum].access_incs == 0) {
                    if (switchpal == 0) {
                        if ((ps[snum].got_access & 1) != 0) {
                            ps[snum].access_incs = 1;
                        } else {
                            FTA(70, ps[snum]);
                        }
                    } else if (switchpal == 21) {
                        if ((ps[snum].got_access & 2) != 0) {
                            ps[snum].access_incs = 1;
                        } else {
                            FTA(71, ps[snum]);
                        }
                    } else if (switchpal == 23) {
                        if ((ps[snum].got_access & 4) != 0) {
                            ps[snum].access_incs = 1;
                        } else {
                            FTA(72, ps[snum]);
                        }
                    }

                    if (ps[snum].access_incs == 1) {
                        if (switchtype == 0) {
                            ps[snum].access_wallnum = (short) w;
                        } else {
                            ps[snum].access_spritenum = (short) w;
                        }
                    }

                    return false;
                }
            case DIPSWITCH2:
            case DIPSWITCH2 + 1:
            case DIPSWITCH3:
            case DIPSWITCH3 + 1:
            case MULTISWITCH:
            case MULTISWITCH + 1:
            case MULTISWITCH + 2:
            case MULTISWITCH + 3:
            case PULLSWITCH:
            case PULLSWITCH + 1:
            case HANDSWITCH:
            case HANDSWITCH + 1:
            case SLOTDOOR:
            case SLOTDOOR + 1:
            case LIGHTSWITCH:
            case LIGHTSWITCH + 1:
            case SPACELIGHTSWITCH:
            case SPACELIGHTSWITCH + 1:
            case SPACEDOORSWITCH:
            case SPACEDOORSWITCH + 1:
            case FRANKENSTINESWITCH:
            case FRANKENSTINESWITCH + 1:
            case LIGHTSWITCH2:
            case LIGHTSWITCH2 + 1:
            case POWERSWITCH1:
            case POWERSWITCH1 + 1:
            case LOCKSWITCH1:
            case LOCKSWITCH1 + 1:
            case POWERSWITCH2:
            case POWERSWITCH2 + 1:
                if (check_activator_motion(lotag)) {
                    return false;
                }
                break;
            default:
                if (!isadoorwall(picnum)) {
                    return false;
                }
                break;
        }

        ListNode<Sprite> node = boardService.getStatNode(0);
        while (node != null) {
            Sprite sp = node.get();
            if (lotag == sp.getLotag()) {
                switch (sp.getPicnum()) {
                    case DIPSWITCH:
                    case TECHSWITCH:
                    case ALIENSWITCH:
                        if (switchtype == 1 && w == node.getIndex()) {
                            sp.setPicnum(sp.getPicnum() + 1);
                        } else if (sp.getHitag() == 0) {
                            correctdips++;
                        }
                        numdips++;
                        break;
                    case TECHSWITCH + 1:
                    case DIPSWITCH + 1:
                    case ALIENSWITCH + 1:
                        if (switchtype == 1 && w == node.getIndex()) {
                            sp.setPicnum(sp.getPicnum() - 1);
                        } else if (sp.getHitag() == 1) {
                            correctdips++;
                        }
                        numdips++;
                        break;
                    case MULTISWITCH:
                    case MULTISWITCH + 1:
                    case MULTISWITCH + 2:
                    case MULTISWITCH + 3:
                        sp.setPicnum(sp.getPicnum() + 1);
                        if (sp.getPicnum() > (MULTISWITCH + 3)) {
                            sp.setPicnum(MULTISWITCH);
                        }
                        break;
                    case ACCESSSWITCH:
                    case ACCESSSWITCH2:
                    case SLOTDOOR:
                    case LIGHTSWITCH:
                    case SPACELIGHTSWITCH:
                    case SPACEDOORSWITCH:
                    case FRANKENSTINESWITCH:
                    case LIGHTSWITCH2:
                    case POWERSWITCH1:
                    case LOCKSWITCH1:
                    case POWERSWITCH2:
                    case HANDSWITCH:
                    case PULLSWITCH:
                    case DIPSWITCH2:
                    case DIPSWITCH3:
                        sp.setPicnum(sp.getPicnum() + 1);
                        break;
                    case PULLSWITCH + 1:
                    case HANDSWITCH + 1:
                    case LIGHTSWITCH2 + 1:
                    case POWERSWITCH1 + 1:
                    case LOCKSWITCH1 + 1:
                    case POWERSWITCH2 + 1:
                    case SLOTDOOR + 1:
                    case LIGHTSWITCH + 1:
                    case SPACELIGHTSWITCH + 1:
                    case SPACEDOORSWITCH + 1:
                    case FRANKENSTINESWITCH + 1:
                    case DIPSWITCH2 + 1:
                    case DIPSWITCH3 + 1:
                        sp.setPicnum(sp.getPicnum() - 1);
                        break;
                }
            }
            node = node.getNext();
        }

        for (int i = 0; i < boardService.getWallCount(); i++) {
            Wall wal = boardService.getWall(i);
            if (wal == null) {
                continue;
            }

            if (lotag == wal.getLotag()) {
                switch (wal.getPicnum()) {
                    case DIPSWITCH:
                    case TECHSWITCH:
                    case ALIENSWITCH:
                        if (switchtype == 0 && i == w) {
                            wal.setPicnum(wal.getPicnum() + 1);
                        } else if (wal.getHitag() == 0) {
                            correctdips++;
                        }
                        numdips++;
                        break;
                    case DIPSWITCH + 1:
                    case TECHSWITCH + 1:
                    case ALIENSWITCH + 1:
                        if (switchtype == 0 && i == w) {
                            wal.setPicnum(wal.getPicnum() - 1);
                        } else if (wal.getHitag() == 1) {
                            correctdips++;
                        }
                        numdips++;
                        break;
                    case MULTISWITCH:
                    case MULTISWITCH + 1:
                    case MULTISWITCH + 2:
                    case MULTISWITCH + 3:
                        wal.setPicnum(wal.getPicnum() + 1);
                        if (wal.getPicnum() > (MULTISWITCH + 3)) {
                            wal.setPicnum(MULTISWITCH);
                        }
                        break;
                    case ACCESSSWITCH:
                    case ACCESSSWITCH2:
                    case SLOTDOOR:
                    case LIGHTSWITCH:
                    case SPACELIGHTSWITCH:
                    case SPACEDOORSWITCH:
                    case LIGHTSWITCH2:
                    case POWERSWITCH1:
                    case LOCKSWITCH1:
                    case POWERSWITCH2:
                    case PULLSWITCH:
                    case HANDSWITCH:
                    case DIPSWITCH2:
                    case DIPSWITCH3:
                        wal.setPicnum(wal.getPicnum() + 1);
                        break;
                    case HANDSWITCH + 1:
                    case PULLSWITCH + 1:
                    case LIGHTSWITCH2 + 1:
                    case POWERSWITCH1 + 1:
                    case LOCKSWITCH1 + 1:
                    case POWERSWITCH2 + 1:
                    case SLOTDOOR + 1:
                    case LIGHTSWITCH + 1:
                    case SPACELIGHTSWITCH + 1:
                    case SPACEDOORSWITCH + 1:
                    case DIPSWITCH2 + 1:
                    case DIPSWITCH3 + 1:
                        wal.setPicnum(wal.getPicnum() - 1);
                        break;
                }
            }
        }

        if (lotag == (short) 65535) {
            LeaveMap();
            if (ud.from_bonus != 0) {
                ud.level_number = ud.from_bonus;
                ud.from_bonus = 0;
            } else {
                ud.level_number++;
            }
            return true;
        }

        final boolean b = picnum == DIPSWITCH || picnum == DIPSWITCH + 1 || picnum == ALIENSWITCH || picnum == ALIENSWITCH + 1
                || picnum == TECHSWITCH || picnum == TECHSWITCH + 1;

        switch (picnum) {
            default:
                if (!isadoorwall(picnum)) {
                    break;
                }
            case DIPSWITCH:
            case DIPSWITCH + 1:
            case TECHSWITCH:
            case TECHSWITCH + 1:
            case ALIENSWITCH:
            case ALIENSWITCH + 1:
                if (b) {
                    if (picnum == ALIENSWITCH || picnum == ALIENSWITCH + 1) {
                        if (switchtype == 1) {
                            xyzsound(ALIEN_SWITCH1, w, sx, sy, ps[snum].posz);
                        } else {
                            xyzsound(ALIEN_SWITCH1, ps[snum].i, sx, sy, ps[snum].posz);
                        }
                    } else {
                        if (switchtype == 1) {
                            xyzsound(SWITCH_ON, w, sx, sy, ps[snum].posz);
                        } else {
                            xyzsound(SWITCH_ON, ps[snum].i, sx, sy, ps[snum].posz);
                        }
                    }
                    if (numdips != correctdips) {
                        break;
                    }
                    xyzsound(END_OF_LEVEL_WARN, ps[snum].i, sx, sy, ps[snum].posz);
                }
            case DIPSWITCH2:
            case DIPSWITCH2 + 1:
            case DIPSWITCH3:
            case DIPSWITCH3 + 1:
            case MULTISWITCH:
            case MULTISWITCH + 1:
            case MULTISWITCH + 2:
            case MULTISWITCH + 3:
            case ACCESSSWITCH:
            case ACCESSSWITCH2:
            case SLOTDOOR:
            case SLOTDOOR + 1:
            case LIGHTSWITCH:
            case LIGHTSWITCH + 1:
            case SPACELIGHTSWITCH:
            case SPACELIGHTSWITCH + 1:
            case SPACEDOORSWITCH:
            case SPACEDOORSWITCH + 1:
            case FRANKENSTINESWITCH:
            case FRANKENSTINESWITCH + 1:
            case LIGHTSWITCH2:
            case LIGHTSWITCH2 + 1:
            case POWERSWITCH1:
            case POWERSWITCH1 + 1:
            case LOCKSWITCH1:
            case LOCKSWITCH1 + 1:
            case POWERSWITCH2:
            case POWERSWITCH2 + 1:
            case HANDSWITCH:
            case HANDSWITCH + 1:
            case PULLSWITCH:
            case PULLSWITCH + 1:

                if (picnum == MULTISWITCH || picnum == (MULTISWITCH + 1) || picnum == (MULTISWITCH + 2)
                        || picnum == (MULTISWITCH + 3)) {
                    lotag += picnum - MULTISWITCH;
                }

                ListNode<Sprite> nx = boardService.getStatNode(3);
                while (nx != null) {
                    Sprite sp = nx.get();
                    int x = nx.getIndex();
                    if (((sp.getHitag()) == lotag)) {
                        switch (sp.getLotag()) {
                            case 12: {
                                Sector sec = boardService.getSector(sp.getSectnum());
                                if (sec != null) {
                                    sec.setFloorpal(0);
                                }
                                hittype[x].temp_data[0]++;
                                if (hittype[x].temp_data[0] == 2) {
                                    hittype[x].temp_data[0]++;
                                }
                                break;
                            }
                            case 24:
                            case 34:
                            case 25:
                                hittype[x].temp_data[4] ^= 1;
                                if (hittype[x].temp_data[4] != 0) {
                                    FTA(15, ps[snum]);
                                } else {
                                    FTA(2, ps[snum]);
                                }
                                break;
                            case 21:
                                FTA(2, ps[screenpeek]);
                                break;
                        }
                    }
                    nx = nx.getNext();
                }

                operateactivators(lotag, snum);
                operateforcefields(ps[snum].i, lotag);
                operatemasterswitches(lotag);

                if (b) {
                    return true;
                }

                if (hitag == 0 && !isadoorwall(picnum)) {
                    if (switchtype == 1) {
                        xyzsound(SWITCH_ON, w, sx, sy, ps[snum].posz);
                    } else {
                        xyzsound(SWITCH_ON, ps[snum].i, sx, sy, ps[snum].posz);
                    }
                } else if (hitag != 0 && hitag < NUM_SOUNDS) {
                    if (switchtype == 1 && (currentGame.getCON().soundm[hitag] & 4) == 0) {
                        xyzsound(hitag, w, sx, sy, ps[snum].posz);
                    } else {
                        spritesound(hitag, ps[snum].i);
                    }
                }

                return true;
        }
        return false;
    }

    public static void activatebysector(int sect, int j) {
        boolean didit = false;

        ListNode<Sprite> node = boardService.getSectNode(sect);
        while (node != null) {
            Sprite sp = node.get();
            if (sp.getPicnum() == ACTIVATOR) {
                operateactivators(sp.getLotag(), -1);
                didit = true;
            }
            node = node.getNext();
        }

        if (!didit) {
            operatesectors(sect, j);
        }
    }

    public static void checkplayerhurt(PlayerStruct p, int moveHit) {
        if ((moveHit & kHitTypeMask) == kHitSprite) {
            moveHit &= kHitIndexMask;

            Sprite hsp = boardService.getSprite(moveHit);
            if (hsp != null && hsp.getPicnum() == CACTUS) {
                if (p.hurt_delay < 8) {
                    Sprite psp =  boardService.getSprite(p.i);
                    if (psp != null) {
                       psp.setExtra(psp.getExtra() - 5);
                    }

                    p.hurt_delay = 16;
                    p.pals_time = 32;
                    p.pals[0] = 32;
                    p.pals[1] = 0;
                    p.pals[2] = 0;
                    spritesound(DUKE_LONGTERM_PAIN, p.i);
                }
            }
            return;
        }

        if ((moveHit & kHitTypeMask) != HIT_WALL) {
            return;
        }
        moveHit &= kHitIndexMask;
        Wall hw = boardService.getWall(moveHit);
        if (hw == null) {
            return;
        }

        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        if (p.hurt_delay > 0) {
            p.hurt_delay--;
        } else if ((hw.getCstat() & 85) != 0) {
            switch (hw.getOverpicnum()) {
                case W_FORCEFIELD:
                case W_FORCEFIELD + 1:
                case W_FORCEFIELD + 2:
                    psp.setExtra(psp.getExtra() - 5);

                    p.hurt_delay = 16;
                    p.pals_time = 32;
                    p.pals[0] = 32;
                    p.pals[1] = 0;
                    p.pals[2] = 0;

                    if (IsOriginalDemo()) {
                        p.posxv = -(EngineUtils.cos(((int) p.ang) & 2047) << 8);
                        p.posyv = -(EngineUtils.sin(((int) p.ang) & 2047) << 8);
                        spritesound(DUKE_LONGTERM_PAIN, p.i);

                        checkhitwall(p.i, moveHit,
                                p.posx + (EngineUtils.cos((int) p.ang & 2047) >> 9),
                                p.posy + (EngineUtils.sin((int) p.ang & 2047) >> 9), p.posz, -1);
                    } else {
                        p.posxv = (int) -(BCosAngle(BClampAngle(p.ang)) * 256.0f);
                        p.posyv = (int) -(BSinAngle(BClampAngle(p.ang)) * 256.0f);
                        spritesound(DUKE_LONGTERM_PAIN, p.i);

                        checkhitwall(p.i, moveHit, (int) (p.posx + (BCosAngle(BClampAngle(p.ang)) / 512.0f)),
                                (int) (p.posy + (BSinAngle(BClampAngle(p.ang)) / 512.0f)), p.posz, -1);
                    }

                    break;

                case BIGFORCE:
                    p.hurt_delay = 26;
                    if (IsOriginalDemo()) {
                        checkhitwall(p.i, moveHit, p.posx + (EngineUtils.sin(((int) p.ang + 512) & 2047) >> 9),
                                p.posy + (EngineUtils.sin((int) p.ang & 2047) >> 9), p.posz, -1);
                    } else {
                        checkhitwall(p.i, moveHit, (int) (p.posx + (BCosAngle(BClampAngle(p.ang)) / 512.0f)),
                                (int) (p.posy + (BSinAngle(BClampAngle(p.ang)) / 512.0f)), p.posz, -1);
                    }
                    break;

            }
        }
    }

    public static void allignwarpelevators() {
        ListNode<Sprite> ni = boardService.getStatNode(3);
        while (ni != null) {
            Sprite sp = ni.get();
            if (sp.getLotag() == 17 && sp.getShade() > 16) {
                ListNode<Sprite> node = boardService.getStatNode(3);
                while (node != null) {
                    Sprite sp2 = node.get();
                    if ((sp2.getLotag()) == 17 && ni.getIndex() != node.getIndex() && (sp.getHitag()) == (sp2.getHitag())) {
                        Sector sec = boardService.getSector(sp.getSectnum());
                        Sector sec2 = boardService.getSector(sp2.getSectnum());
                        if (sec != null && sec2 != null) {
                            sec2.setFloorz(sec.getFloorz());
                            sec2.setCeilingz(sec.getCeilingz());
                        }
                    }

                    node = node.getNext();
                }
            }
            ni = ni.getNext();
        }
    }

    public static void breakwall(int newpn, int spr, int dawallnum) {
        Wall wal = boardService.getWall(dawallnum);
        if (wal != null) {
            wal.setPicnum((short) newpn);
            spritesound(VENT_BUST, spr);
            spritesound(GLASS_HEAVYBREAK, spr);
            lotsofglass(spr, dawallnum, 10);
        }
    }

    public static void checkhitwall(int spr, final int dawallnum, int x, int y, int z, int atwith) {
        Wall wal = boardService.getWall(dawallnum);
        if (wal == null) {
            return;
        }

        if (wal.getOverpicnum() == MIRROR) {
            switch (atwith) {
                case HEAVYHBOMB:
                case RADIUSEXPLOSION:
                case RPG:
                case HYDRENT:
                case SEENINE:
                case OOZFILTER:
                case EXPLODINGBARREL:
                    lotsofglass(spr, dawallnum, 70);
                    wal.setCstat(wal.getCstat() & ~16);
                    wal.setOverpicnum(MIRRORBROKE);
                    spritesound(GLASS_HEAVYBREAK, spr);
                    return;
            }
        }

        Sector nsec = boardService.getSector(wal.getNextsector());
        if (((wal.getCstat() & 16) != 0 || wal.getOverpicnum() == BIGFORCE) && nsec != null) {
            if (nsec.getFloorz() > z) {
                if (nsec.getFloorz() - nsec.getCeilingz() != 0) {
                    switch (wal.getOverpicnum()) {
                        case W_FORCEFIELD:
                        case W_FORCEFIELD + 1:
                        case W_FORCEFIELD + 2:
                            wal.setExtra(1); // tell the forces to animate
                        case BIGFORCE: {
                            int sn = engine.updatesector(x, y, -1);
                            if (sn < 0) {
                                return;
                            }

                            int i = -1;
                            if (atwith == -1) {
                                i = EGS(sn, x, y, z, FORCERIPPLE, -127, 8, 8, 0, 0, 0, spr, (short) 5);
                            } else {
                                if (atwith == CHAINGUN) {
                                    Sprite sp = boardService.getSprite(spr);
                                    if (sp != null) {
                                        i = EGS(sn, x, y, z, FORCERIPPLE, -127, 16 + sp.getXrepeat(),
                                                16 + sp.getYrepeat(), 0, 0, 0, spr, 5);
                                    }
                                } else {
                                    i = EGS(sn, x, y, z, FORCERIPPLE, -127, 32, 32, 0, 0, 0, spr, (short) 5);
                                }
                            }

                            Sprite sp = boardService.getSprite(i);
                            if (sp != null) {
                                sp.setCstat(sp.getCstat() | 18 + 128);
                                sp.setAng(wal.getWallAngle() - 512);

                                spritesound(SOMETHINGHITFORCE, i);
                            }

                            return;
                        }
                        case FANSPRITE: {
                            wal.setOverpicnum(FANSPRITEBROKE);
                            wal.setCstat(wal.getCstat() & 65535 - 65);
                            Wall nw = boardService.getWall(wal.getNextwall());
                            if (nw != null) {
                                nw.setOverpicnum(FANSPRITEBROKE);
                                nw.setCstat(nw.getCstat() & 65535 - 65);
                            }
                            spritesound(VENT_BUST, spr);
                            spritesound(GLASS_BREAKING, spr);
                            return;
                        }
                        case GLASS: {
                            int sn = engine.updatesector(x, y, -1);
                            if (sn < 0) {
                                return;
                            }
                            wal.setOverpicnum(GLASS2);
                            lotsofglass(spr, dawallnum, 10);
                            wal.setCstat(0);

                            Wall nw = boardService.getWall(wal.getNextwall());
                            if (nw != null) {
                                nw.setCstat(0);
                            }

                            int i = EGS(sn, x, y, z, SECTOREFFECTOR, 0, 0, 0, (short) ps[0].ang, 0, 0, spr, (short) 3);

                            Sprite sp =  boardService.getSprite(i);
                            if (sp != null) {
                                sp.setLotag(128);
                                hittype[i].temp_data[1] = 5;
                                hittype[i].temp_data[2] = dawallnum;
                                spritesound(GLASS_BREAKING, i);
                            }
                            return;
                        }
                        case STAINGLASS1: {
                            int sn = engine.updatesector(x, y, -1);
                            if (sn < 0) {
                                return;
                            }
                            lotsofcolourglass(spr, dawallnum, 80);
                            wal.setCstat(0);
                            Wall nw = boardService.getWall(wal.getNextwall());
                            if (nw != null) {
                                nw.setCstat(0);
                            }
                            spritesound(VENT_BUST, spr);
                            spritesound(GLASS_BREAKING, spr);
                            return;
                        }
                    }
                }
            }
        }

        switch (wal.getPicnum()) {
            case COLAMACHINE:
            case VENDMACHINE:
                breakwall(wal.getPicnum() + 2, spr, dawallnum);
                spritesound(VENT_BUST, spr);
                return;

            case OJ:
            case FEMPIC2:
            case FEMPIC3:

            case SCREENBREAK6:
            case SCREENBREAK7:
            case SCREENBREAK8:

            case SCREENBREAK1:
            case SCREENBREAK2:
            case SCREENBREAK3:
            case SCREENBREAK4:
            case SCREENBREAK5:

            case SCREENBREAK9:
            case SCREENBREAK10:
            case SCREENBREAK11:
            case SCREENBREAK12:
            case SCREENBREAK13:
            case SCREENBREAK14:
            case SCREENBREAK15:
            case SCREENBREAK16:
            case SCREENBREAK17:
            case SCREENBREAK18:
            case SCREENBREAK19:
            case BORNTOBEWILDSCREEN:

                lotsofglass(spr, dawallnum, 30);
                wal.setPicnum((short) (W_SCREENBREAK + (engine.krand() % 3)));
                spritesound(GLASS_HEAVYBREAK, spr);
                return;

            case W_TECHWALL5:
            case W_TECHWALL6:
            case W_TECHWALL7:
            case W_TECHWALL8:
            case W_TECHWALL9:
                breakwall(wal.getPicnum() + 1, spr, dawallnum);
                return;
            case W_MILKSHELF:
                breakwall(W_MILKSHELFBROKE, spr, dawallnum);
                return;

            case W_TECHWALL10:
                breakwall(W_HITTECHWALL10, spr, dawallnum);
                return;

            case W_TECHWALL1:
            case W_TECHWALL11:
            case W_TECHWALL12:
            case W_TECHWALL13:
            case W_TECHWALL14:
                breakwall(W_HITTECHWALL1, spr, dawallnum);
                return;

            case W_TECHWALL15:
                breakwall(W_HITTECHWALL15, spr, dawallnum);
                return;

            case W_TECHWALL16:
                breakwall(W_HITTECHWALL16, spr, dawallnum);
                return;

            case W_TECHWALL2:
                breakwall(W_HITTECHWALL2, spr, dawallnum);
                return;

            case W_TECHWALL3:
                breakwall(W_HITTECHWALL3, spr, dawallnum);
                return;

            case W_TECHWALL4:
                breakwall(W_HITTECHWALL4, spr, dawallnum);
                return;

            case ATM:
                wal.setPicnum(ATMBROKE);
                lotsofmoney(boardService.getSprite(spr), 1 + (engine.krand() & 7));
                spritesound(GLASS_HEAVYBREAK, spr);
                break;

            case WALLLIGHT1:
            case WALLLIGHT2:
            case WALLLIGHT3:
            case WALLLIGHT4:
            case TECHLIGHT2:
            case TECHLIGHT4:

                if (rnd(128)) {
                    spritesound(GLASS_HEAVYBREAK, spr);
                } else {
                    spritesound(GLASS_BREAKING, spr);
                }
                lotsofglass(spr, dawallnum, 30);

                if (wal.getPicnum() == WALLLIGHT1) {
                    wal.setPicnum(WALLLIGHTBUST1);
                }

                if (wal.getPicnum() == WALLLIGHT2) {
                    wal.setPicnum(WALLLIGHTBUST2);
                }

                if (wal.getPicnum() == WALLLIGHT3) {
                    wal.setPicnum(WALLLIGHTBUST3);
                }

                if (wal.getPicnum() == WALLLIGHT4) {
                    wal.setPicnum(WALLLIGHTBUST4);
                }

                if (wal.getPicnum() == TECHLIGHT2) {
                    wal.setPicnum(TECHLIGHTBUST2);
                }

                if (wal.getPicnum() == TECHLIGHT4) {
                    wal.setPicnum(TECHLIGHTBUST4);
                }

                if (wal.getLotag() == 0) {
                    return;
                }

                int sn = wal.getNextsector();
                Sector sec = boardService.getSector(sn);
                if (sec == null) {
                    return;
                }
                int darkestwall = 0;

                for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
                    Wall wal2 = wn.get();
                    if (wal2 != null && wal2.getShade() > darkestwall) {
                        darkestwall = wal2.getShade();
                    }
                }

                int j = engine.krand() & 1;
                ListNode<Sprite> ni = boardService.getStatNode(3);
                while (ni != null) {
                    Sprite sp = ni.get();
                    if (sp.getHitag() == wal.getLotag() && sp.getLotag() == 3) {
                        int i = ni.getIndex();
                        hittype[i].temp_data[2] = j;
                        hittype[i].temp_data[3] = darkestwall;
                        hittype[i].temp_data[4] = 1;
                    }
                    ni = ni.getNext();
                }
                break;
        }
    }

    public static void checkhitceiling(final int sn) {
        final Sector sec = boardService.getSector(sn);
        if (sec == null) {
            return;
        }

        switch (sec.getCeilingpicnum()) {
            case WALLLIGHT1:
            case WALLLIGHT2:
            case WALLLIGHT3:
            case WALLLIGHT4:
            case TECHLIGHT2:
            case TECHLIGHT4:
                ceilingglass(ps[myconnectindex].i, sn, 10);
                spritesound(GLASS_BREAKING, ps[screenpeek].i);

                if (sec.getCeilingpicnum() == WALLLIGHT1) {
                    sec.setCeilingpicnum(WALLLIGHTBUST1);
                }

                if (sec.getCeilingpicnum() == WALLLIGHT2) {
                    sec.setCeilingpicnum(WALLLIGHTBUST2);
                }

                if (sec.getCeilingpicnum() == WALLLIGHT3) {
                    sec.setCeilingpicnum(WALLLIGHTBUST3);
                }

                if (sec.getCeilingpicnum() == WALLLIGHT4) {
                    sec.setCeilingpicnum(WALLLIGHTBUST4);
                }

                if (sec.getCeilingpicnum() == TECHLIGHT2) {
                    sec.setCeilingpicnum(TECHLIGHTBUST2);
                }

                if (sec.getCeilingpicnum() == TECHLIGHT4) {
                    sec.setCeilingpicnum(TECHLIGHTBUST4);
                }

                if (sec.getHitag() == 0) {
                    for (ListNode<Sprite> node = boardService.getSectNode(sn); node != null; node = node.getNext()) {
                        Sprite sp = node.get();
                        if (sp.getPicnum() == SECTOREFFECTOR && sp.getLotag() == 12) {
                            for (ListNode<Sprite> n = boardService.getStatNode(3); n != null; n = n.getNext()) {
                                if (n.get().getHitag() == sp.getHitag()) {
                                    hittype[n.getIndex()].temp_data[3] = 1;
                                }
                            }
                            break;
                        }
                    }
                }

                ListNode<Sprite> ni = boardService.getStatNode(3);
                int j = engine.krand() & 1;
                while (ni != null) {
                    Sprite sp = ni.get();
                    if (sp.getHitag() == (sec.getHitag()) && sp.getLotag() == 3) {
                        int i = ni.getIndex();
                        hittype[i].temp_data[2] = j;
                        hittype[i].temp_data[4] = 1;
                    }
                    ni = ni.getNext();
                }

        }

    }

    public static void checkhitsprite(int i, final int sn) {
        Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        switch (s.getPicnum()) {
            case OCEANSPRITE1:
            case OCEANSPRITE2:
            case OCEANSPRITE3:
            case OCEANSPRITE4:
            case OCEANSPRITE5:
                spawn(i, SMALLSMOKE);
                engine.deletesprite(i);
                break;
            case QUEBALL:
            case STRIPEBALL: {
                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                if (sp.getPicnum() == QUEBALL || sp.getPicnum() == STRIPEBALL) {
                    sp.setXvel((short) ((s.getXvel() >> 1) + (s.getXvel() >> 2)));
                    sp.setAng(sp.getAng() - ((s.getAng() << 1) + 1024));
                    s.setAng((short) (EngineUtils.getAngle(s.getX() - sp.getX(), s.getY() - sp.getY()) - 512));
                    if (Sound[POOLBALLHIT].getSoundOwnerCount() < 2) {
                        spritesound(POOLBALLHIT, i);
                    }
                } else {
                    if ((engine.krand() & 3) != 0) {
                        s.setXvel(164);
                        s.setAng(sp.getAng());
                    } else {
                        lotsofglass(i, -1, 3);
                        engine.deletesprite(i);
                    }
                }
                break;
            }
            case TREE1:
            case TREE2:
            case TIRE:
            case CONE:
            case BOX: {
                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                switch (sp.getPicnum()) {
                    case RADIUSEXPLOSION:
                    case RPG:
                    case FIRELASER:
                    case HYDRENT:
                    case HEAVYHBOMB:
                        if (hittype[i].temp_data[0] == 0) {
                            s.setCstat(s.getCstat() & ~257);
                            hittype[i].temp_data[0] = 1;
                            spawn(i, BURNING);
                        }
                        break;
                }
                break;
            }
            case CACTUS: {
                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                switch (sp.getPicnum()) {
                    case RADIUSEXPLOSION:
                    case RPG:
                    case FIRELASER:
                    case HYDRENT:
                    case HEAVYHBOMB:
                        for (int k = 0; k < 64; k++) {
                            int vz = -(engine.krand() & 4095) - (s.getZvel() >> 2);
                            int ve = (engine.krand() & 63) + 64;
                            int va = engine.krand() & 2047;
                            int pn = SCRAP3 + (engine.krand() & 3);
                            int j = EGS(s.getSectnum(), s.getX(), s.getY(), s.getZ() - (engine.krand() % (48 << 8)), pn,
                                    -8, 48, 48, va, ve, vz, i, 5);
                            Sprite sj = boardService.getSprite(j);
                            if (sj != null) {
                                sj.setPal(8);
                            }
                        }

                        if (s.getPicnum() == CACTUS) {
                            s.setPicnum(CACTUSBROKE);
                        }
                        s.setCstat(s.getCstat() & ~257);
                        break;
                }
                break;
            }
            case HANGLIGHT:
            case GENERICPOLE2:
                for (int k = 0; k < 6; k++) {
                    int vz = -(engine.krand() & 4095) - (s.getZvel() >> 2);
                    int ve = (engine.krand() & 63) + 64;
                    int va = engine.krand() & 2047;
                    EGS(s.getSectnum(), s.getX(), s.getY(), s.getZ() - (8 << 8), SCRAP1 + (engine.krand() & 15),
                            -8, 48, 48, va, ve, vz, i, (short) 5);
                }
                spritesound(GLASS_HEAVYBREAK, i);
                engine.deletesprite(i);
                break;

            case FANSPRITE: {
                s.setPicnum(FANSPRITEBROKE);
                s.setCstat(s.getCstat() & (65535 - 257));
                Sector sec = boardService.getSector(s.getSectnum());
                if (sec != null && sec.getFloorpicnum() == FANSHADOW) {
                    sec.setFloorpicnum(FANSHADOWBROKE);
                }

                spritesound(GLASS_HEAVYBREAK, i);
                for (int j = 0; j < 16; j++) {
                    RANDOMSCRAP(s, i);
                }

                break;
            }
            case WATERFOUNTAIN:
            case WATERFOUNTAIN + 1:
            case WATERFOUNTAIN + 2:
            case WATERFOUNTAIN + 3:
                s.setPicnum(WATERFOUNTAINBROKE);
                spawn(i, TOILETWATER);
                break;
            case SATELITE:
            case FUELPOD:
            case SOLARPANNEL:
            case ANTENNA: {
                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                if (sp.getExtra() != currentGame.getCON().script[currentGame.getCON().actorscrptr[SHOTSPARK1]]) {
                    for (int j = 0; j < 15; j++) {
                        int vz = -(engine.krand() & 511) - 256;
                        int ve = (engine.krand() & 127) + 64;
                        int va = engine.krand() & 2047;

                        Sector sec = boardService.getSector(s.getSectnum());
                        if (sec != null) {
                            EGS(s.getSectnum(), s.getX(), s.getY(),
                                    sec.getFloorz() - (12 << 8) - (j << 9), SCRAP1 + (engine.krand() & 15), -8,
                                    64, 64, va, ve, vz, i, 5);
                        }
                    }
                    spawn(i, EXPLOSION2);
                    engine.deletesprite(i);
                }
                break;
            }
            case BOTTLE1:
            case BOTTLE2:
            case BOTTLE3:
            case BOTTLE4:
            case BOTTLE5:
            case BOTTLE6:
            case BOTTLE8:
            case BOTTLE10:
            case BOTTLE11:
            case BOTTLE12:
            case BOTTLE13:
            case BOTTLE14:
            case BOTTLE15:
            case BOTTLE16:
            case BOTTLE17:
            case BOTTLE18:
            case BOTTLE19:
            case WATERFOUNTAINBROKE:
            case DOMELITE:
            case SUSHIPLATE1:
            case SUSHIPLATE2:
            case SUSHIPLATE3:
            case SUSHIPLATE4:
            case SUSHIPLATE5:
            case WAITTOBESEATED:
            case VASE:
            case STATUEFLASH:
            case STATUE:
                if (s.getPicnum() == BOTTLE10) {
                    lotsofmoney(s, 4 + (engine.krand() & 3));
                } else if (s.getPicnum() == STATUE || s.getPicnum() == STATUEFLASH) {
                    lotsofcolourglass(i, -1, 40);
                    spritesound(GLASS_HEAVYBREAK, i);
                } else if (s.getPicnum() == VASE) {
                    lotsofglass(i, -1, 40);
                }

                spritesound(GLASS_BREAKING, i);
                s.setAng((short) (engine.krand() & 2047));
                lotsofglass(i, -1, 8);
                engine.deletesprite(i);
                break;
            case FETUS:
                s.setPicnum(FETUSBROKE);
                spritesound(GLASS_BREAKING, i);
                lotsofglass(i, -1, 10);
                break;
            case FETUSBROKE:
                for (int j = 0; j < 48; j++) {
                    shoot(i, BLOODSPLAT1);
                    s.setAng(s.getAng() + 333);
                }
                spritesound(GLASS_HEAVYBREAK, i);
                spritesound(SQUISHED, i);
            case BOTTLE7:
                spritesound(GLASS_BREAKING, i);
                lotsofglass(i, -1, 10);
                engine.deletesprite(i);
                break;
            case HYDROPLANT:
                s.setPicnum(BROKEHYDROPLANT);
                spritesound(GLASS_BREAKING, i);
                lotsofglass(i, -1, 10);
                break;

            case FORCESPHERE:
                s.setXrepeat(0);
                hittype[s.getOwner()].temp_data[0] = 32;
                hittype[s.getOwner()].temp_data[1] ^= 1;
                hittype[s.getOwner()].temp_data[2]++;
                spawn(i, EXPLOSION2);
                break;

            case BROKEHYDROPLANT:
                if ((s.getCstat() & 1) != 0) {
                    spritesound(GLASS_BREAKING, i);
                    s.setZ(s.getZ() + (16 << 8));
                    s.setCstat(0);
                    lotsofglass(i, -1, 5);
                }
                break;

            case TOILET:
                s.setPicnum(TOILETBROKE);
                s.setCstat(s.getCstat() | (engine.krand() & 1) << 2);
                s.setCstat(s.getCstat() & ~257);
                spawn(i, TOILETWATER);
                spritesound(GLASS_BREAKING, i);
                break;

            case STALL:
                s.setPicnum(STALLBROKE);
                s.setCstat(s.getCstat() | (engine.krand() & 1) << 2);
                s.setCstat(s.getCstat() & ~257);
                spawn(i, TOILETWATER);
                spritesound(GLASS_HEAVYBREAK, i);
                break;

            case HYDRENT:
                s.setPicnum(BROKEFIREHYDRENT);
                spawn(i, TOILETWATER);

                spritesound(GLASS_HEAVYBREAK, i);
                break;

            case GRATE1:
                s.setPicnum(BGRATE1);
                s.setCstat(s.getCstat() & (65535 - 256 - 1));
                spritesound(VENT_BUST, i);
                break;

            case CIRCLEPANNEL:
                s.setPicnum(CIRCLEPANNELBROKE);
                s.setCstat(s.getCstat() & (65535 - 256 - 1));
                spritesound(VENT_BUST, i);
                break;
            case PANNEL1:
            case PANNEL2:
                s.setPicnum(BPANNEL1);
                s.setCstat(s.getCstat() & (65535 - 256 - 1));
                spritesound(VENT_BUST, i);
                break;
            case PANNEL3:
                s.setPicnum(BPANNEL3);
                s.setCstat(s.getCstat() & (65535 - 256 - 1));
                spritesound(VENT_BUST, i);
                break;
            case PIPE1:
            case PIPE2:
            case PIPE3:
            case PIPE4:
            case PIPE5:
            case PIPE6: {
                switch (s.getPicnum()) {
                    case PIPE1:
                        s.setPicnum(PIPE1B);
                        break;
                    case PIPE2:
                        s.setPicnum(PIPE2B);
                        break;
                    case PIPE3:
                        s.setPicnum(PIPE3B);
                        break;
                    case PIPE4:
                        s.setPicnum(PIPE4B);
                        break;
                    case PIPE5:
                        s.setPicnum(PIPE5B);
                        break;
                    case PIPE6:
                        s.setPicnum(PIPE6B);
                        break;
                }

                Sprite ssp = boardService.getSprite(spawn(i, STEAM));
                Sector sec = boardService.getSector(s.getSectnum());
                if (ssp != null && sec != null) {
                    ssp.setZ(sec.getFloorz() - (32 << 8));
                }
                break;
            }
            case MONK:
            case LUKE:
            case INDY:
            case JURYGUY:
                spritesound(s.getLotag(), i);
                spawn(i, s.getHitag());
            case SPACEMARINE: {
                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                s.setExtra(s.getExtra() - sp.getExtra());
                if (s.getExtra() > 0) {
                    break;
                }
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT1);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT2);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT3);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT4);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT1);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT2);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT3);
                s.setAng((short) (engine.krand() & 2047));
                shoot(i, BLOODSPLAT4);
                guts(s, JIBS1, 1, myconnectindex);
                guts(s, JIBS2, 2, myconnectindex);
                guts(s, JIBS3, 3, myconnectindex);
                guts(s, JIBS4, 4, myconnectindex);
                guts(s, JIBS5, 1, myconnectindex);
                guts(s, JIBS3, 6, myconnectindex);
                sound(SQUISHED);
                engine.deletesprite(i);
                break;
            }
            case CHAIR1:
            case CHAIR2:
                s.setPicnum(BROKENCHAIR);
                s.setCstat(0);
                break;
            case CHAIR3:
            case MOVIECAMERA:
            case SCALE:
            case VACUUM:
            case CAMERALIGHT:
            case IVUNIT:
            case POT1:
            case POT2:
            case POT3:
            case TRIPODCAMERA:
                spritesound(GLASS_HEAVYBREAK, i);
                for (int j = 0; j < 16; j++) {
                    RANDOMSCRAP(s, i);
                }
                engine.deletesprite(i);
                break;
            case PLAYERONWATER:
                i = s.getOwner(); // Attension
                s = boardService.getSprite(i);
                if (s == null) {
                    return;
                }
            default: {
                if ((s.getCstat() & 16) != 0 && s.getHitag() == 0 && s.getLotag() == 0 && s.getStatnum() == 0) {
                    break;
                }

                Sprite sp = boardService.getSprite(sn);
                if (sp == null) {
                    break;
                }

                if ((sp.getPicnum() == FREEZEBLAST || sp.getOwner() != i) && s.getStatnum() != 4) {
                    if (badguy(s)) {
                        if (s.getPicnum() == FIREFLY && s.getXrepeat() < 48) {
                            break;
                        }

                        if (sp.getPicnum() == RPG) {
                            sp.setExtra(sp.getExtra() << 1);
                        }

                        if ((s.getPicnum() != DRONE) && (s.getPicnum() != ROTATEGUN)
                                && (s.getPicnum() != COMMANDER)
                                && (s.getPicnum() < GREENSLIME || s.getPicnum() > GREENSLIME + 7)) {
                            if (sp.getPicnum() != FREEZEBLAST) {
                                if (currentGame.getCON().actortype[s.getPicnum()] == 0) {
                                    int j = spawn(sn, JIBS6);
                                    Sprite sj = boardService.getSprite(j);
                                    if (sj == null) {
                                        return;
                                    }

                                    if (sp.getPal() == 6) {
                                        sj.setPal(6);
                                    }

                                    sj.setZ(sj.getZ() + (4 << 8));
                                    sj.setXvel(16);
                                    sj.setXrepeat(24);
                                    sj.setYrepeat(24);
                                    sj.setAng(sj.getAng() + 32 - (engine.krand() & 63));
                                }
                            }
                        }

                        int j = sp.getOwner();
                        Sprite sj = boardService.getSprite(j);
                        if (sj != null && sj.getPicnum() == APLAYER && s.getPicnum() != ROTATEGUN
                                && s.getPicnum() != DRONE) {
                            if (ps[sj.getYvel()].curr_weapon == SHOTGUN_WEAPON) {
                                shoot(i, BLOODSPLAT3);
                                shoot(i, BLOODSPLAT1);
                                shoot(i, BLOODSPLAT2);
                                shoot(i, BLOODSPLAT4);
                            }
                        }

                        if (s.getPicnum() != TANK && !bossguy(s.getPicnum()) && s.getPicnum() != RECON
                                && s.getPicnum() != ROTATEGUN) {
                            if ((s.getCstat() & 48) == 0) {
                                s.setAng((short) ((sp.getAng() + 1024) & 2047));
                            }
                            s.setXvel((short) -(sp.getExtra() << 2));
                            int sectnum = s.getSectnum();
                            if (boardService.isValidSprite(sectnum)) {
                                engine.pushmove(s.getX(), s.getY(), s.getZ(), sectnum, 128, (4 << 8), (4 << 8),
                                        CLIPMASK0);
                                s.setX(pushmove_x);
                                s.setY(pushmove_y);
                                s.setZ(pushmove_z);
                                sectnum = pushmove_sectnum;

                                if (sectnum != s.getSectnum()) {
                                    engine.changespritesect(i, sectnum);
                                }
                            }
                        }

                        if (s.getStatnum() == 2) {
                            engine.changespritestat(i, (short) 1);
                            hittype[i].timetosleep = SLEEPTIME;
                        }
                        if ((s.getXrepeat() < 24 || s.getPicnum() == SHARK) && sp.getPicnum() == SHRINKSPARK) {
                            return;
                        }
                    }

                    if (s.getStatnum() != 2) {
                        if (sp.getPicnum() == FREEZEBLAST && ((s.getPicnum() == APLAYER && s.getPal() == 1)
                                || (currentGame.getCON().freezerhurtowner == 0 && sp.getOwner() == i))) {
                            return;
                        }

                        int hitpic = sp.getPicnum();
                        Sprite so = boardService.getSprite(sp.getOwner());
                        if (so != null && so.getPicnum() == APLAYER) {
                            if (s.getPicnum() == APLAYER && ud.coop != 0 && ud.ffire == 0) {
                                return;
                            }

                            Sprite so2 = boardService.getSprite(s.getOwner());
                            if (so2 != null && hitpic == FIREBALL && so2.getPicnum() != FIREBALL) {
                                hitpic = FLAMETHROWERFLAME;
                            }
                        }

                        hittype[i].picnum = hitpic;
                        hittype[i].extra += sp.getExtra();
                        hittype[i].ang = sp.getAng();
                        hittype[i].owner = sp.getOwner();
                    }

                    if (s.getStatnum() == 10) {
                        int p = s.getYvel();
                        if (ps[p].newowner >= 0) {
                            ps[p].newowner = -1;
                            ps[p].posx = ps[p].oposx;
                            ps[p].posy = ps[p].oposy;
                            ps[p].posz = ps[p].oposz;
                            ps[p].ang = ps[p].oang;

                            System.err.println("a?");
                            ps[p].cursectnum = engine.updatesector(ps[p].posx, ps[p].posy, ps[p].cursectnum);
                            setpal(ps[p]);

                            for (ListNode<Sprite> node = boardService.getStatNode(1); node != null; node = node.getNext()) {
                                Sprite sp1 = node.get();
                                if (sp1.getPicnum() == CAMERA1) {
                                    sp1.setYvel(0);
                                }
                            }
                        }

                        if (s.getXrepeat() < 24 && sp.getPicnum() == SHRINKSPARK) {
                            return;
                        }

                        Sprite hso = boardService.getSprite(hittype[i].owner);
                        if (hso != null && hso.getPicnum() != APLAYER) {
                            if (ud.player_skill >= 3) {
                                sp.setExtra(sp.getExtra() + (sp.getExtra() >> 1));
                            }
                        }
                    }

                }
                break;
            }
        }
    }

    public static void checksectors(int snum) {
        PlayerStruct p = ps[snum];
        final Sector psec = boardService.getSector(p.cursectnum);
        if (psec != null) {
            switch (psec.getLotag()) {
                case 32767:
                    psec.setLotag(0);
                    FTA(9, p);
                    ps[connecthead].secret_rooms++;
                    return;
                case -1:
                    LeaveMap();
                    psec.setLotag(0);
                    if (ud.from_bonus != 0) {
                        ud.level_number = ud.from_bonus;
                        ud.from_bonus = 0;
                    } else {
                        ud.level_number++;
                    }
                    return;
                case -2:
                    psec.setLotag(0);
                    p.timebeforeexit = 26 * 8;
                    p.customexitsound = psec.getHitag();
                    return;
                default:
                    if (psec.getLotag() >= 10000 && psec.getLotag() < 16383) {
                        if (snum == screenpeek || ud.coop == 1) {
                            spritesound(psec.getLotag() - 10000, p.i);
                        }
                        psec.setLotag(0);
                    }
                    break;

            }
        }

        // After this point the the player effects the map with space
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null || psp.getExtra() <= 0) {
            return;
        }

        if (ud.cashman != 0 && (sync[snum].bits & (1 << 29)) != 0) {
            lotsofmoney(psp, 2);
        }

        if (p.newowner >= 0) {
            if (klabs(sync[snum].svel) > 768 || klabs(sync[snum].fvel) > 768) {
                CLEARCAMERAS(p, -1);
                return;
            }
        }

        if ((sync[snum].bits & (1 << 29)) == 0 && (sync[snum].bits & (1 << 31)) == 0) {
            p.toggle_key_flag = 0;
        } else if (p.toggle_key_flag == 0) {

            if ((sync[snum].bits & (1 << 31)) != 0) {
                if (p.newowner >= 0) {
                    CLEARCAMERAS(p, -1);
                }
                return;
            }

            neartagsprite = -1;
            p.toggle_key_flag = 1;
            int hit = hitawall(p);
            final int hitscanwall = pHitInfo.hitwall;
            Wall hwal = boardService.getWall(hitscanwall);

            if (hit < 1280 && hwal != null && hwal.getOverpicnum() == MIRROR) {
                if (hwal.getLotag() > 0 && Sound[hwal.getLotag()].getSoundOwnerCount() == 0 && snum == screenpeek) {
                    spritesound(hwal.getLotag(), p.i);
                    return;
                }
            }

            if (hwal != null && (hwal.getCstat() & 16) != 0) {
                if (hwal.getLotag() != 0) {
                    return;
                }
            }

            if (p.newowner >= 0) {
                neartag(p.oposx, p.oposy, p.oposz, psp.getSectnum(), (short) p.oang, 1280, 1);
            } else {
                neartag(p.posx, p.posy, p.posz, psp.getSectnum(), (short) p.oang, 1280, 1);
                if (neartagsprite == -1 && neartagwall == -1 && neartagsector == -1) {
                    neartag(p.posx, p.posy, p.posz + (8 << 8), psp.getSectnum(), (short) p.oang, 1280, 1);
                }
                if (neartagsprite == -1 && neartagwall == -1 && neartagsector == -1) {
                    neartag(p.posx, p.posy, p.posz + (16 << 8), psp.getSectnum(), (short) p.oang, 1280, 1);
                }
                if (neartagsprite == -1 && neartagwall == -1 && neartagsector == -1) {
                    neartag(p.posx, p.posy, p.posz + (16 << 8), psp.getSectnum(), (short) p.oang, 1280, 3);
                    Sprite nsp = boardService.getSprite(neartagsprite);
                    if (nsp != null) {
                        switch (nsp.getPicnum()) {
                            case FEM1:
                            case FEM2:
                            case FEM3:
                            case FEM4:
                            case FEM5:
                            case FEM6:
                            case FEM7:
                            case FEM8:
                            case FEM9:
                            case FEM10:
                            case PODFEM1:
                            case NAKED1:
                            case STATUE:
                            case TOUGHGAL:
                                return;
                        }
                    }

                    neartagsprite = -1;
                    neartagwall = -1;
                    neartagsector = -1;
                }
            }

            if (p.newowner == -1 && neartagsprite == -1 && neartagsector == -1 && neartagwall == -1) {
                Sector pssec = boardService.getSector(psp.getSectnum());
                if (pssec != null && isanunderoperator(pssec.getLotag())) {
                    neartagsector = psp.getSectnum();
                }
            }

            Sector nsec = boardService.getSector(neartagsector);
            if (nsec != null && (nsec.getLotag() & 16384) != 0) {
                return;
            }

            if (neartagsprite == -1 && neartagwall == -1) {
                if (psec != null && psec.getLotag() == 2) {
                    int oldz = hitasprite(p.i);
                    neartagsprite = pHitInfo.hitsprite;
                    if (oldz > 1280) {
                        neartagsprite = -1;
                    }
                }
            }

            Sprite hsp = boardService.getSprite(neartagsprite);
            if (hsp != null) {
                if (checkhitswitch(snum, neartagsprite, 1)) {
                    return;
                }

                switch (hsp.getPicnum()) {
                    case TOILET:
                    case STALL:
                        if (p.last_pissed_time == 0) {
                            if (ud.lockout == 0) {
                                spritesound(DUKE_URINATE, p.i);
                            }

                            p.last_pissed_time = 26 * 220;
                            p.transporter_hold = 29 * 2;
                            if (p.holster_weapon == 0) {
                                p.holster_weapon = 1;
                                p.weapon_pos = -1;
                            }
                            if (psp.getExtra() <= (currentGame.getCON().max_player_health
                                    - (currentGame.getCON().max_player_health / 10))) {
                                psp.setExtra(psp.getExtra() + currentGame.getCON().max_player_health / 10);
                                p.last_extra = psp.getExtra();
                            } else if (psp.getExtra() < currentGame.getCON().max_player_health) {
                                psp.setExtra((short) currentGame.getCON().max_player_health);
                            }
                        } else if (Sound[FLUSH_TOILET].getSoundOwnerCount() == 0) {
                            spritesound(FLUSH_TOILET, p.i);
                        }
                        return;

                    case NUKEBUTTON: {
                        hitawall(p);
                        int j = pHitInfo.hitwall;
                        Wall hw = boardService.getWall(j);
                        if (hw != null && hw.getOverpicnum() == 0) {
                            if (hittype[neartagsprite].temp_data[0] == 0) {
                                hittype[neartagsprite].temp_data[0] = 1;
                                hsp.setOwner(p.i);
                                p.buttonpalette = hsp.getPal();
                                if (p.buttonpalette != 0) {
                                    ud.secretlevel = hsp.getLotag();
                                } else {
                                    ud.secretlevel = 0;
                                }
                            }
                        }
                        return;
                    }
                    case WATERFOUNTAIN:
                        if (hittype[neartagsprite].temp_data[0] != 1) {
                            hittype[neartagsprite].temp_data[0] = 1;
                            hsp.setOwner(p.i);

                            if (psp.getExtra() < currentGame.getCON().max_player_health) {
                                psp.setExtra(psp.getExtra() + 1);
                                spritesound(DUKE_DRINKING, p.i);
                            }
                        }
                        return;
                    case PLUG:
                        spritesound(SHORT_CIRCUIT, p.i);
                        psp.setExtra(psp.getExtra() - (2 + (engine.krand() & 3)));
                        p.pals[0] = 48;
                        p.pals[1] = 48;
                        p.pals[2] = 64;
                        p.pals_time = 32;
                        break;
                    case VIEWSCREEN:
                    case VIEWSCREEN2: {
                        for (ListNode<Sprite> node = boardService.getStatNode(1); node != null; node = node.getNext()) {
                            Sprite sp = node.get();
                            if (sp.getPicnum() == CAMERA1 && sp.getYvel() == 0
                                    && hsp.getHitag() == sp.getLotag()) {
                                sp.setYvel(1); // Using this camera
                                spritesound(MONITOR_ACTIVE, neartagsprite);

                                hsp.setOwner(node.getIndex());
                                hsp.setYvel(1);
                                hittype[p.i].tempang = sp.getAng();
                                p.posx = sp.getX();
                                p.posy = sp.getY();
                                p.posz = sp.getZ();
                                p.ang = sp.getAng();
                                int j = p.cursectnum;
                                p.cursectnum = sp.getSectnum();
                                setpal(p);
                                p.cursectnum = (short) j;
                                p.newowner = (short) node.getIndex();
                                return;
                            }
                        }
                    }

                    CLEARCAMERAS(p, -1);
                    return;
                }
            }

            if ((sync[snum].bits & (1 << 29)) == 0) {
                return;
            } else if (p.newowner >= 0) {
                CLEARCAMERAS(p, -1);
                return;
            }

            if (neartagwall == -1 && neartagsector == -1 && neartagsprite == -1) {
                if (klabs(hits(p.i)) < 512) {
                    if ((engine.krand() & 255) < 16) {
                        spritesound(DUKE_SEARCH2, p.i);
                    } else {
                        spritesound(DUKE_SEARCH, p.i);
                    }
                    return;
                }
            }

            Wall nw = boardService.getWall(neartagwall);
            if (nw != null) {
                if (nw.getLotag() > 0 && isadoorwall(nw.getPicnum())) {
                    if (hitscanwall == neartagwall || hitscanwall == -1) {
                        checkhitswitch(snum, neartagwall, 0);
                    }
                    return;
                } else if (p.newowner >= 0) {
                    CLEARCAMERAS(p, -1);
                    return;
                }
            }

            nsec = boardService.getSector(neartagsector);
            Sector pssec = boardService.getSector(psp.getSectnum());
            if (nsec != null && (nsec.getLotag() & 16384) == 0
                    && isanearoperator(nsec.getLotag())) {

                for (ListNode<Sprite> node = boardService.getSectNode(neartagsector); node != null; node = node.getNext()) {
                    Sprite sp = node.get();
                    if (sp.getPicnum() == ACTIVATOR || sp.getPicnum() == MASTERSWITCH) {
                        return;
                    }
                }
                operatesectors(neartagsector, p.i);
            } else if (pssec != null && (pssec.getLotag() & 16384) == 0) {
                if (isanunderoperator(pssec.getLotag())) {
                    for (ListNode<Sprite> node = boardService.getSectNode(psp.getSectnum()); node != null; node = node.getNext()) {
                        Sprite sp = node.get();
                        if (sp.getPicnum() == ACTIVATOR || sp.getPicnum() == MASTERSWITCH) {
                            return;
                        }
                    }
                    operatesectors(psp.getSectnum(), p.i);
                } else {
                    checkhitswitch(snum, neartagwall, 0);
                }
            }
        }
    }

    public static void CLEARCAMERAS(PlayerStruct p, int i) {
        if (i < 0) {
            p.posx = p.oposx;
            p.posy = p.oposy;
            p.posz = p.oposz;
            p.ang = p.oang;
            p.newowner = -1;

            p.cursectnum = engine.updatesector(p.posx, p.posy, p.cursectnum);
            setpal(p);

            p.UpdatePlayerLoc();

            for (ListNode<Sprite> node = boardService.getStatNode(1); node != null; node = node.getNext()) {
                Sprite sp = node.get();
                if (sp.getPicnum() == CAMERA1) {
                    sp.setYvel(0);
                }
            }
        } else if (p.newowner >= 0) {
            p.newowner = -1;
        }
    }

    public static void setsectinterpolate(int i) {
        Sprite sp = boardService.getSprite(i);
        if (sp == null) {
            return;
        }

        Sector sec = boardService.getSector(sp.getSectnum());
        if (sec == null) {
            return;
        }

        for (ListNode<Wall> wn = sec.getWallNode(); wn != null; wn = wn.getNext()) {
            Wall w = wn.get();

            game.pInt.setwallinterpolate(wn.getIndex(), w);
            int k = w.getNextwall();
            w = boardService.getWall(k);
            if (w != null) {
                game.pInt.setwallinterpolate(k, w);
                game.pInt.setwallinterpolate(w.getPoint2(), w.getWall2());
            }
        }
    }
}
