// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Types;

import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class NetInfo {

    public int nGameType;
    public int nEpisode;
    public int nLevel;
    public int nDifficulty;
    public int nMonsters;

    public int nRespawnMonsters;
    public int nRespawnItem;
    public int nRespawnInventory;

    public int nMarkers;
    public int nFriendlyFire;

    public void readObject(InputStream is) throws IOException {
        nGameType = StreamUtils.readByte(is);
        nEpisode = StreamUtils.readByte(is);
        nLevel = StreamUtils.readByte(is);
        nDifficulty = StreamUtils.readByte(is);
        nMonsters = StreamUtils.readByte(is);
        nRespawnMonsters = StreamUtils.readByte(is);
        nRespawnItem = StreamUtils.readByte(is);
        nRespawnInventory = StreamUtils.readByte(is);
        nMarkers = StreamUtils.readByte(is);
        nFriendlyFire = StreamUtils.readByte(is);
    }

    public void writeObject(ByteArrayOutputStream os) throws IOException {
         StreamUtils.writeByte(os, nGameType);
         StreamUtils.writeByte(os, nEpisode);
         StreamUtils.writeByte(os, nLevel);
         StreamUtils.writeByte(os, nDifficulty);
         StreamUtils.writeByte(os, nMonsters);
         StreamUtils.writeByte(os, nRespawnMonsters);
         StreamUtils.writeByte(os, nRespawnItem);
         StreamUtils.writeByte(os, nRespawnInventory);
         StreamUtils.writeByte(os, nMarkers);
         StreamUtils.writeByte(os, nFriendlyFire);
    }
}
