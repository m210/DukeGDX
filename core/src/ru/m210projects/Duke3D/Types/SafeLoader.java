// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Types;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.IntArray;
import com.badlogic.gdx.utils.ShortArray;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.BitMap;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Build.filehandle.StreamUtils;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.filehandle.fs.Directory.DUMMY_ENTRY;
import static ru.m210projects.Duke3D.Animate.MAXANIMATES;
import static ru.m210projects.Duke3D.Gamedef.MAXSCRIPTSIZE;
import static ru.m210projects.Duke3D.Globals.MAXANIMWALLS;
import static ru.m210projects.Duke3D.Globals.MAXCYCLERS;
import static ru.m210projects.Duke3D.LoadSave.*;
import static ru.m210projects.Duke3D.Main.game;
import static ru.m210projects.Duke3D.ResourceHandler.episodeManager;

public class SafeLoader {

    public Entry boardfilename;
    public GameInfo addon;
    public String addonFileName;
    public String addonPackedConName;
    public byte warp_on;
    public int gAnimationCount = 0;
    public final ANIMATION[] gAnimationData = new ANIMATION[MAXANIMATES];
    public final short[] pskyoff = new short[MAXPSKYTILES];
    public short pskybits;
    public int parallaxyscale;
    public short connecthead;
    public final short[] connectpoint2 = new short[MAXPLAYERS];
    public int randomseed;
    public final BitMap show2dsector = new BitMap(MAXSECTORSV7);

    // MapInfo
    public Sector[] sector;
    public Wall[] wall;
    public List<Sprite> sprite;
    public final PlayerStruct[] ps = new PlayerStruct[MAXPLAYERS];

    // GameInfo
    public final PlayerOrig[] po = new PlayerOrig[MAXPLAYERS];
    public final Array<Weaponhit> hittype = new Array<>(true, MAXSPRITES, Weaponhit.class);
    public int multimode, volume_number, level_number, player_skill;
    public int from_bonus, secretlevel;
    public boolean respawn_monsters, respawn_items, respawn_inventory, god, monsters_off;
    public int auto_run, crosshair, last_level, eog, coop, marker, ffire;
    public short numplayersprites, global_random, earthquaketime, camsprite;
    public final short[][] frags = new short[MAXPLAYERS][MAXPLAYERS];
    public final int[] msx = new int[2048];
    public final int[] msy = new int[2048];
    public final Animwalltype[] animwall = new Animwalltype[MAXANIMWALLS];
    public final short[][] cyclers = new short[MAXCYCLERS][6];
    public short numcyclers;
    public short numanimwalls;
    public final short[] spriteq = new short[1024];
    public short spriteqloc, spriteqamount;
    public final short[] mirrorwall = new short[64];
    public final short[] mirrorsector = new short[64];
    public short mirrorcnt;
    public short numclouds;
    public final short[] clouds = new short[128];
    public final short[] cloudx = new short[128];
    public final short[] cloudy = new short[128];
    public final IntArray actorscrptr = new IntArray(MAXTILES);
    public final ShortArray actortype = new ShortArray(MAXTILES);
    public final int[] script = new int[MAXSCRIPTSIZE];
    private String message;

    public SafeLoader() {
        for (int i = 0; i < MAXPLAYERS; i++) {
            ps[i] = new PlayerStruct();
            po[i] = new PlayerOrig();
        }

        for (int i = 0; i < MAXANIMWALLS; i++) {
            animwall[i] = new Animwalltype();
        }

        for (int i = 0; i < MAXANIMATES; i++) {
            gAnimationData[i] = new ANIMATION();
        }
    }

    public void LoadGDXBlock(InputStream is) throws IOException {
        warp_on = StreamUtils.readByte(is);
        if (warp_on == 1) {
            boolean isPacked = StreamUtils.readBoolean(is);
            addonFileName = StreamUtils.readDataString(is).toLowerCase();
            if (isPacked) {
                addonPackedConName = StreamUtils.readDataString(is).toLowerCase();
            }
        }
    }

    public void MapLoad(InputStream is) throws IOException {
        boardfilename = DUMMY_ENTRY;
        String name = StreamUtils.readString(is, 144);
        if (!name.isEmpty()) {
            boardfilename = game.getCache().getEntry(name, true);
        }

        sector = new Sector[StreamUtils.readInt(is)];
        for (int i = 0; i < sector.length; i++) {
            sector[i] = new Sector().readObject(is);
        }

        wall = new Wall[StreamUtils.readInt(is)];
        for (int i = 0; i < wall.length; i++) {
            wall[i] = new Wall().readObject(is);
        }

        int numSprites = StreamUtils.readInt(is);
        sprite = new ArrayList<>(numSprites * 2);

        for (int i = 0; i < numSprites; i++) {
            sprite.add(new Sprite().readObject(is));
        }
    }

    public void AnimationLoad(InputStream is) throws IOException {
        for (int i = 0; i < MAXANIMATES; i++) {
            short index = StreamUtils.readShort(is);
            byte type = StreamUtils.readByte(is);
            gAnimationData[i].id = index;
            gAnimationData[i].type = type;
            gAnimationData[i].ptr = null;
            gAnimationData[i].goal = StreamUtils.readInt(is);
            gAnimationData[i].vel = StreamUtils.readInt(is);
            gAnimationData[i].sect = StreamUtils.readShort(is);
        }
        gAnimationCount = StreamUtils.readInt(is);
    }

    public void GameInfoLoad(InputStream is) throws IOException {
        pskybits = StreamUtils.readShort(is);
        parallaxyscale = StreamUtils.readInt(is);
        for (int i = 0; i < MAXPSKYTILES; i++) {
            pskyoff[i] = StreamUtils.readShort(is);
        }

        earthquaketime = StreamUtils.readShort(is);
        from_bonus = StreamUtils.readShort(is);
        secretlevel = StreamUtils.readShort(is);
        respawn_monsters = StreamUtils.readBoolean(is);
        respawn_items = StreamUtils.readBoolean(is);
        respawn_inventory = StreamUtils.readBoolean(is);
        god = StreamUtils.readBoolean(is);
        auto_run = (StreamUtils.readInt(is) == 1) ? 1 : 0;
        crosshair = (StreamUtils.readInt(is) == 1) ? 1 : 0;
        monsters_off = StreamUtils.readBoolean(is);
        last_level = StreamUtils.readInt(is);
        eog = StreamUtils.readInt(is);
        coop = StreamUtils.readInt(is);
        marker = StreamUtils.readInt(is);
        ffire = StreamUtils.readInt(is);
        camsprite = StreamUtils.readShort(is);

        connecthead = StreamUtils.readShort(is);
        for (int i = 0; i < MAXPLAYERS; i++) {
            connectpoint2[i] = StreamUtils.readShort(is);
        }
        numplayersprites = StreamUtils.readShort(is);

        for (int i = 0; i < MAXPLAYERS; i++) {
            for (int j = 0; j < MAXPLAYERS; j++) {
                frags[i][j] = StreamUtils.readShort(is);
            }
        }

        randomseed = StreamUtils.readInt(is);
        global_random = StreamUtils.readShort(is);
    }

    public void StuffLoad(InputStream is) throws IOException {
        numcyclers = StreamUtils.readShort(is);
        for (int i = 0; i < MAXCYCLERS; i++) {
            for (int j = 0; j < 6; j++) {
                cyclers[i][j] = StreamUtils.readShort(is);
            }
        }

        for (int i = 0; i < MAXPLAYERS; i++) {
            ps[i].readObject(is);
        }
        for (int i = 0; i < MAXPLAYERS; i++) {
            po[i].readObject(is);
        }

        numanimwalls = StreamUtils.readShort(is);
        for (int i = 0; i < MAXANIMWALLS; i++) {
            animwall[i].wallnum = StreamUtils.readShort(is);
            animwall[i].tag = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 2048; i++) {
            msx[i] = StreamUtils.readInt(is);
        }
        for (int i = 0; i < 2048; i++) {
            msy[i] = StreamUtils.readInt(is);
        }

        spriteqloc = StreamUtils.readShort(is);
        spriteqamount = StreamUtils.readShort(is);
        for (int i = 0; i < 1024; i++) {
            spriteq[i] = StreamUtils.readShort(is);
        }

        mirrorcnt = StreamUtils.readShort(is);
        for (int i = 0; i < 64; i++) {
            mirrorwall[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 64; i++) {
            mirrorsector[i] = StreamUtils.readShort(is);
        }

        show2dsector.readObject(is);
        numclouds = StreamUtils.readShort(is);
        for (int i = 0; i < 128; i++) {
            clouds[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 128; i++) {
            cloudx[i] = StreamUtils.readShort(is);
        }
        for (int i = 0; i < 128; i++) {
            cloudy[i] = StreamUtils.readShort(is);
        }
    }

    public void ConLoad(InputStream is) throws IOException {
        int len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            actortype.add((short) (StreamUtils.readUnsignedByte(is)));
        }

        len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            script[i] = StreamUtils.readInt(is);
        }

        len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            actorscrptr.add(StreamUtils.readInt(is));
        }

        len = StreamUtils.readInt(is);
        for (int i = 0; i < len; i++) {
            Weaponhit weaponhit = new Weaponhit();
            weaponhit.readObject(is);
            hittype.add(weaponhit);
        }
    }

    public String getMessage() {
        return message;
    }

    public GameInfo LoadGDXHeader(InputStream is) throws IOException {
        volume_number = -1;
        level_number = -1;
        player_skill = -1;
        warp_on = 0;
        addon = null;
        addonFileName = null;
        addonPackedConName = null;

        StreamUtils.skip(is, SAVETIME + SAVENAME);

        multimode = StreamUtils.readInt(is);
        volume_number = StreamUtils.readInt(is);
        level_number = StreamUtils.readInt(is);
        player_skill = StreamUtils.readInt(is);

        StreamUtils.skip(is, SAVESCREENSHOTSIZE);

        LoadGDXBlock(is);
        if (warp_on == 1) { // try to find addon
            addon = findAddon(addonFileName, addonPackedConName);
        }

        return addon;
    }

    public boolean load(InputStream is) {
        message = null;

        hittype.clear();
        actorscrptr.clear();
        actortype.clear();

        try {
            addon = LoadGDXHeader(is);
            MapLoad(is);
            StuffLoad(is);
            ConLoad(is);
            AnimationLoad(is);
            GameInfoLoad(is);

            if (warp_on == 1 && addon == null) { // try to find addon
                message = "Can't find user episode file: " + addonFileName;
                warp_on = 2;

                volume_number = 0;
                level_number = 7;
            }

            System.out.println(is.available());
            if (is.available() == 0) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public static GameInfo findAddon(String addonFileName, String conName) {
        try {
            FileEntry addonEntry = game.getCache().getGameDirectory().getEntry(FileUtils.getPath(addonFileName));
            if (addonEntry.exists()) {
                if (conName == null) {
                    conName = addonEntry.getName();
                }

                String finalIniName = conName;
                return episodeManager.getEpisodeEntries(addonEntry)
                        .stream()
                        .filter(e -> e.getConFile().getName().equalsIgnoreCase(finalIniName))
                        .map(episodeManager::getEpisode)
                        .findAny().orElse(null);
            }
        } catch (Exception e) {
            Console.out.println(e.toString(), OsdColor.RED);
        }
        return null;
    }
}
