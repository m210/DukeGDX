package ru.m210projects.Duke3D.Types;

public enum GameType {
    DUKE_3D,
    NAM;

    public static GameType getType(int value) {
        if (value == 1) {
            return GameType.NAM;
        }
        return GameType.DUKE_3D;
    }
}
