package ru.m210projects.Duke3D.Types.CompareService;

import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.filehandle.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

import static ru.m210projects.Build.Engine.MAXSECTORS;
import static ru.m210projects.Build.Engine.MAXSTATUS;
import static ru.m210projects.Duke3D.Main.boardService;

@SuppressWarnings("unused")
public class SpriteLinkedListItem implements CompareItem {

    @Override
    public boolean compare(InputStream is) throws IOException {
        boolean equals = true;

        for (int i = 0; i < MAXSTATUS; i++) {
            boolean statusEquals = true;
            Set<Integer> thisSpriteSet = new HashSet<>();

            List<Integer> list = new ArrayList<>();
            int size = StreamUtils.readInt(is);
            for (int j = 0; j < size; j++) {
                list.add(StreamUtils.readInt(is));
            }

            if (boardService.getStatNode(i) != null && size > 0) {
                int index = 0;
                for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = node.getNext()) {
                    int compValue = index < size ? list.get(index++) : -1;
                    thisSpriteSet.add(node.getIndex());
                    if (compValue != node.getIndex()) {
                        // System.out.println("Unsync in " + getName() + "::stat: " + i + " at index " + compValue + "(read) != (this) " + node.getIndex());
                        equals = false;
                        statusEquals = false;
                    }
                }
            } else if (boardService.getStatNode(i) != null || size > 0) {
                System.out.println("Unsync in " + getName() + "::stat: " + i);
                equals = false;
            }

            if (!statusEquals) {
                for (int s : list) {
                    if (!thisSpriteSet.contains(s)) {
                        System.out.println("Unsync in " + getName() + "::stat: " + i + " at index (read) " + s);
                    } else {
                        thisSpriteSet.remove(s);
                    }
                }

                for (int s : thisSpriteSet) {
                    System.out.println("Unsync in " + getName() + "::stat: " + i + " at index (this) " + s);
                }
            }

        }

        for (int i = 0; i < MAXSECTORS; i++) {
            boolean sectEquals = true;
            Set<Integer> thisSpriteSet = new HashSet<>();

            List<Integer> list = new ArrayList<>();
            int size = StreamUtils.readInt(is);
            for (int j = 0; j < size; j++) {
                list.add(StreamUtils.readInt(is));
            }

            if (boardService.getSectNode(i) != null && size > 0) {
                int index = 0;
                for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                    int compValue = index < size ? list.get(index++) : -1;
                    thisSpriteSet.add(node.getIndex());
                    if (compValue != node.getIndex()) {
                        // System.out.println("Unsync in " + getName() + "::sect: " + i + " at index " + compValue + "(read) != (this) " + node.getIndex());
                        equals = false;
                        sectEquals = false;
                    }
                }
            } else if (boardService.getSectNode(i) != null || size > 0) {
                if (i == boardService.getSectorCount()) {
                    continue;
                }

                System.out.println("Unsync in " + getName() + "::sect: " + i);
                equals = false;
            }

            if (!sectEquals) {
                for (int s : list) {
                    if (!thisSpriteSet.contains(s)) {
                        System.out.println("Unsync in " + getName() + "::sect: " + i + " at index (read) " + s);
                    } else {
                        thisSpriteSet.remove(s);
                    }
                }

                for (int s : thisSpriteSet) {
                    System.out.println("Unsync in " + getName() + "::sect: " + i + " at index (this) " + s);
                }
            }
        }

        return equals;
    }

    @Override
    public void write(OutputStream os) throws IOException {
        for (int i = 0; i < MAXSTATUS; i++) {
            List<Integer> list = new ArrayList<>();
            if (boardService.getStatNode(i) != null) {
                for (ListNode<Sprite> node = boardService.getStatNode(i); node != null; node = node.getNext()) {
                    list.add(node.getIndex());
                }
            }

            StreamUtils.writeInt(os, list.size());
            for (Integer integer : list) {
                StreamUtils.writeInt(os, integer);
            }
        }

        for (int i = 0; i < MAXSECTORS; i++) {
            List<Integer> list = new ArrayList<>();
            if (boardService.getSectNode(i) != null) {
                for (ListNode<Sprite> node = boardService.getSectNode(i); node != null; node = node.getNext()) {
                    list.add(node.getIndex());
                }
            }
            StreamUtils.writeInt(os, list.size());
            for (Integer integer : list) {
                StreamUtils.writeInt(os, integer);
            }
        }
    }

    @Override
    public String getName() {
        return "SpriteLinkedListItem";
    }
}
