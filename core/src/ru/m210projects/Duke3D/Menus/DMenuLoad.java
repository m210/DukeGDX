// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Menus;

import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.Pattern.CommonMenus.MenuLoadSave;
import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.fs.FileEntry;
import ru.m210projects.Duke3D.Screens.PrecacheScreen;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Gameutils.coordsConvertXScaled;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.LoadSave.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.LOADSCREEN;
import static ru.m210projects.Duke3D.Premap.resetinventory;
import static ru.m210projects.Duke3D.Premap.resetweapons;

public class DMenuLoad extends MenuLoadSave {

    public DMenuLoad(final BuildGame app) {
        super(app, app.getFont(0), 75, 50, 185, 240, 12, 12, 2, LOADSCREEN, (handler, pItem) -> {
            final MenuSlotList item = (MenuSlotList) pItem;
            if (canLoad(item.getFileEntry()) && (!app.isCurrentScreen(gLoadingScreen)
                    && !(app.getScreen() instanceof PrecacheScreen))) {
                app.changeScreen(gLoadingScreen);
                gLoadingScreen.init(() -> {
                    if (!loadgame(item.getFileEntry())) {
                        app.setPrevScreen();
                        if (app.isCurrentScreen(gGameScreen)) {
                            app.pNet.ready2send = true;
                        }
                    } else if (item.getFileEntry().getName().equalsIgnoreCase("autosave.sav") && gDemoScreen.isRecordEnabled()) {
                        PlayerStruct p = ps[myconnectindex];
                        Sprite psp = boardService.getSprite(p.i);
                        int health = 100;
                        if (psp != null) {
                            health = psp.getExtra();
                        }

                        int armor = p.shield_amount;
                        boolean[] gotweapon = new boolean[MAX_WEAPONS];
                        int[] ammo_amount = new int[MAX_WEAPONS];

                        for(int j = 0; j < MAX_WEAPONS; j++) {
                            gotweapon[j] = p.gotweapon[j];
                            ammo_amount[j] = p.ammo_amount[j];
                        }
                        int firstaid_amount = p.firstaid_amount;
                        int steroids_amount = p.steroids_amount;
                        int holoduke_amount = p.holoduke_amount;
                        int jetpack_amount = p.jetpack_amount;
                        int heat_amount = p.heat_amount;
                        int scuba_amount = p.scuba_amount;
                        int boot_amount = p.boot_amount;
                        int inven_icon = p.inven_icon;

                        resetweapons(myconnectindex);
                        resetinventory(myconnectindex);

                        if (psp != null) {
                            psp.setExtra(health);
                        }

                        p.shield_amount = (short) armor;
                        for(int j = 0; j < MAX_WEAPONS; j++) {
                            p.gotweapon[j] = gotweapon[j];
                            p.ammo_amount[j] = ammo_amount[j];
                        }
                        p.firstaid_amount = (short) firstaid_amount;
                        p.steroids_amount = (short) steroids_amount;
                        p.holoduke_amount = (short) holoduke_amount;
                        p.jetpack_amount = (short) jetpack_amount;
                        p.heat_amount = (short) heat_amount;
                        p.scuba_amount = (short) scuba_amount;
                        p.boot_amount = (short) boot_amount;
                        p.inven_icon = inven_icon;

                        uGameFlags |= MODE_EOL;
                    }
                });
            }
        }, false);

        list.questionFont = app.getFont(1);
        list.nListOffset = 15;
        list.backgroundPal = 4;
    }

    @Override
    public boolean loadData(FileEntry filename) {
        return lsReadLoadData(filename) != -1;
    }

    @Override
    public MenuTitle getTitle(BuildGame app, String text) {
        return new DukeTitle(text);
    }

    @Override
    public MenuPicnum getPicnum(Engine draw, int x, int y) {
        return new MenuPicnum(draw, x - 70, y - 3, LOADSCREEN, LOADSCREEN, 0) {
            @Override
            public void draw(MenuHandler handler) {
                Renderer renderer = game.getRenderer();
                int ydim = renderer.getHeight();
                if (nTile != defTile) {
                    renderer.rotatesprite(x + 3 << 16, y << 16, 0x12400, 0, nTile, 0, 0, 10 | 16);
                } else {
                    renderer.rotatesprite((x + 94) << 16, y + 57 << 16, 0x9200, 0, nTile, 0, 0, 10, coordsConvertXScaled(x, ConvertType.Normal), 0, coordsConvertXScaled(x + 186, ConvertType.Normal), ydim - 1);
                }
            }
        };
    }

    @Override
    public MenuText getInfo(BuildGame app, int x, int y) {
        return new MenuText(lsInf.info, app.getFont(0), x - 58, y + 100, 0) {
            @Override
            public void draw(MenuHandler handler) {
                int ty = y;
                if (lsInf.addonfile != null && !lsInf.addonfile.isEmpty()) {
                    font.drawTextScaled(handler.getRenderer(), x, ty, lsInf.addonfile, 1.0f, -128, 12, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.date != null && !lsInf.date.isEmpty()) {
                    font.drawTextScaled(handler.getRenderer(), x, ty, lsInf.date, 1.0f, -128, 12, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                    ty -= 10;
                }
                if (lsInf.info != null) {
                    font.drawTextScaled(handler.getRenderer(), x, ty, lsInf.info, 1.0f, -128, 12, TextAlign.Left, Transparent.None, ConvertType.Normal, true);
                }
            }
        };
    }

}
