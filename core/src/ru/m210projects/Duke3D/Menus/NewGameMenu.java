// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Menus;

import ru.m210projects.Build.Pattern.MenuItems.*;
import ru.m210projects.Build.filehandle.Entry;
import ru.m210projects.Build.filehandle.FileUtils;
import ru.m210projects.Duke3D.Factory.DukeMenuHandler;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Duke3D.Types.GameInfo;
import ru.m210projects.Duke3D.Types.GameType;

import java.nio.file.Path;

import static ru.m210projects.Duke3D.Factory.DukeMenuHandler.*;
import static ru.m210projects.Duke3D.Globals.defGame;
import static ru.m210projects.Duke3D.Main.game;
import static ru.m210projects.Duke3D.ResourceHandler.levelGetEpisode;

public class NewGameMenu extends BuildMenu {

    public NewGameMenu(final Main app) {
        super(app.pMenu);
        final DukeMenuHandler menu = app.menu;

        addItem(new DukeTitle("SELECT AN EPISODE"), false);
        final DUserContent usercont = (DUserContent) menu.mMenus[USERCONTENT];

        GameInfo vaca = null, dc = null, nw = null;
        if (game.isGameType(GameType.DUKE_3D)) {
            vaca = getAddon( new Path[] {FileUtils.getPath("vacation.grp"), FileUtils.getPath("addons", "vacation", "vacation.grp")}, "Life's a Beach");
            if (vaca != null) {
                for (int i = 0; i < vaca.episodes.length; i++) {
                    if (vaca.episodes[i] != null && vaca.episodes[i].Title.contains("DUKEMATCH")) {
                        vaca.episodes[i] = null;
                    }
                }
            }
            dc = getAddon(new Path[] {FileUtils.getPath("dukedc.grp"), FileUtils.getPath("addons", "dc", "dukedc.grp")}, "Duke it Out in D.C.");
            nw = getAddon(new Path[] {FileUtils.getPath("nwinter.grp"), FileUtils.getPath("addons", "nw", "nwinter.grp")}, "Nuclear Winter");
        }

        MenuProc newEpProc = (handler, pItem) -> {
            EpisodeButton but = (EpisodeButton) pItem;
            DifficultyMenu next = (DifficultyMenu) menu.mMenus[DIFFICULTY];
            next.setEpisode(but.game, but.specialOpt);
            menu.mOpen(next, but.nItem);
        };

        int epnum = 0;
        int pos = 30;
        for (int i = 0; i < defGame.nEpisodes; i++) {
            if (defGame.episodes[i] != null) { //empty check
                if (game.isGameType(GameType.NAM) && defGame.episodes[i].Title.contains("MULTIPLAYER")) //disable NAM multiplayer episodes
                {
                    continue;
                }

                EpisodeButton skill = new EpisodeButton(defGame, defGame.episodes[i].Title, app.getFont(2), 0, pos += 19, 320, newEpProc, i);
                addItem(skill, i == 0);
                epnum++;
            }
        }

        if (game.isGameType(GameType.DUKE_3D)) {
            if (vaca != null || dc != null || nw != null) {
                addItem(new MenuButton("Official addons", app.getFont(2), 0, pos += 19, 320, 1, 0, new AddonMenu(app, vaca, dc, nw), -1, null, -1), epnum == 0);
            } else {
                addItem(new MenuButton("Official addons", app.getFont(2), 0, pos += 19, 320, 1, 0, null, -1, null, -1) {
                    @Override
                    public void draw(MenuHandler handler) {
                        this.mCheckEnableItem(false);
                        super.draw(handler);
                    }
                }, epnum == 0);
            }
            epnum++;
        }

        MenuButton mUser = new MenuButton("< USER CONTENT >", app.getFont(2), 0, pos + 19, 320, 1, 0, null, -1, (handler, pItem) -> {
            if (usercont.showmain) {
                usercont.setShowMain(false);
            }
            handler.mOpen(usercont, -1);
        }, -1);
        addItem(mUser, epnum == 0);
    }

    private GameInfo getAddon(Path[] path, String newTitle) {
        for (Path s : path) {
            Entry entry = game.getCache().getEntry(s, true);
            GameInfo addon = levelGetEpisode(entry);
            if (addon != null) {
                addon.title = newTitle;
                return addon;
            }
        }
        return null;
    }

    private static class AddonMenu extends BuildMenu {
        public AddonMenu(final Main app, GameInfo... addons) {
            super(app.pMenu);
            addItem(new DukeTitle("SELECT AN ADDON"), false);

            MenuProc newEpProc = (handler, pItem) -> {
                EpisodeButton but = (EpisodeButton) pItem;

                NewAddonMenu next = (NewAddonMenu) app.menu.mMenus[NEWADDON];
                if (next.setEpisode(but.game)) {
                    DifficultyMenu diffic = (DifficultyMenu) app.menu.mMenus[DIFFICULTY];
                    diffic.setEpisode(but.game, next.getFirstEpisode());
                    app.menu.mOpen(diffic, -1);
                } else {
                    app.menu.mOpen(next, -1);
                }
            };

            int pos = 30;
            int add = 0;
            for (GameInfo addon : addons) {
                if (addon != null) {
                    addItem(new EpisodeButton(addon, addon.title, app.getFont(2), 0, pos += 19, 320, newEpProc, 0), add == 0);
                    add++;
                }
            }
        }
    }


}
