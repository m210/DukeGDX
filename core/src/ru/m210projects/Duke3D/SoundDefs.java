package ru.m210projects.Duke3D;

import java.util.Map;

//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Alexander Makarov-[M210] (m210-2007@mail.ru)

public class SoundDefs {

    public static final int KICK_HIT = 0;
    public static final int PISTOL_RICOCHET = 1;
    public static final int PISTOL_BODYHIT = 2;
    public static final int PISTOL_FIRE = 3;
    public static final int EJECT_CLIP = 4;
    public static final int INSERT_CLIP = 5;
    public static final int CHAINGUN_FIRE = 6;
    public static final int RPG_SHOOT = 7;
    public static final int POOLBALLHIT = 8;
    public static final int RPG_EXPLODE = 9;
    public static final int CAT_FIRE = 10;
    public static final int SHRINKER_FIRE = 11;
//    public static final int ACTOR_SHRINKING = 12;
    public static final int PIPEBOMB_BOUNCE = 13;
    public static final int PIPEBOMB_EXPLODE = 14;
    public static final int LASERTRIP_ONWALL = 15;
    public static final int LASERTRIP_ARMING = 16;
    public static final int LASERTRIP_EXPLODE = 17;
    public static final int VENT_BUST = 18;
    public static final int GLASS_BREAKING = 19;
    public static final int GLASS_HEAVYBREAK = 20;
    public static final int SHORT_CIRCUIT = 21;
    public static final int ITEM_SPLASH = 22;
public static final int DUKE_GASP = 25;
    public static final int SLIM_RECOG = 26;
    // public static final int ENDSEQVOL3SND1                  = 27;
    public static final int DUKE_URINATE = 28;
    public static final int ENDSEQVOL3SND2 = 29;
    public static final int ENDSEQVOL3SND3 = 30;
//    public static final int DUKE_PASSWIND = 32;
    public static final int DUKE_CRACK = 33;
    public static final int SLIM_ATTACK = 34;
    public static final int SOMETHINGHITFORCE = 35;
    public static final int DUKE_DRINKING = 36;
//    public static final int DUKE_KILLED1 = 37;
    public static final int DUKE_GRUNT = 38;
    public static final int DUKE_HARTBEAT = 39;
    public static final int DUKE_ONWATER = 40;
//    public static final int DUKE_DEAD = 41;
    public static final int DUKE_LAND = 42;
    public static final int DUKE_WALKINDUCTS = 43;
public static final int DUKE_UNDERWATER = 48;
    public static final int DUKE_JETPACK_ON = 49;
    public static final int DUKE_JETPACK_IDLE = 50;
    public static final int DUKE_JETPACK_OFF = 51;
public static final int DUKETALKTOBOSS = 56;
public static final int SQUISHED = 69;
    public static final int TELEPORTER = 70;
    public static final int ELEVATOR_ON = 71;
//    public static final int DUKE_KILLED3 = 72;
    public static final int ELEVATOR_OFF = 73;
//    public static final int DOOR_OPERATE1 = 74;
    public static final int SUBWAY = 75;
    public static final int SWITCH_ON = 76;
public static final int FLUSH_TOILET = 79;
//    public static final int HOVER_CRAFT = 80;
    public static final int EARTHQUAKE = 81;
//    public static final int INTRUDER_ALERT = 82;
    public static final int END_OF_LEVEL_WARN = 83;
public static final int WIND_AMBIENCE = 91;
    public static final int SOMETHING_DRIPPING = 92;
public static final int BOS1_RECOG = 97;
public static final int BOS2_RECOG = 102;
public static final int DUKE_GETWEAPON2 = 107;
//    public static final int BOS3_DYING = 108;
    public static final int SHOTGUN_FIRE = 109;
//    public static final int PRED_ROAM = 110;
    public static final int PRED_RECOG = 111;
public static final int CAPT_RECOG = 117;
public static final int PIG_RECOG = 121;
public static final int RECO_ROAM = 125;
    public static final int RECO_RECOG = 126;
    public static final int RECO_ATTACK = 127;
    public static final int RECO_PAIN = 128;
public static final int DRON_RECOG = 131;
public static final int COMM_RECOG = 136;
public static final int OCTA_RECOG = 141;
public static final int TURR_RECOG = 146;
public static final int SLIM_DYING = 149;
//    public static final int BOS3_ROAM = 150;
    public static final int BOS3_RECOG = 151;
public static final int BOS1_WALK = 156;
//    public static final int DRON_ATTACK2 = 157;
    public static final int THUD = 158;
//    public static final int OCTA_ATTACK2 = 159;
    public static final int WIERDSHOT_FLY = 160;
public static final int SLIM_ROAM = 163;
public static final int SHOTGUN_COCK = 169;
public static final int GENERIC_AMBIENCE17 = 177;
public static final int BONUS_SPEECH1 = 195;
    public static final int BONUS_SPEECH2 = 196;
    public static final int BONUS_SPEECH3 = 197;
//    public static final int PIG_CAPTURE_DUKE = 198;
    public static final int BONUS_SPEECH4 = 199;
    public static final int DUKE_LAND_HURT = 200;
public static final int DUKE_SEARCH2 = 207;
    public static final int DUKE_CRACK2 = 208;
    public static final int DUKE_SEARCH = 209;
    public static final int DUKE_GET = 210;
    public static final int DUKE_LONGTERM_PAIN = 211;
    public static final int MONITOR_ACTIVE = 212;
    public static final int NITEVISION_ONOFF = 213;
//    public static final int DUKE_HIT_STRIPPER2 = 214;
    public static final int DUKE_CRACK_FIRST = 215;
    public static final int DUKE_USEMEDKIT = 216;
    public static final int DUKE_TAKEPILLS = 217;
    public static final int DUKE_PISSRELIEF = 218;
    public static final int SELECT_WEAPON = 219;
public static final int JIBBED_ACTOR5 = 226;
    public static final int JIBBED_ACTOR6 = 227;
//    public static final int JIBBED_ACTOR7 = 228;
    public static final int DUKE_GOTHEALTHATLOW = 229;
    public static final int BOSSTALKTODUKE = 230;
//    public static final int WAR_AMBIENCE1 = 231;
    public static final int WAR_AMBIENCE2 = 232;
public static final int EXITMENUSOUND = 243;
    public static final int FLY_BY = 244;
    public static final int DUKE_SCREAM = 245;
    public static final int SHRINKER_HIT = 246;
    public static final int RATTY = 247;
//    public static final int INTO_MENU = 248;
    public static final int BONUSMUSIC = 249;
public static final int ALIEN_SWITCH1 = 272;
public static final int RIPHEADNECK = 284;
public static final int ENDSEQVOL3SND4 = 288;
    public static final int ENDSEQVOL3SND5 = 289;
    public static final int ENDSEQVOL3SND6 = 290;
    public static final int ENDSEQVOL3SND7 = 291;
    public static final int ENDSEQVOL3SND8 = 292;
    public static final int ENDSEQVOL3SND9 = 293;
    public static final int WHIPYOURASS = 294;
    public static final int ENDSEQVOL2SND1 = 295;
    public static final int ENDSEQVOL2SND2 = 296;
    public static final int ENDSEQVOL2SND3 = 297;
    public static final int ENDSEQVOL2SND4 = 298;
    public static final int ENDSEQVOL2SND5 = 299;
    public static final int ENDSEQVOL2SND6 = 300;
    public static final int ENDSEQVOL2SND7 = 301;
//    public static final int GENERIC_AMBIENCE23 = 302;
    public static final int SOMETHINGFROZE = 303;
public static final int WIND_REPEAT = 308;
public static final int BOS4_RECOG = 342;
public static final int LIGHTNING_SLAP = 351;
    public static final int THUNDER = 352;
public static final int INTRO4_1 = 363;
    public static final int INTRO4_2 = 364;
    public static final int INTRO4_3 = 365;
    public static final int INTRO4_4 = 366;
    public static final int INTRO4_5 = 367;
    public static final int INTRO4_6 = 368;
//    public static final int SCREECH = 369;
    public static final int BOSS4_DEADSPEECH = 370;
    public static final int BOSS4_FIRSTSEE = 371;
public static final int VOL4ENDSND1 = 384;
    public static final int VOL4ENDSND2 = 385;
public static final int EXPANDERSHOOT = 388;
public static final int INTRO4_B = 392;
    public static final int BIGBANG = 393;

    // Twentieth Anniversary World Tour
    public static final int FLAMETHROWER_INTRO = 398;
public static final int E5L7_DUKE_QUIT_YOU = 401;
    // MAXIMUM NUMBER OF SOUNDS: 450 ( 0-449 )

    public static Map<Integer, String> commentaries;
}
