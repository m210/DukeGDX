//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.Types.Sprite;
import ru.m210projects.Build.Types.Wall;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Duke3D.Menus.InterfaceMenu;
import ru.m210projects.Duke3D.Types.GameType;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Duke3D.Actors.*;
import static ru.m210projects.Duke3D.Gamedef.*;
import static ru.m210projects.Duke3D.Gameutils.*;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Player.checkavailweapon;
import static ru.m210projects.Duke3D.Player.hits;
import static ru.m210projects.Duke3D.Screen.*;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.SoundDefs.*;
import static ru.m210projects.Duke3D.Sounds.spritesound;
import static ru.m210projects.Duke3D.Sounds.xyzsound;
import static ru.m210projects.Duke3D.Spawn.EGS;
import static ru.m210projects.Duke3D.Spawn.spawn;
import static ru.m210projects.Duke3D.View.*;

public class Weapons {

    public static final short[] aimstats = {10, 13, 1, 2};
    public static final short[] knee_y = {0, -8, -16, -32, -64, -84, -108, -108, -108, -72, -32, -8};
    public static final short[] knuckle_frames = {0, 1, 2, 2, 3, 3, 3, 2, 2, 1, 0};
    public static final byte[] kb_frames = {0, 1, 2, 0, 0};
    public static final byte[] remote_frames = {0, 1, 1, 2, 1, 1, 0, 0, 0, 0, 0};
    public static final byte[] cycloidy = {0, 4, 12, 24, 12, 4, 0};
    public static final byte[] throw_frames = {0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    public static final byte[] cat_frames = {0, 0, 1, 1, 2, 2};
    private static final FireProj[] fire = new FireProj[MAXSPRITES];
    private static final short[] weapon_sprites = {KNEE, FIRSTGUNSPRITE, SHOTGUNSPRITE, CHAINGUNSPRITE, RPGSPRITE,
            HEAVYHBOMB, SHRINKERSPRITE, DEVISTATORSPRITE, TRIPBOMBSPRITE, FREEZESPRITE, HEAVYHBOMB, SHRINKERSPRITE,

            // Twentieth Anniversary World Tour
            FLAMETHROWERSPRITE};
    public static short fistsign;

    public static int aim(Sprite s, int aang) {
        if (s.getPicnum() == APLAYER && ps[s.getYvel()].auto_aim == 0) {
            return -1;
        }

        int a = s.getAng();
        int j = -1;

        boolean gotshrinker = s.getPicnum() == APLAYER && ps[s.getYvel()].curr_weapon == SHRINKER_WEAPON;
        boolean gotfreezer = s.getPicnum() == APLAYER && ps[s.getYvel()].curr_weapon == FREEZE_WEAPON;

        int smax = 0x7fffffff;

        int dx1 = EngineUtils.sin((a + 512 - aang) & 2047);
        int dy1 = EngineUtils.sin((a - aang) & 2047);
        int dx2 = EngineUtils.sin((a + 512 + aang) & 2047);
        int dy2 = EngineUtils.sin((a + aang) & 2047);

        int dx3 = EngineUtils.sin((a + 512) & 2047);
        int dy3 = EngineUtils.sin(a & 2047);

        for (int k = 0; k < 4; k++) {
            if (j >= 0) {
                break;
            }

            for (ListNode<Sprite> node = boardService.getStatNode(aimstats[k]); node != null; node = node.getNext()) {
                final int i = node.getIndex();
                Sprite sp = node.get();
                if (sp.getXrepeat() > 0 && sp.getExtra() >= 0 && (sp.getCstat() & (257 + 32768)) == 257) {
                    if (badguy(sp) || k < 2) {
                        if (badguy(sp) || sp.getPicnum() == APLAYER || sp.getPicnum() == SHARK) {
                            if (sp.getPicnum() == APLAYER && ud.coop == 1 && s.getPicnum() == APLAYER && s != sp) {
                                continue;
                            }

                            if (gotshrinker && sp.getXrepeat() < 30) {
                                switch (sp.getPicnum()) {
                                    case SHARK:
                                        if (sp.getXrepeat() < 20) {
                                            continue;
                                        }
                                        continue;
                                    case GREENSLIME:
                                    case GREENSLIME + 1:
                                    case GREENSLIME + 2:
                                    case GREENSLIME + 3:
                                    case GREENSLIME + 4:
                                    case GREENSLIME + 5:
                                    case GREENSLIME + 6:
                                    case GREENSLIME + 7:
                                        break;
                                    default:
                                        continue;
                                }
                            }
                            if (gotfreezer && sp.getPal() == 1) {
                                continue;
                            }
                        }

                        int xv = (sp.getX() - s.getX());
                        int yv = (sp.getY() - s.getY());

                        if ((dy1 * xv) <= (dx1 * yv)) {
                            if ((dy2 * xv) >= (dx2 * yv)) {
                                int sdist = mulscale(dx3, xv, 14) + mulscale(dy3, yv, 14);
                                if (sdist > 512 && sdist < smax) {
                                    if (s.getPicnum() == APLAYER) {
                                        a = ((klabs(scale(sp.getZ() - s.getZ(), 10, sdist)
                                                - (int) (ps[s.getYvel()].horiz + ps[s.getYvel()].horizoff - 100)) < 100) ? 1 : 0);
                                    } else {
                                        a = 1;
                                    }

                                    boolean cans;
                                    if (sp.getPicnum() == ORGANTIC || sp.getPicnum() == ROTATEGUN) {
                                        cans = engine.cansee(sp.getX(), sp.getY(), sp.getZ(), sp.getSectnum(),
                                                s.getX(), s.getY(), s.getZ() - (32 << 8), s.getSectnum());
                                    } else {
                                        cans = engine.cansee(sp.getX(), sp.getY(), sp.getZ() - (32 << 8),
                                                sp.getSectnum(), s.getX(), s.getY(), s.getZ() - (32 << 8), s.getSectnum());
                                    }

                                    if (a != 0 && cans) {
                                        smax = sdist;
                                        j = i;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return j;
    }

    public static void tracers(int x1, int y1, int z1, int x2, int y2, int z2, int n) {
        int i, xv, yv, zv;
        int sect = -1;

        i = n + 1;
        xv = (x2 - x1) / i;
        yv = (y2 - y1) / i;
        zv = (z2 - z1) / i;

        if ((klabs(x1 - x2) + klabs(y1 - y2)) < 3084) {
            return;
        }

        for (i = n; i > 0; i--) {
            x1 += xv;
            y1 += yv;
            z1 += zv;
            sect = engine.updatesector(x1, y1, sect);
            Sector sec = boardService.getSector(sect);
            if (sec != null) {
                if (sec.getLotag() == 2) {
                    int va = engine.krand() & 2047;
                    int vy = 4 + (engine.krand() & 3);
                    int vx = 4 + (engine.krand() & 3);
                    EGS(sect, x1, y1, z1, WATERBUBBLE, -32, vx, vy, va, 0, 0, ps[0].i, (short) 5);
                } else {
                    EGS(sect, x1, y1, z1, SMALLSMOKE, -32, 14, 14, 0, 0, 0, ps[0].i, (short) 5);
                }
            }
        }
    }

    public static void shoot(final int i, int atwith) {
        final Sprite s = boardService.getSprite(i);
        if (s == null) {
            return;
        }

        final int sect = s.getSectnum();
        final Sector sec = boardService.getSector(sect);
        int vel, zvel = 0;

        if (sec == null) {
            return;
        }

        int sx, sy, sz, sa;
        int p;
        if (s.getPicnum() == APLAYER) {
            p = s.getYvel();

            sx = ps[p].posx;
            sy = ps[p].posy;
            sz = ps[p].posz + ps[p].pyoff + (4 << 8);
            sa = (int) ps[p].ang;

            ps[p].crack_time = 777;

        } else {
            p = -1;
            sa = s.getAng();
            sx = s.getX();
            sy = s.getY();
            sz = s.getZ() - ((s.getYrepeat() * engine.getTile(s.getPicnum()).getHeight()) << 1) + (4 << 8);
            if (s.getPicnum() != ROTATEGUN) {
                sz -= (7 << 8);
                if (badguy(s) && s.getPicnum() != COMMANDER) {
                    sx += (EngineUtils.sin((sa + 1024 + 96) & 2047) >> 7);
                    sy += (EngineUtils.sin((sa + 512 + 96) & 2047) >> 7);
                }
            }
        }

        if (currentGame.getCON().type == 20) { // Twentieth Anniversary World Tour
            switch (atwith) {
                case FIREBALL: {
                    if (s.getExtra() >= 0) {
                        s.setShade(-96);
                    }

                    sz -= (4 << 7);
                    if (s.getPicnum() != BOSS5) {
                        vel = 840;
                    } else {
                        vel = 968;
                        sz += 6144;
                    }

                    if (p < 0) {
                        sa += 16 - (engine.krand() & 31);
                        int j = (short) findplayer(s);
                        Sprite psp = boardService.getSprite(ps[j].i);
                        if (psp != null) {
                            zvel = (((ps[j].oposz - sz + (3 << 8))) * vel) / ldist(psp, s);
                        }
                    } else {
                        zvel = 98 * (100 - ps[p].horizoff - (int) ps[p].horiz);
                        sx += EngineUtils.sin((sa + 860) & 0x7FF) / 448;
                        sy += EngineUtils.sin((sa + 348) & 0x7FF) / 448;
                        sz += (3 << 8);
                    }

                    int sizx = 18;
                    int sizy = 18;
                    if (p >= 0) {
                        sizx = 7;
                        sizy = 7;
                    }

                    int j = EGS(sect, sx, sy, sz, atwith, -127, sizx, sizy, sa, vel, zvel, i, (short) 4);
                    Sprite spr = boardService.getSprite(j);
                    if (spr == null) {
                        return;
                    }

                    spr.setExtra(spr.getExtra() + (engine.krand() & 7));
                    if (s.getPicnum() == BOSS5 || p >= 0) {
                        spr.setXrepeat(40);
                        spr.setYrepeat(40);
                    }
                    spr.setYvel(p);
                    spr.setCstat(128);
                    spr.setClipdist(4);
                    return;
                }

                case FLAMETHROWERFLAME: {
                    if (s.getExtra() >= 0) {
                        s.setShade(-96);
                    }
                    vel = 400;

                    int k = -1;
                    if (p < 0) {
                        int j = findplayer(s);
                        sa = EngineUtils.getAngle(ps[j].oposx - sx, ps[j].oposy - sy);

                        if (s.getPicnum() == BOSS5) {
                            vel = 528;
                            sz += 6144;
                        } else if (s.getPicnum() == BOSS3) {
                            sz -= 8192;
                        }

                        Sprite psp2 = boardService.getSprite(ps[j].i);
                        if (psp2 != null) {
                            int l = ldist(psp2, s);
                            if (l != 0) {
                                zvel = ((ps[j].oposz - sz) * vel) / l;
                            }
                        }

                        if (badguy(s) && (s.getHitag() & face_player_smart) != 0) {
                            sa = (short) (s.getAng() + (engine.krand() & 31) - 16);
                        }

                    } else {
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) * 81;
                        Sprite psp = boardService.getSprite(ps[p].i);

                        if (psp != null && psp.getXvel() != 0) {
                            vel = (int) ((((512 - (1024
                                    - klabs(klabs(EngineUtils.getAngle(sx - ps[p].oposx, sy - ps[p].oposy) - sa) - 1024)))
                                    * 0.001953125f) * psp.getXvel()) + 400);
                        }
                    }

                    if (sec.getLotag() == 2 && (engine.krand() % 5) == 0) {
                        k = spawn(i, WATERBUBBLE);
                    }

                    Sprite sp = boardService.getSprite(k);
                    if (sp == null) {
                        k = spawn(i, atwith);
                        sp = boardService.getSprite(k);
                        if (sp == null) {
                            return;
                        }

                        sp.setXvel(vel);
                        sp.setZvel(zvel);
                    }

                    sp.setX(sx + EngineUtils.sin((sa + 630) & 0x7FF) / 448);
                    sp.setY(sy + EngineUtils.sin((sa + 112) & 0x7FF) / 448);
                    sp.setZ(sz - 256);
                    sp.setSectnum(sect);
                    sp.setCstat(0x80);
                    sp.setAng(sa);
                    sp.setXrepeat(2);
                    sp.setYrepeat(2);
                    sp.setClipdist(40);
                    sp.setYvel(p);
                    sp.setOwner((short) i);

                    if (p == -1) {
                        if (s.getPicnum() == BOSS5) {
                            sp.setX(sp.getX() - EngineUtils.sin(sa & 2047) / 56);
                            sp.setY(sp.getY() - EngineUtils.sin((sa + 1024 + 512) & 2047) / 56);
                            sp.setXrepeat(10);
                            sp.setYrepeat(10);
                        }
                    }
                    return;
                }
                case FIREFLY: {// BOSS5 shot
                    int k = spawn(i, atwith);
                    Sprite sp = boardService.getSprite(k);
                    if (sp == null) {
                        return;
                    }

                    sp.setSectnum(sect);
                    sp.setX(sx);
                    sp.setY(sy);
                    sp.setZ(sz);
                    sp.setAng(sa);
                    sp.setXvel(500);
                    sp.setZvel(0);
                    return;
                }
            }
        }

        switch (atwith) {
            case BLOODSPLAT1:
            case BLOODSPLAT2:
            case BLOODSPLAT3:
            case BLOODSPLAT4:

                if (p >= 0) {
                    sa += 64 - (engine.krand() & 127);
                } else {
                    sa += 1024 + 64 - (engine.krand() & 127);
                }
                zvel = 1024 - (engine.krand() & 2047);
            case KNEE: {
                if (atwith == KNEE) {
                    if (p >= 0) {
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) << 5;
                        sz += (6 << 8);
                        sa += 15;
                    } else {
                        int j = ps[findplayer(s)].i;
                        Sprite psp = boardService.getSprite(j);
                        if (psp == null) {
                            break;
                        }

                        int x = player_dist;
                        zvel = ((psp.getZ() - sz) << 8) / (x + 1);
                        sa = EngineUtils.getAngle(psp.getX() - sx, psp.getY() - sy);
                    }
                }

                engine.hitscan(sx, sy, sz, sect, EngineUtils.sin((sa + 512) & 2047), EngineUtils.sin(sa & 2047), zvel << 6, pHitInfo,
                        CLIPMASK1);

                final int hitsect = pHitInfo.hitsect;
                final int hitsprite = pHitInfo.hitsprite;
                int hitwall = pHitInfo.hitwall;
                final int hitx = pHitInfo.hitx;
                final int hity = pHitInfo.hity;
                final int hitz = pHitInfo.hitz;

                if (atwith == BLOODSPLAT1 || atwith == BLOODSPLAT2 || atwith == BLOODSPLAT3 || atwith == BLOODSPLAT4) {
                    if (FindDistance2D(sx - hitx, sy - hity) < 1024) {
                        Wall hwal = boardService.getWall(hitwall);
                        if (hwal != null && hwal.getOverpicnum() != BIGFORCE) {
                            Sector hsec = boardService.getSector(hitsect);
                            Sector nextsec = boardService.getSector(hwal.getNextsector());
                            if ((nextsec != null && hsec != null
                                    && hsec.getLotag() == 0
                                    && nextsec.getLotag() == 0
                                    && (hsec.getFloorz() - nextsec.getFloorz()) > (16 << 8))
                                    || (nextsec == null && hsec != null && hsec.getLotag() == 0)) {
                                if ((hwal.getCstat() & 16) == 0) {
                                    if (nextsec != null) {
                                        for (ListNode<Sprite> node = boardService.getSectNode(hwal.getNextsector()); node != null; node = node.getNext()) {
                                            Sprite spr = node.get();
                                            if (spr.getStatnum() == 3 && spr.getLotag() == 13) {
                                                return;
                                            }
                                        }
                                    }

                                    Wall nextwal = boardService.getWall(hwal.getNextwall());
                                    if (nextwal != null && nextwal.getHitag() != 0) {
                                        return;
                                    }

                                    if (hwal.getHitag() == 0) {
                                        int k = spawn(i, atwith);
                                        Sprite sp = boardService.getSprite(k);
                                        if (sp != null) {
                                            sp.setXvel(-12);
                                            sp.setAng((EngineUtils.getAngle(
                                                    hwal.getX() - hwal.getWall2().getX(),
                                                    hwal.getY() - hwal.getWall2().getY()) + 512));
                                            sp.setX(hitx);
                                            sp.setY(hity);
                                            sp.setZ(hitz);
                                            sp.setCstat(sp.getCstat() | (engine.krand() & 4));
                                            ssp(k, CLIPMASK0);
                                            engine.setsprite(k, sp.getX(), sp.getY(), sp.getZ());
                                            if (s.getPicnum() == OOZFILTER || s.getPicnum() == NEWBEAST) {
                                                sp.setPal(6);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    return;
                }

                if (hitsect == -1) {
                    break;
                }

                if ((klabs(sx - hitx) + klabs(sy - hity)) < 1024) {
                    if (hitwall != -1 || hitsprite != -1) {
                        int j = EGS(hitsect, hitx, hity, hitz, KNEE, -15, 0, 0, sa, 32, 0, i, (short) 4);

                        Sprite sp = boardService.getSprite(j);
                        if (sp != null) {
                            sp.setExtra(sp.getExtra() + (engine.krand() & 7));
                            if (p >= 0) {
                                int k = spawn(j, SMALLSMOKE);
                                Sprite sp2 = boardService.getSprite(k);
                                if (sp2 != null) {
                                    sp2.setZ(sp2.getZ() - (8 << 8));
                                }
                                spritesound(KICK_HIT, j);
                            }

                            if (p >= 0 && ps[p].steroids_amount > 0 && ps[p].steroids_amount < 400) {
                                sp.setExtra(sp.getExtra() + (currentGame.getCON().max_player_health >> 2));
                            }
                        }

                        Sprite hsp = boardService.getSprite(hitsprite);
                        if (hsp != null && hsp.getPicnum() != ACCESSSWITCH && hsp.getPicnum() != ACCESSSWITCH2) {
                            checkhitsprite(hitsprite, j);

                            if (p >= 0) {
                                checkhitswitch(p, hitsprite, 1);
                            }
                        } else {
                            Wall hwal = boardService.getWall(hitwall);
                            if (hwal != null) {
                                if ((hwal.getCstat() & 2) != 0) {
                                    Sector nsec = boardService.getSector(hwal.getNextsector());
                                    if (nsec != null && hitz >= (nsec.getFloorz())) {
                                        hitwall = hwal.getNextwall();
                                    }
                                }

                                hwal = boardService.getWall(hitwall);
                                if (hwal != null && hwal.getPicnum() != ACCESSSWITCH
                                        && hwal.getPicnum() != ACCESSSWITCH2) {
                                    checkhitwall(j, hitwall, hitx, hity, hitz, atwith);
                                    if (p >= 0) {
                                        checkhitswitch(p, hitwall, 0);
                                    }
                                }
                            }
                        }
                    } else {
                        Sector hsec = boardService.getSector(hitsect);
                        if (hsec != null && p >= 0 && zvel > 0 && hsec.getLotag() == 1) {
                            int j = spawn(ps[p].i, WATERSPLASH2);
                            Sprite sp = boardService.getSprite(j);
                            if (sp != null) {
                                sp.setX(hitx);
                                sp.setY(hity);
                                sp.setAng((short) ps[p].ang); // Total tweek
                                sp.setXvel(32);
                                ssp(i, CLIPMASK0);
                                sp.setXvel(0);
                            }
                        }
                    }
                }

                break;
            }
            case SHOTSPARK1:
            case SHOTGUN:
            case CHAINGUN: {

                if (s.getExtra() >= 0) {
                    s.setShade(-96);
                }

                if (p >= 0) {
                    int j = aim(s, AUTO_AIM_ANGLE);
                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        int dal = ((sp.getXrepeat() * engine.getTile(sp.getPicnum()).getHeight()) << 1) + (5 << 8);
                        switch (sp.getPicnum()) {
                            case GREENSLIME:
                            case GREENSLIME + 1:
                            case GREENSLIME + 2:
                            case GREENSLIME + 3:
                            case GREENSLIME + 4:
                            case GREENSLIME + 5:
                            case GREENSLIME + 6:
                            case GREENSLIME + 7:
                            case ROTATEGUN:
                                dal -= (8 << 8);
                                break;
                        }
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            zvel = ((sp.getZ() - sz - dal) << 8) / ldist(psp, sp);
                            sa = EngineUtils.getAngle(sp.getX() - sx, sp.getY() - sy);
                        }
                    }

                    if (atwith == SHOTSPARK1) {
                        if (j == -1) {
                            sa += 16 - (engine.krand() & 31);
                            zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) << 5;
                            zvel += 128 - (engine.krand() & 255);
                        }
                    } else {
                        sa += 16 - (engine.krand() & 31);
                        if (j == -1) {
                            zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) << 5;
                        }
                        zvel += 128 - (engine.krand() & 255);
                    }
                    sz -= (2 << 8);
                } else {
                    int j = findplayer(s);
                    Sprite psp = boardService.getSprite(ps[j].i);
                    if (psp == null) {
                        return;
                    }

                    sz -= (4 << 8);
                    zvel = ((ps[j].posz - sz) << 8) / (ldist(psp, s));
                    if (s.getPicnum() != BOSS1) {
                        zvel += 128 - (engine.krand() & 255);
                        sa += 32 - (engine.krand() & 63);
                    } else {
                        zvel += 128 - (engine.krand() & 255);
                        sa = (short) (EngineUtils.getAngle(ps[j].posx - sx, ps[j].posy - sy) + 64 - (engine.krand() & 127));
                    }
                }

                s.setCstat(s.getCstat() & ~257);
                engine.hitscan(sx, sy, sz, sect, EngineUtils.sin((sa + 512) & 2047), EngineUtils.sin(sa & 2047), zvel << 6, pHitInfo,
                        CLIPMASK1);

                final int hitsect = pHitInfo.hitsect;
                final int hitsprite = pHitInfo.hitsprite;
                int hitwall = pHitInfo.hitwall;
                final int hitx = pHitInfo.hitx;
                final int hity = pHitInfo.hity;
                final int hitz = pHitInfo.hitz;

                s.setCstat(s.getCstat() | 257);

                Sector hitsec = boardService.getSector(hitsect);
                if (hitsec == null) {
                    return;
                }

                if ((engine.krand() & 15) == 0 && hitsec.getLotag() == 2) {
                    tracers(hitx, hity, hitz, sx, sy, sz, 8 - (ud.multimode >> 1));
                }

                final int k;
                if (p >= 0) {
                    k = EGS(hitsect, hitx, hity, hitz, SHOTSPARK1, -15, 10, 10, sa, 0, 0, i, (short) 4);
                    final Sprite sp = boardService.getSprite(k);
                    if (sp == null) {
                        break;
                    }

                    sp.setExtra(currentGame.getCON().script[currentGame.getCON().actorscrptr[atwith]]);
                    sp.setExtra(sp.getExtra() + (engine.krand() % 6));

                    if (hitwall == -1 && hitsprite == -1) {
                        if (zvel < 0) {
                            if ((hitsec.getCeilingstat() & 1) != 0) {
                                sp.setXrepeat(0);
                                sp.setYrepeat(0);
                                return;
                            } else {
                                checkhitceiling(hitsect);
                            }
                        }
                        spawn(k, SMALLSMOKE);
                    }

                    Sprite hsp = boardService.getSprite(hitsprite);
                    if (hsp != null) {
                        checkhitsprite(hitsprite, k);
                        if (hsp.getPicnum() == APLAYER && (ud.coop != 1 || ud.ffire == 1)) {
                            sp.setXrepeat(0);
                            sp.setYrepeat(0);
                            int l = spawn(k, JIBS6);
                            Sprite sp2 = boardService.getSprite(l);
                            if (sp2 != null) {
                                sp2.setZ(sp2.getZ() + (4 << 8));
                                sp2.setXvel(16);
                                sp2.setXrepeat(24);
                                sp2.setYrepeat(24);
                                sp2.setAng(sp2.getAng() + 64 - (engine.krand() & 127));
                            }
                        } else {
                            spawn(k, SMALLSMOKE);
                        }

                        if (hsp.getPicnum() == DIPSWITCH
                                || hsp.getPicnum() == DIPSWITCH + 1
                                || hsp.getPicnum() == DIPSWITCH2
                                || hsp.getPicnum() == DIPSWITCH2 + 1
                                || hsp.getPicnum() == DIPSWITCH3
                                || hsp.getPicnum() == DIPSWITCH3 + 1
                                || hsp.getPicnum() == HANDSWITCH
                                || hsp.getPicnum() == HANDSWITCH + 1) {
                            checkhitswitch(p, hitsprite, 1);
                            return;
                        }
                    } else {
                        Wall hwal = boardService.getWall(hitwall);
                        if (hwal != null) {
                            spawn(k, SMALLSMOKE);

                            SKIPBULLETHOLE:
                            do {
                                if (isadoorwall(hwal.getPicnum())) {
                                    break; // goto SKIPBULLETHOLE;
                                }

                                if ((hwal.getPicnum() == DIPSWITCH || hwal.getPicnum() == DIPSWITCH + 1
                                        || hwal.getPicnum() == DIPSWITCH2 || hwal.getPicnum() == DIPSWITCH2 + 1
                                        || hwal.getPicnum() == DIPSWITCH3 || hwal.getPicnum() == DIPSWITCH3 + 1
                                        || hwal.getPicnum() == HANDSWITCH || hwal.getPicnum() == HANDSWITCH + 1)) {
                                    checkhitswitch(p, hitwall, 0);
                                    return;
                                }

                                Wall nextwal = boardService.getWall(hwal.getNextwall());
                                if (hwal.getHitag() != 0 || (nextwal != null && nextwal.getHitag() != 0)) {
                                    break; // goto SKIPBULLETHOLE;
                                }

                                if (hitsec.getLotag() == 0) {
                                    if (hwal.getOverpicnum() != BIGFORCE) {
                                        Sector nextsec = boardService.getSector(hwal.getNextsector());
                                        if ((nextsec != null && nextsec.getLotag() == 0)
                                                || (hwal.getNextsector() == -1 && hitsec.getLotag() == 0)) {
                                            if ((hwal.getCstat() & 16) == 0) {
                                                if (nextsec != null) {
                                                    for (ListNode<Sprite> node = boardService.getSectNode(hwal.getNextsector()); node != null; node = node.getNext()) {
                                                        Sprite sp2 = node.get();
                                                        if (sp2.getStatnum() == 3 && sp2.getLotag() == 13) {
                                                            break SKIPBULLETHOLE; // goto SKIPBULLETHOLE;
                                                        }
                                                    }
                                                }

                                                for (ListNode<Sprite> node = boardService.getStatNode(5); node != null; node = node.getNext()) {
                                                    Sprite sp2 = node.get();
                                                    if (sp2.getPicnum() == BULLETHOLE) {
                                                        if (dist(sp2, sp) < (12 + (engine.krand() & 7))) {
                                                            break SKIPBULLETHOLE; // goto SKIPBULLETHOLE;
                                                        }
                                                    }
                                                }
                                                int l = spawn(k, BULLETHOLE);
                                                Sprite hole = boardService.getSprite(l);
                                                if (hole != null) {
                                                    hole.setXvel(-1);
                                                    hole.setAng((EngineUtils.getAngle(
                                                            hwal.getX() - hwal.getWall2().getX(),
                                                            hwal.getY() - hwal.getWall2().getY()) + 512));
                                                    ssp(l, CLIPMASK0);
                                                }
                                            }
                                        }
                                    }
                                }
                            } while (false);

                            if ((hwal.getCstat() & 2) != 0) {
                                Sector nsec = boardService.getSector(hwal.getNextsector());
                                if (nsec != null) {
                                    if (hitz >= (nsec.getFloorz())) {
                                        hitwall = hwal.getNextwall();
                                    }
                                }
                            }

                            checkhitwall(k, hitwall, hitx, hity, hitz, SHOTSPARK1);
                        }
                    }
                } else {
                    k = EGS(hitsect, hitx, hity, hitz, SHOTSPARK1, -15, 24, 24, sa, 0, 0, i, (short) 4);
                    Sprite sp = boardService.getSprite(k);
                    if (sp != null) {
                        sp.setExtra(currentGame.getCON().script[currentGame.getCON().actorscrptr[atwith]]);

                        Sprite hsp = boardService.getSprite(hitsprite);
                        if (hsp != null) {
                            checkhitsprite(hitsprite, k);
                            if (hsp.getPicnum() != APLAYER) {
                                spawn(k, SMALLSMOKE);
                            } else {
                                sp.setXrepeat(0);
                                sp.setYrepeat(0);
                            }
                        } else if (hitwall != -1) {
                            checkhitwall(k, hitwall, hitx, hity, hitz, SHOTSPARK1);
                        }
                    }
                }

                if ((engine.krand() & 255) < 4) {
                    xyzsound(PISTOL_RICOCHET, k, hitx, hity, hitz);
                }

                return;
            }
            case FIRELASER:
            case SPIT:
            case COOLEXPLOSION1: {

                if (s.getExtra() >= 0) {
                    s.setShade(-96);
                }

                int scount = 1;
                if (atwith == SPIT) {
                    vel = 292;
                } else {
                    if (atwith == COOLEXPLOSION1) {
                        if (s.getPicnum() == BOSS2) {
                            vel = 644;
                        } else {
                            vel = 348;
                        }
                    } else {
                        vel = 840;
                    }
                    sz -= (4 << 7);
                }

                if (p >= 0) {
                    int j = aim(s, AUTO_AIM_ANGLE);

                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        int dal = ((sp.getXrepeat() * engine.getTile(sp.getPicnum()).getHeight()) << 1) - (12 << 8);
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            zvel = ((sp.getZ() - sz - dal) * vel) / ldist(psp, sp);
                        }
                        sa = EngineUtils.getAngle(sp.getX() - sx, sp.getY() - sy);
                    } else {
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) * 98;
                    }
                } else {
                    int j = findplayer(s);
                    Sprite psp = boardService.getSprite(ps[j].i);
                    if (psp != null) {
                        sa += 16 - (engine.krand() & 31);
                        zvel = (((ps[j].oposz - sz + (3 << 8))) * vel) / ldist(psp, s);
                    }
                }

                int oldzvel = zvel;

                int sizx, sizy;
                if (atwith == SPIT) {
                    sizx = 18;
                    sizy = 18;
                    sz -= (10 << 8);
                } else {
                    if (atwith == FIRELASER) {
                        if (p >= 0) {
                            sizx = 34;
                            sizy = 34;
                        } else {
                            sizx = 18;
                            sizy = 18;
                        }
                    } else {
                        sizx = 18;
                        sizy = 18;
                    }
                }

                if (p >= 0) {
                    sizx = 7;
                    sizy = 7;
                }

                while (scount > 0) {
                    final int j = EGS(sect, sx, sy, sz, atwith, -127, sizx, sizy, sa, vel, zvel, i, 4);
                    Sprite sp = boardService.getSprite(j);

                    if (sp != null) {
                        sp.setExtra(sp.getExtra() + (engine.krand() & 7));
                        if (atwith == COOLEXPLOSION1) {
                            sp.setShade(0);
                            if (s.getPicnum() == BOSS2) {
                                int l = sp.getXvel();
                                sp.setXvel(1024);
                                ssp(j, CLIPMASK0);
                                sp.setXvel(l);
                                sp.setAng(sp.getAng() + 128 - (engine.krand() & 255));
                            }
                        }

                        sp.setCstat(128);
                        sp.setClipdist(4);

                        sa = (short) (s.getAng() + 32 - (engine.krand() & 63));
                        zvel = oldzvel + 512 - (engine.krand() & 1023);
                    }

                    scount--;
                }

                return;
            }
            case FREEZEBLAST:
                sz += (3 << 8);
            case RPG: {

                if (s.getExtra() >= 0) {
                    s.setShade(-96);
                }

                vel = 644;

                int j;
                if (p >= 0) {
                    j = aim(s, 48);
                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        int dal = ((sp.getXrepeat() * engine.getTile(sp.getPicnum()).getHeight()) << 1) + (8 << 8);
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            zvel = ((sp.getZ() - sz - dal) * vel) / ldist(psp, sp);
                        }

                        if (sp.getPicnum() != RECON) {
                            sa = EngineUtils.getAngle(sp.getX() - sx, sp.getY() - sy);
                        }
                    } else {
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) * 81;
                    }
                    if (atwith == RPG) {
                        spritesound(RPG_SHOOT, i);
                    }

                } else {
                    j = findplayer(s);
                    sa = EngineUtils.getAngle(ps[j].oposx - sx, ps[j].oposy - sy);
                    if (s.getPicnum() == BOSS3) {
                        int zoffs = 32 << 8;
                        if (currentGame.getCON().type == 20) // Twentieth Anniversary World Tour
                        {
                            zoffs = (int) ((s.getYrepeat() / 80.0f) * zoffs);
                        }

                        sz -= zoffs;
                    } else if (s.getPicnum() == BOSS2) {
                        vel += 128;
                        int zoffs = 24 << 8;
                        if (currentGame.getCON().type == 20) // Twentieth Anniversary World Tour
                        {
                            zoffs = (int) ((s.getYrepeat() / 80.0f) * zoffs);
                        }

                        sz += zoffs;
                    }

                    Sprite psp = boardService.getSprite(ps[j].i);
                    if (psp != null) {
                        int l = ldist(psp, s);
                        zvel = ((ps[j].oposz - sz) * vel) / l;
                    }

                    if (badguy(s) && (s.getHitag() & face_player_smart) != 0) {
                        sa = (s.getAng() + (engine.krand() & 31) - 16);
                    }
                }

                int l = -1;
                if (p >= 0 && j >= 0) {
                    l = j;
                }

                j = EGS(sect, sx + (EngineUtils.cos(sa + 348) / 448), sy + (EngineUtils.sin(sa + 348) / 448),
                        sz - (1 << 8), atwith, 0, 14, 14, sa, vel, zvel, i, (short) 4);

                Sprite sp = boardService.getSprite(j);
                if (sp != null) {
                    sp.setExtra(sp.getExtra() + (engine.krand() & 7));
                    if (atwith != FREEZEBLAST) {
                        sp.setYvel(l);
                    } else {
                        sp.setYvel((short) currentGame.getCON().numfreezebounces);
                        sp.setXrepeat(sp.getXrepeat() >> 1);
                        sp.setYrepeat(sp.getYrepeat() >> 1);
                        sp.setZvel(sp.getZvel() - (2 << 4));
                    }

                    if (p == -1) {
                        if (s.getPicnum() == BOSS3) {
                            int xoffs = EngineUtils.sin(sa & 2047) >> 6;
                            int yoffs = EngineUtils.sin((sa + 1024 + 512) & 2047) >> 6;
                            int aoffs = 4;
                            if ((engine.krand() & 1) != 0) {
                                xoffs = -xoffs;
                                yoffs = -yoffs;
                                aoffs = -8;
                            }

                            if (currentGame.getCON().type == 20) { // Twentieth Anniversary World Tour
                                float siz = s.getYrepeat() / 80.0f;
                                xoffs *= (int) siz;
                                yoffs *= (int) siz;
                                aoffs *= (int) siz;
                            }

                            sp.setX(sp.getX() + xoffs);
                            sp.setY(sp.getY() + yoffs);
                            sp.setAng(sp.getAng() + aoffs);

                            sp.setXrepeat(42);
                            sp.setYrepeat(42);
                        } else if (s.getPicnum() == BOSS2) {
                            int xoffs = EngineUtils.sin(sa & 2047) / 56;
                            int yoffs = EngineUtils.sin((sa + 1024 + 512) & 2047) / 56;
                            int aoffs = 8 + (engine.krand() & 255) - 128;

                            if (currentGame.getCON().type == 20) { // Twentieth Anniversary World Tour
                                float siz = s.getYrepeat() / 80.0f;
                                xoffs *= (int) siz;
                                yoffs *= (int) siz;
                                aoffs *= (int) siz;
                            }

                            sp.setX(sp.getX() - xoffs);
                            sp.setY(sp.getY() - yoffs);
                            sp.setAng(sp.getAng() - aoffs);

                            sp.setXrepeat(24);
                            sp.setYrepeat(24);
                        } else if (atwith != FREEZEBLAST) {
                            sp.setXrepeat(30);
                            sp.setYrepeat(30);
                            sp.setExtra(sp.getExtra() >> 2);
                        }
                    } else if (ps[p].curr_weapon == DEVISTATOR_WEAPON) {
                        sp.setExtra(sp.getExtra() >> 2);
                        sp.setAng(sp.getAng() + 16 - (engine.krand() & 31));
                        sp.setZvel(sp.getZvel() + 256 - (engine.krand() & 511));

                        if ((ps[p].hbomb_hold_delay) != 0) {
                            sp.setX(sp.getX() - EngineUtils.sin(sa & 2047) / 644);
                            sp.setY(sp.getY() - EngineUtils.sin((sa + 1024 + 512) & 2047) / 644);
                        } else {
                            sp.setX(sp.getX() + (EngineUtils.sin(sa & 2047) >> 8));
                            sp.setY(sp.getY() + (EngineUtils.sin((sa + 1024 + 512) & 2047) >> 8));
                        }
                        sp.setXrepeat(sp.getXrepeat() >> 1);
                        sp.setYrepeat(sp.getYrepeat() >> 1);
                    }

                    sp.setCstat(128);
                    if (atwith == RPG) {
                        sp.setClipdist(4);
                    } else {
                        sp.setClipdist(40);
                    }
                }

                break;
            }
            case HANDHOLDINGLASER: {
                int pz = sz;
                if (p >= 0) {
                    zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) * 32;
                    pz -= ps[p].pyoff;
                }

                engine.hitscan(sx, sy, pz, sect, EngineUtils.sin((sa + 512) & 2047), EngineUtils.sin(sa & 2047), zvel << 6,
                        pHitInfo, CLIPMASK1);

                final int hitsect = pHitInfo.hitsect;
                final int hitsprite = pHitInfo.hitsprite;
                final int hitwall = pHitInfo.hitwall;
                final int hitx = pHitInfo.hitx;
                final int hity = pHitInfo.hity;
                final int hitz = pHitInfo.hitz;

                int j = 0;
                if (hitsprite != -1) {
                    break;
                }

                Wall hwal = boardService.getWall(hitwall);
                if (hwal != null && hitsect != -1) {
                    if (((hitx - sx) * (hitx - sx) + (hity - sy) * (hity - sy)) < (290 * 290)) {
                        Sector nsec = boardService.getSector(hwal.getNextsector());
                        Sector hitsec = boardService.getSector(hitsect);
                        if (hitsec != null) {
                            if (nsec != null) {
                                if (nsec.getLotag() <= 2 && hitsec.getLotag() <= 2) {
                                    j = 1;
                                }
                            } else if (hitsec.getLotag() <= 2) {
                                j = 1;
                            }
                        }
                    }
                }

                if (j == 1) {
                    int k = EGS(hitsect, hitx, hity, hitz, TRIPBOMB, -16, 4, 5, sa, 0, 0, i, (short) 6);
                    Sprite sp = boardService.getSprite(k);
                    if (sp != null) {
                        sp.setHitag(k);
                        spritesound(LASERTRIP_ONWALL, k);
                        sp.setXvel(-20);
                        ssp(k, CLIPMASK0);
                        sp.setCstat(16);
                        int ang = EngineUtils.getAngle(hwal.getX() - hwal.getWall2().getX(), hwal.getY() - hwal.getWall2().getY()) - 512;
                        sp.setAng(ang);
                        hittype[k].temp_data[5] = ang;

                        if (p >= 0) {
                            ps[p].ammo_amount[TRIPBOMB_WEAPON]--;
                        }
                    }
                }
                return;
            }
            case BOUNCEMINE:
            case MORTER: {

                if (s.getExtra() >= 0) {
                    s.setShade(-96);
                }

                int j = ps[findplayer(s)].i;
                Sprite psp = boardService.getSprite(j);
                if (psp == null) {
                    break;
                }

                int x = ldist(psp, s);

                zvel = -x >> 1;

                if (zvel < -4096) {
                    zvel = -2048;
                }
                vel = x >> 4;

                EGS(sect, sx + (EngineUtils.cos(sa + 512) >> 8), sy + (EngineUtils.sin(sa + 512) >> 8),
                        sz + (6 << 8), atwith, -64, 32, 32, sa, vel, zvel, i, (short) 1);
                break;
            }
            case GROWSPARK: {

                if (p >= 0) {
                    int j = aim(s, AUTO_AIM_ANGLE);
                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        int dal = ((sp.getXrepeat() * engine.getTile(sp.getPicnum()).getHeight()) << 1) + (5 << 8);
                        switch (sp.getPicnum()) {
                            case GREENSLIME:
                            case GREENSLIME + 1:
                            case GREENSLIME + 2:
                            case GREENSLIME + 3:
                            case GREENSLIME + 4:
                            case GREENSLIME + 5:
                            case GREENSLIME + 6:
                            case GREENSLIME + 7:
                            case ROTATEGUN:
                                dal -= (8 << 8);
                                break;
                        }
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            zvel = ((sp.getZ() - sz - dal) << 8) / (ldist(psp, sp));
                        }

                        sa = EngineUtils.getAngle(sp.getX() - sx, sp.getY() - sy);
                    } else {
                        sa += 16 - (engine.krand() & 31);
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) << 5;
                        zvel += 128 - (engine.krand() & 255);
                    }

                    sz -= (2 << 8);
                } else {
                    int j = findplayer(s);
                    sz -= (4 << 8);
                    Sprite psp = boardService.getSprite(ps[j].i);
                    if (psp != null) {
                        zvel = ((ps[j].posz - sz) << 8) / (ldist(psp, s));
                    }
                    zvel += 128 - (engine.krand() & 255);
                    sa += 32 - (engine.krand() & 63);
                }

                s.setCstat(s.getCstat() & ~257);
                engine.hitscan(sx, sy, sz, sect, EngineUtils.sin((sa + 512) & 2047), EngineUtils.sin(sa & 2047), zvel << 6, pHitInfo,
                        CLIPMASK1);

                final int hitsect = pHitInfo.hitsect;
                final int hitsprite = pHitInfo.hitsprite;
                final int hitwall = pHitInfo.hitwall;
                final int hitx = pHitInfo.hitx;
                final int hity = pHitInfo.hity;
                final int hitz = pHitInfo.hitz;

                s.setCstat(s.getCstat() | 257);

                int j = EGS(sect, hitx, hity, hitz, GROWSPARK, -16, 28, 28, sa, 0, 0, i, (short) 1);
                Sprite sp = boardService.getSprite(j);
                if (sp != null) {
                    sp.setPal(2);
                    sp.setCstat(sp.getCstat() | 130);
                    sp.setXrepeat(1);
                    sp.setYrepeat(1);
                }

                Sector hitsec = boardService.getSector(hitsect);
                if (hitwall == -1 && hitsprite == -1 && hitsec != null) {
                    if (zvel < 0 && (hitsec.getCeilingstat() & 1) == 0) {
                        checkhitceiling(hitsect);
                    }
                } else if (hitsprite != -1) {
                    checkhitsprite(hitsprite, j);
                } else {
                    Wall hitwal = boardService.getWall(hitwall);
                    if (hitwal != null && hitwal.getPicnum() != ACCESSSWITCH && hitwal.getPicnum() != ACCESSSWITCH2) {
                        checkhitwall(j, hitwall, hitx, hity, hitz, atwith);
                    }
                }

                break;
            }
            case SHRINKER: {
                if (s.getExtra() >= 0) {
                    s.setShade(-96);
                }

                if (p >= 0) {
                    int j = aim(s, AUTO_AIM_ANGLE);
                    Sprite sp = boardService.getSprite(j);
                    if (sp != null) {
                        int dal = ((sp.getXrepeat() * engine.getTile(sp.getPicnum()).getHeight()) << 1);
                        Sprite psp = boardService.getSprite(ps[p].i);
                        if (psp != null) {
                            zvel = ((sp.getZ() - sz - dal - (4 << 8)) * 768) / (ldist(psp, sp));
                        }
                        sa = EngineUtils.getAngle(sp.getX() - sx, sp.getY() - sy);
                    } else {
                        zvel = (int) (100 - ps[p].horiz - ps[p].horizoff) * 98;
                    }
                } else if (s.getStatnum() != 3) {
                    int j = (short) findplayer(s);
                    Sprite psp = boardService.getSprite(ps[j].i);
                    if (psp != null) {
                        int l = ldist(psp, s);
                        zvel = ((ps[j].oposz - sz) * 512) / l;
                    }
                }

                int j = EGS(sect, sx + (EngineUtils.cos(sa + 512) >> 12), sy + (EngineUtils.sin(sa + 512) >> 12),
                        sz + (2 << 8), SHRINKSPARK, -16, 28, 28, sa, 768, zvel, i, (short) 4);
                Sprite sp = boardService.getSprite(j);
                if (sp != null) {
                    sp.setCstat(128);
                    sp.setClipdist(32);
                }
            }
        }
    }

    public static void checkweapons(PlayerStruct p) {
        short cw = p.curr_weapon;
        if (cw < 1 || cw >= MAX_WEAPONS) {
            return;
        }

        if ((engine.krand() & 1) != 0) {
            spawn(p.i, weapon_sprites[cw]);
        } else {
            switch (cw) {
                case RPG_WEAPON:
                case HANDBOMB_WEAPON:
                    spawn(p.i, EXPLOSION2);
                    break;
            }
        }
    }

    public static void addammo(int weapon, PlayerStruct p, int amount) {
        p.ammo_amount[weapon] += amount;

        if (p.ammo_amount[weapon] > currentGame.getCON().max_ammo_amount[weapon]) {
            p.ammo_amount[weapon] = currentGame.getCON().max_ammo_amount[weapon];
        }
    }

    public static void addweapon(PlayerStruct p, int weapon) {
        if (!p.gotweapon[weapon]) {
            p.gotweapon[weapon] = true;
            if (currentGame.getCON().PLUTOPAK && weapon == SHRINKER_WEAPON) {
                p.gotweapon[GROW_WEAPON] = true;
            }
        }

        p.random_club_frame = 0;

        if (p.holster_weapon == 0) {
            p.weapon_pos = -1;
            p.last_weapon = p.curr_weapon;
        } else {
            p.weapon_pos = 10;
            p.holster_weapon = 0;
            p.last_weapon = -1;
        }

        p.kickback_pic = 0;
        if (p.last_used_weapon != p.curr_weapon) {
            p.last_used_weapon = (byte) p.curr_weapon;
        }
        p.curr_weapon = (short) weapon;

        switch (weapon) {
            case KNEE_WEAPON:
            case TRIPBOMB_WEAPON:
            case HANDREMOTE_WEAPON:
            case HANDBOMB_WEAPON:
                break;
            case SHOTGUN_WEAPON:
                spritesound(SHOTGUN_COCK, p.i);
                break;
            case PISTOL_WEAPON:
                spritesound(INSERT_CLIP, p.i);
                break;
            default:
                spritesound(SELECT_WEAPON, p.i);
                break;
        }
    }

    public static void weaponprocess(int snum) {
        PlayerStruct p = ps[snum];
        final int pi = p.i;
        Sprite s = boardService.getSprite(pi);
        if (s == null) {
            return;
        }

        boolean shrunk = (s.getYrepeat() < 8);
        int sb_snum = 0;
        if (p.cheat_phase <= 0) {
            sb_snum = sync[snum].bits;
        }

        if (p.curr_weapon == SHRINKER_WEAPON || p.curr_weapon == GROW_WEAPON) {
            p.random_club_frame += 64; // Glowing
        }

        if (p.rapid_fire_hold == 1) {
            if ((sb_snum & (1 << 2)) != 0) {
                return;
            }
            p.rapid_fire_hold = 0;
        }

        if (!shrunk && p.tipincs == 0 && p.access_incs == 0) {
            if ((sb_snum & (1 << 2)) != 0 && p.kickback_pic == 0 && p.fist_incs == 0 && p.last_weapon == -1 && (p.weapon_pos == 0 || p.holster_weapon == 1)) {
                p.crack_time = 777;

                if (p.holster_weapon == 1) {
                    if (p.last_pissed_time <= (26 * 218) && p.weapon_pos == -9) {
                        p.holster_weapon = 0;
                        p.weapon_pos = 10;
                        FTA(74, p);
                    }
                } else {
                    switch (p.curr_weapon) {
                        case HANDBOMB_WEAPON:
                            p.hbomb_hold_delay = 0;
                            if (p.ammo_amount[HANDBOMB_WEAPON] > 0) {
                                (p.kickback_pic) = 1;
                            }
                            break;
                        case HANDREMOTE_WEAPON:
                            p.hbomb_hold_delay = 0;
                            (p.kickback_pic) = 1;
                            break;

                        case PISTOL_WEAPON:
                            if (p.ammo_amount[PISTOL_WEAPON] > 0) {
                                p.ammo_amount[PISTOL_WEAPON]--;
                                (p.kickback_pic) = 1;
                            }
                            break;

                        case CHAINGUN_WEAPON:
                            if (p.ammo_amount[CHAINGUN_WEAPON] > 0) // && p.random_club_frame == 0)
                            {
                                (p.kickback_pic) = 1;
                            }
                            break;

                        case SHOTGUN_WEAPON:
                            if (p.ammo_amount[SHOTGUN_WEAPON] > 0 && p.random_club_frame == 0) {
                                (p.kickback_pic) = 1;
                            }
                            break;

                        case TRIPBOMB_WEAPON:
                            if (p.ammo_amount[TRIPBOMB_WEAPON] > 0) {
                                int sx, sy, sz;
                                int sect, hw, hitsp;

                                if (IsOriginalDemo()) {
                                    engine.hitscan(p.posx, p.posy, p.posz, p.cursectnum, EngineUtils.sin(((int) p.ang + 512) & 2047),
                                            EngineUtils.sin((int) p.ang & 2047), (100 - (int) p.horiz - p.horizoff) * 32, pHitInfo,
                                            CLIPMASK1);
                                } else {
                                    engine.hitscan(p.posx, p.posy, p.posz, p.cursectnum, (int) BCosAngle(BClampAngle(p.ang)),
                                            (int) BSinAngle(BClampAngle(p.ang)), (int) (100 - p.horiz - p.horizoff) * 32,
                                            pHitInfo, CLIPMASK1);
                                }

                                sect = pHitInfo.hitsect;
                                hw = pHitInfo.hitwall;
                                hitsp = pHitInfo.hitsprite;
                                sx = pHitInfo.hitx;
                                sy = pHitInfo.hity;
                                sz = pHitInfo.hitz;

                                Sector hitsec = boardService.getSector(sect);
                                if (hitsec == null || hitsp >= 0) {
                                    break;
                                }

                                Wall hitwal = boardService.getWall(hw);
                                if (hitwal != null && hitsec.getLotag() > 2) {
                                    break;
                                }

                                if (hitwal != null && hitwal.getOverpicnum() >= 0) {
                                    if (hitwal.getOverpicnum() == BIGFORCE) {
                                        break;
                                    }
                                }

                                ListNode<Sprite> node = boardService.getSectNode(sect);
                                for (; node != null; node = node.getNext()) {
                                    Sprite sp = node.get();
                                    if (sp.getPicnum() == TRIPBOMB && klabs(sp.getZ() - sz) < (12 << 8)
                                            && ((sp.getX() - sx) * (sp.getX() - sx)
                                            + (sp.getY() - sy) * (sp.getY() - sy)) < (290 * 290)) {
                                        break;
                                    }
                                }

                                if (node == null && hitwal != null && (hitwal.getCstat() & 16) == 0) {
                                    Sector nextsec = boardService.getSector(hitwal.getNextsector());
                                    if ((nextsec != null && nextsec.getLotag() <= 2)
                                            || (hitwal.getNextsector() == -1 && hitsec.getLotag() <= 2)) {
                                        if (((sx - p.posx) * (sx - p.posx) + (sy - p.posy) * (sy - p.posy)) < (290 * 290)) {
                                            p.posz = p.oposz;
                                            p.poszv = 0;
                                            (p.kickback_pic) = 1;
                                        }
                                    }
                                }
                            }
                            break;

                        case SHRINKER_WEAPON:
                        case GROW_WEAPON:
                            if (currentGame.getCON().PLUTOPAK && p.curr_weapon == GROW_WEAPON) {
                                if (p.ammo_amount[GROW_WEAPON] > 0) {
                                    (p.kickback_pic) = 1;
                                    spritesound(EXPANDERSHOOT, pi);
                                }
                            } else if (p.ammo_amount[SHRINKER_WEAPON] > 0) {
                                (p.kickback_pic) = 1;
                                spritesound(SHRINKER_FIRE, pi);
                            }
                            break;

                        case FREEZE_WEAPON:
                            if (p.ammo_amount[FREEZE_WEAPON] > 0) {
                                (p.kickback_pic) = 1;
                                spritesound(CAT_FIRE, pi);
                            }
                            break;
                        case DEVISTATOR_WEAPON:
                            if (p.ammo_amount[DEVISTATOR_WEAPON] > 0) {
                                (p.kickback_pic) = 1;
                                p.hbomb_hold_delay ^= 1;
                                spritesound(CAT_FIRE, pi);
                            }
                            break;

                        case RPG_WEAPON:
                            if (p.ammo_amount[RPG_WEAPON] > 0) {
                                (p.kickback_pic) = 1;
                            }
                            break;

                        case FLAMETHROWER_WEAPON: { // Twentieth Anniversary World Tour
                            if (p.ammo_amount[FLAMETHROWER_WEAPON] > 0) {
                                p.kickback_pic = 1;
                                Sector psec = boardService.getSector(p.cursectnum);
                                if (psec != null && psec.getLotag() != 2) {
                                    spritesound(FLAMETHROWER_INTRO, pi);
                                }
                            }
                            break;
                        }
                        case KNEE_WEAPON:
                            if (p.quick_kick == 0) {
                                (p.kickback_pic) = 1;
                            }
                            break;
                    }
                }
            } else if (p.kickback_pic != 0) {
                switch (p.curr_weapon) {
                    case HANDBOMB_WEAPON:

                        if ((p.kickback_pic) == 6 && (sb_snum & (1 << 2)) != 0) {
                            p.rapid_fire_hold = 1;
                            break;
                        }
                        (p.kickback_pic)++;
                        if ((p.kickback_pic) == 12) {
                            p.ammo_amount[HANDBOMB_WEAPON]--;

                            int k, i, j;
                            if (p.on_ground && (sb_snum & 2) != 0) {
                                k = 15;
                                i = (int) ((p.horiz + p.horizoff - 100) * 20);
                            } else {
                                k = 140;
                                i = -512 - ((int) (p.horiz + p.horizoff - 100) * 20);
                            }

                            if (IsOriginalDemo()) {
                                j = EGS(p.cursectnum, p.posx + (EngineUtils.sin(((int) p.ang + 512) & 2047) >> 6),
                                        p.posy + (EngineUtils.sin((int) p.ang & 2047) >> 6), p.posz, HEAVYHBOMB, -16, 9, 9,
                                        (short) p.ang, (k + (p.hbomb_hold_delay << 5)), i, pi, (short) 1);
                            } else {
                                j = EGS(p.cursectnum, (int) (p.posx + (BCosAngle(BClampAngle(p.ang)) / 64.0f)),
                                        (int) (p.posy + (BSinAngle(BClampAngle(p.ang)) / 64.0f)), p.posz, HEAVYHBOMB, -16, 9, 9,
                                        (short) p.ang, (k + (p.hbomb_hold_delay << 5)), i, pi, (short) 1);
                            }

                            Sprite sp = boardService.getSprite(j);

                            if (sp != null && k == 15) {
                                sp.setYvel(3);
                                sp.setZ(sp.getZ() + (8 << 8));
                            }

                            k = hits(pi);
                            if (sp != null && k < 512) {
                                sp.setAng(sp.getAng() + 1024);
                                sp.setZvel(sp.getZvel() / 3);
                                sp.setXvel(sp.getXvel() / 3);
                            }

                            p.hbomb_on = 1;

                        } else if ((p.kickback_pic) < 12 && (sb_snum & (1 << 2)) != 0) {
                            p.hbomb_hold_delay++;
                        } else if ((p.kickback_pic) > 19) {
                            (p.kickback_pic) = 0;
                            if (game.isGameType(GameType.NAM)) {
                                checkavailweapon(p);
                            } else {
                                p.curr_weapon = HANDREMOTE_WEAPON;
                                p.last_weapon = -1;
                                p.weapon_pos = 10;
                            }
                        }

                        break;

                    case HANDREMOTE_WEAPON:
                        (p.kickback_pic)++;

                        if ((p.kickback_pic) == 2) {
                            p.hbomb_on = 0;
                        }

                        if ((p.kickback_pic) == 10) {
                            (p.kickback_pic) = 0;

                            if (game.isGameType(GameType.DUKE_3D) && p.ammo_amount[HANDBOMB_WEAPON] > 0) {
                                addweapon(p, HANDBOMB_WEAPON);
                            } else if (game.isGameType(GameType.NAM) && p.ammo_amount[TRIPBOMB_WEAPON] > 0) {
                                addweapon(p, TRIPBOMB_WEAPON);
                            } else {
                                checkavailweapon(p);
                            }
                        }
                        break;

                    case PISTOL_WEAPON:
                        if ((p.kickback_pic) == 1) {
                            shoot(pi, SHOTSPARK1);
                            spritesound(PISTOL_FIRE, pi);

                            lastvisinc = engine.getTotalClock() + 32;
                            if (snum == screenpeek) {
                                gVisibility = 0;
                            }
                        } else if ((p.kickback_pic) == 2) {
                            spawn(pi, SHELL);
                        }

                        (p.kickback_pic)++;

                        if ((p.kickback_pic) >= 5) {
                            int PISTOLAMMOAMOUNT = 12;
                            if (game.isGameType(GameType.NAM)) {
                                PISTOLAMMOAMOUNT = 20;
                            }

                            if (p.ammo_amount[PISTOL_WEAPON] <= 0 || (p.ammo_amount[PISTOL_WEAPON] % PISTOLAMMOAMOUNT) != 0) {
                                (p.kickback_pic) = 0;
                                checkavailweapon(p);
                            } else {
                                switch ((p.kickback_pic)) {
                                    case 5:
                                        spritesound(EJECT_CLIP, pi);
                                        break;
                                    case 8:
                                        spritesound(INSERT_CLIP, pi);
                                        break;
                                }
                            }
                        }

                        if ((p.kickback_pic) == 27) {
                            (p.kickback_pic) = 0;
                            checkavailweapon(p);
                        }

                        break;

                    case SHOTGUN_WEAPON:

                        (p.kickback_pic)++;

                        if (p.kickback_pic == 4) {
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);
                            shoot(pi, SHOTGUN);

                            p.ammo_amount[SHOTGUN_WEAPON]--;

                            spritesound(SHOTGUN_FIRE, pi);

                            lastvisinc = engine.getTotalClock() + 32;

                            if (snum == screenpeek) {
                                gVisibility = 0;
                            }
                        }

                        switch (p.kickback_pic) {
                            case 13:
                                checkavailweapon(p);
                                break;
                            case 15:
                                spritesound(SHOTGUN_COCK, pi);
                                break;
                            case 17:
                            case 20:
                                p.kickback_pic++;
                                break;
                            case 24: {
                                int j = spawn(pi, SHOTGUNSHELL);
                                Sprite sp = boardService.getSprite(j);
                                if (sp != null) {
                                    sp.setAng(sp.getAng() + 1024);
                                    ssp(j, CLIPMASK0);
                                    sp.setAng(sp.getAng() + 1024);
                                    p.kickback_pic++;
                                }

                                break;
                            }
                            case 31:
                                p.kickback_pic = 0;
                                return;
                        }
                        break;

                    case CHAINGUN_WEAPON:

                        (p.kickback_pic)++;

                        if (p.kickback_pic <= 12) {
                            if (((p.kickback_pic) % 3) == 0) {
                                p.ammo_amount[CHAINGUN_WEAPON]--;

                                if ((p.kickback_pic % 3) == 0) {
                                    int j = spawn(pi, SHELL);
                                    Sprite sp = boardService.getSprite(j);
                                    if (sp != null) {
                                        sp.setAng((sp.getAng() + 1024) & 2047);
                                        sp.setXvel(sp.getXvel() + 32);
                                        sp.setZ(sp.getZ() + (3 << 8));
                                        ssp(j, CLIPMASK0);
                                    }
                                }

                                spritesound(CHAINGUN_FIRE, pi);
                                shoot(pi, CHAINGUN);
                                lastvisinc = engine.getTotalClock() + 32;
                                if (snum == screenpeek) {
                                    gVisibility = 0;
                                }
                                checkavailweapon(p);

                                if ((sb_snum & (1 << 2)) == 0) {
                                    p.kickback_pic = 0;
                                    break;
                                }
                            }
                        } else {
                            if ((sb_snum & (1 << 2)) != 0) {
                                p.kickback_pic = 1;
                            } else {
                                p.kickback_pic = 0;
                            }
                        }

                        break;

                    case SHRINKER_WEAPON:
                    case GROW_WEAPON:

                        if (currentGame.getCON().PLUTOPAK && p.curr_weapon == GROW_WEAPON) {
                            if ((p.kickback_pic) > 3) {
                                p.kickback_pic = 0;
                                p.ammo_amount[GROW_WEAPON]--;
                                shoot(pi, GROWSPARK);

                                if (snum == screenpeek) {
                                    gVisibility = 0;
                                }
                                lastvisinc = engine.getTotalClock() + 32;
                                checkavailweapon(p);
                            } else {
                                (p.kickback_pic)++;
                            }
                        } else {
                            if ((p.kickback_pic) > 10) {
                                (p.kickback_pic) = 0;

                                p.ammo_amount[SHRINKER_WEAPON]--;
                                shoot(pi, SHRINKER);

                                if (snum == screenpeek) {
                                    gVisibility = 0;
                                }
                                lastvisinc = engine.getTotalClock() + 32;
                                checkavailweapon(p);
                            } else {
                                (p.kickback_pic)++;
                            }
                        }
                        break;

                    case DEVISTATOR_WEAPON:
                        (p.kickback_pic)++;

                        if ((p.kickback_pic & 1) != 0) {
                            if (snum == screenpeek) {
                                gVisibility = 0;
                            }
                            lastvisinc = engine.getTotalClock() + 32;
                            shoot(pi, RPG);
                            p.ammo_amount[DEVISTATOR_WEAPON]--;
                            checkavailweapon(p);
                        }
                        if ((p.kickback_pic) > 5) {
                            (p.kickback_pic) = 0;
                        }
                        break;
                    case FREEZE_WEAPON:

                        if ((p.kickback_pic) < 4) {
                            (p.kickback_pic)++;
                            if ((p.kickback_pic) == 3) {
                                p.ammo_amount[FREEZE_WEAPON]--;
                                if (snum == screenpeek) {
                                    gVisibility = 0;
                                }
                                lastvisinc = engine.getTotalClock() + 32;
                                shoot(pi, FREEZEBLAST);
                                checkavailweapon(p);
                            }
                            if (s.getXrepeat() < 32) {
                                p.kickback_pic = 0;
                                break;
                            }
                        } else {
                            if ((sb_snum & (1 << 2)) != 0) {
                                p.kickback_pic = 1;
                                spritesound(CAT_FIRE, pi);
                            } else {
                                p.kickback_pic = 0;
                            }
                        }
                        break;
                    case FLAMETHROWER_WEAPON:
                        if (currentGame.getCON().type != 20) // Twentieth Anniversary World Tour
                        {
                            break;
                        }
                        (p.kickback_pic)++;
                        if ((p.kickback_pic) == 2) {
                            Sector psec = boardService.getSector(p.cursectnum);
                            if (psec != null && psec.getLotag() != 2) {
                                p.ammo_amount[FLAMETHROWER_WEAPON]--;
                                if (snum == screenpeek) {
                                    gVisibility = 0;
                                }
                                shoot(pi, FIREBALL);
                            }
                            checkavailweapon(p);
                        } else if ((p.kickback_pic) == 16) {
                            if ((sb_snum & (1 << 2)) != 0) {
                                p.kickback_pic = 1;
                                spritesound(FLAMETHROWER_INTRO, pi);
                            } else {
                                p.kickback_pic = 0;
                            }
                        }
                        break;

                    case TRIPBOMB_WEAPON:
                        if (p.kickback_pic < 4) {
                            p.posz = p.oposz;
                            p.poszv = 0;
                            if ((p.kickback_pic) == 3) {
                                shoot(pi, HANDHOLDINGLASER);
                            }
                        }
                        if ((p.kickback_pic) == 16) {
                            (p.kickback_pic) = 0;
                            checkavailweapon(p);
                            p.weapon_pos = -9;
                        } else {
                            (p.kickback_pic)++;
                        }
                        break;
                    case KNEE_WEAPON:
                        (p.kickback_pic)++;

                        if ((p.kickback_pic) == 7) {
                            shoot(pi, KNEE);
                        } else if ((p.kickback_pic) == 14) {
                            if ((sb_snum & (1 << 2)) != 0) {
                                p.kickback_pic = (short) (1 + (engine.krand() & 3));
                            } else {
                                p.kickback_pic = 0;
                            }
                        }

                        if (p.wantweaponfire >= 0) {
                            checkavailweapon(p);
                        }
                        break;

                    case RPG_WEAPON:
                        (p.kickback_pic)++;
                        if ((p.kickback_pic) == 4) {
                            p.ammo_amount[RPG_WEAPON]--;
                            lastvisinc = engine.getTotalClock() + 32;
                            if (snum == screenpeek) {
                                gVisibility = 0;
                            }
                            shoot(pi, RPG);
                            checkavailweapon(p);
                        } else if (p.kickback_pic == 20) {
                            p.kickback_pic = 0;
                        }
                        break;
                }
            }
        }
    }

    public static void moveweapons() {
        Renderer renderer = game.getRenderer();
        ListNode<Sprite> node = boardService.getStatNode(4);
        ListNode<Sprite> nexti;

        for (; node != null; node = nexti) {
            nexti = node.getNext();
            final Sprite s = node.get();
            final int i = node.getIndex();

            if (s.getSectnum() < 0) {
                engine.deletesprite(i);
                continue;
            }

            game.pInt.setsprinterpolate(i, s);

            switch (s.getPicnum()) {
                case RADIUSEXPLOSION:
                case KNEE:
                    engine.deletesprite(i);
                    continue;
                case TONGUE: {
                    hittype[i].temp_data[0] = EngineUtils.sin((hittype[i].temp_data[1]) & 2047) >> 9;
                    hittype[i].temp_data[1] += 32;
                    if (hittype[i].temp_data[1] > 2047) {
                        engine.deletesprite(i);
                        continue;
                    }

                    Sprite spo = boardService.getSprite(s.getOwner());
                    if (spo == null) {
                        continue;
                    }

                    if (spo.getStatnum() == MAXSTATUS) {
                        if (!badguy(spo)) {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    s.setAng(spo.getAng());
                    s.setX(spo.getX());
                    s.setY(spo.getY());
                    if (spo.getPicnum() == APLAYER) {
                        s.setZ(spo.getZ() - (34 << 8));
                    }
                    int k;
                    for (k = 0; k < hittype[i].temp_data[0]; k++) {
                        int q = EGS(s.getSectnum(), s.getX() + ((k * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 9),
                                s.getY() + ((k * EngineUtils.sin(s.getAng() & 2047)) >> 9),
                                s.getZ() + ((k * ksgn(s.getZvel())) * klabs(s.getZvel() / 12)), TONGUE, -40 + (k << 1), 8, 8, 0, 0,
                                0, i, 5);
                        Sprite sp = boardService.getSprite(q);
                        if (sp != null) {
                            sp.setCstat(128);
                            sp.setPal(8);
                        }
                    }
                    int q = EGS(s.getSectnum(), s.getX() + ((k * EngineUtils.sin((s.getAng() + 512) & 2047)) >> 9),
                            s.getY() + ((k * EngineUtils.sin(s.getAng() & 2047)) >> 9),
                            s.getZ() + ((k * ksgn(s.getZvel())) * klabs(s.getZvel() / 12)), INNERJAW, -40, 32, 32, 0, 0, 0, i,
                            5);
                    Sprite sp = boardService.getSprite(q);
                    if (sp != null) {
                        sp.setCstat(128);
                        if (hittype[i].temp_data[1] > 512 && hittype[i].temp_data[1] < (1024)) {
                            sp.setPicnum(INNERJAW + 1);
                        }
                    }
                    break;
                }
                case FREEZEBLAST:
                    if (s.getYvel() < 1 || s.getExtra() < 2 || (s.getXvel() | s.getZvel()) == 0) {
                        int j = spawn(i, TRANSPORTERSTAR);
                        Sprite sp = boardService.getSprite(j);
                        if (sp != null) {
                            sp.setPal(1);
                            sp.setXrepeat(32);
                            sp.setYrepeat(32);
                        }
                        engine.deletesprite(i);
                        continue;
                    }
                case SHRINKSPARK:
                case RPG:
                case FIRELASER:
                case SPIT:
                case COOLEXPLOSION1:
                case FIREBALL: {
                    // Twentieth Anniversary World Tour
                    if (s.getPicnum() == FIREBALL && currentGame.getCON().type != 20) {
                        break;
                    }

                    if (s.getPicnum() == COOLEXPLOSION1) {
                        if (Sound[WIERDSHOT_FLY].getSoundOwnerCount() == 0) {
                            spritesound(WIERDSHOT_FLY, i);
                        }
                    }

                    int p;
                    int xvel, yvel;

                    Sector sec = boardService.getSector(s.getSectnum());
                    if (sec != null && s.getPicnum() == RPG && sec.getLotag() == 2) {
                        xvel = s.getXvel() >> 1;
                        yvel = s.getZvel() >> 1;
                    } else {
                        xvel = s.getXvel();
                        yvel = s.getZvel();
                    }

                    int dax = s.getX();
                    int day = s.getY();
                    int daz = s.getZ();

                    getglobalz(i);
                    sec = boardService.getSector(s.getSectnum());

                    switch (s.getPicnum()) {
                        case RPG:
                            if (sec != null && hittype[i].picnum != BOSS2 && s.getXrepeat() >= 10 && sec.getLotag() != 2) {
                                int j = spawn(i, SMALLSMOKE);
                                Sprite sp = boardService.getSprite(j);
                                if (sp != null) {
                                    sp.setZ(sp.getZ() + (1 << 8));
                                }
                            }
                            break;
                        case FIREBALL: {
                            if (sec == null || sec.getLotag() == 2) {
                                engine.deletesprite(i);
                                continue;
                            }

                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (spo != null && s.getPicnum() == FIREBALL && spo.getPicnum() != FIREBALL) {
                                if (hittype[i].temp_data[0] >= 1 && hittype[i].temp_data[0] < 6) {
                                    float siz = 1.0f - (hittype[i].temp_data[0] * 0.2f);
                                    int trail = hittype[i].temp_data[1];
                                    int j = spawn(i, FIREBALL);

                                    Sprite spr = boardService.getSprite(j);
                                    if (spr != null) {
                                        hittype[i].temp_data[1] = j;
                                        spr.setXvel(s.getXvel());
                                        spr.setYvel(s.getYvel());
                                        spr.setZvel(s.getZvel());
                                        if (hittype[i].temp_data[0] > 1) {
                                            FireProj proj = fire[trail];
                                            if (proj != null) {
                                                spr.setX(proj.x);
                                                spr.setY(proj.y);
                                                spr.setZ(proj.z);
                                                spr.setXvel(proj.xv);
                                                spr.setYvel(proj.yv);
                                                spr.setZvel(proj.zv);
                                            }
                                        }
                                        int size = (int) (s.getXrepeat() * siz);
                                        spr.setYrepeat(size);
                                        spr.setXrepeat(size);
                                        spr.setCstat(s.getCstat());
                                        spr.setExtra(0);

                                        if (fire[j] == null) {
                                            fire[j] = new FireProj();
                                        }
                                        fire[j].set(spr);
                                        engine.changespritestat((short) j, (short) 4);
                                    }
                                }
                                hittype[i].temp_data[0]++;
                            }

                            if (s.getZvel() < 15000) {
                                s.setZvel(s.getZvel() + 200);
                            }
                            break;
                        }
                    }

                    int moveHit = movesprite(i, (xvel * (EngineUtils.sin((s.getAng() + 512) & 2047))) >> 14, (xvel * (EngineUtils.sin(s.getAng() & 2047))) >> 14,
                            yvel, CLIPMASK1);

                    Sprite ysp = boardService.getSprite(s.getYvel());
                    if (s.getPicnum() == RPG && ysp != null) {
                        if (FindDistance2D(s.getX() - ysp.getX(), s.getY() - ysp.getY()) < 256) {
                            moveHit = HIT_SPRITE + s.getYvel();
                        }
                    }

                    sec = boardService.getSector(s.getSectnum());
                    if (sec == null) {
                        engine.deletesprite(i);
                        continue;
                    }

                    if ((moveHit & kHitTypeMask) != kHitSprite) {
                        if (s.getPicnum() != FREEZEBLAST) {
                            if (s.getZ() < hittype[i].ceilingz) {
                                moveHit = kHitSector + (s.getSectnum());
                                s.setZvel(-1);
                            } else if ((s.getZ() > hittype[i].floorz && sec.getLotag() != 1)
                                    || (s.getZ() > hittype[i].floorz + (16 << 8) && sec.getLotag() == 1)) {
                                moveHit = kHitSector + (s.getSectnum());
                                if (sec.getLotag() != 1) {
                                    s.setZvel(1);
                                }
                            }
                        }
                    }

                    if (s.getPicnum() == FIRELASER) {
                        for (int k = -3; k < 2; k++) {
                            int x = EGS(s.getSectnum(), s.getX() + ((k * EngineUtils.cos((s.getAng()) & 2047)) >> 9),
                                    s.getY() + ((k * EngineUtils.sin(s.getAng() & 2047)) >> 9),
                                    s.getZ() + ((k * ksgn(s.getZvel())) * klabs(s.getZvel() / 24)), FIRELASER, -40 + (k << 2),
                                    s.getXrepeat(), s.getYrepeat(), 0, 0, 0, s.getOwner(), (short) 5);

                            Sprite sp = boardService.getSprite(x);
                            if (sp != null) {
                                sp.setCstat(128);
                                sp.setPal(s.getPal());
                            }
                        }
                    } else if (s.getPicnum() == SPIT) {
                        if (s.getZvel() < 6144) {
                            s.setZvel(s.getZvel() + currentGame.getCON().gc - 112);
                        }
                    }

                    if (moveHit != 0) {

                        if (s.getPicnum() == COOLEXPLOSION1) {
                            if ((moveHit & kHitTypeMask) == kHitSprite) {
                                Sprite hsp = boardService.getSprite(moveHit & kHitIndexMask);
                                if (hsp != null && hsp.getPicnum() != APLAYER) {
                                    continue;
                                }
                            }
                            s.setXvel(0);
                            s.setZvel(0);
                        }

                        if ((moveHit & kHitTypeMask) == kHitSprite) {
                            moveHit &= kHitIndexMask;
                            Sprite hsp = boardService.getSprite(moveHit);

                            if (s.getPicnum() == FREEZEBLAST && hsp != null && hsp.getPal() == 1) {
                                if (badguy(hsp) || hsp.getPicnum() == APLAYER) {
                                    int j = spawn(i, TRANSPORTERSTAR);
                                    Sprite sp = boardService.getSprite(j);
                                    if (sp != null) {
                                        sp.setPal(1);
                                        sp.setXrepeat(32);
                                        sp.setYrepeat(32);
                                    }

                                    engine.deletesprite(i);
                                    continue;
                                }
                            }

                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (s.getPicnum() != FIREBALL || s.getPicnum() == FIREBALL && spo != null && spo.getPicnum() != FIREBALL) {
                                checkhitsprite(moveHit, i);
                            }

                            if (hsp != null && hsp.getPicnum() == APLAYER) {
                                p = hsp.getYvel();
                                if (ud.multimode >= 2 && s.getPicnum() == FIREBALL && spo != null && spo.getPicnum() != FIREBALL
                                        && spo.getPicnum() == APLAYER) {
                                    ps[p].numloogs = -1 - s.getYvel();
//			                        sub_A2F7E0(p, v133, s, s.yvel);
                                }

                                spritesound(PISTOL_BODYHIT, moveHit);

                                if (s.getPicnum() == SPIT) {
                                    ps[p].horiz += 32;
                                    ps[p].return_to_center = 8;

                                    if (ps[p].loogcnt == 0) {
                                        if (Sound[DUKE_LONGTERM_PAIN].getSoundOwnerCount() < 1) {
                                            spritesound(DUKE_LONGTERM_PAIN, ps[p].i);
                                        }

                                        int j = 3 + (engine.krand() & 3);
                                        ps[p].numloogs = j;
                                        ps[p].loogcnt = 24 * 4;
                                        int xdim = renderer.getWidth();
                                        int ydim = renderer.getHeight();
                                        for (int x = 0; x < j; x++) {
                                            int lx = engine.krand() % xdim;
                                            int ly = engine.krand() % ydim;
                                            if (p == screenpeek) {
                                                loogiex[x] = lx;
                                                loogiey[x] = ly;
                                            }
                                        }
                                    }
                                }
                            }
                        } else if ((moveHit & kHitTypeMask) == kHitWall) {
                            moveHit &= kHitIndexMask;
                            Wall hwal = boardService.getWall(moveHit);

                            if (hwal != null) {
                                if (s.getPicnum() != FIREBALL && s.getPicnum() != RPG && s.getPicnum() != FREEZEBLAST && s.getPicnum() != SPIT
                                        && (hwal.getOverpicnum() == MIRROR || hwal.getPicnum() == MIRROR)) {
                                    int k = hwal.getWallAngle(); //EngineUtils.getAngle(boardService.getWall(hwal.getPoint2()).getX() - hwal.getX(), boardService.getWall(hwal.getPoint2()).getY() - hwal.getY());
                                    s.setAng((short) (((k << 1) - s.getAng()) & 2047));
                                    s.setOwner(i);
                                    spawn(i, TRANSPORTERSTAR);
                                    continue;
                                } else {
                                    engine.setsprite(i, dax, day, daz);
                                    checkhitwall(i, moveHit, s.getX(), s.getY(), s.getZ(), s.getPicnum());

                                    if (s.getPicnum() == FREEZEBLAST) {
                                        if (hwal.getOverpicnum() != MIRROR && hwal.getPicnum() != MIRROR) {
                                            s.setExtra(s.getExtra() >> 1);
                                            s.setYvel(s.getYvel() - 1);
                                        }

                                        int k = hwal.getWallAngle(); //EngineUtils.getAngle(boardService.getWall(hwal.getPoint2()).getX() - hwal.getX(), boardService.getWall(hwal.getPoint2()).getY() - hwal.getY());
                                        s.setAng((short) (((k << 1) - s.getAng()) & 2047));
                                        continue;
                                    }
                                }
                            }
                        } else if ((moveHit & kHitTypeMask) == kHitSector) {
                            engine.setsprite(i, dax, day, daz);

                            sec = boardService.getSector(s.getSectnum());
                            if (sec != null) {
                                if (s.getZvel() < 0) {
                                    if ((sec.getCeilingstat() & 1) != 0) {
                                        if (sec.getCeilingpal() == 0) {
                                            engine.deletesprite(i);
                                            continue;
                                        }
                                    }

                                    checkhitceiling(s.getSectnum());
                                } else if (s.getPicnum() == FIREBALL) {
                                    Sprite spo = boardService.getSprite(s.getOwner());
                                    if (spo != null && spo.getPicnum() != FIREBALL) {
                                        int j = spawn(i, LAVAPOOL);
                                        Sprite sp = boardService.getSprite(j);
                                        if (sp != null) {
                                            sp.setOwner(s.getOwner());
                                            sp.setYvel(s.getYvel());
                                            hittype[j].owner = s.getOwner();
                                        }
                                        engine.deletesprite(i);
                                        continue;
                                    }
                                }
                            }

                            if (s.getPicnum() == FREEZEBLAST) {
                                bounce(i);
                                ssp(i, CLIPMASK1);
                                s.setExtra(s.getExtra() >> 1);
                                if (s.getXrepeat() > 8) {
                                    s.setXrepeat(s.getXrepeat() - 2);
                                }
                                if (s.getYrepeat() > 8) {
                                    s.setYrepeat(s.getYrepeat() - 2);
                                }
                                s.setYvel(s.getYvel() - 1);
                                continue;
                            }
                        }

                        if (s.getPicnum() != SPIT) {
                            if (s.getPicnum() == RPG) {
                                int k = spawn(i, EXPLOSION2);
                                Sprite sp = boardService.getSprite(k);
                                if (sp != null) {
                                    sp.setX(dax);
                                    sp.setY(day);
                                    sp.setZ(daz);

                                    if (s.getXrepeat() < 10) {
                                        sp.setXrepeat(6);
                                        sp.setYrepeat(6);
                                    } else if ((moveHit & kHitTypeMask) == kHitSector) {
                                        if (s.getZvel() > 0) {
                                            spawn(i, EXPLOSION2BOT);
                                        } else {
                                            sp.setCstat(sp.getCstat() | 8);
                                            sp.setZ(sp.getZ() + (48 << 8));
                                        }
                                    }
                                }
                            } else if (s.getPicnum() == SHRINKSPARK) {
                                spawn(i, SHRINKEREXPLOSION);
                                spritesound(SHRINKER_HIT, i);
                                hitradius(i, currentGame.getCON().shrinkerblastradius, 0, 0, 0, 0);
                            } else if (s.getPicnum() != COOLEXPLOSION1 && s.getPicnum() != FREEZEBLAST && s.getPicnum() != FIRELASER
                                    && s.getPicnum() != FIREBALL) {
                                int k = spawn(i, EXPLOSION2);
                                Sprite sp = boardService.getSprite(k);
                                if (sp != null) {
                                    int size = (s.getXrepeat() >> 1);
                                    sp.setXrepeat(size);
                                    sp.setYrepeat(size);
                                    if ((moveHit & kHitTypeMask) == kHitSector) {
                                        if (s.getZvel() < 0) {
                                            sp.setCstat(sp.getCstat() | 8);
                                            sp.setZ(sp.getZ() + (72 << 8));
                                        }
                                    }
                                }
                            }

                            if (s.getPicnum() == RPG) {
                                spritesound(RPG_EXPLODE, i);

                                if (s.getXrepeat() >= 10) {
                                    int x = s.getExtra();
                                    hitradius(i, currentGame.getCON().rpgblastradius, x >> 2, x >> 1, x - (x >> 2), x);
                                } else {
                                    int x = s.getExtra() + (global_random & 3);
                                    hitradius(i, (currentGame.getCON().rpgblastradius >> 1), x >> 2, x >> 1, x - (x >> 2),
                                            x);
                                }
                            }

                            Sprite spo = boardService.getSprite(s.getOwner());
                            if (s.getPicnum() == FIREBALL && spo != null && spo.getPicnum() != FIREBALL) {
                                int j = spawn(i, EXPLOSION2);
                                int size = s.getXrepeat() >> 1;
                                Sprite sp = boardService.getSprite(j);
                                if (sp != null) {
                                    sp.setXrepeat(size);
                                    sp.setYrepeat(size);
                                }
                            }
                        }
                        if (s.getPicnum() != COOLEXPLOSION1) {
                            engine.deletesprite(i);
                            continue;
                        }
                    }

                    sec = boardService.getSector(s.getSectnum());
                    if (s.getPicnum() == COOLEXPLOSION1) {
                        s.setShade(s.getShade() + 1);
                        if (s.getShade() >= 40) {
                            engine.deletesprite(i);
                            continue;
                        }
                    } else if (sec != null && s.getPicnum() == RPG && sec.getLotag() == 2 && s.getXrepeat() >= 10 && rnd(140)) {
                        spawn(i, WATERBUBBLE);
                    }

                    continue;
                }
                case SHOTSPARK1:
                    int p = findplayer(s);
                    execute(currentGame.getCON(), i, p, player_dist);
                    break;
            }
        }
    }

    public static boolean animatefist(int gs, int snum) {
        Renderer renderer = game.getRenderer();

        int fisti = ps[snum].fist_incs;
        if (fisti > 32) {
            fisti = 32;
        }

        if (fisti <= 0) {
            return false;
        }

        int looking_arc = klabs(ps[snum].look_ang) / 9;
        int fistzoom = 65536 - (EngineUtils.sin((512 + (fisti << 6)) & 2047) << 2);
        if (fistzoom > 90612) {
            fistzoom = 90612;
        }

        if (fistzoom < 40920) {
            fistzoom = 40290;
        }

        int fistz = 194 + (EngineUtils.sin(((6 + fisti) << 7) & 2047) >> 9);
        int fistpal = 0;
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp != null && psp.getPal() == 1) {
            fistpal = 1;
        } else {
            Sector psec = boardService.getSector(ps[snum].cursectnum);
            if (psec != null) {
                fistpal = psec.getFloorpal();
            }
        }

        renderer.rotatesprite((-fisti + 222 + (int) (sync[snum].avel / 16f)) << 16, (looking_arc + fistz) << 16, fistzoom,
                0, FIST, gs, fistpal, 2 | 8);

        return true;
    }

    public static void animateknee(int gs, int snum) {
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp == null) {
            return;
        }

        if (ps[snum].knee_incs > 11 || ps[snum].knee_incs == 0 || psp.getExtra() <= 0) {
            return;
        }

        int looking_arc = knee_y[ps[snum].knee_incs] + klabs(ps[snum].look_ang) / 9;
        looking_arc -= (ps[snum].hard_landing << 3);

        int pal = 0;
        if (psp.getPal() == 1) {
            pal = 1;
        } else {
            Sector psec = boardService.getSector(ps[snum].cursectnum);
            if (psec != null) {
                pal = psec.getFloorpal();
            }

            if (pal == 0) {
                pal = ps[snum].palookup;
            }
        }

        myospal(105 + (int) (sync[snum].avel / 16f) - (ps[snum].look_ang >> 1) + (knee_y[ps[snum].knee_incs] >> 2),
                looking_arc + 280 - (((int) ps[snum].horiz - ps[snum].horizoff) >> 4), KNEE, gs, 4, pal);

    }

    public static boolean animateknuckles(int gs, int snum) {
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp == null || ps[snum].knuckle_incs == 0 || psp.getExtra() <= 0) {
            return false;
        }

        int looking_arc = (klabs(ps[snum].look_ang) / 9);
        looking_arc -= (ps[snum].hard_landing << 3);

        int pal = 0;
        if (psp.getPal() == 1) {
            pal = 1;
        } else {
            Sector psec = boardService.getSector(ps[snum].cursectnum);
            if (psec != null) {
                pal = psec.getFloorpal();
            }
        }

        myospal(160 + (int) (sync[snum].avel / 16f) - (ps[snum].look_ang >> 1),
                looking_arc + 180 - (((int) ps[snum].horiz - ps[snum].horizoff) >> 4),
                CRACKKNUCKLES + knuckle_frames[ps[snum].knuckle_incs >> 1], gs, 4, pal);

        return true;
    }

    public static void displayweapon(int snum) {
        int gun_pos, looking_arc, cw;
        int weapon_xoffset, i, j;

        PlayerStruct p = ps[snum];
        int kb = p.kickback_pic;
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        int o = 0;

        looking_arc = klabs(p.look_ang) / 9;

        int gs = psp.getShade();
        if (gs > 24) {
            gs = 24;
        }

        if (p.newowner >= 0 || ud.camerasprite >= 0 || over_shoulder_on > 0
                || (psp.getPal() != 1 && psp.getExtra() <= 0) || animatefist(gs, snum)
                || animateknuckles(gs, snum) || animatetip(gs, snum) || animateaccess(gs, snum)) {
            return;
        }

        animateknee(gs, snum);

        gun_pos = 80 - (p.weapon_pos * p.weapon_pos);

        weapon_xoffset = (160) - 90;
        weapon_xoffset -= (EngineUtils.sin(((p.weapon_sway >> 1) + 512) & 2047) / (1024 + 512));
        weapon_xoffset -= 58 + p.weapon_ang;
        if (psp.getXrepeat() < 32) {
            gun_pos -= klabs(EngineUtils.sin((p.weapon_sway << 2) & 2047) >> 9);
        } else {
            gun_pos -= klabs(EngineUtils.sin((p.weapon_sway >> 1) & 2047) >> 10);
        }

        gun_pos -= (p.hard_landing << 3);

        if (screensize > 2
                || (game.menu.gShowMenu && (game.menu.getCurrentMenu() instanceof InterfaceMenu) && ud.screen_size > 2)) {
            gun_pos += engine.getTile(BOTTOMSTATUSBAR).getHeight() / 2;
        }

        if (p.last_weapon >= 0) {
            cw = p.last_weapon;
        } else {
            cw = p.curr_weapon;
        }

        int pal = 0;
        j = 14 - p.quick_kick;
        if (j != 14) {
            if (psp.getPal() == 1) {
                pal = 1;
            } else {
                Sector psec = boardService.getSector(p.cursectnum);
                if (psec != null) {
                    pal = psec.getFloorpal();
                    if (pal == 0) {
                        pal = p.palookup;
                    }
                }
            }

            if (j < 5 || j > 9) {
                myospal(weapon_xoffset + 80 - (p.look_ang >> 1), looking_arc + 250 - gun_pos, KNEE, gs, o | 4, pal);
            } else {
                myospal(weapon_xoffset + 160 - 16 - (p.look_ang >> 1), looking_arc + 214 - gun_pos, KNEE + 1, gs, o | 4,
                        pal);
            }
        }

        if (psp.getXrepeat() < 40) {
            if (p.jetpack_on == 0) {
                i = psp.getXvel();
                looking_arc += 32 - (i >> 1);
                fistsign += (short) (i >> 1);
            }
            cw = weapon_xoffset;
            weapon_xoffset += EngineUtils.sin((fistsign) & 2047) >> 10;
            myos(weapon_xoffset + 250 - (p.look_ang >> 1),
                    looking_arc + 258 - (klabs(EngineUtils.sin((fistsign) & 2047) >> 8)), FIST, gs, o);
            weapon_xoffset = cw;
            weapon_xoffset -= EngineUtils.sin((fistsign) & 2047) >> 10;
            myos(weapon_xoffset + 40 - (p.look_ang >> 1),
                    looking_arc + 200 + (klabs(EngineUtils.sin((fistsign) & 2047) >> 8)), FIST, gs, o | 4);
        } else {
            switch (cw) {
                case KNEE_WEAPON:
                    if (kb > 0) {
                        if (psp.getPal() == 1) {
                            pal = 1;
                        } else {
                            Sector psec = boardService.getSector(p.cursectnum);
                            if (psec != null) {
                                pal = psec.getFloorpal();
                                if (pal == 0) {
                                    pal = p.palookup;
                                }
                            }
                        }

                        if ((kb) < 5 || (kb) > 9) {
                            myospal(weapon_xoffset + 220 - (p.look_ang >> 1), looking_arc + 250 - gun_pos, KNEE, gs, o,
                                    pal);
                        } else {
                            myospal(weapon_xoffset + 160 - (p.look_ang >> 1), looking_arc + 214 - gun_pos, KNEE + 1, gs, o,
                                    pal);
                        }
                    }
                    break;

                case TRIPBOMB_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    weapon_xoffset += 8;
                    gun_pos -= 10;

                    if ((kb) > 6) {
                        looking_arc += ((kb) << 3);
                    } else if ((kb) < 4) {
                        myospal(weapon_xoffset + 142 - (p.look_ang >> 1), looking_arc + 234 - gun_pos, HANDHOLDINGLASER + 3,
                                gs, o, pal);
                    }

                    myospal(weapon_xoffset + 130 - (p.look_ang >> 1), looking_arc + 249 - gun_pos,
                            HANDHOLDINGLASER + ((kb) >> 2), gs, o, pal);
                    myospal(weapon_xoffset + 152 - (p.look_ang >> 1), looking_arc + 249 - gun_pos,
                            HANDHOLDINGLASER + ((kb) >> 2), gs, o | 4, pal);

                    break;

                case RPG_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    weapon_xoffset -= EngineUtils.sin((768 + ((kb) << 7)) & 2047) >> 11;
                    gun_pos += EngineUtils.sin((768 + ((kb) << 7) & 2047)) >> 11;

                    if (kb > 0) {
                        if (kb < 8) {
                            myospal(weapon_xoffset + 164, (looking_arc << 1) + 176 - gun_pos, RPGGUN + ((kb) >> 1), gs, o,
                                    pal);
                        }
                    }

                    myospal(weapon_xoffset + 164, (looking_arc << 1) + 176 - gun_pos, RPGGUN, gs, o, pal);
                    break;

                case SHOTGUN_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    weapon_xoffset -= 8;

                    switch (kb) {
                        case 1:
                        case 2:
                            myospal(weapon_xoffset + 168 - (p.look_ang >> 1), looking_arc + 201 - gun_pos, SHOTGUN + 2, -128, o,
                                    pal);
                        case 0:
                        case 6:
                        case 7:
                        case 8:
                            myospal(weapon_xoffset + 146 - (p.look_ang >> 1), looking_arc + 202 - gun_pos, SHOTGUN, gs, o, pal);
                            break;
                        case 3:
                        case 4:
                        case 5:
                        case 9:
                        case 10:
                        case 11:
                        case 12:
                            if (kb < 5) {
                                gun_pos -= 40;
                                weapon_xoffset += 20;

                                myospal(weapon_xoffset + 178 - (p.look_ang >> 1), looking_arc + 194 - gun_pos,
                                        SHOTGUN + 1 + ((kb - 1) >> 1), -128, o, pal);
                            }

                            myospal(weapon_xoffset + 158 - (p.look_ang >> 1), looking_arc + 220 - gun_pos, SHOTGUN + 3, gs, o,
                                    pal);

                            break;
                        case 13:
                        case 14:
                        case 15:
                            myospal(32 + weapon_xoffset + 166 - (p.look_ang >> 1), looking_arc + 210 - gun_pos, SHOTGUN + 4, gs,
                                    o, pal);
                            break;
                        case 16:
                        case 17:
                        case 18:
                        case 19:
                        case 24:
                        case 25:
                        case 26:
                        case 27:
                            myospal(64 + weapon_xoffset + 170 - (p.look_ang >> 1), looking_arc + 196 - gun_pos, SHOTGUN + 5, gs,
                                    o, pal);
                            break;
                        case 20:
                        case 21:
                        case 22:
                        case 23:
                            myospal(64 + weapon_xoffset + 176 - (p.look_ang >> 1), looking_arc + 196 - gun_pos, SHOTGUN + 6, gs,
                                    o, pal);
                            break;
                        case 28:
                        case 29:
                        case 30:
                            myospal(32 + weapon_xoffset + 156 - (p.look_ang >> 1), looking_arc + 206 - gun_pos, SHOTGUN + 4, gs,
                                    o, pal);
                            break;
                    }
                    break;

                case CHAINGUN_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    if (kb > 0) {
                        gun_pos -= EngineUtils.sin((kb) << 7) >> 12;
                    }

                    if (kb > 0 && psp.getPal() != 1) {
                        weapon_xoffset += 1 - (engine.rand() & 3); // displayweapon
                    }

                    myospal(weapon_xoffset + 168 - (p.look_ang >> 1), looking_arc + 260 - gun_pos, CHAINGUN, gs, o, pal);
                    if (kb == 0) {
                        myospal(weapon_xoffset + 178 - (p.look_ang >> 1), looking_arc + 234 - gun_pos, CHAINGUN + 1, gs, o,
                                pal);
                    } else {
                        if (kb > 4 && kb < 12) {
                            i = 0;
                            if (psp.getPal() != 1) {
                                i = engine.rand() & 7; // displayweapon
                            }
                            myospal(i + weapon_xoffset - 4 + 140 - (p.look_ang >> 1),
                                    i + looking_arc - ((kb) >> 1) + 208 - gun_pos, CHAINGUN + 5 + ((kb - 4) / 5), gs, o,
                                    pal);
                            if (psp.getPal() != 1) {
                                i = engine.rand() & 7; // displayweapon
                            }
                            myospal(i + weapon_xoffset - 4 + 184 - (p.look_ang >> 1),
                                    i + looking_arc - ((kb) >> 1) + 208 - gun_pos, CHAINGUN + 5 + ((kb - 4) / 5), gs, o,
                                    pal);
                        }
                        if (kb < 8) {
                            i = engine.rand() & 7; // displayweapon
                            myospal(i + weapon_xoffset - 4 + 162 - (p.look_ang >> 1),
                                    i + looking_arc - ((kb) >> 1) + 208 - gun_pos, CHAINGUN + 5 + ((kb - 2) / 5), gs, o,
                                    pal);
                            myospal(weapon_xoffset + 178 - (p.look_ang >> 1), looking_arc + 234 - gun_pos,
                                    CHAINGUN + 1 + ((kb) >> 1), gs, o, pal);
                        } else {
                            myospal(weapon_xoffset + 178 - (p.look_ang >> 1), looking_arc + 234 - gun_pos, CHAINGUN + 1, gs,
                                    o, pal);
                        }
                    }
                    break;
                case PISTOL_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    if ((kb) < 5) {
                        short l = (short) (195 - 12 + weapon_xoffset);

                        if ((kb) == 2) {
                            l -= 3;
                        }
                        myospal((l - (p.look_ang >> 1)), (looking_arc + 244 - gun_pos), FIRSTGUN + kb_frames[kb], gs, 2,
                                pal);
                    } else {
                        if ((kb) < 10) {
                            myospal(194 - (p.look_ang >> 1), looking_arc + 230 - gun_pos, FIRSTGUN + 4, gs, o, pal);
                        } else if ((kb) < 15) {
                            myospal(244 - ((kb) << 3) - (p.look_ang >> 1), looking_arc + 130 - gun_pos + ((kb) << 4),
                                    FIRSTGUN + 6, gs, o, pal);
                            myospal(224 - (p.look_ang >> 1), looking_arc + 220 - gun_pos, FIRSTGUN + 5, gs, o, pal);
                        } else if ((kb) < 20) {
                            myospal(124 + ((kb) << 1) - (p.look_ang >> 1), looking_arc + 430 - gun_pos - ((kb) << 3),
                                    FIRSTGUN + 6, gs, o, pal);
                            myospal(224 - (p.look_ang >> 1), looking_arc + 220 - gun_pos, FIRSTGUN + 5, gs, o, pal);
                        } else if ((kb) < 23) {
                            myospal(184 - (p.look_ang >> 1), looking_arc + 235 - gun_pos, FIRSTGUN + 8, gs, o, pal);
                            myospal(224 - (p.look_ang >> 1), looking_arc + 210 - gun_pos, FIRSTGUN + 5, gs, o, pal);
                        } else if ((kb) < 25) {
                            myospal(164 - (p.look_ang >> 1), looking_arc + 245 - gun_pos, FIRSTGUN + 8, gs, o, pal);
                            myospal(224 - (p.look_ang >> 1), looking_arc + 220 - gun_pos, FIRSTGUN + 5, gs, o, pal);
                        } else if ((kb) < 27) {
                            myospal(194 - (p.look_ang >> 1), looking_arc + 235 - gun_pos, FIRSTGUN + 5, gs, o, pal);
                        }
                    }

                    break;
                case HANDBOMB_WEAPON: {
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    if (kb != 0) {

                        if ((kb) < 7) {
                            gun_pos -= 10 * (kb); // D
                        } else if ((kb) < 12) {
                            gun_pos += 20 * ((kb) - 10); // U
                        } else if ((kb) < 20) {
                            gun_pos -= 9 * ((kb) - 14); // D
                        }

                        myospal(weapon_xoffset + 190 - (p.look_ang >> 1), looking_arc + 250 - gun_pos,
                                HANDTHROW + throw_frames[(kb)], gs, o, pal);
                    } else {
                        myospal(weapon_xoffset + 190 - (p.look_ang >> 1), looking_arc + 260 - gun_pos, HANDTHROW, gs, o,
                                pal);
                    }
                }
                break;

                case HANDREMOTE_WEAPON: {
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    weapon_xoffset = -48;

                    if (kb != 0) {
                        myospal(weapon_xoffset + 150 - (p.look_ang >> 1), looking_arc + 258 - gun_pos,
                                HANDREMOTE + remote_frames[(kb)], gs, o, pal);
                    } else {
                        myospal(weapon_xoffset + 150 - (p.look_ang >> 1), looking_arc + 258 - gun_pos, HANDREMOTE, gs, o,
                                pal);
                    }
                }
                break;
                case DEVISTATOR_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        }
                    }

                    if (kb != 0) {

                        i = sgn((kb) >> 2);

                        if (p.hbomb_hold_delay != 0) {
                            myospal((cycloidy[kb] >> 1) + weapon_xoffset + 268 - (p.look_ang >> 1),
                                    cycloidy[kb] + looking_arc + 238 - gun_pos, DEVISTATOR + i, -32, o, pal);
                            myospal(weapon_xoffset + 30 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, DEVISTATOR, gs,
                                    o | 4, pal);
                        } else {
                            myospal(-(cycloidy[kb] >> 1) + weapon_xoffset + 30 - (p.look_ang >> 1),
                                    cycloidy[kb] + looking_arc + 240 - gun_pos, DEVISTATOR + i, -32, o | 4, pal);
                            myospal(weapon_xoffset + 268 - (p.look_ang >> 1), looking_arc + 238 - gun_pos, DEVISTATOR, gs,
                                    o, pal);
                        }
                    } else {
                        myospal(weapon_xoffset + 268 - (p.look_ang >> 1), looking_arc + 238 - gun_pos, DEVISTATOR, gs, o,
                                pal);
                        myospal(weapon_xoffset + 30 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, DEVISTATOR, gs, o | 4,
                                pal);
                    }
                    break;

                case FREEZE_WEAPON:
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        } else {
                            pal = 0;
                        }
                    }

                    if (kb != 0) {
                        if (psp.getPal() != 1) {
                            weapon_xoffset += engine.rand() & 3; // displayweapon
                            looking_arc += engine.rand() & 3; // displayweapon
                        }
                        gun_pos -= 16;
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 261 - gun_pos, FREEZE + 2, -32, o,
                                pal);
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 235 - gun_pos,
                                FREEZE + 3 + cat_frames[kb % 6], -32, o, pal);
                    } else {
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 261 - gun_pos, FREEZE, gs, o, pal);
                    }

                    break;
                case FLAMETHROWER_WEAPON: { // Twentieth Anniversary World Tour XXX
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        } else {
                            pal = 0;
                        }
                    }

                    Sector psec = boardService.getSector(p.cursectnum);
                    if (kb < 1 || psec != null && psec.getLotag() == 2) {
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 261 - gun_pos, FLAMETHROWER, gs, o,
                                pal);
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 261 - gun_pos, FLAMETHROWERPILOT,
                                gs, o, pal);
                    } else {
                        if (psp.getPal() != 1) {
                            weapon_xoffset += engine.rand() & 1; // displayweapon
                            looking_arc += engine.rand() & 1; // displayweapon
                        }
                        gun_pos -= 16;
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 261 - gun_pos, FLAMETHROWER + 1,
                                -32, o, pal);
                        myospal(weapon_xoffset + 210 - (p.look_ang >> 1), looking_arc + 235 - gun_pos,
                                FLAMETHROWER + 2 + cat_frames[kb % 6], -32, o, pal);
                    }
                    break;
                }
                case SHRINKER_WEAPON:
                case GROW_WEAPON:
                    weapon_xoffset += 28;
                    looking_arc += 18;
                    if (psp.getPal() == 1) {
                        pal = 1;
                    } else {
                        Sector psec = boardService.getSector(p.cursectnum);
                        if (psec != null) {
                            pal = psec.getFloorpal();
                        } else {
                            pal = 0;
                        }
                    }
                    if ((kb) == 0) {
                        if (cw == GROW_WEAPON) {
                            myospal(weapon_xoffset + 184 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER + 2,
                                    16 - (EngineUtils.sin(p.random_club_frame & 2047) >> 10), o, 2);

                            myospal(weapon_xoffset + 188 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER - 2, gs,
                                    o, pal);
                        } else {
                            myospal(weapon_xoffset + 184 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER + 2,
                                    16 - (EngineUtils.sin(p.random_club_frame & 2047) >> 10), o, 0);

                            myospal(weapon_xoffset + 188 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER, gs, o,
                                    pal);
                        }
                    } else {
                        if (psp.getPal() != 1) {
                            weapon_xoffset += engine.rand() & 3; // displayweapon
                            gun_pos += (engine.rand() & 3); // displayweapon
                        }

                        if (cw == GROW_WEAPON) {
                            myospal(weapon_xoffset + 184 - (p.look_ang >> 1), looking_arc + 240 - gun_pos,
                                    SHRINKER + 3 + ((kb) & 3), -32, o, 2);

                            myospal(weapon_xoffset + 188 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER - 1, gs,
                                    o, pal);

                        } else {
                            myospal(weapon_xoffset + 184 - (p.look_ang >> 1), looking_arc + 240 - gun_pos,
                                    SHRINKER + 3 + ((kb) & 3), -32, o, 0);

                            myospal(weapon_xoffset + 188 - (p.look_ang >> 1), looking_arc + 240 - gun_pos, SHRINKER + 1, gs,
                                    o, pal);
                        }
                    }
                    break;
            }
        }
    }

    private static class FireProj {
        public int x, y, z;
        public short xv, yv, zv;

        public void set(Sprite spr) {
            this.x = spr.getX();
            this.y = spr.getY();
            this.z = spr.getZ();
            this.xv = spr.getXvel();
            this.yv = spr.getYvel();
            this.zv = spr.getZvel();
        }
    }
}
