package ru.m210projects.Duke3D.Factory;

import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.ConvertType;
import ru.m210projects.Build.Types.Transparent;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.OsdCommandPrompt;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Duke3D.Globals.MODE_TYPE;
import static ru.m210projects.Duke3D.Globals.ud;
import static ru.m210projects.Duke3D.Main.engine;
import static ru.m210projects.Duke3D.Main.game;
import static ru.m210projects.Duke3D.Names.SPINNINGNUKEICON;

public class DukePrompt extends OsdCommandPrompt implements OsdCommandPrompt.ActionListener {

    public DukePrompt() {
        super(512, 32);
        setActionListener(this);
    }

    public void draw() {
        Renderer renderer = game.getRenderer();

        int y = 200 - 8;
        if (ud.screen_size > 0) {
            y = 200 - 45;
        }

        final String text = getTextInput();
        final int cursorPos = getCursorPosition();
        final Font font = game.getFont(1);
        final int nShade = 0;
        int pos = 160 - font.getWidth(text, 1.0f) / 2;

        int curX = pos;
        for (int i = 0; i < text.length(); i++) {
            pos += font.drawCharScaled(renderer, pos, y, text.charAt(i), 1.0f, nShade, 0, Transparent.None, ConvertType.Normal, false);
            if (i == cursorPos - 1) {
                curX = pos;
            }
        }

        int pal = isOsdOverType() ? 1 : 0;
        renderer.rotatesprite((curX + 4) << 16, (y + 3) << 16, 32768, 0, SPINNINGNUKEICON + (((engine.getTotalClock() >> 3)) % 7), 0, pal, 10);
    }

    @Override
    public void onEnter(String message) {
        setCaptureInput(false);
        MODE_TYPE = false;
        game.net.SendMessage(numplayers, message.toCharArray(), message.length());
    }


}
