package ru.m210projects.Duke3D.Factory;

import com.badlogic.gdx.Gdx;
import ru.m210projects.Build.Pattern.ScreenAdapters.InitScreen;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.DefaultOsdFunc;
import ru.m210projects.Build.osd.OsdColor;

import static ru.m210projects.Build.net.Mmulti.numplayers;
import static ru.m210projects.Build.Pragmas.divscale;
import static ru.m210projects.Build.Pragmas.mulscale;
import static ru.m210projects.Duke3D.Cheats.IsCheatCode;
import static ru.m210projects.Duke3D.Cheats.cheatCode;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;

public class DukeOsdFunc extends DefaultOsdFunc {

    public DukeOsdFunc() {
        super(game.getRenderer());

        BGTILE = BIGHOLE;
        BGCTILE = INGAMEDUKETHREEDEE;
        BORDTILE = VIEWBORDER;

        BITSTH = 1 + 8 + 16;
        BITSTL = 1 + 8 + 16;
        BITS = 8 + 16 + 64 + 4;
        BORDERANG = 512;
        SHADE = 10;
        PALETTE = 0;

        OsdColor.RED.setPal(10);
        OsdColor.BLUE.setPal(0);
        OsdColor.YELLOW.setPal(7); //23
        OsdColor.WHITE.setPal(12);
        OsdColor.GREY.setPal(13);
        OsdColor.GREEN.setPal(8);
        OsdColor.BROWN.setPal(15);
    }

    @Override
    protected Font getFont() {
        return game.getFont(1);
    }

    @Override
    protected int calcStartX(int x, int scale) {
        return mulscale((long) (8.0f * x), scale, 16);
    }

    @Override
    public void showOsd(boolean cursorCatched) {
        if (!(game.getScreen() instanceof InitScreen) && !game.pMenu.gShowMenu) {
            Gdx.input.setCursorCatched(!cursorCatched);
        }
    }

    @Override
    public int getcolumnwidth(int osdtextscale) {
        return (int) (divscale(renderer.getWidth(), osdtextscale, 16) / 8.0f);
    }

    @Override
    public boolean textHandler(String message) {
        if (numplayers > 1) {
            return false;
        }

        char[] lockeybuf = message.toCharArray();
        int i = 0;
        while (i < lockeybuf.length && lockeybuf[i] != 0) {
            lockeybuf[i++] += 1;
        }
        String cheat = new String(lockeybuf).toUpperCase();

        int ep = -1, lvl = -1;
        boolean wrap1 = false;
        boolean wrap2 = false;

        boolean isScotty = false;
        if (cheat.startsWith(cheatCode[2]) || cheat.startsWith(cheatCode[9])) {
            isScotty = true;

            i = 0;
            while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                i++;
            }
            cheat = cheat.substring(0, i);
            message = message.replaceAll("[\\s]{2,}", " ");
            int startpos = ++i;
            while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                i++;
            }

            if (i <= message.length()) {
                String nEpisode = message.substring(startpos, i);
                nEpisode = nEpisode.replaceAll("[^0-9]", "");
                if (!nEpisode.isEmpty()) {
                    try {
                        ep = Integer.parseInt(nEpisode);
                        wrap1 = true;
                        startpos = ++i;
                        while (i < message.length() && message.charAt(i) != 0 && message.charAt(i) != ' ') {
                            i++;
                        }
                        if (i <= message.length()) {
                            String nLevel = message.substring(startpos, i);
                            nLevel = nLevel.replaceAll("[^0-9]", "");
                            if (!nLevel.isEmpty()) {
                                lvl = Integer.parseInt(nLevel);
                                wrap2 = true;
                            }
                        }
                    } catch (Exception ignored) {
                    }
                }
            }
        }

        boolean isCheat = false;
        for (String s : cheatCode) {
            if (cheat.equalsIgnoreCase(s)) {
                isCheat = true;
                break;
            }
        }

        if (!game.isCurrentScreen(gGameScreen) && isCheat) {
            Console.out.println(message + ": not in a game");
            return true;
        }

        if (wrap1) {
            if (wrap2) {
                return IsCheatCode(cheat, ep, lvl);
            } else {
                if (!IsCheatCode(cheat, ep)) {
                    Console.out.println("dnscotty <episode> <level>");
                    return true;

                }
            }
        } else {
            if (isScotty) {
                Console.out.println("dnscotty <episode> <level>");
                return true;
            }

            return IsCheatCode(cheat);
        }
        return true;
    }
}
