// This file is part of DukeGDX.
// Copyright (C) 2019  Alexander Makarov-[M210] (m210-2007@mail.ru)
//
// DukeGDX is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DukeGDX is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DukeGDX.  If not, see <http://www.gnu.org/licenses/>.

package ru.m210projects.Duke3D.Factory;

import ru.m210projects.Build.BoardService;
import ru.m210projects.Build.Engine;
import ru.m210projects.Build.Pattern.BuildGame;
import ru.m210projects.Build.osd.CommandResponse;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.commands.OsdCommand;

import static ru.m210projects.Duke3D.Globals.*;

public class DukeEngine extends Engine {

    public static String randomStack = "";

    public DukeEngine(BuildGame game) throws Exception {
        super(game);
        inittimer(game.pCfg.isLegacyTimer(), TICRATE, TICSPERFRAME);

        Console.out.registerCommand(new OsdCommand("fastdemo", "fastdemo \"demo speed\"") {
            @Override
            public CommandResponse execute(String[] argv) {
                if (argv.length != 1) {
                    return CommandResponse.DESCRIPTION_RESPONSE;
                }

                try {
                    getTimer().setSkipTicks(Math.max(0, Integer.parseInt(argv[0])));
                } catch (Exception e) {
                    return CommandResponse.BAD_ARGUMENT_RESPONSE;
                }
                return CommandResponse.OK_RESPONSE;
            }
        });
    }

    @Override
    protected BoardService createBoardService() {
        return new DukeBoardService();
    }

    @Override
    public int krand() {
//        boolean showLines = false;
//		StringBuilder sb = new StringBuilder();
//		randomStack += "\r\n";
//		randomStack += "bseed: " + randomseed;
//		for (StackTraceElement element : Thread.currentThread().getStackTrace()) {
//			if (element.getMethodName().equalsIgnoreCase("ProcessFrame")) {
//				break;
//			}
//
//            if (!showLines) {
//                sb.append("\t").append(element.getClassName()).append(".").append(element.getMethodName());
//            } else {
//                sb.append("\t").append(element);
//            }
//
//			sb.append("\r\n");
//		}
//		randomStack += sb.toString();

        return super.krand();
    }
}
