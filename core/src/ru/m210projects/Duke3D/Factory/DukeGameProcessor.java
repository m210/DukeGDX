package ru.m210projects.Duke3D.Factory;

import com.badlogic.gdx.math.Vector2;
import ru.m210projects.Build.Pattern.BuildNet;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Types.Sector;
import ru.m210projects.Build.input.GameProcessor;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.settings.GameKeys;
import ru.m210projects.Duke3D.Config;
import ru.m210projects.Duke3D.Input;
import ru.m210projects.Duke3D.Main;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.myconnectindex;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Sounds.spritesound;
import static ru.m210projects.Duke3D.View.FTA;

public class DukeGameProcessor extends GameProcessor {

    public static final int MAXHORIZ = 127;
    private static final int MAXANGVEL = 127;
    private static final int TURBOTURNTIME = (TICRATE / 8); // 7
    private static final int NORMALTURN = 15;
    private static final int PREAMBLETURN = 5;
    private static final int NORMALKEYMOVE = 40;
    private static final int MAXVEL = ((NORMALKEYMOVE * 2) + 10);
    private static final int MAXSVEL = ((NORMALKEYMOVE * 2) + 10);
    private final DukePrompt dukePrompt;
    private int turnheldtime; //MED
    private int lastcontroltime; //MED


    public DukeGameProcessor(Main main) {
        super(main);
        this.dukePrompt = new DukePrompt();
    }

    public DukePrompt getDukeMessage() {
        return dukePrompt;
    }

    @Override
    public void fillInput(BuildNet.NetInput netInput) {
        mouseDelta.x /= 8.0f;
        mouseDelta.y /= 4.0f;
        PlayerStruct p = ps[myconnectindex];
        Input input = (Input) netInput;

        if (game.pMenu.gShowMenu || Console.out.isShowing() || MODE_TYPE /*|| (game.gPaused && !ctrlKeyStatus(KEY_PAUSE))*/) {
            input.fvel = 0;
            input.svel = 0;
            input.avel = 0;
            input.horz = 0;
            input.bits = ((gamequit) << 26);

            input.fvel += (short) fricxv;
            input.svel += (short) fricyv;

            return;
        }

        if(isGameKeyPressed(GameKeys.Send_Message)) {
            spritesound(92, ps[myconnectindex].i);
            MODE_TYPE = true;
            dukePrompt.setCaptureInput(true);
        }

        if (multiflag == 1) {
            input.bits = 1 << 17;
            input.bits |= multiwhat << 18;
            input.bits |= multipos << 19;
            multiflag = 0;
            return;
        }

        int tics = engine.getTotalClock() - lastcontroltime;
        lastcontroltime = engine.getTotalClock();

        if (isGameKeyJustPressed(GameKeys.Mouse_Aiming)) {
            cfg.setgMouseAim(!cfg.isgMouseAim());
            FTA(44 + (cfg.isgMouseAim() ? 1 : 0), p);
        }

        input.bits = isGameKeyPressed(GameKeys.Jump) ? 1 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Crouch) ? 2 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Weapon_Fire) ? 4 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Aim_Up) ? 8 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Aim_Down) ? 16 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Run) ? 32 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Tilt_Left) ? 64 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Tilt_Right) ? 128 : 0;

        for (int i = 0; i < 10; i++) {
            if (isGameKeyJustPressed(cfg.getKeymap()[i + cfg.weaponIndex])) {
                input.bits |= (i + 1) << 8;
            }
        }

        if (isGameKeyJustPressed(GameKeys.Previous_Weapon)) {
            input.bits |= (11) << 8;
        }

        if (isGameKeyJustPressed(GameKeys.Next_Weapon)) {
            input.bits |= (12) << 8;
        }

        if (isGameKeyJustPressed(Config.DukeKeys.Last_Weap_Switch)) {
            input.bits |= (13) << 8;
        }

        input.bits |= isGameKeyPressed(Config.DukeKeys.Steroids) ? 1 << 12 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Look_Up) ? 1 << 13 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Look_Down) ? 1 << 14 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.NightVision) ? 1 << 15 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.MedKit) ? 1 << 16 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Aim_Center) ? 1 << 18 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Holster_Weapon) ? 1 << 19 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Inventory_Left) ? 1 << 20 : 0;
        input.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.PAUSE) ? 1 << 21 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Quick_Kick) ? 1 << 22 : 0;
        input.bits |= cfg.isgMouseAim() ? 1 << 23 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Holo_Duke) ? 1 << 24 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Jetpack) ? 1 << 25 : 0;
        input.bits |= gamequit << 26;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Inventory_Right) ? 1 << 27 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Turn_Around) ? 1 << 28 : 0;
        input.bits |= isGameKeyPressed(GameKeys.Open) ? 1 << 29 : 0;
        input.bits |= isGameKeyPressed(Config.DukeKeys.Inventory_Use) ? 1 << 30 : 0;
        input.bits |= isKeyJustPressed(com.badlogic.gdx.Input.Keys.ESCAPE) ? 1 << 31 : 0;

        if ((input.bits & 2) != 0) {
            p.crouch_toggle = 0;
        }

        Sector psec = boardService.getSector(p.cursectnum);
        boolean CrouchMode;
        if (psec != null && psec.getLotag() != 2) {
            CrouchMode = isGameKeyJustPressed(Config.DukeKeys.Crouch_toggle);
        } else {
            CrouchMode = isGameKeyPressed(Config.DukeKeys.Crouch_toggle);
        }
        if (psec != null && psec.getLotag() == 2) {
            p.crouch_toggle = CrouchMode ? (byte) 1 : 0;
        } else if (CrouchMode) {
            p.crouch_toggle ^= 1;
        }

        if (p.crouch_toggle == 1) {
            input.bits |= 2;
        }

        boolean running = ((ud.auto_run == 0 && (input.bits & 32) != 0) || ((input.bits & 32) == 0 && ud.auto_run != 0));

        short vel;
        short svel = vel = 0;
        float angvel;
        float horiz = angvel = 0;
        int turnamount = NORMALTURN;
        short keymove = NORMALKEYMOVE;
        if (running) {
            turnamount = NORMALTURN << 1;
            keymove = NORMALKEYMOVE << 1;
        }

        if (isGameKeyPressed(GameKeys.Strafe)) {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                svel -= (short) -keymove;
            }
            if (isGameKeyPressed(GameKeys.Turn_Right)) {
                svel -= keymove;
            }
            svel = (short) BClipRange(svel - 20 * ctrlGetMouseStrafe(), -keymove, keymove);
        } else {
            if (isGameKeyPressed(GameKeys.Turn_Left)) {
                turnheldtime += tics;
                if (turnheldtime >= TURBOTURNTIME) {
                    angvel -= turnamount;
                } else {
                    angvel -= PREAMBLETURN;
                }
            } else if (isGameKeyPressed(GameKeys.Turn_Right)) {
                turnheldtime += tics;
                if (turnheldtime >= TURBOTURNTIME) {
                    angvel += turnamount;
                } else {
                    angvel += PREAMBLETURN;
                }
            } else {
                turnheldtime = 0;
            }
            angvel = BClipRange(angvel + ctrlGetMouseTurn(), -1024, 1024);
        }

        if (isGameKeyPressed(GameKeys.Strafe_Left)) {
            svel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Strafe_Right)) {
            svel -= keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Forward)) {
            vel += keymove;
        }
        if (isGameKeyPressed(GameKeys.Move_Backward)) {
            vel -= keymove;
        }

        Renderer renderer = game.getRenderer();
        int ydim = renderer.getHeight();

        if (cfg.isgMouseAim()) {
            horiz = BClipRange(ctrlGetMouseLook(!cfg.isgInvertmouse()), -(ydim >> 1), 100 + (ydim >> 1));
        } else {
            vel = (short) BClipRange(vel - ctrlGetMouseMove(), -4 * keymove, 4 * keymove);
        }

        Vector2 stick1 = ctrlGetStick(JoyStick.LOOKING);
        Vector2 stick2 = ctrlGetStick(JoyStick.MOVING);

        float lookx = stick1.x;
        float looky = stick1.y;

        if (looky != 0) {
            float k = 8.0f;
            horiz = BClipRange(horiz - k * looky * cfg.getJoyLookSpeed(), -(ydim >> 1), 100 + (ydim >> 1));
        }

        if (lookx != 0) {
            float k = 8.0f;
            angvel = BClipRange(angvel + k * lookx * cfg.getJoyTurnSpeed(), -1024, 1024);
        }


        if (stick2.y != 0) {
            vel = (short) BClipRange(vel - (keymove * stick2.y), -4 * keymove, 4 * keymove);
        }

        if (stick2.x != 0) {
            svel = (short) BClipRange(svel - (keymove * stick2.x), -4 * keymove, 4 * keymove);
        }

        if (vel < -MAXVEL) {
            vel = -MAXVEL;
        }
        if (vel > MAXVEL) {
            vel = MAXVEL;
        }
        if (svel < -MAXSVEL) {
            svel = -MAXSVEL;
        }
        if (svel > MAXSVEL) {
            svel = MAXSVEL;
        }
        if (angvel < -MAXANGVEL) {
            angvel = -MAXANGVEL;
        }
        if (angvel > MAXANGVEL) {
            angvel = MAXANGVEL;
        }
        if (horiz < -MAXHORIZ) {
            horiz = -MAXHORIZ;
        }
        if (horiz > MAXHORIZ) {
            horiz = MAXHORIZ;
        }

        if (ud.scrollmode && ud.overhead_on != 0) {
            ud.folfvel = vel;
            ud.folavel = angvel;
            input.fvel = 0;
            input.svel = 0;
            input.avel = 0;
            input.horz = 0;
            return;
        }

        int momx = (int) (vel * BCosAngle(BClampAngle(p.ang)) / 512.0f);
        int momy = (int) (vel * BSinAngle(BClampAngle(p.ang)) / 512.0f);

        momx += (int) (svel * BSinAngle(BClampAngle(p.ang)) / 512.0f);
        momy += (int) (svel * BCosAngle(BClampAngle(p.ang + 1024)) / 512.0f);

        momx += fricxv;
        momy += fricyv;

        input.fvel = (short) momx;
        input.svel = (short) momy;
        input.avel = angvel;
        input.horz = horiz;
    }

    @Override
    public boolean keyDown(int keycode) {
        if (dukePrompt.isCaptured()) {
            if (keycode == com.badlogic.gdx.Input.Keys.ESCAPE) {
                dukePrompt.setCaptureInput(false);
                dukePrompt.clear();
                MODE_TYPE = false;
            } else {
                dukePrompt.keyDown(keycode);
            }
            return true;
        }
        return super.keyDown(keycode);
    }

    @Override
    public boolean keyUp(int i) {
        dukePrompt.keyUp(i);
        return super.keyUp(i);
    }

    @Override
    public boolean keyRepeat(int keycode) {
        if (dukePrompt.isCaptured()) {
            dukePrompt.keyRepeat(keycode);
            return true;
        }
        return super.keyRepeat(keycode);
    }

    @Override
    public boolean keyTyped(char i) {
        if (dukePrompt.isCaptured()) {
            dukePrompt.keyTyped(i);
            return true;
        }
        return super.keyTyped(i);
    }
}
