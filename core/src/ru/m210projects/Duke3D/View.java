//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Jonathon Fowler (jf@jonof.id.au)
//and Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Duke3D;

import ru.m210projects.Build.EngineUtils;
import ru.m210projects.Build.Pattern.Tools.Interpolation.ILoc;
import ru.m210projects.Build.Render.Mirror;
import ru.m210projects.Build.Render.RenderedSpriteList;
import ru.m210projects.Build.Render.Renderer;
import ru.m210projects.Build.Render.Software.Software;
import ru.m210projects.Build.Types.*;
import ru.m210projects.Build.Types.collections.ListNode;
import ru.m210projects.Build.Types.font.BitmapFont;
import ru.m210projects.Build.Types.font.Font;
import ru.m210projects.Build.Types.font.TextAlign;
import ru.m210projects.Build.filehandle.art.ArtEntry;
import ru.m210projects.Build.filehandle.art.DynamicArtEntry;
import ru.m210projects.Build.osd.Console;
import ru.m210projects.Build.osd.OsdColor;
import ru.m210projects.Duke3D.Factory.DukeNetwork;
import ru.m210projects.Duke3D.Menus.InterfaceMenu;
import ru.m210projects.Duke3D.Screens.DemoScreen;
import ru.m210projects.Duke3D.Types.PlayerOrig;
import ru.m210projects.Duke3D.Types.PlayerStruct;

import java.util.Arrays;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static ru.m210projects.Build.Engine.*;
import static ru.m210projects.Build.Gameutils.*;
import static ru.m210projects.Build.net.Mmulti.*;
import static ru.m210projects.Build.Pragmas.*;
import static ru.m210projects.Build.Render.AbstractRenderer.DEFAULT_SCREEN_FADE;
import static ru.m210projects.Build.Strhandler.Bitoa;
import static ru.m210projects.Build.Strhandler.buildString;
import static ru.m210projects.Duke3D.Actors.badguy;
import static ru.m210projects.Duke3D.Factory.DukeMenuHandler.HELP;
import static ru.m210projects.Duke3D.Gamedef.getincangle;
import static ru.m210projects.Duke3D.Globals.*;
import static ru.m210projects.Duke3D.LoadSave.lastload;
import static ru.m210projects.Duke3D.Main.*;
import static ru.m210projects.Duke3D.Names.*;
import static ru.m210projects.Duke3D.Premap.rorsector;
import static ru.m210projects.Duke3D.Premap.rortype;
import static ru.m210projects.Duke3D.Screen.*;
import static ru.m210projects.Duke3D.DSector.*;
import static ru.m210projects.Duke3D.Weapons.displayweapon;

public class View {

    public static final String deathMessage = "or \"ENTER\" to load last saved game";
    public static final int MAXUSERQUOTES = 4;
    public static final short[] tip_y = {0, -8, -16, -32, -64, -84, -108, -108, -108, -108, -108, -108, -108, -108, -108, -108, -96, -72, -64, -32, -16};
    public static final short[] access_y = {0, -8, -16, -32, -64, -84, -108, -108, -108, -108, -108, -108, -108, -108, -108, -108, -96, -72, -64, -32, -16};
    private static final char[] buffer = new char[256];
    private static final PlayerOrig viewout = new PlayerOrig();
    public static int oyrepeat = -1;
    public static int gPlayerIndex = -1;
    public static int quotebot, quotebotgoal;
    public static final short[] user_quote_time = new short[MAXUSERQUOTES];
    public static final char[][] user_quote = new char[MAXUSERQUOTES][80];
    public static short fta, ftq, zoom, over_shoulder_on;
    public static final int[] loogiex = new int[64];
    public static final int[] loogiey = new int[64];
    public static int cameradist = 0, cameraclock = 0;
    public static int gNameShowTime;
    public static String lastmessage;
    public static int lastvisinc;
    public static final byte[] oldgotsector = new byte[MAXSECTORS >> 3];

    public static void adduserquote(char[] daquote) {
        for (int i = MAXUSERQUOTES - 1; i > 0; i--) {
            System.arraycopy(user_quote[i - 1], 0, user_quote[i], 0, 80);
            user_quote_time[i] = user_quote_time[i - 1];
        }
        System.arraycopy(daquote, 0, user_quote[0], 0, Math.min(daquote.length, 80));

        int len = 0;
        while (len < daquote.length && daquote[len] != 0) {
            len++;
        }
        Console.out.println(new String(daquote, 0, len), OsdColor.GREEN);

        user_quote_time[0] = 180;
    }

    public static void displayloogie(int snum) {
        Renderer renderer = game.getRenderer();
        int i, a, x, y, z;

        if (ps[snum].loogcnt == 0 || snum != screenpeek) {
            return;
        }

        y = (ps[snum].loogcnt << 2);
        for (i = 0; i < ps[snum].numloogs; i++) {
            a = klabs(EngineUtils.sin(((ps[snum].loogcnt + i) << 5) & 2047)) >> 5;
            z = 4096 + ((ps[snum].loogcnt + i) << 9);
            x = (int) (-sync[snum].avel) + (EngineUtils.sin(((ps[snum].loogcnt + i) << 6) & 2047) >> 10);

            renderer.rotatesprite((loogiex[i] + x) << 16, (200 + loogiey[i] - y) << 16, z - (i << 8), 256 - a, LOOGIE, 0, 0, 2 | 8);
        }
    }

    public static void displayrest(int smoothratio) {
        Renderer renderer = game.getRenderer();
        int a, i, j;

        int cposx, cposy;
        float cang;

        int cr = 0, cg = 0, cb = 0, cf = 0;
        boolean dotint = false;
        PlayerStruct pp = ps[screenpeek];

        if (changepalette != 0) {
            setgamepalette(pp, pp.palette);
            changepalette = 0;
        }

        if (pp.pals_time > 0 && pp.loogcnt == 0) {
            dotint = true;
            cr = pp.pals[0];
            cg = pp.pals[1];
            cb = pp.pals[2];
            cf = pp.pals_time;

            if (game.getRenderer() instanceof Software) {
                restorepalette = true;
            }
        } else if (restorepalette) {
            setgamepalette(pp, pp.palette);

            dotint = true;
            restorepalette = false;
        } else if (pp.loogcnt > 0) {
            dotint = true;
            cg = 64;
            cf = pp.loogcnt >> 1;
        }

        if (dotint && !game.menu.gShowMenu) {
            palto(cr, cg, cb, cf | 128);
            game.getRenderer().showScreenFade(DEFAULT_SCREEN_FADE);
        }

        i = pp.cursectnum;

        Sector psec = boardService.getSector(i);
        if (psec != null) {
            show2dsector.setBit(i);

            for (ListNode<Wall> wn = psec.getWallNode(); wn != null; wn = wn.getNext()) {
                Wall wal = wn.get();

                Sector sec = boardService.getSector(wal.getNextsector());
                if (sec == null) {
                    continue;
                }

                if ((wal.getCstat() & 0x0071) != 0) {
                    continue;
                }

                Wall nextwal = boardService.getWall(wal.getNextwall());
                if (nextwal == null || (nextwal.getCstat() & 0x0071) != 0) {
                    continue;
                }
                if (sec.getLotag() == 32767) {
                    continue;
                }
                if (sec.getCeilingz() >= sec.getFloorz()) {
                    continue;
                }
                show2dsector.setBit(wal.getNextsector());
            }
        }

        boolean isMenuShowing = game.menu.gShowMenu && !(game.menu.getCurrentMenu() instanceof InterfaceMenu);

        if (ud.camerasprite == -1) {
            if (ud.overhead_on != 2) {
                if (pp.newowner >= 0) {
                    cameratext(pp.newowner);
                } else {
                    displayweapon(screenpeek);
                    displayloogie(screenpeek);
                    if (over_shoulder_on == 0 && !isMenuShowing) {
                        displaymasks(screenpeek);
                    }
                }
                moveclouds();
            }

            if (ud.overhead_on > 0) {
                if (!ud.scrollmode) {
                    if (pp.newowner == -1) {
                        if (screenpeek == myconnectindex && numplayers > 1) {
                            DukeNetwork net = game.net;
                            cposx = net.predictOld.x + mulscale((net.predict.x - net.predictOld.x), smoothratio, 16);
                            cposy = net.predictOld.y + mulscale((net.predict.y - net.predictOld.y), smoothratio, 16);
                            cang = net.predictOld.ang + (BClampAngle(net.predict.ang + 1024 - net.predictOld.ang) - 1024) * smoothratio / 65536.0f;
                        } else {
                            cposx = pp.prevView.x + mulscale((pp.posx - pp.prevView.x), smoothratio, 16);
                            cposy = pp.prevView.y + mulscale((pp.posy - pp.prevView.y), smoothratio, 16);
                            cang = pp.prevView.ang + (BClampAngle(pp.ang + 1024 - pp.prevView.ang) - 1024) * smoothratio / 65536.0f;
                        }
                    } else {
                        cposx = pp.oposx;
                        cposy = pp.oposy;
                        cang = pp.oang;
                    }
                } else {
                    ud.fola += (int) (ud.folavel / 8.0f);
                    ud.folx += (ud.folfvel * EngineUtils.sin((512 + 2048 - ud.fola) & 2047)) >> 14;
                    ud.foly += (ud.folfvel * EngineUtils.sin((512 + 1024 - 512 - ud.fola) & 2047)) >> 14;

                    cposx = ud.folx;
                    cposy = ud.foly;
                    cang = ud.fola;
                }

                if (ud.overhead_on == 2) {
                    renderer.clearview(0);
                    renderer.drawmapview(cposx, cposy, zoom, (short) cang);
                }
                renderer.drawoverheadmap(cposx, cposy, zoom, (short) cang);

                if (ud.overhead_on == 2) {
                    if (ud.screen_size > 0) {
                        a = 147;
                    } else {
                        a = 182;
                    }

                    Arrays.fill(buffer, (char) 0);
                    buildString(buffer, 0, currentGame.episodes[ud.volume_number].Title);
                    game.getFont(0).drawTextScaled(renderer, 5, a, buffer, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                    Arrays.fill(buffer, (char) 0);
                    buildString(buffer, 0, currentGame.episodes[ud.volume_number].getMapTitle(ud.level_number));
                    game.getFont(0).drawTextScaled(renderer, 5, a + 6, buffer, 1.0f, -128, 0, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);

                    if (cfg.gShowStat != 0) {
                        int k = (int) (2.5f * game.getFont(1).getSize() * cfg.gStatSize / 65536.0f) - 5;
                        if (ud.coop != 1 && ud.screen_size > 0 && ud.multimode > 1) {
                            j = 0;
                            k = 8;
                            for (i = connecthead; i >= 0; i = connectpoint2[i]) {
                                if (i > j) {
                                    j = i;
                                }
                            }

                            if (j >= 4 && j <= 8) {
                                k += 8;
                            } else if (j > 8 && j <= 12) {
                                k += 16;
                            } else if (j > 12) {
                                k += 24;
                            }
                        }
                        viewDrawStats(10, 13 + k, cfg.gStatSize);
                    }
                }
            }
        }

        if (isMenuShowing) {
            return;
        }

        if (ps[myconnectindex].newowner == -1 && ud.overhead_on == 0 && ud.crosshair != 0 && ud.camerasprite == -1) {
            renderer.rotatesprite((160 - (ps[myconnectindex].look_ang >> 1)) << 16, 100 << 16, cfg.gCrossSize, 0, CROSSHAIR, 0, 0, 2 + 1);
        }

        coolgaugetext(screenpeek);
        operatefta();
        Sprite psp = boardService.getSprite(ps[myconnectindex].i);

        if (fta > 1 && psp != null && psp.getExtra() <= 0 && myconnectindex == screenpeek && ud.multimode < 2 && lastload != null && lastload.exists() && !DemoScreen.isDemoPlaying()) {
            int k = getftacoord();
            game.getFont(1).drawTextScaled(renderer, 320 >> 1, k + 10, deathMessage, 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (ud.screen_size > 0 && cfg.gShowStat == 1 && ud.overhead_on != 2) {
            int y = 192;
            if (ud.screen_size >= 2) {
                y = 162;
            }
            if (ud.screen_size == 1) {
                y = 168;
            }
            viewDrawStats(10, y, cfg.gStatSize);
        }

        if (game.isCurrentScreen(gGameScreen) && engine.getTotalClock() < gNameShowTime) {
            Transparent transp = Transparent.None;
            if (engine.getTotalClock() > gNameShowTime - 20) {
                transp = Transparent.Bit1;
            }
            if (engine.getTotalClock() > gNameShowTime - 10) {
                transp = Transparent.Bit2;
            }

            if (cfg.showMapInfo != 0 && !game.menu.gShowMenu) {
                if (mUserFlag != UserFlag.UserMap || boardfilename == null) {
                    if (ud.volume_number < nMaxEpisodes && currentGame.episodes[ud.volume_number] != null) {
                        game.getFont(2).drawTextScaled(renderer, 160, 114, currentGame.episodes[ud.volume_number].getMapTitle(ud.level_number), 1.0f, -128, 0, TextAlign.Center, transp, ConvertType.Normal, false);
                    }
                } else {
                    game.getFont(2).drawTextScaled(renderer, 160, 114, boardfilename.getName(), 1.0f, -128, 0, TextAlign.Center, transp, ConvertType.Normal, false);
                }
            }
        }

        if (game.getProcessor().getDukeMessage().isCaptured()) {
            game.getProcessor().getDukeMessage().draw();
        }

        if (game.gPaused && !game.menu.gShowMenu) {
            game.getFont(2).drawTextScaled(renderer, 160, 100, "GAME PAUSED", 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (gPlayerIndex != -1 && gPlayerIndex != myconnectindex) {
            int len;
            if (ud.user_name[gPlayerIndex] == null || ud.user_name[gPlayerIndex].isEmpty()) {
                len = buildString(buf, 0, "Player ", gPlayerIndex + 1);
            } else {
                len = buildString(buf, 0, ud.user_name[gPlayerIndex]);
            }
            len = buildString(buf, len, " (", ps[gPlayerIndex].last_extra);
            buildString(buf, len, "hp)");

            int shade = 16 - (engine.getTotalClock() & 0x3F);
            int windowy1 = 0;
            int y = scale(windowy1, 200, renderer.getHeight()) + 100;
            if (ud.screen_size <= 3) {
                y += engine.getTile(BOTTOMSTATUSBAR).getHeight() / 2;
            }

            game.getFont(1).drawTextScaled(renderer, 160, y, buf, 1.0f, shade, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        if (ud.coords != 0) {
            coords(screenpeek);
        }
    }

    public static int getftacoord() {
        int k = 0;
        if (ud.screen_size > 0 && ud.multimode > 1) {
            int j = 0;
            k = 8;
            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (i > j) {
                    j = i;
                }
            }

            if (j >= 4 && j <= 8) {
                k += 8;
            } else if (j > 8 && j <= 12) {
                k += 16;
            } else if (j > 12) {
                k += 24;
            }
        }

        if (ftq == 115 || ftq == 116) {
            k = quotebot;
            for (int i = 0; i < MAXUSERQUOTES; i++) {
                if (user_quote_time[i] <= 0) {
                    break;
                }
                k -= 8;
            }
            k -= 4;
        }

        return k;
    }

    public static void operatefta() {
        Renderer renderer = game.getRenderer();
        int i, j, k;

        if (ud.screen_size > 0) {
            j = 200 - 45;
        } else {
            j = 200 - 8;
        }
        quotebot = Math.min(quotebot, j);
        if (MODE_TYPE) {
            j -= 8;
        }
        quotebotgoal = j;
        j = quotebot;
        for (i = 0; i < MAXUSERQUOTES; i++) {
            k = user_quote_time[i];
            if (k <= 0) {
                break;
            }
            if (k > 4) {
                game.getFont(1).drawTextScaled(renderer, 320 >> 1, j, user_quote[i], 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
            } else if (k > 2) {
                game.getFont(1).drawTextScaled(renderer, 320 >> 1, j, user_quote[i], 1.0f, 0, 0, TextAlign.Center, Transparent.Bit1, ConvertType.Normal, false);
            } else {
                game.getFont(1).drawTextScaled(renderer, 320 >> 1, j, user_quote[i], 1.0f, 0, 0, TextAlign.Center, Transparent.Bit2, ConvertType.Normal, false);
            }
            j -= 8;
        }

        if (fta <= 1) {
            return;
        }

        k = getftacoord();

        j = fta;
        if (j > 4) {
            game.getFont(1).drawTextScaled(renderer, 320 >> 1, k, currentGame.getCON().fta_quotes[ftq], 1.0f, 0, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        } else if (j > 2) {
            game.getFont(1).drawTextScaled(renderer, 320 >> 1, k, currentGame.getCON().fta_quotes[ftq], 1.0f, 0, 0, TextAlign.Center, Transparent.Bit1, ConvertType.Normal, false);
        } else {
            game.getFont(1).drawTextScaled(renderer, 320 >> 1, k, currentGame.getCON().fta_quotes[ftq], 1.0f, 0, 0, TextAlign.Center, Transparent.Bit2, ConvertType.Normal, false);
        }
    }

    public static void displayfragbar(int yoffset, boolean showpalette) {
        Renderer renderer = game.getRenderer();
        int row = (ud.multimode - 1) / 4;
        if (row >= 0) {
//			int framesx = xdim / tilesizx[BIGHOLE];
//			int framesy = mulscale(tilesizy[FRAGBAR] * (row + 1), divscale(ydim, 200, 16), 16);
//			int x = 0;
//			for(int i = 0; i <= framesx; i++) {
//		    	renderer.rotatesprite(x<<16, 0, 0x10000, 0, BIGHOLE, 0, 0, 8 | 16 | 256, 0, 0, xdim-1, framesy);
//		    	x += tilesizx[BIGHOLE];
//		    }

            if (yoffset > 0) {
                yoffset -= 9 * row;
            }
            for (int r = 0; r <= row; r++) {
                renderer.rotatesprite(0, yoffset + (r * engine.getTile(FRAGBAR).getHeight()) << 16, 65600, 0, FRAGBAR, 0, 0, 2 + 8 + 16 + 64);
            }

            for (int i = connecthead; i >= 0; i = connectpoint2[i]) {
                if (ud.user_name[i] == null || ud.user_name[i].isEmpty()) {
                    buildString(buffer, 0, "Player ", i + 1);
                } else {
                    buildString(buffer, 0, ud.user_name[i]);
                }

                int pal = 0;
                if (showpalette && boardService.getBoard() != null) {
                    Sprite psp = boardService.getSprite(ps[i].i);
                    if (psp != null) {
                        pal = psp.getPal();
                    }
                }

                game.getFont(0).drawTextScaled(renderer, 21 + (73 * (i & 3)), yoffset + 2 + ((i & 28) << 1), buffer, 1.0f, 0, pal, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
                buildString(buffer, 0, "", ps[i].frag - ps[i].fraggedself);
                game.getFont(0).drawTextScaled(renderer, 17 + 50 + (73 * (i & 3)), yoffset + 2 + ((i & 28) << 1), buffer, 1.0f, 0, pal, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }
        }
    }

    @SuppressWarnings("unused")
    public static void debuginfo(int x, int y) {
        Renderer renderer = game.getRenderer();
//		int oy = y;
        BitmapFont font = EngineUtils.getLargeFont();
        buildString(buffer, 0, "totalclock= ", engine.getTotalClock());
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);

        y += 10;

        buildString(buffer, 0, "global_random= ", global_random);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "randomseed= ", engine.getrand());
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "posx= ", ps[0].posx);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "posy= ", ps[0].posy);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "posz= ", ps[0].posz);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "ang= ", Float.toString(ps[0].ang));
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "horiz= ", Float.toString(ps[0].horiz));
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "xvel= ", Float.toString(ps[0].posxv));
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "yvel= ", Float.toString(ps[0].posyv));
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;

        buildString(buffer, 0, "ps[0].auto_aim= ", ps[0].auto_aim);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        y += 10;
        buildString(buffer, 0, "ps[1].auto_aim= ", ps[1].auto_aim);
        font.drawText(renderer, x, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);

        /*
         * buildString(buffer, 0, "movefifoplc= ", movefifoplc);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "movefifoend= ", movefifoend[myconnectindex]);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "bufferjitter= ", bufferjitter);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "ps[0].auto_aim= ", ps[0].auto_aim);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10; buildString(buffer, 0,
         * "ps[1].auto_aim= ", ps[1].auto_aim); engine.printext256(x,y,31,-1,buffer,0);
         * y += 10;
         *
         *
         * buildString(buffer, 0, "syncvalhead= ", syncvalhead[myconnectindex]);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "syncvaltail= ", syncvaltail);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "syncvaltottail= ", syncvaltottail);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "syncval[myconnectindex]= ",
         * syncval[myconnectindex][syncvaltail&(MOVEFIFOSIZ-1)]);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "syncval[connecthead]= ",
         * syncval[connecthead][syncvaltail&(MOVEFIFOSIZ-1)]);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "global_random= ", global_random);
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "randomseed= ", engine.getrand());
         * engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * buildString(buffer, 0, "syncvalhead[connecthead]= ",
         * syncvalhead[connecthead]); engine.printext256(x,y,31,-1,buffer,0); y += 10;
         *
         * y = oy; x = 280; for(int i = syncvaltail - 5; i < syncvaltail + 5; i++) {
         * buildString(buffer, 0, "syncval[connecthead]= ",
         * syncval[connecthead][i&(MOVEFIFOSIZ-1)]); int pal = 31; if(i == syncvaltail)
         * pal = 30; engine.printext256(x,y,pal,-1,buffer,0); y += 10; }
         */
    }

    public static void coolgaugetext(int snum) // HUD
    {
        Renderer renderer = game.getRenderer();
        int i = 0, j, o, ss;

        PlayerStruct p = ps[snum];
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

//	    debuginfo(20, 40);

        if (p.invdisptime > 0) {
            displayinventory(p);
        }

        int xdim = renderer.getWidth();
        int ydim = renderer.getHeight();
        if (screenpeek != myconnectindex) {
            if (ud.user_name[screenpeek] == null || ud.user_name[screenpeek].isEmpty()) {
                buildString(buf, 0, "View from player ", screenpeek + 1);
            } else {
                buildString(buf, 0, "View from ", ud.user_name[screenpeek]);
            }
            int shade = 16 - (engine.getTotalClock() & 0x3F);
            int windowy1 = 0;
            game.getFont(1).drawTextScaled(renderer, 160, scale(windowy1, 200, ydim) + 10, buf, 1.0f, shade, 0, TextAlign.Center, Transparent.None, ConvertType.Normal, false);
        }

        ss = ud.screen_size;
        if (ss < 1) {
            return;
        }

        if ((ud.multimode > 1 || mFakeMultiplayer)) {
            displayfragbar(0, true);
        }

        if (ss == 1) // DRAW MINI STATUS BAR:
        {
            renderer.rotatesprite(5 << 16, (200 - 28) << 16, 65536, 0, HEALTHBOX, 0, 21, 10 + 16 + 256);

            if (psp.getPal() == 1 && p.last_extra < 2) {
                digitalnumber(20, 200 - 17, 1, -16, 10 + 16 + 256);
            } else {
                digitalnumber(20, 200 - 17, p.last_extra, -16, 10 + 16 + 256);
            }

            int x = 37;
            if (p.curr_weapon == HANDREMOTE_WEAPON) {
                i = HANDBOMB_WEAPON;
            } else {
                i = p.curr_weapon;
            }
            if (p.ammo_amount[i] != 0) {
                renderer.rotatesprite(x << 16, (200 - 28) << 16, 65536, 0, AMMOBOX, 0, 21, 10 + 16 + 256);
                digitalnumber(x + 16, 200 - 17, p.ammo_amount[i], -16, 10 + 16 + 256);
                x += engine.getTile(AMMOBOX).getWidth() + 1;
            }

            if (p.shield_amount != 0) {
                renderer.rotatesprite(x << 16, (200 - 28) << 16, 65536, 0, HUDARMOR, 0, 21, 10 + 16 + 256);
                digitalnumber(x + 16, 200 - 17, p.shield_amount, -16, 10 + 16 + 256);
                x += engine.getTile(HUDARMOR).getWidth() + 1;
            }

            if (p.got_access != 0) {

                renderer.rotatesprite(x << 16, (200 - 28) << 16, 65536, 0, HUDKEYS, 0, 21, 10 + 16 + 256);
                if ((p.got_access & 4) != 0) {
                    renderer.rotatesprite(x + 5 << 16, 182 << 16, 65536, 0, ACCESS_ICON, 0, 23, 10 + 16 + 256);
                }
                if ((p.got_access & 2) != 0) {
                    renderer.rotatesprite(x + 18 << 16, 182 << 16, 65536, 0, ACCESS_ICON, 0, 21, 10 + 16 + 256);
                }
                if ((p.got_access & 1) != 0) {
                    renderer.rotatesprite(x + 11 << 16, 189 << 16, 65536, 0, ACCESS_ICON, 0, 0, 10 + 16 + 256);
                }
                x += engine.getTile(HUDKEYS).getWidth() + 1;
            }

            if (p.inven_icon != 0) {
                renderer.rotatesprite(x << 16, (200 - 30) << 16, 65536, 0, INVENTORYBOX, 0, 21, 10 + 16 + 256, 0, 0, xdim - 1, ydim - 1);

                switch (p.inven_icon) {
                    case 1:
                        i = FIRSTAID_ICON;
                        break;
                    case 2:
                        i = STEROIDS_ICON;
                        break;
                    case 3:
                        i = HOLODUKE_ICON;
                        break;
                    case 4:
                        i = JETPACK_ICON;
                        break;
                    case 5:
                        i = HEAT_ICON;
                        break;
                    case 6:
                        i = AIRTANK_ICON;
                        break;
                    case 7:
                        i = BOOT_ICON;
                        break;
                    default:
                        i = -1;
                }

                if (i >= 0) {
                    renderer.rotatesprite((x + 4) << 16, (200 - 21) << 16, 65536, 0, i, 0, 0, 10 + 16 + 256);
                }

                game.getFont(0).drawCharScaled(renderer, (x + 35), 190, '%', 1.0f, 0, 6, Transparent.None, ConvertType.AlignLeft, false);

                j = 0x80000000;
                switch (p.inven_icon) {
                    case 1:
                        i = p.firstaid_amount;
                        break;
                    case 2:
                        i = ((p.steroids_amount + 3) >> 2);
                        break;
                    case 3:
                        i = ((p.holoduke_amount + 15) / 24);
                        j = p.holoduke_on;
                        break;
                    case 4:
                        i = ((p.jetpack_amount + 15) >> 4);
                        j = p.jetpack_on;
                        break;
                    case 5:
                        i = p.heat_amount / 12;
                        j = p.heat_on;
                        break;
                    case 6:
                        i = ((p.scuba_amount + 63) >> 6);
                        break;
                    case 7:
                        i = (p.boot_amount >> 1);
                        break;
                }
                invennum(x + 27, 200 - 6, (char) i, 0, 10 + 256);

                if (j > 0) {
                    game.getFont(0).drawTextScaled(renderer, x + 31, 180, "ON", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                } else if (j != 0x80000000) {
                    game.getFont(0).drawTextScaled(renderer, x + 27, 180, "OFF", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                }

                if (p.inven_icon >= 6) {
                    game.getFont(0).drawTextScaled(renderer, x + 22, 180, "AUTO", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
                }
            }

            return;
        } else if (ss == 2) {
            int x = 261;
            int y = 183;

            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, ALTHUDRIGHT, 0, 0, 10 | 512);

            if (ud.multimode > 1 && ud.coop != 1) {
                digitalnumber(x + 39, y, max(p.frag - p.fraggedself, 0), -16, 10 + 16 + 512);
            } else {
                if ((p.got_access & 4) != 0) {
                    renderer.rotatesprite(x + 26 << 16, y - 1 << 16, 65536, 0, ACCESS_ICON, 0, 23, 10 + 16 + 512);
                }
                if ((p.got_access & 2) != 0) {
                    renderer.rotatesprite(x + 39 << 16, y - 1 << 16, 65536, 0, ACCESS_ICON, 0, 21, 10 + 16 + 512);
                }
                if ((p.got_access & 1) != 0) {
                    renderer.rotatesprite(x + 32 << 16, y + 6 << 16, 65536, 0, ACCESS_ICON, 0, 0, 10 + 16 + 512);
                }
            }

            if (p.inven_icon != 0) {
                o = 0;

                x += 12;
                switch (p.inven_icon) {
                    case 1:
                        i = FIRSTAID_ICON;
                        break;
                    case 2:
                        i = STEROIDS_ICON;
                        break;
                    case 3:
                        i = HOLODUKE_ICON;
                        break;
                    case 4:
                        i = JETPACK_ICON;
                        break;
                    case 5:
                        i = HEAT_ICON;
                        break;
                    case 6:
                        i = AIRTANK_ICON;
                        break;
                    case 7:
                        i = BOOT_ICON;
                        break;
                }
                renderer.rotatesprite((x - 30 - o) << 16, y - 4 << 16, 65536, 0, i, 0, 0, 10 + 16 + 512);

                game.getFont(0).drawCharScaled(renderer, x + 1 - o, y + 7, '%', 1.0f, 0, 6, Transparent.None, ConvertType.AlignRight, false);

                if (p.inven_icon >= 6) {
                    game.getFont(0).drawTextScaled(renderer, x - 12 - o, y - 3, "AUTO", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignRight, false);
                }

                switch (p.inven_icon) {
                    case 3:
                        j = p.holoduke_on;
                        break;
                    case 4:
                        j = p.jetpack_on;
                        break;
                    case 5:
                        j = p.heat_on;
                        break;
                    default:
                        j = 0x80000000;
                }
                if (j > 0) {
                    game.getFont(0).drawTextScaled(renderer, x - 3 - o, y - 3, "ON", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignRight, false);
                } else if (j != 0x80000000) {
                    game.getFont(0).drawTextScaled(renderer, x - 7 - o, y - 3, "OFF", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.AlignRight, false);
                }

                switch (p.inven_icon) {
                    case 1:
                        i = p.firstaid_amount;
                        break;
                    case 2:
                        i = ((p.steroids_amount + 3) >> 2);
                        break;
                    case 3:
                        i = ((p.holoduke_amount + 15) / 24);
                        break;
                    case 4:
                        i = ((p.jetpack_amount + 15) >> 4);
                        break;
                    case 5:
                        i = p.heat_amount / 12;
                        break;
                    case 6:
                        i = ((p.scuba_amount + 63) >> 6);
                        break;
                    case 7:
                        i = (p.boot_amount >> 1);
                        break;
                }
                invennum(x - 7 - o, y + 11, i, 0, 10 + 512);
            }

            x = 68;

            renderer.rotatesprite(x << 16, y << 16, (1 << 16), 0, ALTHUDLEFT, 0, 0, 10 | 256);

            if (psp.getPal() == 1 && p.last_extra < 2) {
                digitalnumber(x - 49, y, 1, -16, 10 + 16 + 256);
            } else {
                digitalnumber(x - 49, y, p.last_extra, -16, 10 + 16 + 256);
            }

            if (p.shield_amount != 0) {
                digitalnumber(x - 17, y, p.shield_amount, -16, 10 + 16 + 256);
            }

            if (p.curr_weapon == HANDREMOTE_WEAPON) {
                i = HANDBOMB_WEAPON;
            } else {
                i = p.curr_weapon;
            }
            if (p.ammo_amount[i] != 0) {
                digitalnumber(x + 16, y, p.ammo_amount[i], -16, 10 + 16 + 256);
            }

            return;
        }

        // DRAW/UPDATE FULL STATUS BAR:
        patchstatusbar(0, 0, 320, 200);
        if (ud.multimode > 1 && ud.coop != 1) {
            renderer.rotatesprite(278 << 16, (200 - 28) << 16, 65536, 0, KILLSICON, 0, 0, 10 + 16);
        }

        if (ud.multimode > 1 && ud.coop != 1) {
            digitalnumber(287, 200 - 17, max(p.frag - p.fraggedself, 0), -16, 10 + 16);
        } else {
            if ((p.got_access & 4) != 0) {
                renderer.rotatesprite(275 << 16, 182 << 16, 65536, 0, ACCESS_ICON, 0, 23, 10 + 16);
            }
            if ((p.got_access & 2) != 0) {
                renderer.rotatesprite(288 << 16, 182 << 16, 65536, 0, ACCESS_ICON, 0, 21, 10 + 16);
            }
            if ((p.got_access & 1) != 0) {
                renderer.rotatesprite(281 << 16, 189 << 16, 65536, 0, ACCESS_ICON, 0, 0, 10 + 16);
            }
        }
        weapon_amounts(p, 96, 182);

        if (psp.getPal() == 1 && p.last_extra < 2) {
            digitalnumber(32, 200 - 17, 1, -16, 10 + 16);
        } else {
            digitalnumber(32, 200 - 17, p.last_extra, -16, 10 + 16);
        }

        digitalnumber(64, 200 - 17, p.shield_amount, -16, 10 + 16);

        if (p.curr_weapon != KNEE_WEAPON) {
            if (p.curr_weapon == HANDREMOTE_WEAPON) {
                i = HANDBOMB_WEAPON;
            } else {
                i = p.curr_weapon;
            }
            digitalnumber(230 - 22, 200 - 17, p.ammo_amount[i], -16, 10 + 16);
        }

        if (p.inven_icon != 0) {
            o = 0;

            switch (p.inven_icon) {
                case 1:
                    i = FIRSTAID_ICON;
                    break;
                case 2:
                    i = STEROIDS_ICON;
                    break;
                case 3:
                    i = HOLODUKE_ICON;
                    break;
                case 4:
                    i = JETPACK_ICON;
                    break;
                case 5:
                    i = HEAT_ICON;
                    break;
                case 6:
                    i = AIRTANK_ICON;
                    break;
                case 7:
                    i = BOOT_ICON;
                    break;
            }
            renderer.rotatesprite((231 - o) << 16, (200 - 21) << 16, 65536, 0, i, 0, 0, 10 + 16);

            game.getFont(0).drawCharScaled(renderer, 292 - 30 - o, 190, '%', 1.0f, 0, 6, Transparent.None, ConvertType.Normal, false);

            if (p.inven_icon >= 6) {
                game.getFont(0).drawTextScaled(renderer, 284 - 35 - o, 180, "AUTO", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }

            switch (p.inven_icon) {
                case 3:
                    j = p.holoduke_on;
                    break;
                case 4:
                    j = p.jetpack_on;
                    break;
                case 5:
                    j = p.heat_on;
                    break;
                default:
                    j = 0x80000000;
            }
            if (j > 0) {
                game.getFont(0).drawTextScaled(renderer, 288 - 30 - o, 180, "ON", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            } else if (j != 0x80000000) {
                game.getFont(0).drawTextScaled(renderer, 284 - 30 - o, 180, "OFF", 1.0f, 0, 2, TextAlign.Left, Transparent.None, ConvertType.Normal, false);
            }

            switch (p.inven_icon) {
                case 1:
                    i = p.firstaid_amount;
                    break;
                case 2:
                    i = ((p.steroids_amount + 3) >> 2);
                    break;
                case 3:
                    i = ((p.holoduke_amount + 15) / 24);
                    break;
                case 4:
                    i = ((p.jetpack_amount + 15) >> 4);
                    break;
                case 5:
                    i = p.heat_amount / 12;
                    break;
                case 6:
                    i = ((p.scuba_amount + 63) >> 6);
                    break;
                case 7:
                    i = (p.boot_amount >> 1);
                    break;
            }
            invennum(284 - 30 - o, 200 - 6, (char) i, 0, 10);

        }
    }

    public static void displayrooms(int snum, int smoothratio) {
        int dst, j;
        short k;
        int tposx, tposy, i;
        short tang;
        Renderer renderer = game.getRenderer();

        PlayerStruct p = ps[snum];
        Sprite psp = boardService.getSprite(p.i);
        if (psp == null) {
            return;
        }

        gPlayerIndex = -1;
        if ((!game.menu.gShowMenu && ud.overhead_on == 2) || game.menu.isOpened(game.menu.mMenus[HELP])) {
            return;
        }

        visibility = gVisibility;

        int cposx = p.posx;
        int cposy = p.posy;
        int cposz = p.posz;
        float cang = p.ang;
        float choriz = p.horiz + p.horizoff;
        int sect = p.cursectnum;

        Sector psec = boardService.getSector(sect);
        if (psec == null) {
            return;
        }

        animatecamsprite();

        Sprite s = boardService.getSprite(ud.camerasprite);
        if (s != null) {
            if (s.getYvel() < 0) {
                s.setYvel(-100);
            } else if (s.getYvel() > 199) {
                s.setYvel(300);
            }

            cang = (short) (hittype[ud.camerasprite].tempang + mulscale((((s.getAng() + 1024 - hittype[ud.camerasprite].tempang) & 2047) - 1024), smoothratio, 16));

            se40code(s.getX(), s.getY(), s.getZ(), cang, s.getYvel(), smoothratio);

            renderer.drawrooms(s.getX(), s.getY(), s.getZ() - (4 << 8), cang, s.getYvel(), s.getSectnum());
            animatesprites(s.getX(), s.getY(), s.getZ() - (4 << 8), (short) cang, smoothratio);
            renderer.drawmasks();
        } else {
            i = divscale(1, psp.getYrepeat() + 28, 22);
            if (i != oyrepeat) {
                oyrepeat = i;
                vscrn(ud.screen_size);
            }

            if ((snum == myconnectindex) && (numplayers > 1)) {
                DukeNetwork net = game.net;
                cposx = net.predictOld.x + mulscale((net.predict.x - net.predictOld.x), smoothratio, 16);
                cposy = net.predictOld.y + mulscale((net.predict.y - net.predictOld.y), smoothratio, 16);
                cposz = net.predictOld.z + mulscale((net.predict.z - net.predictOld.z), smoothratio, 16);
                cang = net.predictOld.ang + (BClampAngle(net.predict.ang + 1024 - net.predictOld.ang) - 1024) * smoothratio / 65536.0f;
                cang += net.predictOld.lookang + (BClampAngle(net.predict.lookang + 1024 - net.predictOld.lookang) - 1024) * smoothratio / 65536.0f;
                choriz = net.predictOld.horiz + net.predictOld.horizoff + (((net.predict.horiz + net.predict.horizoff - net.predictOld.horiz - net.predictOld.horizoff) * smoothratio) / 65536.0f);
                sect = net.predict.sectnum;

                if ((ud.screen_tilting != 0 && p.rotscrnang != 0)) {
                    renderer.settiltang(net.predictOld.rotscrnang + mulscale(((net.predict.rotscrnang - net.predictOld.rotscrnang + 1024) & 2047) - 1024, smoothratio, 16));
                } else {
                    renderer.settiltang(0);
                }
            } else {
                cposx = p.prevView.x + mulscale((cposx - p.prevView.x), smoothratio, 16);
                cposy = p.prevView.y + mulscale((cposy - p.prevView.y), smoothratio, 16);
                cposz = p.prevView.z + mulscale((cposz - p.prevView.z), smoothratio, 16);
                cang = p.prevView.ang + (BClampAngle(cang + 1024 - p.prevView.ang) - 1024) * smoothratio / 65536.0f;
                cang += p.prevView.lookang + (BClampAngle(p.look_ang + 1024 - p.prevView.lookang) - 1024) * smoothratio / 65536.0f;
                choriz = (p.prevView.horiz + p.prevView.horizoff + ((choriz - p.prevView.horiz - p.prevView.horizoff) * smoothratio) / 65536.0f);

                if ((ud.screen_tilting != 0 && p.rotscrnang != 0)) {
                    renderer.settiltang(p.prevView.rotscrnang + mulscale(((p.rotscrnang - p.prevView.rotscrnang + 1024) & 2047) - 1024, smoothratio, 16));
                } else {
                    renderer.settiltang(0);
                }
            }

            Sprite spo = boardService.getSprite(p.newowner);
            if (spo != null) {
                cang = hittype[p.i].tempang + (BClampAngle(p.ang + 1024 - hittype[p.i].tempang) - 1024) * smoothratio / 65536.0f;
                choriz = p.horiz + p.horizoff;
                cposx = p.posx;
                cposy = p.posy;
                cposz = p.posz;
                sect = spo.getSectnum();
            } else if (over_shoulder_on == 0) {
                if (numplayers < 2) {
                    cposz += p.opyoff + mulscale((p.pyoff - p.opyoff), smoothratio, 16);
                }
            } else {
                view(p, cposx, cposy, cposz, sect, cang, choriz);

                cposx = viewout.ox;
                cposy = viewout.oy;
                cposz = viewout.oz;
                sect = viewout.os;
            }

            psec = boardService.getSector(sect);
            if (psec == null) {
                return;
            }

            cz.set(hittype[p.i].ceilingz);
            fz.set(hittype[p.i].floorz);

            if (earthquaketime > 0 && p.on_ground) {
                cposz += 256 - (((earthquaketime) & 1) << 9);
                cang += (2 - ((earthquaketime) & 2)) << 2;
            }

            if (psp.getPal() == 1) {
                cposz -= (18 << 8);
            }

            Sprite newspo = boardService.getSprite(p.newowner);
            if (newspo != null) {
                choriz = (short) (100 + newspo.getShade());
            } else if (p.spritebridge == 0) {
                if (cposz < (p.truecz + (4 << 8))) {
                    cposz = cz.get() + (4 << 8);
                } else if (cposz > (p.truefz - (4 << 8))) {
                    cposz = fz.get() - (4 << 8);
                }
            }

            engine.getzsofslope(sect, cposx, cposy, fz, cz);
            if (cposz < cz.get() + (4 << 8)) {
                cposz = cz.get() + (4 << 8);
            }
            if (cposz > fz.get() - (4 << 8)) {
                cposz = fz.get() - (4 << 8);
            }

            if (choriz > 299) {
                choriz = 299;
            } else if (choriz < -99) {
                choriz = -99;
            }

            se40code(cposx, cposy, cposz, cang, choriz, smoothratio);

            byte[] gotpic = renderer.getRenderedPics();
            if ((gotpic[MIRROR >> 3] & (1 << (MIRROR & 7))) > 0) {
                dst = 0x7fffffff;
                i = 0;
                for (k = 0; k < mirrorcnt; k++) {
                    Wall mwal = boardService.getWall(mirrorwall[k]);
                    if (mwal == null) {
                        continue;
                    }

                    j = klabs(mwal.getX() - cposx);
                    j += klabs(mwal.getY() - cposy);
                    if (j < dst) {
                        dst = j;
                        i = k;
                    }
                }

                Wall mwal = boardService.getWall(mirrorwall[i]);
                if (mwal != null && mwal.getOverpicnum() == MIRROR) {
                    Mirror mirror = renderer.preparemirror(cposx, cposy, cposz, cang, choriz, mirrorwall[i], mirrorsector[i]);

                    tposx = (int) mirror.getX();
                    tposy = (int) mirror.getY();
                    tang = (short) mirror.getAngle();

                    j = visibility;
                    visibility = (j >> 1) + (j >> 2);

                    renderer.drawrooms(tposx, tposy, cposz, tang, choriz, (short) (mirrorsector[i] + boardService.getSectorCount()));

                    display_mirror = 1;
                    animatesprites(tposx, tposy, cposz, tang, smoothratio);
                    display_mirror = 0;

                    renderer.drawmasks();
                    renderer.completemirror(); // Reverse screen x-wise in this function
                    visibility = j;
                }
                gotpic[MIRROR >> 3] &= ~(1 << (MIRROR & 7));
            }

            renderer.drawrooms(cposx, cposy, cposz, cang, choriz, sect);
            animatesprites(cposx, cposy, cposz, (short) cang, smoothratio);
            renderer.drawmasks();
        }
    }

    public static void FTA(int q, PlayerStruct p) {
        if (q >= currentGame.getCON().fta_quotes.length) {
            Console.out.println("Invalid quote " + q, OsdColor.RED);
            return;
        }

        if (ud.fta_on == 1 && p == ps[screenpeek]) {
            if (fta > 0 && q != 115 && q != 116) {
                if (ftq == 115 || ftq == 116) {
                    return;
                }
            }

            fta = 100;

            if (ftq != q || q == 26) {
                ftq = (short) q;
            }

            char[] quote = currentGame.getCON().fta_quotes[ftq];
            int len = 0;
            while (len < quote.length && quote[len] != 0) {
                len++;
            }

            String message = new String(quote, 0, len);
            if (!message.equals(lastmessage)) {
                Console.out.println(message);
                lastmessage = message;
            }
        }
    }

    public static void animatesprites(int x, int y, int z, short a, int smoothratio) {
        int i, j, k, p;

        Renderer renderer = game.getRenderer();
        RenderedSpriteList renderedSpriteList = renderer.getRenderedSprites();

        for (j = 0; j < renderedSpriteList.getSize(); j++) {
            TSprite t = renderedSpriteList.get(j);
            i = (short) t.getSpriteNum();
            Sprite s = boardService.getSprite(i);
            if (s == null)  {
                continue;
            }

            Sector sec = boardService.getSector(t.getSectnum());
            if (sec == null) {
                continue;
            }

//			if(!game.isSoftwareRenderer() && ps[screenpeek].heat_on != 0) {
//				if((t.picnum >= STARTALPHANUM && t.picnum <= ENDALPHANUM)
//					|| (t.picnum >= (BIGALPHANUM - 10) && t.picnum < BIGAPPOS)
//					|| (t.picnum >= MINIFONT && t.picnum <= MINIFONT + 63)) {
//					t.pal = 6;
//					t.shade -= 10;
//				}
//			} //Nightvision hack

            switch (t.getPicnum()) {
                case DEVELOPERCOMMENTARY:
                case DEVELOPERCOMMENTARY + 1:
                    if (!cfg.bDevCommentry) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                    }
                    break;
                case BLOODPOOL:
                case PUKE:
                case FOOTPRINTS:
                case FOOTPRINTS2:
                case FOOTPRINTS3:
                case FOOTPRINTS4:
                    if (t.getShade() == 127) {
                        continue;
                    }
                    break;
                case RESPAWNMARKERRED:
                case RESPAWNMARKERYELLOW:
                case RESPAWNMARKERGREEN:
                    if (ud.marker == 0) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                    }
                    continue;
                case CHAIR3:

                    k = (short) ((((t.getAng() + 3072 + 128 - a) & 2047) >> 8) & 7);
                    if (k > 4) {
                        k = (short) (8 - k);
                        t.setCstat(t.getCstat() | 4);
                    } else {
                        t.setCstat(t.getCstat() & ~4);
                    }
                    t.setPicnum((s.getPicnum() + k));
                    break;
                case BLOODSPLAT1:
                case BLOODSPLAT2:
                case BLOODSPLAT3:
                case BLOODSPLAT4:
                    if (ud.lockout != 0) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                    } else if (t.getPal() == 6) {
                        t.setShade(-127);
                        continue;
                    }
                case BULLETHOLE:
                case CRACK1:
                case CRACK2:
                case CRACK3:
                case CRACK4:
                    t.setShade(16);
                    continue;
                case NEON1:
                case NEON2:
                case NEON3:
                case NEON4:
                case NEON5:
                case NEON6:
                    continue;
                case GREENSLIME:
                case GREENSLIME + 1:
                case GREENSLIME + 2:
                case GREENSLIME + 3:
                case GREENSLIME + 4:
                case GREENSLIME + 5:
                case GREENSLIME + 6:
                case GREENSLIME + 7:
                    break;
                default:
                    if (((t.getCstat() & 16) != 0) || (badguy(t) && t.getExtra() > 0) || t.getStatnum() == 10) {
                        continue;
                    }
            }

            byte l;
            if ((sec.getCeilingstat() & 1) != 0) {
                l = sec.getCeilingshade();
            } else {
                l = sec.getFloorshade();
            }

            if (l < -127) {
                l = -127;
            }
            t.setShade(l);
        }

        for (j = 0; j < renderedSpriteList.getSize(); j++) // Between drawrooms() and drawmasks()
        { // is the perfect time to animate sprites
            TSprite t = renderedSpriteList.get(j);
            i = t.getSpriteNum();
            Sprite s = boardService.getSprite(i);
            if (s == null) {
                continue;
            }

            Sector sec = boardService.getSector(s.getSectnum());
            if (sec == null) {
                continue;
            }

            switch (s.getPicnum()) {
                case SECTOREFFECTOR:
                    if (t.getLotag() == 27 && gDemoScreen.isDemoRecording()) {
                        t.setPicnum((short) (11 + ((engine.getTotalClock() >> 3) & 1)));
                        t.setCstat(t.getCstat() | 128);
                    } else {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                    }
                    break;
                case NATURALLIGHTNING:
                    t.setShade(-127);
                    break;
                case FEM1:
                case FEM2:
                case FEM3:
                case FEM4:
                case FEM5:
                case FEM6:
                case FEM7:
                case FEM8:
                case FEM9:
                case FEM10:
                case MAN:
                case MAN2:
                case WOMAN:
                case NAKED1:
                case PODFEM1:
                case FEMMAG1:
                case FEMMAG2:
                case FEMPIC1:
                case FEMPIC2:
                case FEMPIC3:
                case FEMPIC4:
                case FEMPIC5:
                case FEMPIC6:
                case FEMPIC7:
                case BLOODYPOLE:
                case FEM6PAD:
                case STATUE:
                case STATUEFLASH:
                case OOZ:
                case OOZ2:
                case WALLBLOOD1:
                case WALLBLOOD2:
                case WALLBLOOD3:
                case WALLBLOOD4:
                case WALLBLOOD5:
                case WALLBLOOD7:
                case WALLBLOOD8:
                case SUSHIPLATE1:
                case SUSHIPLATE2:
                case SUSHIPLATE3:
                case SUSHIPLATE4:
                case FETUS:
                case FETUSJIB:
                case FETUSBROKE:
                case HOTMEAT:
                case FOODOBJECT16:
                case DOLPHIN1:
                case DOLPHIN2:
                case TOUGHGAL:
                case TAMPON:
                case XXXSTACY:
                case 4946:
                case 4947:
                case 693:
                case 2254:
                case 4560:
                case 4561:
                case 4562:
                case 4498:
                case 4957:
                    if (ud.lockout != 0) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                        continue;
                    }
            }

            if (t.getStatnum() == 99) {
                continue;
            }
            if (s.getStatnum() != 1 && s.getPicnum() == APLAYER && ps[s.getYvel()].newowner == -1 && s.getOwner() >= 0) {
                t.setX(ps[s.getYvel()].oposx + mulscale(ps[s.getYvel()].posx - ps[s.getYvel()].oposx, smoothratio, 16));
                t.setY(ps[s.getYvel()].oposy + mulscale(ps[s.getYvel()].posy - ps[s.getYvel()].oposy, smoothratio, 16));
                t.setZ(ps[s.getYvel()].oposz + mulscale(ps[s.getYvel()].posz - ps[s.getYvel()].oposz, smoothratio, 16));
                t.setZ(t.getZ() + (40 << 8));
            } else if ((s.getStatnum() == 0) || s.getStatnum() == 10 || s.getStatnum() == 6 || s.getStatnum() == 4 || s.getStatnum() == 5 || s.getStatnum() == 1) {
                // only interpolate certain moving things
                ILoc oldLoc = game.pInt.getsprinterpolate(t.getOwner());
                if (oldLoc != null) {
                    int ox = oldLoc.x;
                    int oy = oldLoc.y;
                    int oz = oldLoc.z;
                    short nAngle = oldLoc.ang;

                    // interpolate sprite position
                    ox += mulscale(t.getX() - oldLoc.x, smoothratio, 16);
                    oy += mulscale(t.getY() - oldLoc.y, smoothratio, 16);
                    oz += mulscale(t.getZ() - oldLoc.z, smoothratio, 16);
                    nAngle += (short) mulscale(((t.getAng() - oldLoc.ang + 1024) & kAngleMask) - 1024, smoothratio, 16);

                    t.setX(ox);
                    t.setY(oy);
                    t.setZ(oz);
                    t.setAng(nAngle);
                }
            }

            int t1 = hittype[i].temp_data[1];
            int t3 = hittype[i].temp_data[3];
            int t4 = hittype[i].temp_data[4];

            switch (s.getPicnum()) {
                case DUKELYINGDEAD:
                    t.setZ(t.getZ() + (24 << 8));
                    break;
                case BLOODPOOL:
                case FOOTPRINTS:
                case FOOTPRINTS2:
                case FOOTPRINTS3:
                case FOOTPRINTS4:
                    if (t.getPal() == 6) {
                        t.setShade(-127);
                    }
                case PUKE:
                case MONEY:
                case MONEY + 1:
                case MAIL:
                case MAIL + 1:
                case PAPER:
                case PAPER + 1:
                    if (ud.lockout != 0 && s.getPal() == 2) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                        continue;
                    }
                    break;
                case TRIPBOMB:
                    continue;
                case FORCESPHERE:
                    if (t.getStatnum() == 5) {
                        Sprite spo = boardService.getSprite(s.getOwner());
                        if (spo == null) {
                            continue;
                        }

                        int sqa = EngineUtils.getAngle(spo.getX() - ps[screenpeek].posx, spo.getY() - ps[screenpeek].posy);
                        int sqb = EngineUtils.getAngle(spo.getX() - t.getX(), spo.getY() - t.getY());

                        if (klabs(getincangle(sqa, sqb)) > 512) {
                            Sprite psp = boardService.getSprite(ps[screenpeek].i);
                            if (psp != null && ldist(spo, t) < ldist(psp, spo)) {
                                t.setXrepeat(0);
                                t.setYrepeat(0);
                            }
                        }
                    }
                    continue;
                case BURNING:
                case BURNING2: {
                    Sprite spo = boardService.getSprite(s.getOwner());
                    if (spo == null) {
                        break;
                    }

                    if (spo.getStatnum() == 10) {
                        if (display_mirror == 0 && spo.getYvel() == screenpeek && over_shoulder_on == 0) {
                            t.setXrepeat(0);
                        } else {
                            t.setAng(EngineUtils.getAngle(x - t.getX(), y - t.getY()));
                            t.setX(spo.getX());
                            t.setY(spo.getY());
                            t.setX(t.getX() + (EngineUtils.cos((t.getAng()) & 2047) >> 10));
                            t.setY(t.getY() + (EngineUtils.sin(t.getAng() & 2047) >> 10));
                        }
                    }
                    break;
                }
                case ATOMICHEALTH:
                    t.setZ(t.getZ() - (4 << 8));
                    break;
                case CRYSTALAMMO:
                    t.setShade((byte) (EngineUtils.sin((engine.getTotalClock() << 4) & 2047) >> 10));
                    continue;
                case VIEWSCREEN:
                case VIEWSCREEN2:
                    if (camsprite >= 0 && hittype[s.getOwner()].temp_data[0] == 1) {
                        t.setPicnum(STATIC);
                        t.setCstat(t.getCstat() | (engine.rand() & 12)); // animatesprites
                        t.setXrepeat(t.getXrepeat() + 8);
                        t.setYrepeat(t.getYrepeat() + 8);
                    } else if (camsprite >= 0 && VIEWSCR_Lock > 200) {
                        ArtEntry view = engine.getTile(TILE_VIEWSCR);
                        if (view instanceof DynamicArtEntry) {
                            t.setPicnum((short) TILE_VIEWSCR);
                        }
                    }
                    break;

                case SHRINKSPARK:
                    t.setPicnum((short) (SHRINKSPARK + ((engine.getTotalClock() >> 4) & 3)));
                    break;
                case GROWSPARK:
                    t.setPicnum((short) (GROWSPARK + ((engine.getTotalClock() >> 4) & 3)));
                    break;
                case RPG:
                    k = EngineUtils.getAngle(s.getX() - x, s.getY() - y);
                    k = (short) (((s.getAng() + 3072 + 128 - k) & 2047) / 170);
                    if (k > 6) {
                        k = (short) (12 - k);
                        t.setCstat(t.getCstat() | 4);
                    } else {
                        t.setCstat(t.getCstat() & ~4);
                    }
                    t.setPicnum((short) (RPG + k));
                    break;

                case RECON:

                    k = EngineUtils.getAngle(s.getX() - x, s.getY() - y);
                    k = (short) (((s.getAng() + 3072 + 128 - k) & 2047) / 170);

                    if (k > 6) {
                        k = (short) (12 - k);
                        t.setCstat(t.getCstat() | 4);
                    } else {
                        t.setCstat(t.getCstat() & ~4);
                    }

                    if (klabs(t3) > 64) {
                        k += 7;
                    }
                    t.setPicnum((short) (RECON + k));

                    break;

                case APLAYER:

                    p = s.getYvel();

                    if (t.getPal() == 1) {
                        t.setZ(t.getZ() - (18 << 8));
                    }

                    if (over_shoulder_on > 0 && ps[p].newowner < 0) {
                        t.setCstat(t.getCstat() | 2);
                        if (ps[myconnectindex] == ps[p] && numplayers >= 2) {
                            DukeNetwork net = game.net;
                            int tx = net.predictOld.x + mulscale((net.predict.x - net.predictOld.x), smoothratio, 16);
                            int ty = net.predictOld.y + mulscale((net.predict.y - net.predictOld.y), smoothratio, 16);
                            int tz = net.predictOld.z + mulscale((net.predict.z - net.predictOld.z), smoothratio, 16) + (40 << 8);

                            t.update(tx, ty, tz, net.predict.sectnum);
                            t.setAng((short) (net.predictOld.ang + (BClampAngle(net.predict.ang + 1024 - net.predictOld.ang) - 1024) * smoothratio / 65536.0f));
                        }
                    }

                    if ((display_mirror == 1 || screenpeek != p || s.getOwner() == -1) && ud.multimode > 1 && ud.showweapons != 0 && ps[p].getPlayerSprite().getExtra() > 0 && ps[p].curr_weapon > 0) {
                        TSprite tsp = renderedSpriteList.obtain();

                        tsp.set(t);
                        tsp.setStatnum(99);
                        tsp.setYrepeat((short) (t.getYrepeat() >> 3));
                        if (t.getYrepeat() < 4) {
                            t.setYrepeat(4);
                        }
                        tsp.setShade(t.getShade());
                        tsp.setCstat(0);

                        switch (ps[p].curr_weapon) {
                            case PISTOL_WEAPON:
                                tsp.setPicnum(FIRSTGUNSPRITE);
                                break;
                            case SHOTGUN_WEAPON:
                                tsp.setPicnum(SHOTGUNSPRITE);
                                break;
                            case CHAINGUN_WEAPON:
                                tsp.setPicnum(CHAINGUNSPRITE);
                                break;
                            case RPG_WEAPON:
                                tsp.setPicnum(RPGSPRITE);
                                break;
                            case HANDREMOTE_WEAPON:
                            case HANDBOMB_WEAPON:
                                tsp.setPicnum(HEAVYHBOMB);
                                break;
                            case TRIPBOMB_WEAPON:
                                tsp.setPicnum(TRIPBOMBSPRITE);
                                break;
                            case GROW_WEAPON:
                                tsp.setPicnum(GROWSPRITEICON);
                                break;
                            case SHRINKER_WEAPON:
                                tsp.setPicnum(SHRINKERSPRITE);
                                break;
                            case FREEZE_WEAPON:
                                tsp.setPicnum(FREEZESPRITE);
                                break;
                            case FLAMETHROWER_WEAPON: // Twentieth Anniversary World Tour
                                if (currentGame.getCON().type == 20) {
                                    tsp.setPicnum(FLAMETHROWERSPRITE);
                                }
                                break;
                            case DEVISTATOR_WEAPON:
                                tsp.setPicnum(DEVISTATORSPRITE);
                                break;
                        }

                        if (s.getOwner() >= 0) {
                            tsp.setZ(ps[p].posz - (12 << 8));
                        } else {
                            tsp.setZ(s.getZ() - (51 << 8));
                        }
                        if (ps[p].curr_weapon == HANDBOMB_WEAPON) {
                            tsp.setXrepeat(10);
                            tsp.setYrepeat(10);
                        } else {
                            tsp.setXrepeat(16);
                            tsp.setYrepeat(16);
                        }
                        tsp.setPal(0);
                    }

                    if (s.getOwner() == -1) {
                        k = (short) ((((s.getAng() + 3072 + 128 - a) & 2047) >> 8) & 7);
                        if (k > 4) {
                            k = (short) (8 - k);
                            t.setCstat(t.getCstat() | 4);
                        } else {
                            t.setCstat(t.getCstat() & ~4);
                        }

                        Sector sec2 = boardService.getSector(t.getSectnum());
                        if (sec2 != null && sec2.getLotag() == 2) {
                            k += 1795 - 1405;
                        } else if ((hittype[i].floorz - s.getZ()) > (64 << 8)) {
                            k += 60;
                        }

                        t.setPicnum(t.getPicnum() + k);
                        t.setPal(ps[p].palookup);

                        if (sec.getFloorpal() != 0) {
                            t.setPal(sec.getFloorpal());
                        }

                        continue;
                    }

                    if (ps[p].on_crane == -1 && (sec.getLotag() & 0x7ff) != 1) {
                        int l = s.getZ() - hittype[ps[p].i].floorz + (3 << 8);
                        if (l > 1024 && s.getYrepeat() > 32 && s.getExtra() > 0) {
                            t.setYoffset((short) (l / (t.getYrepeat() << 2))); // GDX 24.10.2018 multiplayer unsync
                        } else {
                            t.setYoffset(0);
                        }
                    }

                    if (ps[p].newowner > -1) {
                        t4 = currentGame.getCON().script[currentGame.getCON().actorscrptr[APLAYER] + 1];
                        t3 = 0;
                        t1 = currentGame.getCON().script[currentGame.getCON().actorscrptr[APLAYER] + 2];
                    }

                    if (ud.camerasprite == -1 && ps[p].newowner == -1) {
                        if (s.getOwner() >= 0 && display_mirror == 0 && over_shoulder_on == 0) {
                            if (ud.multimode < 2 || p == screenpeek) {
                                t.setOwner(-1);
                                t.setXrepeat(0);
                                t.setYrepeat(0);
                                continue;
                            }
                        }
                    }

                    if (sec.getFloorpal() != 0) {
                        t.setPal(sec.getFloorpal());
                    }

                    if (s.getOwner() == -1) {
                        continue;
                    }

                    if (t.getZ() > hittype[i].floorz && t.getXrepeat() < 32) {
                        t.setZ(hittype[i].floorz);
                    }

                    int tx = t.getX() - x;
                    int ty = t.getY() - y;
                    int angle = ((1024 + EngineUtils.getAngle(tx, ty) - a) & kAngleMask) - 1024;
                    long dist = EngineUtils.qdist(tx, ty);

                    if (klabs(mulscale(angle, dist, 14)) < 4) {
                        int horizoff = (int) (100 - ps[screenpeek].horiz);
                        long z1 = mulscale(dist, horizoff, 3) + z;

                        int zTop = t.getZ();
                        int zBot = zTop;
                        int yoffs = engine.getTile(APLAYER).getOffsetY();
                        zTop -= (yoffs + engine.getTile(APLAYER).getHeight()) * (t.getYrepeat() << 2);
                        zBot += -yoffs * (t.getYrepeat() << 2);

                        if ((z1 < zBot) && (z1 > zTop)) {
                            Sprite psp = boardService.getSprite(ps[screenpeek].i);
                            if (psp != null && engine.cansee(x, y, z, psp.getSectnum(), t.getX(), t.getY(), t.getZ(), t.getSectnum())) {
                                gPlayerIndex = t.getYvel();
                            }
                        }
                    }

                    break;

                case JIBS1:
                case JIBS2:
                case JIBS3:
                case JIBS4:
                case JIBS5:
                case JIBS6:
                case HEADJIB1:
                case LEGJIB1:
                case ARMJIB1:
                case LIZMANHEAD1:
                case LIZMANARM1:
                case LIZMANLEG1:
                case DUKELEG:
                case DUKEGUN:
                case DUKETORSO:
                    if (ud.lockout != 0) {
                        t.setXrepeat(0);
                        t.setYrepeat(0);
                        continue;
                    }
                    if (t.getPal() == 6) {
                        t.setShade(-120);
                    }

                case SCRAP1:
                case SCRAP2:
                case SCRAP3:
                case SCRAP4:
                case SCRAP5:
                case SCRAP6:
                case SCRAP6 + 1:
                case SCRAP6 + 2:
                case SCRAP6 + 3:
                case SCRAP6 + 4:
                case SCRAP6 + 5:
                case SCRAP6 + 6:
                case SCRAP6 + 7:

                    if (hittype[i].picnum == BLIMP && t.getPicnum() == SCRAP1 && s.getYvel() >= 0) {
                        t.setPicnum(s.getYvel());
                    } else {
                        t.setPicnum(t.getPicnum() + hittype[i].temp_data[0]);
                    }
                    t.setShade(t.getShade() - 6);

                    if (sec.getFloorpal() != 0) {
                        t.setPal(sec.getFloorpal());
                    }
                    break;

                case WATERBUBBLE: {
                    Sector sec2 = boardService.getSector(t.getSectnum());
                    if (sec2 != null && sec2.getFloorpicnum() == FLOORSLIME) {
                        t.setPal(7);
                        break;
                    }
                }
                default:
                    if (sec.getFloorpal() != 0) {
                        t.setPal(sec.getFloorpal());
                    }
                    break;
            }

            if (currentGame.getCON().actorscrptr[s.getPicnum()] != 0) {
                if (t4 != 0) {
                    int l = currentGame.getCON().script[t4 + 2];

                    switch (l) {
                        case 2:
                            k = (short) ((((s.getAng() + 3072 + 128 - a) & 2047) >> 8) & 1);
                            break;

                        case 3:
                        case 4:
                            k = (short) ((((s.getAng() + 3072 + 128 - a) & 2047) >> 7) & 7);
                            if (k > 3) {
                                t.setCstat(t.getCstat() | 4);
                                k = (short) (7 - k);
                            } else {
                                t.setCstat(t.getCstat() & ~4);
                            }
                            break;

                        case 5:
                            k = EngineUtils.getAngle(s.getX() - x, s.getY() - y);
                            k = (short) ((((s.getAng() + 3072 + 128 - k) & 2047) >> 8) & 7);
                            if (k > 4) {
                                k = (short) (8 - k);
                                t.setCstat(t.getCstat() | 4);
                            } else {
                                t.setCstat(t.getCstat() & ~4);
                            }
                            break;
                        case 7:
                            k = EngineUtils.getAngle(s.getX() - x, s.getY() - y);
                            k = (short) (((s.getAng() + 3072 + 128 - k) & 2047) / 170);
                            if (k > 6) {
                                k = (short) (12 - k);
                                t.setCstat(t.getCstat() | 4);
                            } else {
                                t.setCstat(t.getCstat() & ~4);
                            }
                            break;
                        case 8:
                            k = (short) ((((s.getAng() + 3072 + 128 - a) & 2047) >> 8) & 7);
                            t.setCstat(t.getCstat() & ~4);
                            break;
                        default:
                            k = 0;
                            break;
                    }

                    t.setPicnum(t.getPicnum() + (k + (currentGame.getCON().script[t4]) + l * t3));

                    if (l > 0) {
                        while (t.getPicnum() > 0 && t.getPicnum() < MAXTILES && engine.getTile(t.getPicnum()).getWidth() == 0) {
                            t.setPicnum(t.getPicnum() - l); // Hack, for actors
                        }
                    }

                    if (hittype[i].dispicnum >= 0) {
                        hittype[i].dispicnum = t.getPicnum();
                    }
                } else if (display_mirror == 1) {
                    t.setCstat(t.getCstat() | 4);
                }
            }

            if (s.getStatnum() == 13 || badguy(s) || (s.getPicnum() == APLAYER && s.getOwner() >= 0)) {
                if (t.getStatnum() != 99 && s.getPicnum() != EXPLOSION2 && s.getPicnum() != HANGLIGHT && s.getPicnum() != DOMELITE) {
                    if (s.getPicnum() != HOTMEAT) {
                        if (hittype[i].dispicnum < 0) {
                            hittype[i].dispicnum++;
                            continue;
                        } else if (ud.shadows != 0 && renderedSpriteList.getSize() < (MAXSPRITESONSCREEN - 2)) {
                            int daz, xrep, yrep;

                            if (((sec.getLotag() & 0xff) > 2 || s.getStatnum() == 4 || s.getStatnum() == 5 || s.getPicnum() == DRONE || s.getPicnum() == COMMANDER)) {
                                daz = sec.getFloorz();
                            } else {
                                daz = hittype[i].floorz;
                            }

                            if ((s.getZ() - daz) < (8 << 8)) {
                                if (ps[screenpeek].posz < daz) {
                                    Sprite tspr = renderedSpriteList.obtain();
                                    tspr.set(t);
                                    int camangle = EngineUtils.getAngle(x - tspr.getX(), y - tspr.getY());
                                    tspr.setX(tspr.getX() - mulscale(EngineUtils.sin((camangle + 512) & 2047), 100, 16));
                                    tspr.setY(tspr.getY() + mulscale(EngineUtils.sin((camangle + 1024) & 2047), 100, 16));
                                    tspr.setStatnum(99);
                                    tspr.setYrepeat((short) (t.getYrepeat() >> 3));
                                    if (tspr.getYrepeat() < 4) {
                                        tspr.setYrepeat(4);
                                    }

                                    tspr.setShade(127);
                                    tspr.setCstat(tspr.getCstat() | 2);

                                    tspr.setZ(daz);
                                    xrep = tspr.getXrepeat();
                                    tspr.setXrepeat((short) xrep);
                                    tspr.setPal(4);

                                    yrep = tspr.getYrepeat();
                                    tspr.setYrepeat((short) yrep);
                                }
                            }
                        }

                        if (ps[screenpeek].heat_amount > 0 && ps[screenpeek].heat_on != 0) {
                            t.setPal(6);
                            t.setShade(0);
                        }
                    }
                }
            }

            Sprite spo = boardService.getSprite(s.getOwner());
            Sector tsec = boardService.getSector(t.getSectnum());

            switch (s.getPicnum()) {
                case LASERLINE:
                    if (tsec != null && tsec.getLotag() == 2) {
                        t.setPal(8);
                    }

                    if (spo != null) {
                        t.setZ(spo.getZ() - (3 << 8));
                    }

                    if (currentGame.getCON().lasermode == 2 && ps[screenpeek].heat_on == 0) {
                        t.setYrepeat(0);
                    }
                case EXPLOSION2:
                case EXPLOSION2BOT:
                case FREEZEBLAST:
                case ATOMICHEALTH:
                case FIRELASER:
                case SHRINKSPARK:
                case GROWSPARK:
                case CHAINGUN:
                case SHRINKEREXPLOSION:
                case RPG:
                case FLOORFLAME:
                    if (t.getPicnum() == EXPLOSION2) {
                        gVisibility = -127;
                        lastvisinc = engine.getTotalClock() + 32;
                    }
                    t.setShade(-127);
                    break;
                case FIRE:
                case FIRE2:
                case BURNING:
                case BURNING2:
                    if (tsec != null && spo != null && spo.getPicnum() != TREE1 && spo.getPicnum() != TREE2) {
                        t.setZ(tsec.getFloorz());
                    }
                    t.setShade(-127);
                    break;
                case COOLEXPLOSION1:
                    t.setShade(-127);
                    t.setPicnum(t.getPicnum() + (s.getShade() >> 1));
                    break;
                case PLAYERONWATER:

                    k = (short) ((((t.getAng() + 3072 + 128 - a) & 2047) >> 8) & 7);
                    if (k > 4) {
                        k = (short) (8 - k);
                        t.setCstat(t.getCstat() | 4);
                    } else {
                        t.setCstat(t.getCstat() & ~4);
                    }

                    t.setPicnum((short) (s.getPicnum() + k + ((hittype[i].temp_data[0] < 4) ? 5 : 0)));

                    if (spo != null) {
                        t.setShade(spo.getShade());
                    }

                    break;

                case WATERSPLASH2:
                    t.setPicnum((short) (WATERSPLASH2 + t1));
                    break;
                case REACTOR2:
                    t.setPicnum((short) (s.getPicnum() + hittype[i].temp_data[2]));
                    break;
                case SHELL:
                    t.setPicnum((short) (s.getPicnum() + (hittype[i].temp_data[0] & 1)));
                case SHOTGUNSHELL:
                    t.setCstat(t.getCstat() | 12);
                    if (hittype[i].temp_data[0] > 1) {
                        t.setCstat(t.getCstat() & ~4);
                    }
                    if (hittype[i].temp_data[0] > 2) {
                        t.setCstat(t.getCstat() & ~12);
                    }
                    break;
                case FRAMEEFFECT1:
                    if (spo != null) {
                        if (spo.getPicnum() == APLAYER) {
                            if (ud.camerasprite == -1) {
                                if (screenpeek == spo.getYvel() && display_mirror == 0) {
                                    t.setOwner(-1);
                                    break;
                                }
                            }
                        }
                        if ((spo.getCstat() & 32768) == 0) {
                            t.setPicnum((short) hittype[s.getOwner()].dispicnum);
                            t.setPal(spo.getPal());
                            t.setShade(spo.getShade());
                            t.setAng(spo.getAng());
                            t.setCstat((short) (2 | spo.getCstat()));
                        }
                    }
                    break;

                case CAMERA1:
                case RAT:
                    k = (short) ((((t.getAng() + 3072 + 128 - a) & 2047) >> 8) & 7);
                    if (k > 4) {
                        k = (short) (8 - k);
                        t.setCstat(t.getCstat() | 4);
                    } else {
                        t.setCstat(t.getCstat() & ~4);
                    }
                    t.setPicnum((short) (s.getPicnum() + k));
                    break;
            }

            hittype[i].dispicnum = t.getPicnum();
            if (tsec != null && tsec.getFloorpicnum() == MIRROR) {
                t.setXrepeat(0);
                t.setYrepeat(0);
            }
        }
    }

    public static void displaymasks(short snum) {
        Renderer renderer = game.getRenderer();
        Sector psec = boardService.getSector(ps[snum].cursectnum);
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp == null || psec == null) {
            return;
        }

        int p = psec.getFloorpal();

        if (psp.getPal() == 1) {
            p = 1;
        }
        if (ps[snum].scuba_on != 0) {
            int windowx1 = 0;
            int windowy1 = 0;
            int windowx2 = renderer.getWidth();
            int windowy2 = renderer.getHeight();

            ArtEntry pic = engine.getTile(SCUBAMASK);
            if (pic.exists()) {
                if (ud.screen_size > 2) {
                    renderer.rotatesprite(44 << 16, (200 - 8 - (pic.getHeight()) << 16), 65536, 0, SCUBAMASK, 0, p, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                    renderer.rotatesprite((320 - 43) << 16, (200 - 8 - (pic.getHeight()) << 16), 65536, 1024, SCUBAMASK, 0, p, 10 + 4 + 16, windowx1, windowy1, windowx2, windowy2);
                } else {
                    renderer.rotatesprite(44 << 16, (200 - (pic.getHeight()) << 16), 65536, 0, SCUBAMASK, 0, p, 10 + 16, windowx1, windowy1, windowx2, windowy2);
                    renderer.rotatesprite((320 - 43) << 16, (200 - (pic.getHeight()) << 16), 65536, 1024, SCUBAMASK, 0, p, 10 + 4 + 16, windowx1, windowy1, windowx2, windowy2);
                }
            }
        }
    }

    public static boolean animatetip(int gs, int snum) {
        if (ps[snum].tipincs == 0) {
            return false;
        }

        Sector psec = boardService.getSector(ps[snum].cursectnum);
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp == null || psec == null) {
            return false;
        }

        int looking_arc = klabs(ps[snum].look_ang) / 9;
        looking_arc -= (ps[snum].hard_landing << 3);

        int p = 1;
        if (psp.getPal() != 1) {
            p = psec.getFloorpal();
        }

        if (ps[snum].tipincs < tip_y.length) {
            myospal(170 + (int) (sync[snum].avel / 16f) - (ps[snum].look_ang >> 1), (tip_y[ps[snum].tipincs] >> 1) + looking_arc + 240 - (((int) ps[snum].horiz - ps[snum].horizoff) >> 4), TIP + ((26 - ps[snum].tipincs) >> 4), gs, 0, p);
        }

        return true;
    }

    public static boolean animateaccess(int gs, int snum) {
        int looking_arc;
        int p = 0;

        Sector psec = boardService.getSector(ps[snum].cursectnum);
        Sprite psp = boardService.getSprite(ps[snum].i);
        if (psp == null || psec == null) {
            return false;
        }

        if (ps[snum].access_incs == 0 || psp.getExtra() <= 0) {
            return false;
        }

        looking_arc = access_y[ps[snum].access_incs] + klabs(ps[snum].look_ang) / 9;
        looking_arc -= (ps[snum].hard_landing << 3);

        Sprite access = boardService.getSprite(ps[snum].access_spritenum);
        if (access != null) {
            p = access.getPal();
        }

        if ((ps[snum].access_incs - 3) > 0 && ((ps[snum].access_incs - 3) >> 3) != 0) {
            myospal(170 + (int) (sync[snum].avel / 16f) - (ps[snum].look_ang >> 1) + (access_y[ps[snum].access_incs] >> 2), looking_arc + 266 - (((int) ps[snum].horiz - ps[snum].horizoff) >> 4), HANDHOLDINGLASER + (ps[snum].access_incs >> 3), gs, 0, p);
        } else {
            myospal(170 + (int) (sync[snum].avel / 16f) - (ps[snum].look_ang >> 1) + (access_y[ps[snum].access_incs] >> 2), looking_arc + 266 - (((int) ps[snum].horiz - ps[snum].horizoff) >> 4), HANDHOLDINGACCESS, gs, 4, p);
        }

        return true;
    }

    public static void addmessage(String message) {
        buildString(currentGame.getCON().fta_quotes[122], 0, message);
        FTA(122, ps[myconnectindex]);
    }

    public static void viewDrawStats(int x, int y, int zoom) {
        Renderer renderer = game.getRenderer();
        if (cfg.gShowStat == 0) {
            return;
        }

        float viewzoom = (zoom / 65536.0f);

        buildString(buffer, 0, "kills:  ");
        int alignx = game.getFont(1).getWidth(buffer, viewzoom);

        int yoffset = (int) (2.5f * game.getFont(1).getSize() * viewzoom);
        y -= yoffset;

        int staty = y;

        game.getFont(1).drawTextScaled(renderer, x, staty, buffer, viewzoom, 0, 10, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);

        int offs = Bitoa(ps[connecthead].actors_killed, buffer);
        buildString(buffer, offs, " / ", ps[connecthead].max_actors_killed);
        game.getFont(1).drawTextScaled(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 15, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);

        staty = y + (int) (8 * viewzoom);

        buildString(buffer, 0, "secrets:  ");
        game.getFont(1).drawTextScaled(renderer, x, staty, buffer, viewzoom, 0, 10, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
        alignx = game.getFont(1).getWidth(buffer, viewzoom);
        offs = Bitoa(ps[connecthead].secret_rooms, buffer);
        buildString(buffer, offs, " / ", ps[connecthead].max_secret_rooms);
        game.getFont(1).drawTextScaled(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 15, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);

        staty = y + (int) (16 * viewzoom);

        buildString(buffer, 0, "time:  ");
        game.getFont(1).drawTextScaled(renderer, x, staty, buffer, viewzoom, 0, 10, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
        alignx = game.getFont(1).getWidth(buffer, viewzoom);

        int minutes = ps[myconnectindex].player_par / (26 * 60);
        int sec = (ps[myconnectindex].player_par / 26) % 60;

        offs = Bitoa(minutes, buffer, 2);
        buildString(buffer, offs, " :   ", sec, 2);
        game.getFont(1).drawTextScaled(renderer, x + (alignx + 2), staty, buffer, viewzoom, 0, 15, TextAlign.Left, Transparent.None, ConvertType.AlignLeft, false);
    }

    public static void view(PlayerStruct pp, int vx, int vy, int vz, int vsectnum, float ang, float horiz) {
        viewout.ox = vx;
        viewout.oy = vy;
        viewout.oz = vz;
        viewout.os = vsectnum;

        int nx = (int) (-BCosAngle(ang) / 16.0f);
        int ny = (int) (-BSinAngle(ang) / 16.0f);
        int nz = (int) ((horiz - 100) * 128 - 4096);

        Sprite sp = boardService.getSprite(pp.i);
        if (sp == null) {
            return;
        }

        short bakcstat = sp.getCstat();
        sp.setCstat(sp.getCstat() & ~0x101);

        vsectnum = engine.updatesectorz(vx, vy, vz, vsectnum);

        engine.hitscan(vx, vy, vz, vsectnum, nx, ny, nz, pHitInfo, CLIPMASK1);
        int hitx = pHitInfo.hitx, hity = pHitInfo.hity;

        if (vsectnum < 0) {
            sp.setCstat(bakcstat);
            return;
        }

        int hx = hitx - (vx);
        int hy = hity - (vy);
        if ((klabs(hx) + klabs(hy)) - (klabs(nx) + klabs(ny)) < 1024) {
            int wx = 1;
            if (nx < 0) {
                wx = -1;
            }
            int wy = 1;
            if (ny < 0) {
                wy = -1;
            }

            hx -= wx << 9;
            hy -= wy << 9;

            int dist = 0;
            if (nx != 0 && ny != 0) {
                if (klabs(nx) > klabs(ny)) {
                    dist = divscale(hx, nx, 16);
                } else {
                    dist = divscale(hy, ny, 16);
                }
            }

            if (dist < cameradist) {
                cameradist = dist;
            }
        }

        vx += mulscale(nx, cameradist, 16);
        vy += mulscale(ny, cameradist, 16);
        vz += mulscale(nz, cameradist, 16);

        cameradist = min(cameradist + ((engine.getTotalClock() - cameraclock) << 10), 65536);
        cameraclock = engine.getTotalClock();

        vsectnum = engine.updatesectorz(vx, vy, vz, vsectnum);

        sp.setCstat(bakcstat);

        viewout.ox = vx;
        viewout.oy = vy;
        viewout.oz = vz;
        viewout.os = vsectnum;
    }

    public static void coords(short snum) {
        short y = 0;

        Renderer renderer = game.getRenderer();
        if (ud.coop != 1) {
            if (ud.multimode > 1 && ud.multimode < 5) {
                y = 8;
            } else if (ud.multimode > 4) {
                y = 16;
            }
        }

        Font font = EngineUtils.getSmallFont();
        buildString(buffer, 0, "X= ", ps[snum].posx);
        font.drawText(renderer, 250, y, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "Y= ", ps[snum].posy);
        font.drawText(renderer, 250, y + 7, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "Z= ", ps[snum].posz);
        font.drawText(renderer, 250, y + 14, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "A= ", (int) ps[snum].ang);
        font.drawText(renderer, 250, y + 21, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "ZV= ", ps[snum].poszv);
        font.drawText(renderer, 250, y + 28, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "OG= ", ps[snum].on_ground ? 1 : 0);
        font.drawText(renderer, 250, y + 35, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "AM= ", ps[snum].ammo_amount[GROW_WEAPON]);
        font.drawText(renderer, 250, y + 43, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "LFW= ", ps[snum].last_full_weapon);
        font.drawText(renderer, 250, y + 50, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        Sector sec = boardService.getSector(ps[snum].cursectnum);
        buildString(buffer, 0, "SECTL= ", sec != null ? sec.getLotag() : 0);
        font.drawText(renderer, 250, y + 57, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "SEED= ", engine.getrand());
        font.drawText(renderer, 250, y + 64, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
        buildString(buffer, 0, "THOLD= ", ps[snum].transporter_hold);
        font.drawText(renderer, 250, y + 64 + 7, buffer, 1.0f, 0, 31, TextAlign.Left, Transparent.None, false);
    }

    public static void SE40_Draw(int spnum, int x, int y, int z, float a, float h, int smoothratio) {
        Sprite spr = boardService.getSprite(spnum);
        if (spr == null) {
            return;
        }

        int rtype = 0;
        boolean drawror = false;
        Renderer renderer = game.getRenderer();
        byte[] gotsector = renderer.getRenderedSectors();
        for (int i = 0; i < 16; i++) {
            if (rorsector[i] != -1) {
                if ((gotsector[rorsector[i] >> 3] & (1 << (rorsector[i] & 7))) != 0 && ((rortype[i] == 1 && spr.getLotag() == 41) || (rortype[i] == 2 && spr.getLotag() == 40))) {
                    drawror = true;
                    rtype = rortype[i];
                    break;
                }
            }
        }
        if (!drawror) {
            return;
        }

        int nUpper = -1, nLower = -1;
        if (rtype == 1) // ceiling
        {
            nLower = spnum;
        }
        if (rtype == 2) // floor
        {
            nUpper = spnum;
        }

        for (ListNode<Sprite> node = boardService.getStatNode(15); node != null; node = node.getNext()) {
            int i = node.getIndex();
            Sprite sp = node.get();
            if (i != spnum && sp.getPicnum() == 1 && sp.getHitag() == spr.getHitag()) {
                if (rtype == 1) {
                    nUpper = i;
                }
                if (rtype == 2) {
                    nLower = i;
                }
                break;
            }
        }

        Sprite pUpper = boardService.getSprite(nUpper);
        Sprite pLower = boardService.getSprite(nLower);
        if (pUpper == null || pLower == null) {
            return;
        }

        Sector usec = boardService.getSector(pUpper.getSectnum());
        Sector lsec = boardService.getSector(pLower.getSectnum());
        if (usec == null || lsec == null) {
            return;
        }

        int rx = 0, ry = 0, rz = 0;
        Sector sec = null;
        int rsect = -1;

        if (rtype == 1) {
            sec = usec;
            rsect = pUpper.getSectnum();

            rx = pUpper.getX() - pLower.getX();
            ry = pUpper.getY() - pLower.getY();
            rz = usec.getFloorz() - lsec.getCeilingz();
            sec.setFloorstat(sec.getFloorstat() | 1024);
        }

        if (rtype == 2) {
            sec = lsec;
            rsect = pLower.getSectnum();

            rx = pLower.getX() - pUpper.getX();
            ry = pLower.getY() - pUpper.getY();
            rz = lsec.getCeilingz() - usec.getFloorz();
            sec.setCeilingstat(sec.getCeilingstat() | 1024);
        }

        if (sec == null) {
            return;
        }

        System.arraycopy(gotsector, 0, oldgotsector, 0, gotsector.length);

        renderer.drawrooms(x + rx, y + ry, z + rz, a, h, rsect + boardService.getSectorCount());
        animatesprites(x + rx, y + ry, z + rz, (short) a, smoothratio);
        renderer.drawmasks();

        System.arraycopy(oldgotsector, 0, gotsector, 0, gotsector.length);

        if (rtype == 1) {
            sec.setFloorstat(sec.getFloorstat() & ~1024);
        }

        if (rtype == 2) {
            sec.setCeilingstat(sec.getCeilingstat() & ~1024);
        }
    }

    public static void se40code(int x, int y, int z, float a, float h, int smoothratio) {
        for (ListNode<Sprite> node = boardService.getStatNode(15); node != null; node = node.getNext()) {
            switch (node.get().getLotag()) {
                case 40: // floor
                case 41: // ceiling
                    SE40_Draw(node.getIndex(), x, y, z, a, h, smoothratio);
                    break;
            }
        }
    }

    public static void cameratext(short i) {
        Renderer renderer = game.getRenderer();
        int windowx1 = 0;
        int windowy1 = 0;
        int windowx2 = renderer.getWidth();
        int windowy2 = renderer.getHeight();

        if (hittype[i].temp_data[0] == 0) {
            renderer.rotatesprite(24 << 16, 20 << 16, 65536, 0, CAMCORNER, 0, 0, 2 + 8 + 256, windowx1, windowy1, windowx2, windowy2);
            renderer.rotatesprite((320 - 24) << 16, 20 << 16, 65536, 0, CAMCORNER + 1, 0, 0, 2 + 8 + 512, windowx1, windowy1, windowx2, windowy2);
            renderer.rotatesprite(22 << 16, 143 << 16, 65536, 512, CAMCORNER + 1, 0, 0, 2 + 4 + 8 + 256, windowx1, windowy1, windowx2, windowy2);
            renderer.rotatesprite((310 - 10) << 16, 143 << 16, 65536, 512, CAMCORNER + 1, 0, 0, 2 + 8 + 512, windowx1, windowy1, windowx2, windowy2);
            if ((engine.getTotalClock() & 16) != 0) {
                renderer.rotatesprite(46 << 16, 18 << 16, 65536, 0, CAMLIGHT, 0, 0, 2 + 8 + 256, windowx1, windowy1, windowx2, windowy2);
            }
        } else {
            int flipbits = (engine.getTotalClock() << 1) & 48;
            for (int y, x = 0; x < 394; x += 64) {
                for (y = 0; y < 200; y += 64) {
                    renderer.rotatesprite(x << 16, y << 16, 65536, 0, STATIC, 0, 0, 10 + 256 + flipbits, windowx1, windowy1, windowx2, windowy2);
                }
            }
        }
    }
}
